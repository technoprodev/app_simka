<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenis_jabatan".
 *
 * @property integer $id
 * @property string $jenis_jabatan
 *
 * @property JenjangJabatan[] $jenjangJabatans
 */
class JenisJabatan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_jabatan';
    }

    public function rules()
    {
        return [
            //id

            //jenis_jabatan
            [['jenis_jabatan'], 'required'],
            [['jenis_jabatan'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_jabatan' => 'Jenis Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatans()
    {
        return $this->hasMany(JenjangJabatan::className(), ['jenis_jabatan' => 'id']);
    }
}
