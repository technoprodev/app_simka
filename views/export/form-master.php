<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$errorMessage = '';
$errorVue = false;
if ($model[str_replace('-', '_', $tab)]->hasErrors()) {
    $errorMessage .= Html::errorSummary($model[str_replace('-', '_', $tab)], ['class' => '']);
}

$pilihanKepangkatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT k.id, CONCAT(k.pangkat, ' (', k.golongan, ' / ', k.ruang, ')') AS kepangkatan FROM kepangkatan k
            order by kepangkatan
        ", []
    )->queryAll(), 'id', 'kepangkatan'
);
$pilihanJenisSk = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT js.id, js.jenis_sk FROM jenis_sk js
            order by jenis_sk
        ", []
    )->queryAll(), 'id', 'jenis_sk'
);
$pilihanJenjangJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, CONCAT(jjj.jenis_jabatan, ' - ', jj.jenjang_jabatan) AS jenjang_jabatan FROM jenjang_jabatan jj
            JOIN jenis_jabatan jjj ON jjj.id = jj.jenis_jabatan
            order by jenjang_jabatan
        ", []
    )->queryAll(), 'id', 'jenjang_jabatan'
);
$pilihanJenisJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, jj.jenis_jabatan FROM jenis_jabatan jj
            order by jenis_jabatan
        ", []
    )->queryAll(), 'id', 'jenis_jabatan'
);
$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, uk.unit_kerja AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanJenisUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT juk.id, juk.jenis_unit_kerja AS unit_kerja FROM jenis_unit_kerja juk
            order by jenis_unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanJenisPelaksana = (new \app_simka\models\PegawaiPelaksana())->getEnum('jenis_pelaksana');
$pilihanJenjangPendidikan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jp.id, jp.jenjang_pendidikan FROM jenjang_pendidikan jp
        ", []
    )->queryAll(), 'id', 'jenjang_pendidikan'
);
$pilihanJenisDiklat = (new \app_simka\models\PegawaiDiklat())->getEnum('jenis_diklat');
$pilihanNegara = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT n.id, n.nama FROM negara n
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div class="fs-16 text-azure fs-italic">
                <?= ucfirst(str_replace('_', ' ', $tab)) ?>
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="margin-top-30"></div>

            <?php if ($errorMessage) : ?>
                <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div>
                <?php if ($tab == 'agama') : ?>
                    <?= $form->field($model['agama'], 'agama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['agama'], 'agama', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['agama'], 'agama', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['agama'], 'agama', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['agama'], 'agama')->end(); ?>
                <?php elseif ($tab == 'bentuk-wajah') : ?>
                    <?= $form->field($model['bentuk_wajah'], 'bentuk_wajah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['bentuk_wajah'], 'bentuk_wajah', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['bentuk_wajah'], 'bentuk_wajah', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['bentuk_wajah'], 'bentuk_wajah', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['bentuk_wajah'], 'bentuk_wajah')->end(); ?>
                <?php elseif ($tab == 'eselon') : ?>
                    <?= $form->field($model['eselon'], 'eselon', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['eselon'], 'eselon', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['eselon'], 'eselon', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['eselon'], 'eselon', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['eselon'], 'eselon')->end(); ?>
                <?php elseif ($tab == 'grade') : ?>
                    <?= $form->field($model['grade'], 'grade', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['grade'], 'grade', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['grade'], 'grade', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['grade'], 'grade', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['grade'], 'grade')->end(); ?>
                    
                    <?= $form->field($model['grade'], 'nominal_tunkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['grade'], 'nominal_tunkin', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['grade'], 'nominal_tunkin', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['grade'], 'nominal_tunkin', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['grade'], 'nominal_tunkin')->end(); ?>

                    <?= $form->field($model['grade'], 'pajak_tunkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['grade'], 'pajak_tunkin', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['grade'], 'pajak_tunkin', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['grade'], 'pajak_tunkin', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['grade'], 'pajak_tunkin')->end(); ?>
                <?php elseif ($tab == 'jenis-hukuman-disiplin') : ?>
                    <?= $form->field($model['jenis_hukuman_disiplin'], 'jenis_hukuman_disiplin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenis_hukuman_disiplin'], 'jenis_hukuman_disiplin', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenis_hukuman_disiplin'], 'jenis_hukuman_disiplin', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenis_hukuman_disiplin'], 'jenis_hukuman_disiplin', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenis_hukuman_disiplin'], 'jenis_hukuman_disiplin')->end(); ?>
                <?php elseif ($tab == 'jenis-jabatan') : ?>
                    <?= $form->field($model['jenis_jabatan'], 'jenis_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenis_jabatan'], 'jenis_jabatan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenis_jabatan'], 'jenis_jabatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenis_jabatan'], 'jenis_jabatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenis_jabatan'], 'jenis_jabatan')->end(); ?>
                <?php elseif ($tab == 'jenis-pemberhentian') : ?>
                    <?= $form->field($model['jenis_pemberhentian'], 'jenis_pemberhentian', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenis_pemberhentian'], 'jenis_pemberhentian', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenis_pemberhentian'], 'jenis_pemberhentian', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenis_pemberhentian'], 'jenis_pemberhentian', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenis_pemberhentian'], 'jenis_pemberhentian')->end(); ?>
                <?php elseif ($tab == 'jenis-sk') : ?>
                    <?= $form->field($model['jenis_sk'], 'jenis_sk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenis_sk'], 'jenis_sk', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenis_sk'], 'jenis_sk', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenis_sk'], 'jenis_sk', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenis_sk'], 'jenis_sk')->end(); ?>
                <?php elseif ($tab == 'jenis-unit-kerja') : ?>
                    <?= $form->field($model['jenis_unit_kerja'], 'jenis_unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenis_unit_kerja'], 'jenis_unit_kerja', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenis_unit_kerja'], 'jenis_unit_kerja', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenis_unit_kerja'], 'jenis_unit_kerja', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenis_unit_kerja'], 'jenis_unit_kerja')->end(); ?>
                <?php elseif ($tab == 'jenjang-jabatan') : ?>
                    <?= $form->field($model['jenjang_jabatan'], 'jenjang_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenjang_jabatan'], 'jenis_jabatan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeDropDownList($model['jenjang_jabatan'], 'jenis_jabatan', $pilihanJenisJabatan, ['prompt' => '- pilih jenis jabatan -', 'class' => 'form-dropdown select2']); ?>
                            <?= Html::error($model['jenjang_jabatan'], 'jenis_jabatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenjang_jabatan'], 'jenis_jabatan')->end(); ?>

                    <?= $form->field($model['jenjang_jabatan'], 'jenjang_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenjang_jabatan'], 'jenjang_jabatan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenjang_jabatan'], 'jenjang_jabatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenjang_jabatan'], 'jenjang_jabatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenjang_jabatan'], 'jenjang_jabatan')->end(); ?>
                <?php elseif ($tab == 'jenjang-pendidikan') : ?>
                    <?= $form->field($model['jenjang_pendidikan'], 'jenjang_pendidikan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['jenjang_pendidikan'], 'jenjang_pendidikan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['jenjang_pendidikan'], 'jenjang_pendidikan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['jenjang_pendidikan'], 'jenjang_pendidikan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['jenjang_pendidikan'], 'jenjang_pendidikan')->end(); ?>
                <?php elseif ($tab == 'kecamatan') : ?>
                    <?= $form->field($model['kecamatan'], 'kecamatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['kecamatan'], 'kecamatan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['kecamatan'], 'kecamatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['kecamatan'], 'kecamatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['kecamatan'], 'kecamatan')->end(); ?>
                <?php elseif ($tab == 'kedudukan-pegawai') : ?>
                    <?= $form->field($model['kedudukan_pegawai'], 'kedudukan_pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['kedudukan_pegawai'], 'kedudukan_pegawai', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['kedudukan_pegawai'], 'kedudukan_pegawai', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['kedudukan_pegawai'], 'kedudukan_pegawai', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['kedudukan_pegawai'], 'kedudukan_pegawai')->end(); ?>
                <?php elseif ($tab == 'kelurahan') : ?>
                    <?= $form->field($model['kelurahan'], 'kelurahan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['kelurahan'], 'kelurahan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['kelurahan'], 'kelurahan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['kelurahan'], 'kelurahan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['kelurahan'], 'kelurahan')->end(); ?>
                <?php elseif ($tab == 'kepangkatan') : ?>
                    <?= $form->field($model['kepangkatan'], 'kepangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['kepangkatan'], 'kepangkatan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['kepangkatan'], 'kepangkatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['kepangkatan'], 'kepangkatan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['kepangkatan'], 'kepangkatan')->end(); ?>
                <?php elseif ($tab == 'kota-kabupaten') : ?>
                    <?= $form->field($model['kota_kabupaten'], 'kota_kabupaten', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['kota_kabupaten'], 'kota_kabupaten', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['kota_kabupaten'], 'kota_kabupaten', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['kota_kabupaten'], 'kota_kabupaten', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['kota_kabupaten'], 'kota_kabupaten')->end(); ?>
                <?php elseif ($tab == 'model-rambut') : ?>
                    <?= $form->field($model['model_rambut'], 'model_rambut', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['model_rambut'], 'model_rambut', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['model_rambut'], 'model_rambut', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['model_rambut'], 'model_rambut', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['model_rambut'], 'model_rambut')->end(); ?>
                <?php elseif ($tab == 'negara') : ?>
                    <?= $form->field($model['negara'], 'negara', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['negara'], 'negara', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['negara'], 'negara', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['negara'], 'negara', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['negara'], 'negara')->end(); ?>
                <?php elseif ($tab == 'pekerjaan') : ?>
                    <?= $form->field($model['pekerjaan'], 'pekerjaan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['pekerjaan'], 'pekerjaan', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['pekerjaan'], 'pekerjaan', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['pekerjaan'], 'pekerjaan', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['pekerjaan'], 'pekerjaan')->end(); ?>
                <?php elseif ($tab == 'provinsi') : ?>
                    <?= $form->field($model['provinsi'], 'provinsi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['provinsi'], 'provinsi', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['provinsi'], 'provinsi', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['provinsi'], 'provinsi', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['provinsi'], 'provinsi')->end(); ?>
                <?php elseif ($tab == 'status-kepegawaian') : ?>
                    <?= $form->field($model['status_kepegawaian'], 'status_kepegawaian', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['status_kepegawaian'], 'status_kepegawaian', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['status_kepegawaian'], 'status_kepegawaian', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['status_kepegawaian'], 'status_kepegawaian', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['status_kepegawaian'], 'status_kepegawaian')->end(); ?>

                    <?= $form->field($model['status_kepegawaian'], 'pajak_tunkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['status_kepegawaian'], 'pajak_tunkin', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['status_kepegawaian'], 'pajak_tunkin', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['status_kepegawaian'], 'pajak_tunkin', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['status_kepegawaian'], 'pajak_tunkin')->end(); ?>
                <?php elseif ($tab == 'status-marital') : ?>
                    <?= $form->field($model['status_marital'], 'status_marital', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['status_marital'], 'status_marital', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['status_marital'], 'status_marital', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['status_marital'], 'status_marital', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['status_marital'], 'status_marital')->end(); ?>
                <?php elseif ($tab == 'tingkat-hukuman-disiplin') : ?>
                    <?= $form->field($model['tingkat_hukuman_disiplin'], 'tingkat_hukuman_disiplin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['tingkat_hukuman_disiplin'], 'tingkat_hukuman_disiplin', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['tingkat_hukuman_disiplin'], 'tingkat_hukuman_disiplin', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['tingkat_hukuman_disiplin'], 'tingkat_hukuman_disiplin', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['tingkat_hukuman_disiplin'], 'tingkat_hukuman_disiplin')->end(); ?>
                <?php elseif ($tab == 'unit-kerja') : ?>
                    <?= $form->field($model['unit_kerja'], 'unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['unit_kerja'], 'unit_kerja', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['unit_kerja'], 'unit_kerja', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['unit_kerja'], 'unit_kerja', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['unit_kerja'], 'unit_kerja')->end(); ?>

                    <?= $form->field($model['unit_kerja'], 'parent', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['unit_kerja'], 'parent', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeDropDownList($model['unit_kerja'], 'parent', $pilihanUnitKerja, ['prompt' => '- pilih unit kerja -', 'class' => 'form-dropdown select2']); ?>
                            <?= Html::error($model['unit_kerja'], 'parent', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['unit_kerja'], 'parent')->end(); ?>

                    <?= $form->field($model['unit_kerja'], 'jenis_unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['unit_kerja'], 'jenis_unit_kerja', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeDropDownList($model['unit_kerja'], 'jenis_unit_kerja', $pilihanJenisUnitKerja, ['prompt' => '- pilih jenis unit kerja -', 'class' => 'form-dropdown select2']); ?>
                            <?= Html::error($model['unit_kerja'], 'jenis_unit_kerja', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['unit_kerja'], 'jenis_unit_kerja')->end(); ?>
                <?php elseif ($tab == 'warna-kulit') : ?>
                    <?= $form->field($model['warna_kulit'], 'warna_kulit', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <div class="box-1 padding-x-0 padding-y-5">
                            <?= Html::activeLabel($model['warna_kulit'], 'warna_kulit', ['class' => 'form-label text-grayer']); ?>
                        </div>
                        <div class="box-11 m-padding-x-0">
                            <?= Html::activeTextInput($model['warna_kulit'], 'warna_kulit', ['class' => 'form-text', 'maxlength' => true]); ?>
                            <?= Html::error($model['warna_kulit'], 'warna_kulit', ['class' => 'form-info']); ?>
                        </div>
                    <?= $form->field($model['warna_kulit'], 'warna_kulit')->end(); ?>
                <?php endif; ?>
            </div>

            <div>
                <div class="margin-top-30"></div>

                <hr class="margin-y-10 border-top border-light-orange">
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                    <div class="margin-top-15"></div>
                    <div class="clearfix">
                        <?= Html::a('Kembali ke Daftar Data Master', ['master', 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
