alter table kehadiran add column jadwal_batas_masuk datetime;
alter table kehadiran add column jadwal_batas_pulang datetime;

ALTER TABLE `jadwal_kehadiran`
CHANGE `senin_batas_masuk` `senin_batas_awal_masuk` time NULL AFTER `senin_pulang`,
ADD `senin_batas_akhir_masuk` time NULL AFTER `senin_batas_awal_masuk`,
CHANGE `senin_batas_pulang` `senin_batas_awal_pulang` time NULL AFTER `senin_batas_akhir_masuk`,
ADD `senin_batas_akhir_pulang` time NULL AFTER `senin_batas_awal_pulang`,
CHANGE `selasa_batas_masuk` `selasa_batas_awal_masuk` time NULL AFTER `selasa_pulang`,
ADD `selasa_batas_akhir_masuk` time NULL AFTER `selasa_batas_awal_masuk`,
CHANGE `selasa_batas_pulang` `selasa_batas_awal_pulang` time NULL AFTER `selasa_batas_akhir_masuk`,
ADD `selasa_batas_akhir_pulang` time NULL AFTER `selasa_batas_awal_pulang`,
CHANGE `rabu_batas_masuk` `rabu_batas_awal_masuk` time NULL AFTER `rabu_pulang`,
ADD `rabu_batas_akhir_masuk` time NULL AFTER `rabu_batas_awal_masuk`,
CHANGE `rabu_batas_pulang` `rabu_batas_awal_pulang` time NULL AFTER `rabu_batas_akhir_masuk`,
ADD `rabu_batas_akhir_pulang` time NULL AFTER `rabu_batas_awal_pulang`,
CHANGE `kamis_batas_masuk` `kamis_batas_awal_masuk` time NULL AFTER `kamis_pulang`,
ADD `kamis_batas_akhir_masuk` time NULL AFTER `kamis_batas_awal_masuk`,
CHANGE `kamis_batas_pulang` `kamis_batas_awal_pulang` time NULL AFTER `kamis_batas_akhir_masuk`,
ADD `kamis_batas_akhir_pulang` time NULL AFTER `kamis_batas_awal_pulang`,
CHANGE `jumat_batas_masuk` `jumat_batas_awal_masuk` time NULL AFTER `jumat_pulang`,
ADD `jumat_batas_akhir_masuk` time NULL AFTER `jumat_batas_awal_masuk`,
CHANGE `jumat_batas_pulang` `jumat_batas_awal_pulang` time NULL AFTER `jumat_batas_akhir_masuk`,
ADD `jumat_batas_akhir_pulang` time NULL AFTER `jumat_batas_awal_pulang`,
CHANGE `sabtu_batas_masuk` `sabtu_batas_awal_masuk` time NULL AFTER `sabtu_pulang`,
ADD `sabtu_batas_akhir_masuk` time NULL AFTER `sabtu_batas_awal_masuk`,
CHANGE `sabtu_batas_pulang` `sabtu_batas_awal_pulang` time NULL AFTER `sabtu_batas_akhir_masuk`,
ADD `sabtu_batas_akhir_pulang` time NULL AFTER `sabtu_batas_awal_pulang`,
CHANGE `minggu_batas_masuk` `minggu_batas_awal_masuk` time NULL AFTER `minggu_pulang`,
ADD `minggu_batas_akhir_masuk` time NULL AFTER `minggu_batas_awal_masuk`,
CHANGE `minggu_batas_pulang` `minggu_batas_awal_pulang` time NULL AFTER `minggu_batas_akhir_masuk`,
ADD `minggu_batas_akhir_pulang` time NULL AFTER `minggu_batas_awal_pulang`;

ALTER TABLE `kehadiran`
ADD `jadwal_batas_awal_masuk` datetime NULL AFTER `keterangan`,
CHANGE `jadwal_batas_masuk` `jadwal_batas_akhir_masuk` datetime NULL AFTER `jadwal_batas_awal_masuk`,
ADD `jadwal_batas_awal_pulang` datetime NULL AFTER `jadwal_batas_akhir_masuk`,
CHANGE `jadwal_batas_pulang` `jadwal_batas_akhir_pulang` datetime NULL AFTER `jadwal_batas_awal_pulang`;