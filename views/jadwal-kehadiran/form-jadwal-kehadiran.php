<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);

$pilihanBerlakuBagi = (new \app_simka\models\JadwalKehadiran())->getEnum('berlaku_bagi');

$pilihanPegawai = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT p.id, p.nama FROM pegawai p
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);

$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, uk.unit_kerja FROM unit_kerja uk
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanStatus = (new \app_simka\models\JadwalKehadiran())->getEnum('status');

//
$errorMessage = '';
$errorVue = false;
if ($model['jadwal_kehadiran']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['jadwal_kehadiran'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Formuli Jadwal Kehadiran
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_bagi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'berlaku_bagi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'berlaku_bagi', $pilihanBerlakuBagi, ['prompt' => '- pilih berlaku bagi -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'berlaku_bagi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_bagi')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'unit_kerja', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'unit_kerja', $pilihanUnitKerja, ['prompt' => '- pilih unit kerja -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'unit_kerja', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'unit_kerja')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'pegawai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'pegawai', $pilihanPegawai, ['prompt' => '- pilih pegawai -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'pegawai', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'pegawai')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'prioritas', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'prioritas', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'prioritas', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 1']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'prioritas', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'prioritas')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'status', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'status', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'status', $pilihanStatus, ['prompt' => '- pilih status -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'status', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'status')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_dari', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'berlaku_dari', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'berlaku_dari', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 30/12/2020']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'berlaku_dari', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_dari')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_sampai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'berlaku_sampai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'berlaku_sampai', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 30/12/2020']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'berlaku_sampai', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'berlaku_sampai')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'catatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'catatan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'catatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'catatan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'catatan')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'fleksi_time', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'fleksi_time', ['class' => 'form-label text-grayer', 'label' => 'Flexi Time (menit)']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'fleksi_time', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 90']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'fleksi_time', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'fleksi_time')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'senin_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'senin_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'senin_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'senin_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'senin_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'senin_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'selasa_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'selasa_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'selasa_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'selasa_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'selasa_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'selasa_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'rabu_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'rabu_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'rabu_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'rabu_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'rabu_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'rabu_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'kamis_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'kamis_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'kamis_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'kamis_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'kamis_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'kamis_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'jumat_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'jumat_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'jumat_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'jumat_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'jumat_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'jumat_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'sabtu_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'sabtu_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'sabtu_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'sabtu_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'sabtu_libur')->end(); ?>

                <hr>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_awal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_batas_awal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_batas_awal_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_batas_awal_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_awal_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_akhir_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_batas_akhir_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_batas_akhir_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_batas_akhir_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_akhir_masuk')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_awal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_batas_awal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_batas_awal_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_batas_awal_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_awal_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_akhir_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_batas_akhir_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_batas_akhir_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_batas_akhir_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_batas_akhir_pulang')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_waktu_mulai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_waktu_mulai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_waktu_mulai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 12:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_waktu_mulai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_waktu_mulai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_waktu_selesai_istirahat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_waktu_selesai_istirahat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['jadwal_kehadiran'], 'minggu_waktu_selesai_istirahat', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 13:00:00']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_waktu_selesai_istirahat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_waktu_selesai_istirahat')->end(); ?>

                <?= $form->field($model['jadwal_kehadiran'], 'minggu_libur', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['jadwal_kehadiran'], 'minggu_libur', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['jadwal_kehadiran'], 'minggu_libur', ['Ya' => 'Ya', 'Tidak' => 'Tidak'], ['prompt' => '- pilih apakah libur -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['jadwal_kehadiran'], 'minggu_libur', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['jadwal_kehadiran'], 'minggu_libur')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
