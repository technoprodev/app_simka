<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kelurahan".
 *
 * @property string $id
 * @property string $id_kecamatan
 * @property string $nama
 *
 * @property Kecamatan $kecamatan
 * @property Pegawai[] $pegawais
 * @property Pegawai[] $pegawais0
 * @property Pegawai[] $pegawais1
 * @property Pegawai[] $pegawais2
 */
class Kelurahan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kelurahan';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 10],
            [['id'], 'unique'],

            //id_kecamatan
            [['id_kecamatan'], 'required'],
            [['id_kecamatan'], 'string', 'max' => 7],
            [['id_kecamatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['id_kecamatan' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kecamatan' => 'Id Kecamatan',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'id_kecamatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['kelurahan_sekarang' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais0()
    {
        return $this->hasMany(Pegawai::className(), ['kelurahan_ktp' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais1()
    {
        return $this->hasMany(Pegawai::className(), ['kelurahan_ayah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais2()
    {
        return $this->hasMany(Pegawai::className(), ['kelurahan_ibu' => 'id']);
    }
}
