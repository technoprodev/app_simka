<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_kenaikan_gaji_berkala".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $gaji_pokok_lama
 * @property string $gaji_pokok_baru
 * @property string $terhitung_mulai_tanggal
 * @property string $terhitung_sampai_tanggal
 * @property string $terhitung_sampai_sekarang
 * @property integer $kepangkatan_saat_kenaikan
 * @property string $catatan
 * @property integer $jenis_sk
 * @property string $nomor_sk
 * @property string $tanggal_sk
 * @property string $pejabat_penetap_sk
 * @property string $arsip_sk
 *
 * @property Pegawai $pegawai0
 * @property Kepangkatan $kepangkatanSaatKenaikan
 * @property JenisSk $jenisSk
 */
class PegawaiKenaikanGajiBerkala extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_arsip_sk_upload;
    public $virtual_arsip_sk_download;

    public static function tableName()
    {
        return 'pegawai_kenaikan_gaji_berkala';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //gaji_pokok_lama
            [['gaji_pokok_lama'], 'required'],
            [['gaji_pokok_lama'], 'integer'],

            //gaji_pokok_baru
            [['gaji_pokok_baru'], 'required'],
            [['gaji_pokok_baru'], 'integer'],

            //terhitung_mulai_tanggal
            [['terhitung_mulai_tanggal'], 'required'],
            [['terhitung_mulai_tanggal'], 'safe'],

            //terhitung_sampai_tanggal
            [['terhitung_sampai_tanggal'], 'safe'],

            //terhitung_sampai_sekarang
            [['terhitung_sampai_sekarang'], 'string'],

            //kepangkatan_saat_kenaikan
            [['kepangkatan_saat_kenaikan'], 'required'],
            [['kepangkatan_saat_kenaikan'], 'integer'],
            [['kepangkatan_saat_kenaikan'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan_saat_kenaikan' => 'id']],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'integer'],
            [['jenis_sk'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSk::className(), 'targetAttribute' => ['jenis_sk' => 'id']],

            //nomor_sk
            [['nomor_sk'], 'required'],
            [['nomor_sk'], 'string', 'max' => 256],

            //tanggal_sk
            [['tanggal_sk'], 'required'],
            [['tanggal_sk'], 'safe'],

            //pejabat_penetap_sk
            [['pejabat_penetap_sk'], 'required'],
            [['pejabat_penetap_sk'], 'string', 'max' => 256],

            //arsip_sk
            // [['arsip_sk'], 'required'],
            [['arsip_sk'], 'string', 'max' => 256],
            
            //virtual_arsip_sk_download
            [['virtual_arsip_sk_download'], 'safe'],
            
            //virtual_arsip_sk_upload
            [['virtual_arsip_sk_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_kenaikan_gaji_berkala-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_arsip_sk_upload) {
            $path = Yii::getAlias('@upload-pegawai_kenaikan_gaji_berkala-arsip_sk') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_arsip_sk_upload->saveAs($path . '/' . $this->arsip_sk);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_kenaikan_gaji_berkala-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->arsip_sk) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_kenaikan_gaji_berkala-arsip_sk');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_arsip_sk_download = $path . '/' . $this->arsip_sk;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'gaji_pokok_lama' => 'Gaji Pokok Lama',
            'gaji_pokok_baru' => 'Gaji Pokok Baru',
            'terhitung_mulai_tanggal' => 'Terhitung Mulai Tanggal',
            'terhitung_sampai_tanggal' => 'Terhitung Sampai Tanggal',
            'terhitung_sampai_sekarang' => 'Terhitung Sampai Sekarang',
            'kepangkatan_saat_kenaikan' => 'Kepangkatan Saat Kenaikan',
            'catatan' => 'Catatan',
            'jenis_sk' => 'Jenis Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_sk' => 'Tanggal Sk',
            'pejabat_penetap_sk' => 'Pejabat Penetap Sk',
            'arsip_sk' => 'Arsip Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatanSaatKenaikan()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan_saat_kenaikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSk()
    {
        return $this->hasOne(JenisSk::className(), ['id' => 'jenis_sk']);
    }
}
