ALTER TABLE `pegawai_diklat`
CHANGE `sertifikat` `sertifikat` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `keterangan`;

ALTER TABLE `pegawai_kenaikan_gaji_berkala`
CHANGE `arsip_sk` `arsip_sk` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `pejabat_penetap_sk`;

ALTER TABLE `pegawai_kepangkatan`
CHANGE `arsip_sk` `arsip_sk` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `pejabat_penetap_sk`;

ALTER TABLE `pegawai_pelaksana`
CHANGE `arsip_sk` `arsip_sk` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `pejabat_penetap_sk`;

ALTER TABLE `pegawai_pendidikan`
CHANGE `ijazah` `ijazah` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `keterangan`;

ALTER TABLE `pegawai_penetapan_angka_kredit`
CHANGE `arsip_pak` `arsip_pak` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `total_angkat_kredit`;

ALTER TABLE `pegawai_mutasi`
CHANGE `arsip_sk` `arsip_sk` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `pejabat_penetap_sk`;
