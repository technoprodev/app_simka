-- APPLY JK GLOBAL
DELIMITER $$

DROP PROCEDURE IF EXISTS apply_jk_global;
CREATE PROCEDURE apply_jk_global(list_pegawai varchar(255))
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_fleksi_time int;
  declare v_waktu_mulai_istirahat time;
  declare v_waktu_selesai_istirahat time;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_awal_masuk time;
  declare v_senin_batas_akhir_masuk time;
  declare v_senin_batas_awal_pulang time;
  declare v_senin_batas_akhir_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_awal_masuk time;
  declare v_selasa_batas_akhir_masuk time;
  declare v_selasa_batas_awal_pulang time;
  declare v_selasa_batas_akhir_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_awal_masuk time;
  declare v_rabu_batas_akhir_masuk time;
  declare v_rabu_batas_awal_pulang time;
  declare v_rabu_batas_akhir_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_awal_masuk time;
  declare v_kamis_batas_akhir_masuk time;
  declare v_kamis_batas_awal_pulang time;
  declare v_kamis_batas_akhir_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_awal_masuk time;
  declare v_jumat_batas_akhir_masuk time;
  declare v_jumat_batas_awal_pulang time;
  declare v_jumat_batas_akhir_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_awal_masuk time;
  declare v_sabtu_batas_akhir_masuk time;
  declare v_sabtu_batas_awal_pulang time;
  declare v_sabtu_batas_akhir_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_awal_masuk time;
  declare v_minggu_batas_akhir_masuk time;
  declare v_minggu_batas_awal_pulang time;
  declare v_minggu_batas_akhir_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      p.id as pegawai, jk.berlaku_dari, jk.berlaku_sampai, jk.fleksi_time, jk.waktu_mulai_istirahat, jk.waktu_selesai_istirahat,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_awal_masuk, jk.senin_batas_akhir_masuk, jk.senin_batas_awal_pulang, jk.senin_batas_akhir_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_awal_masuk, jk.selasa_batas_akhir_masuk, jk.selasa_batas_awal_pulang, jk.selasa_batas_akhir_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_awal_masuk, jk.rabu_batas_akhir_masuk, jk.rabu_batas_awal_pulang, jk.rabu_batas_akhir_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_awal_masuk, jk.kamis_batas_akhir_masuk, jk.kamis_batas_awal_pulang, jk.kamis_batas_akhir_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_awal_masuk, jk.jumat_batas_akhir_masuk, jk.jumat_batas_awal_pulang, jk.jumat_batas_akhir_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_awal_masuk, jk.sabtu_batas_akhir_masuk, jk.sabtu_batas_awal_pulang, jk.sabtu_batas_akhir_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_awal_masuk, jk.minggu_batas_akhir_masuk, jk.minggu_batas_awal_pulang, jk.minggu_batas_akhir_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    inner join pegawai p on list_pegawai is null or find_in_set(p.id, list_pegawai) <> 0
    where jk.berlaku_bagi='Seluruh Unit Kerja' and jk.status='Sedang Berlaku'
    order by jk.prioritas asc
    ;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_fleksi_time, v_waktu_mulai_istirahat, v_waktu_selesai_istirahat,
      v_senin_masuk, v_senin_pulang, v_senin_batas_awal_masuk, v_senin_batas_akhir_masuk, v_senin_batas_awal_pulang, v_senin_batas_akhir_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_awal_masuk, v_selasa_batas_akhir_masuk, v_selasa_batas_awal_pulang, v_selasa_batas_akhir_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_awal_masuk, v_rabu_batas_akhir_masuk, v_rabu_batas_awal_pulang, v_rabu_batas_akhir_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_awal_masuk, v_kamis_batas_akhir_masuk, v_kamis_batas_awal_pulang, v_kamis_batas_akhir_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_awal_masuk, v_jumat_batas_akhir_masuk, v_jumat_batas_awal_pulang, v_jumat_batas_akhir_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_awal_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_batas_awal_pulang, v_sabtu_batas_akhir_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_awal_masuk, v_minggu_batas_akhir_masuk, v_minggu_batas_awal_pulang, v_minggu_batas_akhir_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      -- update jadwal kehadiran
      update kehadiran k
      set
        k.fleksi_time=v_fleksi_time,
        jadwal_masuk=get_time(
          k.tanggal,
          v_minggu_masuk, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_libur='Ya'
        ),
        jadwal_pulang=get_time_after(
          k.tanggal,
          v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
        ),
        jadwal_batas_awal_masuk=get_time_before(
          k.tanggal,
          v_minggu_masuk, v_minggu_batas_awal_masuk, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_batas_awal_masuk, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_batas_awal_masuk, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_batas_awal_masuk, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_batas_awal_masuk, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_batas_awal_masuk, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_batas_awal_masuk, v_sabtu_libur='Ya'
        ),
        jadwal_batas_akhir_masuk=get_time_after(
          k.tanggal,
          v_minggu_masuk, v_minggu_batas_akhir_masuk, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_batas_akhir_masuk, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_batas_akhir_masuk, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_batas_akhir_masuk, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_batas_akhir_masuk, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_batas_akhir_masuk, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_libur='Ya'
        ),
        jadwal_batas_awal_pulang=get_time_after(
          k.tanggal,
          v_minggu_masuk, v_minggu_batas_awal_pulang, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_batas_awal_pulang, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_batas_awal_pulang, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_batas_awal_pulang, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_batas_awal_pulang, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_batas_awal_pulang, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_batas_awal_pulang, v_sabtu_libur='Ya'
        ),
        jadwal_batas_akhir_pulang=get_time_after(
          k.tanggal,
          v_minggu_masuk, v_minggu_batas_akhir_pulang, v_minggu_libur='Ya',
          v_senin_masuk, v_senin_batas_akhir_pulang, v_senin_libur='Ya',
          v_selasa_masuk, v_selasa_batas_akhir_pulang, v_selasa_libur='Ya',
          v_rabu_masuk, v_rabu_batas_akhir_pulang, v_rabu_libur='Ya',
          v_kamis_masuk, v_kamis_batas_akhir_pulang, v_kamis_libur='Ya',
          v_jumat_masuk, v_jumat_batas_akhir_pulang, v_jumat_libur='Ya',
          v_sabtu_masuk, v_sabtu_batas_akhir_pulang, v_sabtu_libur='Ya'
        ),
        keterangan=case
          when keterangan is null then
          case
            when DAYOFWEEK(k.tanggal)=1 and v_minggu_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=2 and v_senin_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=3 and v_selasa_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=4 and v_rabu_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=5 and v_kamis_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=6 and v_jumat_libur='Ya' then 7
            when DAYOFWEEK(k.tanggal)=7 and v_sabtu_libur='Ya' then 7
          end
          else keterangan
        end,
        k.waktu_mulai_istirahat=timestamp(k.tanggal, v_waktu_mulai_istirahat),
        k.waktu_selesai_istirahat=timestamp(k.tanggal, v_waktu_selesai_istirahat)
        where k.tanggal between v_berlaku_dari and v_berlaku_sampai
          -- and (list_pegawai is null or find_in_set(k.pegawai, list_pegawai) <> 0)
          and k.pegawai=v_pegawai
      ;

      -- insert jadwal kehadiran (kalau belum ada)
      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, fleksi_time, waktu_mulai_istirahat, waktu_selesai_istirahat, jadwal_masuk, jadwal_pulang, jadwal_batas_awal_masuk, jadwal_batas_akhir_masuk, jadwal_batas_awal_pulang, jadwal_batas_akhir_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          v_fleksi_time, timestamp(v_tanggal, v_waktu_mulai_istirahat), timestamp(v_tanggal, v_waktu_selesai_istirahat),
          get_time(
            v_tanggal,
            v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_time_before(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_pulang, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 7
              else 1
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DELIMITER ;
