<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartAsset::register($this);

$this->registerJsFile('@web/app/kepegawaian/list-bukti-kehadiran1.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$bulans = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', ];
$tahuns = ['2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024', ];

$chart = [
    'label' => ['Belum Diproses', 'Direvisi', 'Diterima', 'Ditolak'],
    'data' => [0, 0, 0, 0],
    'color' => ['#b83333', '#b87633', '#3376b8', '#33b876']
];

$bk = Yii::$app->db->createCommand('SELECT status_pengajuan, count(*) as jumlah from bukti_kehadiran where MONTH(dari_tanggal) = :bulan and YEAR(dari_tanggal) = :tahun group by status_pengajuan', ['bulan' => $model['filter_kehadiran']->bulan, 'tahun' => $model['filter_kehadiran']->tahun])->queryAll();
foreach ($bk as $key => $value) {
    switch ($value['status_pengajuan']) {
        case 'Diajukan':
            $chart['data'][0] = $value['jumlah'];
            $chart['label'][0] .= ' ' . $value['jumlah'];
            break;
        case 'Direvisi':
            $chart['data'][1] = $value['jumlah'];
            $chart['label'][1] .= ' ' . $value['jumlah'];
            break;
        case 'Diterima':
            $chart['data'][2] = $value['jumlah'];
            $chart['label'][2] .= ' ' . $value['jumlah'];
            break;
        case 'Ditolak':
            $chart['data'][3] = $value['jumlah'];
            $chart['label'][3] .= ' ' . $value['jumlah'];
            break;
        default:
            # code...
            break;
    }
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Statistik</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app'], 'method' => 'get', 'action' => Url::to(['kepegawaian/bukti-kehadiran'])]); ?>
            <div class="box box-break-md box-gutter">
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'bulan', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'bulan', $bulans, ['class' => 'form-dropdown', 'name' => 'bulan']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'bulan')->end(); ?>
                </div>
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'tahun', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'tahun', $tahuns, ['class' => 'form-dropdown', 'name' => 'tahun']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'tahun')->end(); ?>
                </div>
                <div class="box-2">
                    <div class="form-wrapper margin-0">
                        <button class="button button-block border-azure bg-azure m-margin-top-5" type="submit">Tampilkan</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

        <hr class="margin-y-10 border-top border-light-orange">

        <div>
            <div class="scroll-x"><div style="width: 100%;"><canvas id="chart" style="height: 300px;"></canvas></div></div>
            <script>
                window.addEventListener('load', function() {
                    var ctx = document.getElementById('chart').getContext('2d');
                    window.myLine = Chart.Bar(ctx, {
                        data: {
                            labels: <?= json_encode($chart['label']) ?>,
                            datasets: [{
                                // label: 'asdf',
                                borderColor: <?= json_encode($chart['color']) ?>,
                                backgroundColor: <?= json_encode($chart['color']) ?>,
                                fill: false,
                                data: <?= json_encode($chart['data']) ?>,
                                yAxisID: 'y-axis-1',
                            }]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Statistik Bukti Kehadiran',
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Bukti Kehadiran'
                                    }
                                }],
                                yAxes: [{
                                    type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                    display: true,
                                    position: 'left',
                                    id: 'y-axis-1',
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Jumlah'
                                    },
                                }],
                            },
                            stacked: false,
                            maintainAspectRatio: false,
                            elements: {
                                line: {
                                    tension: 0, // disables bezier curves
                                }
                            },
                            animation: {
                                duration: 0, // general animation time
                            },
                            hover: {
                                animationDuration: 0, // duration of animations when hovering an item
                            },
                            hoverMode: 'index',
                            responsiveAnimationDuration: 0, // animation duration after a resize
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            legend: {
                                display: false,
                            },
                        }
                    });
                    myLine.update();
                });
            </script>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Daftar Pengajuan</span>
            <div class="pull-right">
                <?= Html::a('Unggah Bukti Kehadiran', ['bukti-kehadiran-create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-bukti-kehadiran table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>status pengajuan</th>
                        <th>keterangan kehadiran</th>
                        <th>diajukan oleh</th>
                        <th>dari tanggal</th>
                        <th>sampai tanggal</th>
                        <th>nomor surat</th>
                        <th>waktu pengajuan</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status pengajuan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan kehadiran..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search diajukan oleh..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dari tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search sampai tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor surat..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu pengajuan..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Monitoring Pengajuan</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-bukti-kehadiran-monitoring table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>status pengajuan</th>
                        <th>keterangan kehadiran</th>
                        <th>diajukan oleh</th>
                        <th>dari tanggal</th>
                        <th>sampai tanggal</th>
                        <th>nomor surat</th>
                        <th>waktu pengajuan</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status pengajuan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan kehadiran..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search diajukan oleh..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dari tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search sampai tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor surat..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu pengajuan..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Riwayat Pengajuan</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-bukti-kehadiran-riwayat table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>status pengajuan</th>
                        <th>keterangan kehadiran</th>
                        <th>diajukan oleh</th>
                        <th>dari tanggal</th>
                        <th>sampai tanggal</th>
                        <th>nomor surat</th>
                        <th>waktu pengajuan</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status pengajuan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan kehadiran..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search diajukan oleh..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dari tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search sampai tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor surat..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu pengajuan..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>