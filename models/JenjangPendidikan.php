<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenjang_pendidikan".
 *
 * @property integer $id
 * @property string $jenjang_pendidikan
 *
 * @property Pegawai[] $pegawais
 * @property Pegawai[] $pegawais0
 * @property Pegawai[] $pegawais1
 * @property Pegawai[] $pegawais2
 * @property PegawaiAnak[] $pegawaiAnaks
 * @property PegawaiPendidikan[] $pegawaiPendidikans
 */
class JenjangPendidikan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenjang_pendidikan';
    }

    public function rules()
    {
        return [
            //id

            //jenjang_pendidikan
            [['jenjang_pendidikan'], 'required'],
            [['jenjang_pendidikan'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenjang_pendidikan' => 'Jenjang Pendidikan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['jenjang_pendidikan_terakhir_ayah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais0()
    {
        return $this->hasMany(Pegawai::className(), ['jenjang_pendidikan_terakhir_ibu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais1()
    {
        return $this->hasMany(Pegawai::className(), ['jenjang_pendidikan_terakhir_pasangan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais2()
    {
        return $this->hasMany(Pegawai::className(), ['jenjang_pendidikan_terakhir' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiAnaks()
    {
        return $this->hasMany(PegawaiAnak::className(), ['jenjang_pendidikan_terakhir' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPendidikans()
    {
        return $this->hasMany(PegawaiPendidikan::className(), ['jenjang_pendidikan' => 'id']);
    }
}
