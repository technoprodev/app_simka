<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "tingkat_hukuman_disiplin".
 *
 * @property integer $id
 * @property string $tingkat_hukuman_disiplin
 *
 * @property JenisHukumanDisiplin[] $jenisHukumanDisiplins
 */
class TingkatHukumanDisiplin extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'tingkat_hukuman_disiplin';
    }

    public function rules()
    {
        return [
            //id

            //tingkat_hukuman_disiplin
            [['tingkat_hukuman_disiplin'], 'required'],
            [['tingkat_hukuman_disiplin'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tingkat_hukuman_disiplin' => 'Tingkat Hukuman Disiplin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisHukumanDisiplins()
    {
        return $this->hasMany(JenisHukumanDisiplin::className(), ['tingkat_hukuman_disiplin' => 'id']);
    }
}
