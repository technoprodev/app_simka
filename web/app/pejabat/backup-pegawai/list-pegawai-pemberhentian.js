$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai-pemberhentian');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('pejabat/datatables-pegawai-pemberhentian', {idPegawai : idPegawai}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: {
                            _: 'jp.jenis_pemberhentian',
                            display: 'jenis_pemberhentian',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.terhitung_mulai_tanggal',
                            display: 'terhitung_mulai_tanggal',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.pangkat',
                            display: 'pangkat',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + ' (' + row.ruang + ' / ' + row.golongan + ')</span>' +
                                '</div>' +
                                '';
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.instansi_saat_pemberhentian',
                            display: 'instansi_saat_pemberhentian',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.unit_kerja_saat_pemberhentian',
                            display: 'unit_kerja_saat_pemberhentian',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jj.jenjang_jabatan',
                            display: 'jenjang_jabatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + row.jenis_jabatan + ' - ' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_jabatan_saat_diberhentikan',
                            display: 'nama_jabatan_saat_diberhentikan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.catatan',
                            display: 'catatan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jenis_sk',
                            display: 'jenis_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nomor_sk',
                            display: 'nomor_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tanggal_sk',
                            display: 'tanggal_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.pejabat_penetap_sk',
                            display: 'pejabat_penetap_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});