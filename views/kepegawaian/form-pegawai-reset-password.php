<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/form-pegawai.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$pegawaiAnaks = [];
if (isset($model['pegawai_anak']))
    foreach ($model['pegawai_anak'] as $key => $pegawaiAnak)
        $pegawaiAnaks[] = $pegawaiAnak->attributes;
$pegawaiDiklats = [];
if (isset($model['pegawai_diklat']))
    foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat)
        $pegawaiDiklats[] = $pegawaiDiklat->attributes;
$pegawaiHukumanDisiplins = [];
if (isset($model['pegawai_hukuman_disiplin']))
    foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin)
        $pegawaiHukumanDisiplins[] = $pegawaiHukumanDisiplin->attributes;
$pegawaiKenaikanGajiBerkalas = [];
if (isset($model['pegawai_kenaikan_gaji_berkala']))
    foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala)
        $pegawaiKenaikanGajiBerkalas[] = $pegawaiKenaikanGajiBerkala->attributes;
$pegawaiKepangkatans = [];
if (isset($model['pegawai_kepangkatan']))
    foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan)
        $pegawaiKepangkatans[] = $pegawaiKepangkatan->attributes;
$pegawaiKunjunganLuarNegeris = [];
if (isset($model['pegawai_kunjungan_luar_negeri']))
    foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri)
        $pegawaiKunjunganLuarNegeris[] = $pegawaiKunjunganLuarNegeri->attributes;
$pegawaiKursusPelatihans = [];
if (isset($model['pegawai_kursus_pelatihan']))
    foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan)
        $pegawaiKursusPelatihans[] = $pegawaiKursusPelatihan->attributes;
$pegawaiMasaPersiapanPensiuns = [];
if (isset($model['pegawai_masa_persiapan_pensiun']))
    foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun)
        $pegawaiMasaPersiapanPensiuns[] = $pegawaiMasaPersiapanPensiun->attributes;
$pegawaiMutasis = [];
if (isset($model['pegawai_mutasi']))
    foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi)
        $pegawaiMutasis[] = $pegawaiMutasi->attributes;
$pegawaiOrganisasis = [];
if (isset($model['pegawai_organisasi']))
    foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi)
        $pegawaiOrganisasis[] = $pegawaiOrganisasi->attributes;
$pegawaiPelaksanas = [];
if (isset($model['pegawai_pelaksana']))
    foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana)
        $pegawaiPelaksanas[] = $pegawaiPelaksana->attributes;
$pegawaiPemberhentians = [];
if (isset($model['pegawai_pemberhentian']))
    foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian)
        $pegawaiPemberhentians[] = $pegawaiPemberhentian->attributes;
$pegawaiPendidikans = [];
if (isset($model['pegawai_pendidikan']))
    foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan)
        $pegawaiPendidikans[] = $pegawaiPendidikan->attributes;
$pegawaiPenetapanAngkaKredits = [];
if (isset($model['pegawai_penetapan_angka_kredit']))
    foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit)
        $pegawaiPenetapanAngkaKredits[] = $pegawaiPenetapanAngkaKredit->attributes;
$pegawaiPenghargaans = [];
if (isset($model['pegawai_penghargaan']))
    foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan)
        $pegawaiPenghargaans[] = $pegawaiPenghargaan->attributes;
$pegawaiSaudaraKandungs = [];
if (isset($model['pegawai_saudara_kandung']))
    foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung)
        $pegawaiSaudaraKandungs[] = $pegawaiSaudaraKandung->attributes;

$this->registerJs(
    // 'vm.$data.pegawai.virtual_category = ' . json_encode($model['pegawai']->virtual_category) . ';' .
    'vm.$data.pegawai.pegawaiAnaks = vm.$data.pegawai.pegawaiAnaks.concat(' . json_encode($pegawaiAnaks) . ');' .
    'vm.$data.pegawai.pegawaiDiklats = vm.$data.pegawai.pegawaiDiklats.concat(' . json_encode($pegawaiDiklats) . ');' .
    'vm.$data.pegawai.pegawaiHukumanDisiplins = vm.$data.pegawai.pegawaiHukumanDisiplins.concat(' . json_encode($pegawaiHukumanDisiplins) . ');' .
    'vm.$data.pegawai.pegawaiKenaikanGajiBerkalas = vm.$data.pegawai.pegawaiKenaikanGajiBerkalas.concat(' . json_encode($pegawaiKenaikanGajiBerkalas) . ');' .
    'vm.$data.pegawai.pegawaiKepangkatans = vm.$data.pegawai.pegawaiKepangkatans.concat(' . json_encode($pegawaiKepangkatans) . ');' .
    'vm.$data.pegawai.pegawaiKunjunganLuarNegeris = vm.$data.pegawai.pegawaiKunjunganLuarNegeris.concat(' . json_encode($pegawaiKunjunganLuarNegeris) . ');' .
    'vm.$data.pegawai.pegawaiKursusPelatihans = vm.$data.pegawai.pegawaiKursusPelatihans.concat(' . json_encode($pegawaiKursusPelatihans) . ');' .
    'vm.$data.pegawai.pegawaiMasaPersiapanPensiuns = vm.$data.pegawai.pegawaiMasaPersiapanPensiuns.concat(' . json_encode($pegawaiMasaPersiapanPensiuns) . ');' .
    'vm.$data.pegawai.pegawaiMutasis = vm.$data.pegawai.pegawaiMutasis.concat(' . json_encode($pegawaiMutasis) . ');' .
    'vm.$data.pegawai.pegawaiOrganisasis = vm.$data.pegawai.pegawaiOrganisasis.concat(' . json_encode($pegawaiOrganisasis) . ');' .
    'vm.$data.pegawai.pegawaiPelaksanas = vm.$data.pegawai.pegawaiPelaksanas.concat(' . json_encode($pegawaiPelaksanas) . ');' .
    'vm.$data.pegawai.pegawaiPemberhentians = vm.$data.pegawai.pegawaiPemberhentians.concat(' . json_encode($pegawaiPemberhentians) . ');' .
    'vm.$data.pegawai.pegawaiPendidikans = vm.$data.pegawai.pegawaiPendidikans.concat(' . json_encode($pegawaiPendidikans) . ');' .
    'vm.$data.pegawai.pegawaiPenetapanAngkaKredits = vm.$data.pegawai.pegawaiPenetapanAngkaKredits.concat(' . json_encode($pegawaiPenetapanAngkaKredits) . ');' .
    'vm.$data.pegawai.pegawaiPenghargaans = vm.$data.pegawai.pegawaiPenghargaans.concat(' . json_encode($pegawaiPenghargaans) . ');' .
    'vm.$data.pegawai.pegawaiSaudaraKandungs = vm.$data.pegawai.pegawaiSaudaraKandungs.concat(' . json_encode($pegawaiSaudaraKandungs) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['user']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
if ($model['pegawai']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pegawai'], ['class' => '']);
}

if (isset($model['pegawai_anak'])) foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
    if ($pegawaiAnak->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiAnak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_diklat'])) foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
    if ($pegawaiDiklat->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiDiklat, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_hukuman_disiplin'])) foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
    if ($pegawaiHukumanDisiplin->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiHukumanDisiplin, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kenaikan_gaji_berkala'])) foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
    if ($pegawaiKenaikanGajiBerkala->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKenaikanGajiBerkala, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kepangkatan'])) foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
    if ($pegawaiKepangkatan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKepangkatan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kunjungan_luar_negeri'])) foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
    if ($pegawaiKunjunganLuarNegeri->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKunjunganLuarNegeri, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kursus_pelatihan'])) foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
    if ($pegawaiKursusPelatihan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKursusPelatihan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_masa_persiapan_pensiun'])) foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
    if ($pegawaiMasaPersiapanPensiun->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMasaPersiapanPensiun, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_mutasi'])) foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
    if ($pegawaiMutasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMutasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_organisasi'])) foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
    if ($pegawaiOrganisasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiOrganisasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pelaksana'])) foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
    if ($pegawaiPelaksana->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPelaksana, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pemberhentian'])) foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
    if ($pegawaiPemberhentian->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPemberhentian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pendidikan'])) foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
    if ($pegawaiPendidikan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penetapan_angka_kredit'])) foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
    if ($pegawaiPenetapanAngkaKredit->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenetapanAngkaKredit, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penghargaan'])) foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
    if ($pegawaiPenghargaan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenghargaan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_saudara_kandung'])) foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
    if ($pegawaiSaudaraKandung->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiSaudaraKandung, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}

$pilihanKepangkatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT k.id, CONCAT(k.pangkat, ' (', k.golongan, ' / ', k.ruang, ')') AS kepangkatan FROM kepangkatan k
            order by kepangkatan
        ", []
    )->queryAll(), 'id', 'kepangkatan'
);
$pilihanJenisSk = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT js.id, js.jenis_sk FROM jenis_sk js
            order by jenis_sk
        ", []
    )->queryAll(), 'id', 'jenis_sk'
);
$pilihanJenjangJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, CONCAT(jjj.jenis_jabatan, ' - ', jj.jenjang_jabatan) AS jenjang_jabatan FROM jenjang_jabatan jj
            JOIN jenis_jabatan jjj ON jjj.id = jj.jenis_jabatan
            order by jenjang_jabatan
        ", []
    )->queryAll(), 'id', 'jenjang_jabatan'
);
$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(juk.jenis_unit_kerja, ' - ', uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanEselon = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT e.id, e.eselon FROM eselon e
            order by e.eselon
        ", []
    )->queryAll(), 'id', 'eselon'
);
$pilihanJenisPelaksana = (new \app_simka\models\PegawaiPelaksana())->getEnum('jenis_pelaksana');
$pilihanJenjangPendidikan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jp.id, jp.jenjang_pendidikan FROM jenjang_pendidikan jp
        ", []
    )->queryAll(), 'id', 'jenjang_pendidikan'
);
$pilihanJenisDiklat = (new \app_simka\models\PegawaiDiklat())->getEnum('jenis_diklat');
$pilihanNegara = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT n.id, n.nama FROM negara n
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">

        <div class="fs-18 text-azure fs-italic">
            Reset Password
        </div>

        <hr class="margin-y-10 border-top border-light-orange">
<?php endif; ?>

        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="box box-break-sm">
                    <div class="box-3 padding-x-0 padding-y-5">
                        <div class="text-grayer">Usename</div>
                    </div>
                    <div class="box-9 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['user']->username ? $model['user']->username : '<span class="text-grayer">(belum diisi)</span>' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-3 padding-x-0 padding-y-5">
                        <div class="text-grayer">Email</div>
                    </div>
                    <div class="box-9 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['user']->email ? $model['user']->email : '<span class="text-grayer">(belum diisi)</span>' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <?= $form->field($model['user'], 'password', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-3 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'password', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-9 m-padding-x-0">
                        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'password', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'password')->end(); ?>

                <?= $form->field($model['user'], 'password_repeat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-3 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-9 m-padding-x-0">
                        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'password_repeat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'password_repeat')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
