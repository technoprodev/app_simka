<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/form-pegawai.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$pegawaiAnaks = [];
if (isset($model['pegawai_anak']))
    foreach ($model['pegawai_anak'] as $key => $pegawaiAnak)
        $pegawaiAnaks[] = $pegawaiAnak->attributes;
$pegawaiDiklats = [];
if (isset($model['pegawai_diklat']))
    foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat)
        $pegawaiDiklats[] = $pegawaiDiklat->attributes;
$pegawaiHukumanDisiplins = [];
if (isset($model['pegawai_hukuman_disiplin']))
    foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin)
        $pegawaiHukumanDisiplins[] = $pegawaiHukumanDisiplin->attributes;
$pegawaiKenaikanGajiBerkalas = [];
if (isset($model['pegawai_kenaikan_gaji_berkala']))
    foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala)
        $pegawaiKenaikanGajiBerkalas[] = $pegawaiKenaikanGajiBerkala->attributes;
$pegawaiKepangkatans = [];
if (isset($model['pegawai_kepangkatan']))
    foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan)
        $pegawaiKepangkatans[] = $pegawaiKepangkatan->attributes;
$pegawaiKunjunganLuarNegeris = [];
if (isset($model['pegawai_kunjungan_luar_negeri']))
    foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri)
        $pegawaiKunjunganLuarNegeris[] = $pegawaiKunjunganLuarNegeri->attributes;
$pegawaiKursusPelatihans = [];
if (isset($model['pegawai_kursus_pelatihan']))
    foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan)
        $pegawaiKursusPelatihans[] = $pegawaiKursusPelatihan->attributes;
$pegawaiMasaPersiapanPensiuns = [];
if (isset($model['pegawai_masa_persiapan_pensiun']))
    foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun)
        $pegawaiMasaPersiapanPensiuns[] = $pegawaiMasaPersiapanPensiun->attributes;
$pegawaiMutasis = [];
if (isset($model['pegawai_mutasi']))
    foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi)
        $pegawaiMutasis[] = $pegawaiMutasi->attributes;
$pegawaiOrganisasis = [];
if (isset($model['pegawai_organisasi']))
    foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi)
        $pegawaiOrganisasis[] = $pegawaiOrganisasi->attributes;
$pegawaiPelaksanas = [];
if (isset($model['pegawai_pelaksana']))
    foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana)
        $pegawaiPelaksanas[] = $pegawaiPelaksana->attributes;
$pegawaiPemberhentians = [];
if (isset($model['pegawai_pemberhentian']))
    foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian)
        $pegawaiPemberhentians[] = $pegawaiPemberhentian->attributes;
$pegawaiPendidikans = [];
if (isset($model['pegawai_pendidikan']))
    foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan)
        $pegawaiPendidikans[] = $pegawaiPendidikan->attributes;
$pegawaiPenetapanAngkaKredits = [];
if (isset($model['pegawai_penetapan_angka_kredit']))
    foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit)
        $pegawaiPenetapanAngkaKredits[] = $pegawaiPenetapanAngkaKredit->attributes;
$pegawaiPenghargaans = [];
if (isset($model['pegawai_penghargaan']))
    foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan)
        $pegawaiPenghargaans[] = $pegawaiPenghargaan->attributes;
$pegawaiSaudaraKandungs = [];
if (isset($model['pegawai_saudara_kandung']))
    foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung)
        $pegawaiSaudaraKandungs[] = $pegawaiSaudaraKandung->attributes;

$this->registerJs(
    // 'vm.$data.pegawai.virtual_category = ' . json_encode($model['pegawai']->virtual_category) . ';' .
    'vm.$data.pegawai.pegawaiAnaks = vm.$data.pegawai.pegawaiAnaks.concat(' . json_encode($pegawaiAnaks) . ');' .
    'vm.$data.pegawai.pegawaiDiklats = vm.$data.pegawai.pegawaiDiklats.concat(' . json_encode($pegawaiDiklats) . ');' .
    'vm.$data.pegawai.pegawaiHukumanDisiplins = vm.$data.pegawai.pegawaiHukumanDisiplins.concat(' . json_encode($pegawaiHukumanDisiplins) . ');' .
    'vm.$data.pegawai.pegawaiKenaikanGajiBerkalas = vm.$data.pegawai.pegawaiKenaikanGajiBerkalas.concat(' . json_encode($pegawaiKenaikanGajiBerkalas) . ');' .
    'vm.$data.pegawai.pegawaiKepangkatans = vm.$data.pegawai.pegawaiKepangkatans.concat(' . json_encode($pegawaiKepangkatans) . ');' .
    'vm.$data.pegawai.pegawaiKunjunganLuarNegeris = vm.$data.pegawai.pegawaiKunjunganLuarNegeris.concat(' . json_encode($pegawaiKunjunganLuarNegeris) . ');' .
    'vm.$data.pegawai.pegawaiKursusPelatihans = vm.$data.pegawai.pegawaiKursusPelatihans.concat(' . json_encode($pegawaiKursusPelatihans) . ');' .
    'vm.$data.pegawai.pegawaiMasaPersiapanPensiuns = vm.$data.pegawai.pegawaiMasaPersiapanPensiuns.concat(' . json_encode($pegawaiMasaPersiapanPensiuns) . ');' .
    'vm.$data.pegawai.pegawaiMutasis = vm.$data.pegawai.pegawaiMutasis.concat(' . json_encode($pegawaiMutasis) . ');' .
    'vm.$data.pegawai.pegawaiOrganisasis = vm.$data.pegawai.pegawaiOrganisasis.concat(' . json_encode($pegawaiOrganisasis) . ');' .
    'vm.$data.pegawai.pegawaiPelaksanas = vm.$data.pegawai.pegawaiPelaksanas.concat(' . json_encode($pegawaiPelaksanas) . ');' .
    'vm.$data.pegawai.pegawaiPemberhentians = vm.$data.pegawai.pegawaiPemberhentians.concat(' . json_encode($pegawaiPemberhentians) . ');' .
    'vm.$data.pegawai.pegawaiPendidikans = vm.$data.pegawai.pegawaiPendidikans.concat(' . json_encode($pegawaiPendidikans) . ');' .
    'vm.$data.pegawai.pegawaiPenetapanAngkaKredits = vm.$data.pegawai.pegawaiPenetapanAngkaKredits.concat(' . json_encode($pegawaiPenetapanAngkaKredits) . ');' .
    'vm.$data.pegawai.pegawaiPenghargaans = vm.$data.pegawai.pegawaiPenghargaans.concat(' . json_encode($pegawaiPenghargaans) . ');' .
    'vm.$data.pegawai.pegawaiSaudaraKandungs = vm.$data.pegawai.pegawaiSaudaraKandungs.concat(' . json_encode($pegawaiSaudaraKandungs) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['user']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
if ($model['pegawai']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pegawai'], ['class' => '']);
}

if (isset($model['pegawai_anak'])) foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
    if ($pegawaiAnak->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiAnak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_diklat'])) foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
    if ($pegawaiDiklat->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiDiklat, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_hukuman_disiplin'])) foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
    if ($pegawaiHukumanDisiplin->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiHukumanDisiplin, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kenaikan_gaji_berkala'])) foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
    if ($pegawaiKenaikanGajiBerkala->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKenaikanGajiBerkala, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kepangkatan'])) foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
    if ($pegawaiKepangkatan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKepangkatan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kunjungan_luar_negeri'])) foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
    if ($pegawaiKunjunganLuarNegeri->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKunjunganLuarNegeri, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kursus_pelatihan'])) foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
    if ($pegawaiKursusPelatihan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKursusPelatihan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_masa_persiapan_pensiun'])) foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
    if ($pegawaiMasaPersiapanPensiun->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMasaPersiapanPensiun, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_mutasi'])) foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
    if ($pegawaiMutasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMutasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_organisasi'])) foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
    if ($pegawaiOrganisasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiOrganisasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pelaksana'])) foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
    if ($pegawaiPelaksana->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPelaksana, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pemberhentian'])) foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
    if ($pegawaiPemberhentian->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPemberhentian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pendidikan'])) foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
    if ($pegawaiPendidikan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penetapan_angka_kredit'])) foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
    if ($pegawaiPenetapanAngkaKredit->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenetapanAngkaKredit, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penghargaan'])) foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
    if ($pegawaiPenghargaan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenghargaan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_saudara_kandung'])) foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
    if ($pegawaiSaudaraKandung->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiSaudaraKandung, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}

$pilihanPekerjaan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT p.id, p.pekerjaan FROM pekerjaan p
            order by pekerjaan
        ", []
    )->queryAll(), 'id', 'pekerjaan'
);
$pilihanAgama = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT a.id, a.agama FROM agama a
            order by agama
        ", []
    )->queryAll(), 'id', 'agama'
);
$pilihanEselon = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT e.id, e.eselon FROM eselon e
            order by eselon
        ", []
    )->queryAll(), 'id', 'eselon'
);
$pilihanGrade = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT g.id, g.grade FROM grade g
            order by grade
        ", []
    )->queryAll(), 'id', 'grade'
);
$pilihanKedudukanPegawai = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kp.id, kp.kedudukan_pegawai FROM kedudukan_pegawai kp
            order by kedudukan_pegawai
        ", []
    )->queryAll(), 'id', 'kedudukan_pegawai'
);
$pilihanKepangkatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT k.id, CONCAT(k.pangkat, ' (', k.golongan, ' / ', k.ruang, ')') AS kepangkatan FROM kepangkatan k
            order by kepangkatan
        ", []
    )->queryAll(), 'id', 'kepangkatan'
);
$pilihanStatusKepegawaian = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sk.id, sk.status_kepegawaian FROM status_kepegawaian sk
            order by status_kepegawaian
        ", []
    )->queryAll(), 'id', 'status_kepegawaian'
);
$pilihanJenisSk = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT js.id, js.jenis_sk FROM jenis_sk js
            order by jenis_sk
        ", []
    )->queryAll(), 'id', 'jenis_sk'
);
$pilihanJenjangJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, CONCAT(jjj.jenis_jabatan, ' - ', jj.jenjang_jabatan) AS jenjang_jabatan FROM jenjang_jabatan jj
            JOIN jenis_jabatan jjj ON jjj.id = jj.jenis_jabatan
            order by jenjang_jabatan
        ", []
    )->queryAll(), 'id', 'jenjang_jabatan'
);
$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanUnitKerjaAdministrasi = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanStatusPernikahan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sm.id, sm.status_marital FROM status_marital sm
            order by status_marital
        ", []
    )->queryAll(), 'id', 'status_marital'
);
$pilihanPeriodeOrganisasi = (new \app_simka\models\PegawaiOrganisasi())->getEnum('periode_organisasi');
$pilihanStatusHidupAyah = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ayah');
$pilihanStatusHidupIbu = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ibu');
$pilihanStatusHidupPasangan = (new \app_simka\models\Pegawai())->getEnum('status_hidup_pasangan');
$pilihanGolonganDarah = (new \app_simka\models\Pegawai())->getEnum('golongan_darah');
$pilihanJenisKelamin = (new \app_simka\models\Pegawai())->getEnum('jenis_kelamin');
$pilihanJenisPelaksana = (new \app_simka\models\PegawaiPelaksana())->getEnum('jenis_pelaksana');
$pilihanStatusAnak = (new \app_simka\models\PegawaiAnak())->getEnum('status_anak');
$pilihanStatusTunjangan = (new \app_simka\models\PegawaiAnak())->getEnum('status_tunjangan');
$pilihanJenjangPendidikan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jp.id, jp.jenjang_pendidikan FROM jenjang_pendidikan jp
        ", []
    )->queryAll(), 'id', 'jenjang_pendidikan'
);
$pilihanJenisDiklat = (new \app_simka\models\PegawaiDiklat())->getEnum('jenis_diklat');
$pilihanNegara = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT n.id, n.nama FROM negara n
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);
$pilihanModelRambut = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, model_rambut FROM model_rambut
            order by model_rambut
        ", []
    )->queryAll(), 'id', 'model_rambut'
);
$pilihanBentukWajah = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, bentuk_wajah FROM bentuk_wajah
            order by bentuk_wajah
        ", []
    )->queryAll(), 'id', 'bentuk_wajah'
);
$pilihanWarnaKulit = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, warna_kulit FROM warna_kulit
            order by warna_kulit
        ", []
    )->queryAll(), 'id', 'warna_kulit'
);
$pilihanMendapatTunkin = (new \app_simka\models\Pegawai())->getEnum('mendapat_tunkin');

$roles = Yii::$app->authManager->getRoles();
foreach ($roles as $key => $value) $roles[$key] = $value->description;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Data Personal
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['pegawai'], 'nama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'nama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'nama', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'nama', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'nama')->end(); ?>

                <?= $form->field($model['pegawai'], 'nama_panggilan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'nama_panggilan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'nama_panggilan', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'nama_panggilan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'nama_panggilan')->end(); ?>

                <?= $form->field($model['pegawai'], 'jenis_kelamin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'jenis_kelamin', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'jenis_kelamin', $pilihanJenisKelamin, ['prompt' => '- pilih jenis kelamin -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'jenis_kelamin', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'jenis_kelamin')->end(); ?>

                <?= $form->field($model['pegawai'], 'tempat_lahir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'tempat_lahir', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'tempat_lahir', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'tempat_lahir', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'tempat_lahir')->end(); ?>

                <?= $form->field($model['pegawai'], 'agama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'agama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'agama', $pilihanAgama, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'agama', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'agama')->end(); ?>

                <?= $form->field($model['pegawai'], 'status_marital', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'status_marital', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'status_marital', $pilihanStatusPernikahan, ['prompt' => '- pilih status pernikahan -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'status_marital', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'status_marital')->end(); ?>

                <?= $form->field($model['pegawai'], 'golongan_darah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'golongan_darah', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'golongan_darah', $pilihanGolonganDarah, ['prompt' => '- pilih golongan darah -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'golongan_darah', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'golongan_darah')->end(); ?>

                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'jenjang_pendidikan_terakhir', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_pendidikan_terakhir', $pilihanJenjangPendidikan, ['prompt' => '- pilih jenjang pendidikan -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'jenjang_pendidikan_terakhir', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir')->end(); ?>

                <?= $form->field($model['pegawai'], 'gelar_depan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'gelar_depan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'gelar_depan', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'gelar_depan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'gelar_depan')->end(); ?>

                <?= $form->field($model['pegawai'], 'gelar_belakang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'gelar_belakang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'gelar_belakang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'gelar_belakang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'gelar_belakang')->end(); ?>

                <?= $form->field($model['pegawai'], 'tinggi_badan_cm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'tinggi_badan_cm')->end(); ?>

                <?= $form->field($model['pegawai'], 'model_rambut', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'model_rambut', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'model_rambut', $pilihanModelRambut, ['prompt' => '- pilih Model Rambut -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'model_rambut', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'model_rambut')->end(); ?>

                <?= $form->field($model['pegawai'], 'bentuk_wajah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'bentuk_wajah', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'bentuk_wajah', $pilihanBentukWajah, ['prompt' => '- pilih Bentuk Wajah -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'bentuk_wajah', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'bentuk_wajah')->end(); ?>

                <?= $form->field($model['pegawai'], 'warna_kulit', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'warna_kulit', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'warna_kulit', $pilihanWarnaKulit, ['prompt' => '- pilih warna kulit -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'warna_kulit', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'warna_kulit')->end(); ?>

                <?= $form->field($model['pegawai'], 'hobi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'hobi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'hobi', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'hobi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'hobi')->end(); ?>

                <?= $form->field($model['pegawai'], 'kemampuan_berbahasa', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'kemampuan_berbahasa')->end(); ?>

                <?= $form->field($model['pegawai'], 'poto', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'poto', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeFileInput($model['pegawai'], 'virtual_poto_upload'); ?>
                        <?= Html::error($model['pegawai'], 'poto', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'poto')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <div class="fs-18 text-azure fs-italic">
                    Data Pegawai
                </div>

                <hr class="margin-y-10 border-top border-light-orange">

                <?= $form->field($model['pegawai'], 'nip', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'nip', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'nip', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'nip', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'nip')->end(); ?>

                <?= $form->field($model['pegawai'], 'pin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'pin', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'pin', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'pin', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'pin')->end(); ?>

                <?= $form->field($model['pegawai'], 'status_kepegawaian', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'status_kepegawaian', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'status_kepegawaian', $pilihanStatusKepegawaian, ['prompt' => '- pilih status kepegawaian -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'status_kepegawaian', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'status_kepegawaian')->end(); ?>

                <?= $form->field($model['pegawai'], 'kepangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'kepangkatan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'kepangkatan', $pilihanKepangkatan, ['prompt' => '- pilih kepangkatan -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'kepangkatan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'kepangkatan')->end(); ?>

                <?= $form->field($model['pegawai'], 'jenjang_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_jabatan', $pilihanJenjangJabatan, ['prompt' => '- pilih jenjang jabatan -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'jenjang_jabatan')->end(); ?>

                <?= $form->field($model['pegawai'], 'nama_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'nama_jabatan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'nama_jabatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'nama_jabatan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'nama_jabatan')->end(); ?>

                <?= $form->field($model['pegawai'], 'unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'unit_kerja', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'unit_kerja', $pilihanUnitKerja, ['prompt' => '- pilih unit kerja -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'unit_kerja', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'unit_kerja')->end(); ?>

                <?= $form->field($model['pegawai'], 'unit_kerja_administrasi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'unit_kerja_administrasi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'unit_kerja_administrasi', $pilihanUnitKerjaAdministrasi, ['prompt' => '- pilih unit kerja administrasi -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'unit_kerja_administrasi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'unit_kerja_administrasi')->end(); ?>

                <?= $form->field($model['pegawai'], 'eselon', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'eselon', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'eselon', $pilihanEselon, ['prompt' => '- pilih eselon -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'eselon', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'eselon')->end(); ?>

                <?= $form->field($model['pegawai'], 'grade', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'grade', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'grade', $pilihanGrade, ['prompt' => '- pilih grade -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'grade', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'grade')->end(); ?>

                <?= $form->field($model['pegawai'], 'kedudukan_pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'kedudukan_pegawai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'kedudukan_pegawai', $pilihanKedudukanPegawai, ['prompt' => '- pilih kedudukan pegawai -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'kedudukan_pegawai', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'kedudukan_pegawai')->end(); ?>

                <?= $form->field($model['pegawai'], 'mendapat_tunkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'mendapat_tunkin', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'mendapat_tunkin', $pilihanMendapatTunkin, ['prompt' => '- pilih mendapat tunkin -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'mendapat_tunkin', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'mendapat_tunkin')->end(); ?>

                <?= $form->field($model['pegawai'], 'nik', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'nik', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'nik', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'nik', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'nik')->end(); ?>

                <?= $form->field($model['pegawai'], 'npwp', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'npwp', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'npwp', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'npwp', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'npwp')->end(); ?>

                <?= $form->field($model['pegawai'], 'telpon', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'telpon', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'telpon', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'telpon', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'telpon')->end(); ?>

                <?= $form->field($model['pegawai'], 'email_kantor', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'email_kantor', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'email_kantor', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'email_kantor', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'email_kantor')->end(); ?>

                <?= $form->field($model['pegawai'], 'email_pribadi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'email_pribadi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'email_pribadi', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'email_pribadi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'email_pribadi')->end(); ?>

                <?= $form->field($model['pegawai'], 'karpeg', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'karpeg', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'karpeg', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'karpeg', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'karpeg')->end(); ?>

                <?= $form->field($model['pegawai'], 'karis_atau_karsu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'karis_atau_karsu')->end(); ?>

                <?= $form->field($model['pegawai'], 'taspen', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'taspen', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'taspen', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'taspen', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'taspen')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <div class="fs-18 text-azure fs-italic">
                    Data Login
                </div>

                <hr class="margin-y-10 border-top border-light-orange">

                <?= $form->field($model['user'], 'username', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'username', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'username', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'username')->end(); ?>

                <?= $form->field($model['user'], 'email', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'email', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'email', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'email')->end(); ?>

                <?= $form->field($model['user'], 'password', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'password', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'password', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'password')->end(); ?>

                <?= $form->field($model['user'], 'password_repeat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['user'], 'password_repeat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['user'], 'password_repeat')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <div class="fs-18 text-azure fs-italic">
                    Role Pegawai
                </div>

                <hr class="margin-y-10 border-top border-light-orange">

                <div class="form-wrapper box box-break-sm">
                    <div class="box-1 padding-x-0 padding-y-5">
                        <label class="form-label">Roles</label>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <div class="form-checkbox">
                                <?= Html::checkboxList(
                                    'assignments[]',
                                    $model['assignments'],
                                    $roles,
                                    ['tag' => false]
                                ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
