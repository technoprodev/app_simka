<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/list-keterangan-kehadiran.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <span class="fs-18 text-azure fs-italic">Daftar Lengkap Keterangan Kehadiran</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-keterangan-kehadiran table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>kode</th>
                        <th>keterangan</th>
                        <th>dihitung hadir</th>
                        <th>potong tunkin</th>
                        <th>potongan tunkin persen</th>
                        <th>potong uang makan</th>
                        <th>potong disiplin</th>
                        <th>batas awal unggah</th>
                        <th>batas akhir unggah</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kode..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dihitung hadir..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potong tunkin..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potongan tunkin persen..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potong uang makan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potong disiplin..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search batas awal unggah..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search batas akhir unggah..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>