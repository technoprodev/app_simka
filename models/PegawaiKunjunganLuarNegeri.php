<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_kunjungan_luar_negeri".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $negara_kunjungan
 * @property string $tujuan_kunjungan
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $sumber_biaya
 * @property string $sertifikat
 *
 * @property Pegawai $pegawai0
 * @property Negara $negaraKunjungan
 */
class PegawaiKunjunganLuarNegeri extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;

    public static function tableName()
    {
        return 'pegawai_kunjungan_luar_negeri';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //negara_kunjungan
            [['negara_kunjungan'], 'required'],
            [['negara_kunjungan'], 'string', 'max' => 2],
            [['negara_kunjungan'], 'exist', 'skipOnError' => true, 'targetClass' => Negara::className(), 'targetAttribute' => ['negara_kunjungan' => 'id']],

            //tujuan_kunjungan
            [['tujuan_kunjungan'], 'required'],
            [['tujuan_kunjungan'], 'string', 'max' => 256],

            //tanggal_mulai
            [['tanggal_mulai'], 'required'],
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'required'],
            [['tanggal_selesai'], 'safe'],

            //sumber_biaya
            [['sumber_biaya'], 'required'],
            [['sumber_biaya'], 'string', 'max' => 256],

            //sertifikat
            [['sertifikat'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'negara_kunjungan' => 'Negara Kunjungan',
            'tujuan_kunjungan' => 'Tujuan Kunjungan',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'sumber_biaya' => 'Sumber Biaya',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNegaraKunjungan()
    {
        return $this->hasOne(Negara::className(), ['id' => 'negara_kunjungan']);
    }
}
