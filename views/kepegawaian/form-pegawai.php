<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/form-pegawai.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$pegawaiAnaks = [];
if (isset($model['pegawai_anak']))
    foreach ($model['pegawai_anak'] as $key => $pegawaiAnak)
        $pegawaiAnaks[] = $pegawaiAnak->attributes;
$pegawaiDiklats = [];
if (isset($model['pegawai_diklat']))
    foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat)
        $pegawaiDiklats[] = $pegawaiDiklat->attributes;
$pegawaiHukumanDisiplins = [];
if (isset($model['pegawai_hukuman_disiplin']))
    foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin)
        $pegawaiHukumanDisiplins[] = $pegawaiHukumanDisiplin->attributes;
$pegawaiKenaikanGajiBerkalas = [];
if (isset($model['pegawai_kenaikan_gaji_berkala']))
    foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala)
        $pegawaiKenaikanGajiBerkalas[] = $pegawaiKenaikanGajiBerkala->attributes;
$pegawaiKepangkatans = [];
if (isset($model['pegawai_kepangkatan']))
    foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan)
        $pegawaiKepangkatans[] = $pegawaiKepangkatan->attributes;
$pegawaiKunjunganLuarNegeris = [];
if (isset($model['pegawai_kunjungan_luar_negeri']))
    foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri)
        $pegawaiKunjunganLuarNegeris[] = $pegawaiKunjunganLuarNegeri->attributes;
$pegawaiKursusPelatihans = [];
if (isset($model['pegawai_kursus_pelatihan']))
    foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan)
        $pegawaiKursusPelatihans[] = $pegawaiKursusPelatihan->attributes;
$pegawaiMasaPersiapanPensiuns = [];
if (isset($model['pegawai_masa_persiapan_pensiun']))
    foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun)
        $pegawaiMasaPersiapanPensiuns[] = $pegawaiMasaPersiapanPensiun->attributes;
$pegawaiMutasis = [];
if (isset($model['pegawai_mutasi']))
    foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi)
        $pegawaiMutasis[] = $pegawaiMutasi->attributes;
$pegawaiOrganisasis = [];
if (isset($model['pegawai_organisasi']))
    foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi)
        $pegawaiOrganisasis[] = $pegawaiOrganisasi->attributes;
$pegawaiPelaksanas = [];
if (isset($model['pegawai_pelaksana']))
    foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana)
        $pegawaiPelaksanas[] = $pegawaiPelaksana->attributes;
$pegawaiPemberhentians = [];
if (isset($model['pegawai_pemberhentian']))
    foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian)
        $pegawaiPemberhentians[] = $pegawaiPemberhentian->attributes;
$pegawaiPendidikans = [];
if (isset($model['pegawai_pendidikan']))
    foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan)
        $pegawaiPendidikans[] = $pegawaiPendidikan->attributes;
$pegawaiPenetapanAngkaKredits = [];
if (isset($model['pegawai_penetapan_angka_kredit']))
    foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit)
        $pegawaiPenetapanAngkaKredits[] = $pegawaiPenetapanAngkaKredit->attributes;
$pegawaiPenghargaans = [];
if (isset($model['pegawai_penghargaan']))
    foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan)
        $pegawaiPenghargaans[] = $pegawaiPenghargaan->attributes;
$pegawaiSaudaraKandungs = [];
if (isset($model['pegawai_saudara_kandung']))
    foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung)
        $pegawaiSaudaraKandungs[] = $pegawaiSaudaraKandung->attributes;

$this->registerJs(
    // 'vm.$data.pegawai.virtual_category = ' . json_encode($model['pegawai']->virtual_category) . ';' .
    'vm.$data.pegawai.pegawaiAnaks = vm.$data.pegawai.pegawaiAnaks.concat(' . json_encode($pegawaiAnaks) . ');' .
    'vm.$data.pegawai.pegawaiDiklats = vm.$data.pegawai.pegawaiDiklats.concat(' . json_encode($pegawaiDiklats) . ');' .
    'vm.$data.pegawai.pegawaiHukumanDisiplins = vm.$data.pegawai.pegawaiHukumanDisiplins.concat(' . json_encode($pegawaiHukumanDisiplins) . ');' .
    'vm.$data.pegawai.pegawaiKenaikanGajiBerkalas = vm.$data.pegawai.pegawaiKenaikanGajiBerkalas.concat(' . json_encode($pegawaiKenaikanGajiBerkalas) . ');' .
    'vm.$data.pegawai.pegawaiKepangkatans = vm.$data.pegawai.pegawaiKepangkatans.concat(' . json_encode($pegawaiKepangkatans) . ');' .
    'vm.$data.pegawai.pegawaiKunjunganLuarNegeris = vm.$data.pegawai.pegawaiKunjunganLuarNegeris.concat(' . json_encode($pegawaiKunjunganLuarNegeris) . ');' .
    'vm.$data.pegawai.pegawaiKursusPelatihans = vm.$data.pegawai.pegawaiKursusPelatihans.concat(' . json_encode($pegawaiKursusPelatihans) . ');' .
    'vm.$data.pegawai.pegawaiMasaPersiapanPensiuns = vm.$data.pegawai.pegawaiMasaPersiapanPensiuns.concat(' . json_encode($pegawaiMasaPersiapanPensiuns) . ');' .
    'vm.$data.pegawai.pegawaiMutasis = vm.$data.pegawai.pegawaiMutasis.concat(' . json_encode($pegawaiMutasis) . ');' .
    'vm.$data.pegawai.pegawaiOrganisasis = vm.$data.pegawai.pegawaiOrganisasis.concat(' . json_encode($pegawaiOrganisasis) . ');' .
    'vm.$data.pegawai.pegawaiPelaksanas = vm.$data.pegawai.pegawaiPelaksanas.concat(' . json_encode($pegawaiPelaksanas) . ');' .
    'vm.$data.pegawai.pegawaiPemberhentians = vm.$data.pegawai.pegawaiPemberhentians.concat(' . json_encode($pegawaiPemberhentians) . ');' .
    'vm.$data.pegawai.pegawaiPendidikans = vm.$data.pegawai.pegawaiPendidikans.concat(' . json_encode($pegawaiPendidikans) . ');' .
    'vm.$data.pegawai.pegawaiPenetapanAngkaKredits = vm.$data.pegawai.pegawaiPenetapanAngkaKredits.concat(' . json_encode($pegawaiPenetapanAngkaKredits) . ');' .
    'vm.$data.pegawai.pegawaiPenghargaans = vm.$data.pegawai.pegawaiPenghargaans.concat(' . json_encode($pegawaiPenghargaans) . ');' .
    'vm.$data.pegawai.pegawaiSaudaraKandungs = vm.$data.pegawai.pegawaiSaudaraKandungs.concat(' . json_encode($pegawaiSaudaraKandungs) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['pegawai']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pegawai'], ['class' => '']);
}

if (isset($model['pegawai_anak'])) foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
    if ($pegawaiAnak->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiAnak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_diklat'])) foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
    if ($pegawaiDiklat->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiDiklat, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_hukuman_disiplin'])) foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
    if ($pegawaiHukumanDisiplin->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiHukumanDisiplin, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kenaikan_gaji_berkala'])) foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
    if ($pegawaiKenaikanGajiBerkala->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKenaikanGajiBerkala, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kepangkatan'])) foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
    if ($pegawaiKepangkatan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKepangkatan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kunjungan_luar_negeri'])) foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
    if ($pegawaiKunjunganLuarNegeri->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKunjunganLuarNegeri, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kursus_pelatihan'])) foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
    if ($pegawaiKursusPelatihan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKursusPelatihan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_masa_persiapan_pensiun'])) foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
    if ($pegawaiMasaPersiapanPensiun->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMasaPersiapanPensiun, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_mutasi'])) foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
    if ($pegawaiMutasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMutasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_organisasi'])) foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
    if ($pegawaiOrganisasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiOrganisasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pelaksana'])) foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
    if ($pegawaiPelaksana->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPelaksana, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pemberhentian'])) foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
    if ($pegawaiPemberhentian->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPemberhentian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pendidikan'])) foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
    if ($pegawaiPendidikan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penetapan_angka_kredit'])) foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
    if ($pegawaiPenetapanAngkaKredit->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenetapanAngkaKredit, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penghargaan'])) foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
    if ($pegawaiPenghargaan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenghargaan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_saudara_kandung'])) foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
    if ($pegawaiSaudaraKandung->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiSaudaraKandung, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}

$pilihanPekerjaan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT p.id, p.pekerjaan FROM pekerjaan p
            order by pekerjaan
        ", []
    )->queryAll(), 'id', 'pekerjaan'
);
$pilihanAgama = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT a.id, a.agama FROM agama a
            order by agama
        ", []
    )->queryAll(), 'id', 'agama'
);
$pilihanEselon = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT e.id, e.eselon FROM eselon e
            order by eselon
        ", []
    )->queryAll(), 'id', 'eselon'
);
$pilihanGrade = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT g.id, g.grade FROM grade g
            order by grade
        ", []
    )->queryAll(), 'id', 'grade'
);
$pilihanKedudukanPegawai = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kp.id, kp.kedudukan_pegawai FROM kedudukan_pegawai kp
            order by kedudukan_pegawai
        ", []
    )->queryAll(), 'id', 'kedudukan_pegawai'
);
$pilihanKepangkatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT k.id, CONCAT(k.pangkat, ' (', k.golongan, ' / ', k.ruang, ')') AS kepangkatan FROM kepangkatan k
            order by kepangkatan
        ", []
    )->queryAll(), 'id', 'kepangkatan'
);
$pilihanStatusKepegawaian = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sk.id, sk.status_kepegawaian FROM status_kepegawaian sk
            order by status_kepegawaian
        ", []
    )->queryAll(), 'id', 'status_kepegawaian'
);
$pilihanJenisSk = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT js.id, js.jenis_sk FROM jenis_sk js
            order by jenis_sk
        ", []
    )->queryAll(), 'id', 'jenis_sk'
);
$pilihanJenjangJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, CONCAT(jjj.jenis_jabatan, ' - ', jj.jenjang_jabatan) AS jenjang_jabatan FROM jenjang_jabatan jj
            JOIN jenis_jabatan jjj ON jjj.id = jj.jenis_jabatan
            order by jenjang_jabatan
        ", []
    )->queryAll(), 'id', 'jenjang_jabatan'
);
$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanUnitKerjaAdministrasi = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanStatusPernikahan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sm.id, sm.status_marital FROM status_marital sm
            order by status_marital
        ", []
    )->queryAll(), 'id', 'status_marital'
);
$pilihanPeriodeOrganisasi = (new \app_simka\models\PegawaiOrganisasi())->getEnum('periode_organisasi');
$pilihanStatusHidupAyah = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ayah');
$pilihanStatusHidupIbu = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ibu');
$pilihanStatusHidupPasangan = (new \app_simka\models\Pegawai())->getEnum('status_hidup_pasangan');
$pilihanGolonganDarah = (new \app_simka\models\Pegawai())->getEnum('golongan_darah');
$pilihanJenisKelamin = (new \app_simka\models\Pegawai())->getEnum('jenis_kelamin');
$pilihanJenisPelaksana = (new \app_simka\models\PegawaiPelaksana())->getEnum('jenis_pelaksana');
$pilihanStatusAnak = (new \app_simka\models\PegawaiAnak())->getEnum('status_anak');
$pilihanStatusTunjangan = (new \app_simka\models\PegawaiAnak())->getEnum('status_tunjangan');
$pilihanJenjangPendidikan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jp.id, jp.jenjang_pendidikan FROM jenjang_pendidikan jp
        ", []
    )->queryAll(), 'id', 'jenjang_pendidikan'
);
$pilihanJenisDiklat = (new \app_simka\models\PegawaiDiklat())->getEnum('jenis_diklat');
$pilihanNegara = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT n.id, n.nama FROM negara n
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);
$pilihanModelRambut = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, model_rambut FROM model_rambut
            order by model_rambut
        ", []
    )->queryAll(), 'id', 'model_rambut'
);
$pilihanBentukWajah = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, bentuk_wajah FROM bentuk_wajah
            order by bentuk_wajah
        ", []
    )->queryAll(), 'id', 'bentuk_wajah'
);
$pilihanWarnaKulit = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, warna_kulit FROM warna_kulit
            order by warna_kulit
        ", []
    )->queryAll(), 'id', 'warna_kulit'
);
$pilihanMendapatTunkin = (new \app_simka\models\Pegawai())->getEnum('mendapat_tunkin');
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-2 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="text-center">
            <?php if ($model['pegawai']->poto) : ?>
                <img src="<?= $model['pegawai']->virtual_poto_download ?>" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Laki-laki') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/male.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Perempuan') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/female.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php else : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/avatar.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php endif; ?>
        </div>
        <div class="margin-top-5"></div>
        <div class="text-center">
            <span class="text-azure fs-19 fw-bold"><?= $model['pegawai']->nama ? $model['pegawai']->nama : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">NIP</span><br>
            <span class="text-dark fs-14"><?= $model['pegawai']->nip ? $model['pegawai']->nip : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Telpon</span><br>
            <span class="text-dark"><?= $model['pegawai']->telpon ? $model['pegawai']->telpon : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Kantor</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_kantor ? $model['pegawai']->email_kantor : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Pribadi</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_pribadi ? $model['pegawai']->email_pribadi : '(belum diisi)' ?></span>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Personal
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama ? $model['pegawai']->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Panggilan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_panggilan ? $model['pegawai']->nama_panggilan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenis Kelamin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenis_kelamin ? $model['pegawai']->jenis_kelamin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">TTL</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->tempat_lahir ? $model['pegawai']->tempat_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>, <?= $model['pegawai']->tanggal_lahir ? $model['pegawai']->tanggal_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Agama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->agama ? $model['pegawai']->agama0->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Status Pernikahan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->status_marital ? $model['pegawai']->statusMarital->status_marital : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Golongan Darah</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->golongan_darah ? $model['pegawai']->golongan_darah : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <!--  -->
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Pendidikan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenjang_pendidikan_terakhir ? $model['pegawai']->jenjangPendidikanTerakhir->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Gelar</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= !$model['pegawai']->gelar_depan && !$model['pegawai']->gelar_belakang ? '-' : '' ?>
                        <?= $model['pegawai']->gelar_depan ? $model['pegawai']->gelar_depan : '' ?>
                        <?= $model['pegawai']->gelar_depan && $model['pegawai']->gelar_belakang ? ' & ' : '' ?>
                        <?= $model['pegawai']->gelar_belakang ? $model['pegawai']->gelar_belakang : '' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Tinggi / Berat</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->tinggi_badan_cm ? $model['pegawai']->tinggi_badan_cm . ' cm' : '<span class="text-grayer">(belum diisi)</span>' ?> / <?= $model['pegawai']->berat_badan_kg ? $model['pegawai']->berat_badan_kg . ' kg' : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Model Rambut</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->model_rambut ? $model['pegawai']->modelRambut->model_rambut : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Bentuk Wajah</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->bentuk_wajah ? $model['pegawai']->bentukWajah->bentuk_wajah : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Warna Kulit</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->warna_kulit ? $model['pegawai']->warnaKulit->warna_kulit : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Hobi</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->hobi ? $model['pegawai']->hobi : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kemampuan Berbahasa</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kemampuan_berbahasa ? $model['pegawai']->kemampuan_berbahasa : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Pegawai
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NIP</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nip ? $model['pegawai']->nip : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Pin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->pin ? $model['pegawai']->pin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kepangkatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenjang Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php
                $bottomToTop = [];
                $uk = $model['pegawai']->unitKerja;
                $i = 0;
                while ($uk && $i <= 10) {
                    $bottomToTop[] = [
                        'jenisUnitKerja' => $uk->jenis_unit_kerja ? $uk->jenisUnitKerja->jenis_unit_kerja : '',
                        'unitKerja' => $uk->unit_kerja,
                    ];
                    $uk = $uk->parent0;
                    $i++;
                }

                $topToBottom = [];
                for ($i = count($bottomToTop)-1; $i >= 0; $i--) { 
                    $topToBottom[] = $bottomToTop[$i];
                }
            ?>
            <?php foreach ($topToBottom as $key => $value) : ?>
            <?php if ($value['jenisUnitKerja'] != 'Staf') :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer"><?= $value['jenisUnitKerja'] ?></div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $value['unitKerja'] ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php if (!$topToBottom) :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Satuan Organisasi</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <span class="text-grayer">(belum diisi)</span>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Eselon</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->eselon ? $model['pegawai']->eselon0->eselon : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Grade</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->grade ? $model['pegawai']->grade0->grade : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kedudukan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kedudukan_pegawai ? $model['pegawai']->kedudukanPegawai->kedudukan_pegawai : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Mendapat Tunkin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->mendapat_tunkin ? $model['pegawai']->mendapat_tunkin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <!--  -->
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NIK</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nik ? $model['pegawai']->nik : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NPWP</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->npwp ? $model['pegawai']->npwp : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Karpeg</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->karpeg ? $model['pegawai']->karpeg : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Karis / Karsu</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->karis_atau_karsu ? $model['pegawai']->karis_atau_karsu : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Taspen</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->taspen ? $model['pegawai']->taspen : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="tab-wrapper">
            <ul class="tab-menu">
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'personal']) ?>" class="<?= $tab == 'personal' ? 'active' : '' ?>">Data Personal</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'pegawai']) ?>" class="<?= $tab == 'pegawai' ? 'active' : '' ?>">Data Pegawai</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'jabatan']) ?>" class="<?= $tab == 'jabatan' ? 'active' : '' ?>">Jabatan</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'kepangkatan']) ?>" class="<?= $tab == 'kepangkatan' ? 'active' : '' ?>">Kepangkatan</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'kenaikan-gaji-berkala']) ?>" class="<?= $tab == 'kenaikan-gaji-berkala' ? 'active' : '' ?>">Kenaikan Gaji Berkala</a></li>
                <!-- <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'pelaksana']) ?>" class="<?= $tab == 'pelaksana' ? 'active' : '' ?>">Pelaksana</a></li> -->
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'penetapan-angka-kredit']) ?>" class="<?= $tab == 'penetapan-angka-kredit' ? 'active' : '' ?>">Penetapan Angka Kredit</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'orang-tua']) ?>" class="<?= $tab == 'orang-tua' ? 'active' : '' ?>">Orang Tua</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'saudara-kandung']) ?>" class="<?= $tab == 'saudara-kandung' ? 'active' : '' ?>">Saudara Kandung</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'pasangan']) ?>" class="<?= $tab == 'pasangan' ? 'active' : '' ?>">Suami / Istri</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'anak']) ?>" class="<?= $tab == 'anak' ? 'active' : '' ?>">Anak</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'pendidikan']) ?>" class="<?= $tab == 'pendidikan' ? 'active' : '' ?>">Pendidikan</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'penghargaan']) ?>" class="<?= $tab == 'penghargaan' ? 'active' : '' ?>">Penghargaan</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'diklat']) ?>" class="<?= $tab == 'diklat' ? 'active' : '' ?>">Diklat</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'kursus-pelatihan']) ?>" class="<?= $tab == 'kursus-pelatihan' ? 'active' : '' ?>">Kursus Pelatihan</a></li>
                <!-- <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'kunjungan-luar-negeri']) ?>" class="<?= $tab == 'kunjungan-luar-negeri' ? 'active' : '' ?>">Kunjungan Luar Negeri</a></li> -->
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'organisasi']) ?>" class="<?= $tab == 'organisasi' ? 'active' : '' ?>">Organisasi</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'hukuman-disiplin']) ?>" class="<?= $tab == 'hukuman-disiplin' ? 'active' : '' ?>">Hukuman Disiplin</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'pemberhentian']) ?>" class="<?= $tab == 'pemberhentian' ? 'active' : '' ?>">Pemberhentian</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'masa-persiapan-pensiun']) ?>" class="<?= $tab == 'masa-persiapan-pensiun' ? 'active' : '' ?>">Masa Persiapan Pensiun</a></li>
                <li><a href="<?= Yii::$app->urlManager->createUrl(['kepegawaian/pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'sisa-cuti']) ?>" class="<?= $tab == 'sisa-cuti' ? 'active' : '' ?>">Sisa Cuti</a></li>
            </ul>
            <?php if ($tab == 'personal') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Data Personal
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'nama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama')->end(); ?>

                                <?= $form->field($model['pegawai'], 'nama_panggilan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_panggilan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_panggilan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_panggilan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_panggilan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenis_kelamin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenis_kelamin', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenis_kelamin', $pilihanJenisKelamin, ['prompt' => '- pilih jenis kelamin -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenis_kelamin', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenis_kelamin')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tempat_lahir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tempat_lahir', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tempat_lahir', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'tempat_lahir', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tempat_lahir')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tanggal_lahir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tanggal_lahir', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tanggal_lahir', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 18/12/1992']); ?>
                                        <?= Html::error($model['pegawai'], 'tanggal_lahir', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tanggal_lahir')->end(); ?>

                                <?= $form->field($model['pegawai'], 'agama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'agama', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'agama', $pilihanAgama, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'agama', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'agama')->end(); ?>

                                <?= $form->field($model['pegawai'], 'status_marital', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'status_marital', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'status_marital', $pilihanStatusPernikahan, ['prompt' => '- pilih status pernikahan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'status_marital', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'status_marital')->end(); ?>

                                <?= $form->field($model['pegawai'], 'golongan_darah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'golongan_darah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'golongan_darah', $pilihanGolonganDarah, ['prompt' => '- pilih golongan darah -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'golongan_darah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'golongan_darah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_pendidikan_terakhir', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_pendidikan_terakhir', $pilihanJenjangPendidikan, ['prompt' => '- pilih jenjang pendidikan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_pendidikan_terakhir', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir')->end(); ?>

                                <?= $form->field($model['pegawai'], 'gelar_depan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'gelar_depan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'gelar_depan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'gelar_depan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'gelar_depan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'gelar_belakang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'gelar_belakang', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'gelar_belakang', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'gelar_belakang', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'gelar_belakang')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tinggi_badan_cm', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'tinggi_badan_cm', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tinggi_badan_cm')->end(); ?>

                                <?= $form->field($model['pegawai'], 'berat_badan_kg', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'berat_badan_kg', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'berat_badan_kg', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'berat_badan_kg', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'berat_badan_kg')->end(); ?>

                                <?= $form->field($model['pegawai'], 'model_rambut', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'model_rambut', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'model_rambut', $pilihanModelRambut, ['prompt' => '- pilih Model Rambut -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'model_rambut', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'model_rambut')->end(); ?>

                                <?= $form->field($model['pegawai'], 'bentuk_wajah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'bentuk_wajah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'bentuk_wajah', $pilihanBentukWajah, ['prompt' => '- pilih Bentuk Wajah -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'bentuk_wajah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'bentuk_wajah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'warna_kulit', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'warna_kulit', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'warna_kulit', $pilihanWarnaKulit, ['prompt' => '- pilih warna kulit -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'warna_kulit', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'warna_kulit')->end(); ?>

                                <?= $form->field($model['pegawai'], 'hobi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'hobi', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'hobi', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'hobi', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'hobi')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kemampuan_berbahasa', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'kemampuan_berbahasa', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kemampuan_berbahasa')->end(); ?>

                                <?= $form->field($model['pegawai'], 'poto', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'poto', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeFileInput($model['pegawai'], 'virtual_poto_upload'); ?>
                                        <?= Html::error($model['pegawai'], 'poto', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'poto')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'pegawai') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Data Pegawai
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'nip', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nip', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nip', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nip', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nip')->end(); ?>

                                <?= $form->field($model['pegawai'], 'pin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'pin', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'pin', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'pin', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'pin')->end(); ?>

                                <?= $form->field($model['pegawai'], 'status_kepegawaian', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'status_kepegawaian', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'status_kepegawaian', $pilihanStatusKepegawaian, ['prompt' => '- pilih status kepegawaian -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'status_kepegawaian', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'status_kepegawaian')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kepangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kepangkatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'kepangkatan', $pilihanKepangkatan, ['prompt' => '- pilih kepangkatan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'kepangkatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kepangkatan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenjang_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_jabatan', $pilihanJenjangJabatan, ['prompt' => '- pilih jenjang jabatan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_jabatan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'nama_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_jabatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_jabatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_jabatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_jabatan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'unit_kerja', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'unit_kerja', $pilihanUnitKerja, ['prompt' => '- pilih unit kerja -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'unit_kerja', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'unit_kerja')->end(); ?>

                                <?= $form->field($model['pegawai'], 'unit_kerja_administrasi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'unit_kerja_administrasi', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'unit_kerja_administrasi', $pilihanUnitKerjaAdministrasi, ['prompt' => '- pilih unit kerja administrasi -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'unit_kerja_administrasi', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'unit_kerja_administrasi')->end(); ?>

                                <?= $form->field($model['pegawai'], 'eselon', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'eselon', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'eselon', $pilihanEselon, ['prompt' => '- pilih eselon -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'eselon', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'eselon')->end(); ?>

                                <?= $form->field($model['pegawai'], 'grade', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'grade', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'grade', $pilihanGrade, ['prompt' => '- pilih grade -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'grade', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'grade')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kedudukan_pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kedudukan_pegawai', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'kedudukan_pegawai', $pilihanKedudukanPegawai, ['prompt' => '- pilih kedudukan pegawai -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'kedudukan_pegawai', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kedudukan_pegawai')->end(); ?>

                                <?= $form->field($model['pegawai'], 'mendapat_tunkin', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'mendapat_tunkin', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'mendapat_tunkin', $pilihanMendapatTunkin, ['prompt' => '- pilih mendapat tunkin -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'mendapat_tunkin', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'mendapat_tunkin')->end(); ?>

                                <?= $form->field($model['pegawai'], 'nik', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nik', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nik', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nik', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nik')->end(); ?>

                                <?= $form->field($model['pegawai'], 'npwp', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'npwp', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'npwp', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'npwp', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'npwp')->end(); ?>

                                <?= $form->field($model['pegawai'], 'telpon', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'telpon', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'telpon', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'telpon', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'telpon')->end(); ?>

                                <?= $form->field($model['pegawai'], 'email_kantor', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'email_kantor', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'email_kantor', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'email_kantor', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'email_kantor')->end(); ?>

                                <?= $form->field($model['pegawai'], 'email_pribadi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'email_pribadi', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'email_pribadi', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'email_pribadi', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'email_pribadi')->end(); ?>

                                <?= $form->field($model['pegawai'], 'karpeg', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'karpeg', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'karpeg', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'karpeg', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'karpeg')->end(); ?>

                                <?= $form->field($model['pegawai'], 'karis_atau_karsu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'karis_atau_karsu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'karis_atau_karsu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'taspen', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'taspen', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'taspen', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'taspen', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'taspen')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'jabatan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Jabatan Terakhir
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'jenjang_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_jabatan', $pilihanJenjangJabatan, ['prompt' => '- pilih jenjang jabatan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_jabatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_jabatan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'nama_jabatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_jabatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_jabatan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_jabatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_jabatan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'unit_kerja', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'unit_kerja', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'unit_kerja', $pilihanUnitKerja, ['prompt' => '- pilih unit kerja -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'unit_kerja', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'unit_kerja')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Jabatan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_mutasi'])) foreach ($model['pegawai_mutasi'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]instansi")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]instansi")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]unit_kerja")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]unit_kerja")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]jenjang_jabatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]jenjang_jabatan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]nama_jabatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]nama_jabatan")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_sampai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_sampai_tanggal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_sampai_sekarang")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]terhitung_sampai_sekarang")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]catatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]catatan")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]jenis_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]nomor_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_mutasi'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiMutasis == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiMutasis">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaimutasi-' + key + '-id'" v-bind:name="'PegawaiMutasi[' + key + '][id]'" type="text" v-model="pegawai.pegawaiMutasis[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-instansi'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-instansi'" class="form-label">Instansi</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-instansi'" v-bind:name="'PegawaiMutasi[' + key + '][instansi]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].instansi">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-unit_kerja'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-unit_kerja'" class="form-label">Unit Kerja</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-unit_kerja'" v-bind:name="'PegawaiMutasi[' + key + '][unit_kerja]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].unit_kerja">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-jenjang_jabatan'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-jenjang_jabatan'" class="form-label">Jenjang Jabatan</label>
                                                        <select v-bind:id="'pegawaimutasi-' + key + '-jenjang_jabatan'" v-bind:name="'PegawaiMutasi[' + key + '][jenjang_jabatan]'" class="form-dropdown" v-model="pegawai.pegawaiMutasis[key].jenjang_jabatan">
                                                            <option value="">- pilih jenjang jabatan -</option>
                                                            <?php foreach ($pilihanJenjangJabatan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-nama_jabatan'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-nama_jabatan'" class="form-label">Nama Jabatan</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-nama_jabatan'" v-bind:name="'PegawaiMutasi[' + key + '][nama_jabatan]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].nama_jabatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-terhitung_mulai_tanggal'" class="form-label">TMT</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiMutasi[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].terhitung_mulai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-terhitung_sampai_tanggal'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-terhitung_sampai_tanggal'" class="form-label">Sampai Tanggal</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-terhitung_sampai_tanggal'" v-bind:name="'PegawaiMutasi[' + key + '][terhitung_sampai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].terhitung_sampai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-catatan'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-catatan'" class="form-label">Catatan</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-catatan'" v-bind:name="'PegawaiMutasi[' + key + '][catatan]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].catatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-jenis_sk'" class="form-label">Jenis SK</label>
                                                        <select v-bind:id="'pegawaimutasi-' + key + '-jenis_sk'" v-bind:name="'PegawaiMutasi[' + key + '][jenis_sk]'" class="form-dropdown" v-model="pegawai.pegawaiMutasis[key].jenis_sk">
                                                            <option value="">- pilih jenis sk -</option>
                                                            <?php foreach ($pilihanJenisSk as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-nomor_sk'" class="form-label">Nomor SK</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-nomor_sk'" v-bind:name="'PegawaiMutasi[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-tanggal_sk'" class="form-label">Tanggal SK</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-tanggal_sk'" v-bind:name="'PegawaiMutasi[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].tanggal_sk" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap SK</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiMutasi[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMutasis[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div v-bind:class="'form-wrapper field-pegawaimutasi-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaimutasi-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip SK</label>
                                                        <input v-bind:id="'pegawaimutasi-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiMutasi[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiMutasis[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeMutasi(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addMutasi" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Jabatan</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'kepangkatan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Kepangkatan Terakhir
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'kepangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kepangkatan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'kepangkatan', $pilihanKepangkatan, ['prompt' => '- pilih kepangkatan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'kepangkatan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kepangkatan')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Kepangkatan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_kepangkatan'])) foreach ($model['pegawai_kepangkatan'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]kepangkatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]kepangkatan")->end(); ?>

                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]status_kepegawaian")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]status_kepegawaian")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_sampai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_sampai_tanggal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_sampai_sekarang")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]terhitung_sampai_sekarang")->end(); ?>

                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]jenis_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]nomor_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_kepangkatan'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiKepangkatans == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiKepangkatans">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaikepangkatan-' + key + '-id'" v-bind:name="'PegawaiKepangkatan[' + key + '][id]'" type="text" v-model="pegawai.pegawaiKepangkatans[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-4">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-kepangkatan'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-kepangkatan'" class="form-label">Kepangkatan</label>
                                                        <select v-bind:id="'pegawaikepangkatan-' + key + '-kepangkatan'" v-bind:name="'PegawaiKepangkatan[' + key + '][kepangkatan]'" class="form-dropdown" v-model="pegawai.pegawaiKepangkatans[key].kepangkatan">
                                                            <option value="">- pilih kepangkatan -</option>
                                                            <?php foreach ($pilihanKepangkatan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-4">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-status_kepegawian'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-status_kepegawian'" class="form-label">Status</label>
                                                        <select v-bind:id="'pegawaikepangkatan-' + key + '-status_kepegawian'" v-bind:name="'PegawaiKepangkatan[' + key + '][status_kepegawaian]'" class="form-dropdown" v-model="pegawai.pegawaiKepangkatans[key].status_kepegawaian">
                                                            <option value="">- pilih status kepegawaian -</option>
                                                            <?php foreach ($pilihanStatusKepegawaian as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-terhitung_mulai_tanggal'" class="form-label">TMT</label>
                                                        <input v-bind:id="'pegawaikepangkatan-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiKepangkatan[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiKepangkatans[key].terhitung_mulai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-terhitung_sampai_tanggal'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-terhitung_sampai_tanggal'" class="form-label">Sampai Tanggal</label>
                                                        <input v-bind:id="'pegawaikepangkatan-' + key + '-terhitung_sampai_tanggal'" v-bind:name="'PegawaiKepangkatan[' + key + '][terhitung_sampai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiKepangkatans[key].terhitung_sampai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-jenis_sk'" class="form-label">Jenis SK</label>
                                                        <select v-bind:id="'pegawaikepangkatan-' + key + '-jenis_sk'" v-bind:name="'PegawaiKepangkatan[' + key + '][jenis_sk]'" class="form-dropdown" v-model="pegawai.pegawaiKepangkatans[key].jenis_sk">
                                                            <option value="">- pilih jenis sk -</option>
                                                            <?php foreach ($pilihanJenisSk as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-nomor_sk'" class="form-label">Nomor SK</label>
                                                        <input v-bind:id="'pegawaikepangkatan-' + key + '-nomor_sk'" v-bind:name="'PegawaiKepangkatan[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKepangkatans[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-tanggal_sk'" class="form-label">Tanggal SK</label>
                                                        <input v-bind:id="'pegawaikepangkatan-' + key + '-tanggal_sk'" v-bind:name="'PegawaiKepangkatan[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKepangkatans[key].tanggal_sk" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap SK</label>
                                                        <input v-bind:id="'pegawaikepangkatan-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiKepangkatan[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKepangkatans[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikepangkatan-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaikepangkatan-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip SK</label>
                                                         <input v-bind:id="'pegawaikepangkatan-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiKepangkatan[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiKepangkatans[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeKepangkatan(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addKepangkatan" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Kepangkatan</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'kenaikan-gaji-berkala') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Kenaikan Gaji Berkala
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_kenaikan_gaji_berkala'])) foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]gaji_pokok_lama")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]gaji_pokok_lama")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]gaji_pokok_baru")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]gaji_pokok_baru")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_sampai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_sampai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_sampai_sekarang")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]terhitung_sampai_sekarang")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]kepangkatan_saat_kenaikan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]kepangkatan_saat_kenaikan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]catatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]catatan")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]jenis_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]nomor_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_kenaikan_gaji_berkala'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiKenaikanGajiBerkalas == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiKenaikanGajiBerkalas">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaikenaikangajiberkala-' + key + '-id'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][id]'" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-gaji_pokok_lama'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-gaji_pokok_lama'" class="form-label">Gapok Lama</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-gaji_pokok_lama'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][gaji_pokok_lama]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].gaji_pokok_lama">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-gaji_pokok_baru'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-gaji_pokok_baru'" class="form-label">Gapok Baru</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-gaji_pokok_baru'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][gaji_pokok_baru]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].gaji_pokok_baru">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-terhitung_mulai_tanggal'" class="form-label">TMT</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].terhitung_mulai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-terhitung_sampai_tanggal'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-terhitung_sampai_tanggal'" class="form-label">Sampai Tanggal</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-terhitung_sampai_tanggal'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][terhitung_sampai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].terhitung_sampai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-kepangkatan_saat_kenaikan'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-kepangkatan_saat_kenaikan'" class="form-label">Pangkat Saat Kenaikan</label>
                                                        <select v-bind:id="'pegawaikenaikangajiberkala-' + key + '-kepangkatan_saat_kenaikan'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][kepangkatan_saat_kenaikan]'" class="form-dropdown" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].kepangkatan_saat_kenaikan">
                                                            <option value="">- pilih kepangkatan -</option>
                                                            <?php foreach ($pilihanKepangkatan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-catatan'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-catatan'" class="form-label">Catatan</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-catatan'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][catatan]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].catatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-jenis_sk'" class="form-label">Jenis SK</label>
                                                        <select v-bind:id="'pegawaikenaikangajiberkala-' + key + '-jenis_sk'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][jenis_sk]'" class="form-dropdown" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].jenis_sk">
                                                            <option value="">- pilih jenis sk -</option>
                                                            <?php foreach ($pilihanJenisSk as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-nomor_sk'" class="form-label">Nomor SK</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-nomor_sk'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-tanggal_sk'" class="form-label">Tanggal SK</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-tanggal_sk'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].tanggal_sk" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap SK</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikenaikangajiberkala-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaikenaikangajiberkala-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip SK</label>
                                                        <input v-bind:id="'pegawaikenaikangajiberkala-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiKenaikanGajiBerkala[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiKenaikanGajiBerkalas[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeKenaikanGajiBerkala(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addKenaikanGajiBerkala" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Kenaikan Gaji Berkala</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'pelaksana' && false) : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Pelaksana
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_pelaksana'])) foreach ($model['pegawai_pelaksana'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenis_pelaksana")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenis_pelaksana")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]instansi")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]instansi")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]unit_kerja")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]unit_kerja")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenjang_jabatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenjang_jabatan")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]nama_jabatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]nama_jabatan")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]terhitung_sampai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]terhitung_sampai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]catatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]catatan")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]jenis_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]nomor_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]arsip_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pelaksana'][$key], "[$key]arsip_sk")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiPelaksanas == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiPelaksanas">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaipelaksana-' + key + '-id'" v-bind:name="'PegawaiPelaksana[' + key + '][id]'" type="text" v-model="pegawai.pegawaiPelaksanas[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-jenis_pelaksana'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-jenis_pelaksana'" class="form-label">Jenis Pelaksana</label>
                                                        <select v-bind:id="'pegawaipelaksana-' + key + '-jenis_pelaksana'" v-bind:name="'PegawaiPelaksana[' + key + '][jenis_pelaksana]'" class="form-dropdown" v-model="pegawai.pegawaiPelaksanas[key].jenis_pelaksana">
                                                            <option value="">- pilih jenis pelaksana -</option>
                                                            <?php foreach ($pilihanJenisPelaksana as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-instansi'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-instansi'" class="form-label">Instansi</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-instansi'" v-bind:name="'PegawaiPelaksana[' + key + '][instansi]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].instansi">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-unit_kerja'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-unit_kerja'" class="form-label">Unit Kerja</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-unit_kerja'" v-bind:name="'PegawaiPelaksana[' + key + '][unit_kerja]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].unit_kerja">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-jenjang_jabatan'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-jenjang_jabatan'" class="form-label">Jenjang Jabatan</label>
                                                        <select v-bind:id="'pegawaipelaksana-' + key + '-jenjang_jabatan'" v-bind:name="'PegawaiPelaksana[' + key + '][jenjang_jabatan]'" class="form-dropdown" v-model="pegawai.pegawaiPelaksanas[key].jenjang_jabatan">
                                                            <option value="">- pilih jenjang jabatan -</option>
                                                            <?php foreach ($pilihanJenjangJabatan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-nama_jabatan'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-nama_jabatan'" class="form-label">Nama Jabatan</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-nama_jabatan'" v-bind:name="'PegawaiPelaksana[' + key + '][nama_jabatan]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].nama_jabatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-terhitung_mulai_tanggal'" class="form-label">TMT</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiPelaksana[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].terhitung_mulai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-terhitung_sampai_tanggal'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-terhitung_sampai_tanggal'" class="form-label">Sampai Tanggal</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-terhitung_sampai_tanggal'" v-bind:name="'PegawaiPelaksana[' + key + '][terhitung_sampai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].terhitung_sampai_tanggal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-catatan'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-catatan'" class="form-label">Catatan</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-catatan'" v-bind:name="'PegawaiPelaksana[' + key + '][catatan]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].catatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-jenis_sk'" class="form-label">Jenis SK</label>
                                                        <select v-bind:id="'pegawaipelaksana-' + key + '-jenis_sk'" v-bind:name="'PegawaiPelaksana[' + key + '][jenis_sk]'" class="form-dropdown" v-model="pegawai.pegawaiPelaksanas[key].jenis_sk">
                                                            <option value="">- pilih jenis sk -</option>
                                                            <?php foreach ($pilihanJenisSk as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-nomor_sk'" class="form-label">Nomor SK</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-nomor_sk'" v-bind:name="'PegawaiPelaksana[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-tanggal_sk'" class="form-label">Tanggal SK</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-tanggal_sk'" v-bind:name="'PegawaiPelaksana[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].tanggal_sk" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap SK</label>
                                                        <input v-bind:id="'pegawaipelaksana-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiPelaksana[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipelaksana-' + key + '-arsip_sk'">
                                                        <label v-bind:for="'pegawaipelaksana-' + key + '-arsip_sk'" class="form-label">Arsip SK</label>
                                                        <!-- <input v-bind:id="'pegawaipelaksana-' + key + '-arsip_sk'" v-bind:name="'PegawaiPelaksana[' + key + '][arsip_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPelaksanas[key].arsip_sk"> -->
                                                        <input type="file" class="form-file">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removePelaksana(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addPelaksana" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Pelaksana</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'penetapan-angka-kredit') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Penetapan Angka Kredit
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_penetapan_angka_kredit'])) foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]periode_penilaian_awal")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]periode_penilaian_awal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]periode_penilaian_akhir")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]periode_penilaian_akhir")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]unsur_utama")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]unsur_utama")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]unsur_penunjang")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]unsur_penunjang")->end(); ?>

                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]total_angkat_kredit")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]total_angkat_kredit")->end(); ?>

                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]virtual_arsip_pak_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_penetapan_angka_kredit'][$key], "[$key]virtual_arsip_pak_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiPenetapanAngkaKredits == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiPenetapanAngkaKredits">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaipenetapanangkakredit-' + key + '-id'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][id]'" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-periode_penilaian_awal'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-periode_penilaian_awal'" class="form-label">Awal Penilaian</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-periode_penilaian_awal'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][periode_penilaian_awal]'" class="form-text" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].periode_penilaian_awal" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-periode_penilaian_akhir'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-periode_penilaian_akhir'" class="form-label">Akhir Penilaian</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-periode_penilaian_akhir'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][periode_penilaian_akhir]'" class="form-text" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].periode_penilaian_akhir" placeholder="cth: 30/12/2019">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-unsur_utama'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-unsur_utama'" class="form-label">Unsur Utama</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-unsur_utama'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][unsur_utama]'" class="form-text" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].unsur_utama">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-unsur_penunjang'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-unsur_penunjang'" class="form-label">Unsur Penunjang</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-unsur_penunjang'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][unsur_penunjang]'" class="form-text" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].unsur_penunjang">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-total_angkat_kredit'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-total_angkat_kredit'" class="form-label">Total Angkat Kredit</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-total_angkat_kredit'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][total_angkat_kredit]'" class="form-text" type="text" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].total_angkat_kredit">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenetapanangkakredit-' + key + '-virtual_arsip_pak_upload'">
                                                        <label v-bind:for="'pegawaipenetapanangkakredit-' + key + '-virtual_arsip_pak_upload'" class="form-label">Arsip PAK</label>
                                                        <input v-bind:id="'pegawaipenetapanangkakredit-' + key + '-virtual_arsip_pak_upload'" v-bind:name="'PegawaiPenetapanAngkaKredit[' + key + '][virtual_arsip_pak_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiPenetapanAngkaKredits[key].virtual_arsip_pak_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removePenetapanAngkaKredit(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addPenetapanAngkaKredit" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Penetapan Angka Kredit</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'orang-tua') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Data Ayah
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'nama_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tempat_lahir_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tempat_lahir_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tempat_lahir_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'tempat_lahir_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tempat_lahir_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tanggal_lahir_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tanggal_lahir_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tanggal_lahir_ayah', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 18/12/1992']); ?>
                                        <?= Html::error($model['pegawai'], 'tanggal_lahir_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tanggal_lahir_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'agama_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'agama_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'agama_ayah', $pilihanAgama, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'agama_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'agama_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_pendidikan_terakhir_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_pendidikan_terakhir_ayah', $pilihanJenjangPendidikan, ['prompt' => '- pilih jenjang pendidikan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_pendidikan_terakhir_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'pekerjaan_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'pekerjaan_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'pekerjaan_ayah', $pilihanPekerjaan, ['prompt' => '- pilih pekerjaan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'pekerjaan_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'pekerjaan_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'status_hidup_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'status_hidup_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'status_hidup_ayah', $pilihanStatusHidupAyah, ['prompt' => '- pilih status hidup ayah -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'status_hidup_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'status_hidup_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'telpon_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'telpon_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'telpon_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'telpon_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'telpon_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'alamat_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'alamat_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'alamat_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'alamat_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'alamat_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kelurahan_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kelurahan_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'kelurahan_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'kelurahan_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kelurahan_ayah')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kode_pos_ayah', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kode_pos_ayah', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'kode_pos_ayah', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'kode_pos_ayah', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kode_pos_ayah')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <div class="fs-16 text-azure fs-italic">
                                    Data Ibu
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'nama_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tempat_lahir_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tempat_lahir_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tempat_lahir_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'tempat_lahir_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tempat_lahir_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tanggal_lahir_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tanggal_lahir_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tanggal_lahir_ibu', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 18/12/1992']); ?>
                                        <?= Html::error($model['pegawai'], 'tanggal_lahir_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tanggal_lahir_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'agama_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'agama_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'agama_ibu', $pilihanAgama, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'agama_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'agama_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_pendidikan_terakhir_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_pendidikan_terakhir_ibu', $pilihanJenjangPendidikan, ['prompt' => '- pilih jenjang pendidikan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_pendidikan_terakhir_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'pekerjaan_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'pekerjaan_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'pekerjaan_ibu', $pilihanPekerjaan, ['prompt' => '- pilih pekerjaan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'pekerjaan_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'pekerjaan_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'status_hidup_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'status_hidup_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'status_hidup_ibu', $pilihanStatusHidupIbu, ['prompt' => '- pilih status hidup ibu -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'status_hidup_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'status_hidup_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'telpon_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'telpon_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'telpon_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'telpon_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'telpon_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'alamat_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'alamat_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'alamat_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'alamat_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'alamat_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kelurahan_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kelurahan_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'kelurahan_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'kelurahan_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kelurahan_ibu')->end(); ?>

                                <?= $form->field($model['pegawai'], 'kode_pos_ibu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'kode_pos_ibu', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'kode_pos_ibu', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'kode_pos_ibu', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'kode_pos_ibu')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'saudara-kandung') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Daftar Saudara Kandung
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_saudara_kandung'])) foreach ($model['pegawai_saudara_kandung'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]nama")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]nama")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]jenis_kelamin")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]jenis_kelamin")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]tempat_lahir")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]tempat_lahir")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]tanggal_lahir")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]tanggal_lahir")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]pekerjaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]pekerjaan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]anak_ke")->begin(); ?>
                                    <?= $form->field($model['pegawai_saudara_kandung'][$key], "[$key]anak_ke")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiSaudaraKandungs == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiSaudaraKandungs">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaisaudarakandung-' + key + '-id'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][id]'" type="text" v-model="pegawai.pegawaiSaudaraKandungs[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-nama'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-nama'" class="form-label">Nama</label>
                                                        <input v-bind:id="'pegawaisaudarakandung-' + key + '-nama'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][nama]'" class="form-text" type="text" v-model="pegawai.pegawaiSaudaraKandungs[key].nama">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-jenis_kelamin'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-jenis_kelamin'" class="form-label">Jenis Kelamin</label>
                                                        <select v-bind:id="'pegawaisaudarakandung-' + key + '-jenis_kelamin'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][jenis_kelamin]'" class="form-dropdown" v-model="pegawai.pegawaiSaudaraKandungs[key].jenis_kelamin">
                                                            <option value="">- pilih jenis kelamin -</option>
                                                            <?php foreach ($pilihanJenisKelamin as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-tempat_lahir'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-tempat_lahir'" class="form-label">Tempat Lahir</label>
                                                        <input v-bind:id="'pegawaisaudarakandung-' + key + '-tempat_lahir'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][tempat_lahir]'" class="form-text" type="text" v-model="pegawai.pegawaiSaudaraKandungs[key].tempat_lahir">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-tanggal_lahir'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-tanggal_lahir'" class="form-label">Tanggal Lahir</label>
                                                        <input v-bind:id="'pegawaisaudarakandung-' + key + '-tanggal_lahir'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][tanggal_lahir]'" class="form-text" type="text" v-model="pegawai.pegawaiSaudaraKandungs[key].tanggal_lahir" placeholder="cth: 12/18/1992">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-pekerjaan'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-pekerjaan'" class="form-label">Pekerjaan</label>
                                                        <select v-bind:id="'pegawaisaudarakandung-' + key + '-pekerjaan'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][pekerjaan]'" class="form-dropdown" v-model="pegawai.pegawaiSaudaraKandungs[key].pekerjaan">
                                                            <option value="">- pilih pekerjaan -</option>
                                                            <?php foreach ($pilihanPekerjaan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div v-bind:class="'form-wrapper field-pegawaisaudarakandung-' + key + '-anak_ke'">
                                                        <label v-bind:for="'pegawaisaudarakandung-' + key + '-anak_ke'" class="form-label">Anak Ke</label>
                                                        <input v-bind:id="'pegawaisaudarakandung-' + key + '-anak_ke'" v-bind:name="'PegawaiSaudaraKandung[' + key + '][anak_ke]'" class="form-text" type="text" v-model="pegawai.pegawaiSaudaraKandungs[key].anak_ke">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeSaudaraKandung(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addSaudaraKandung" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Data Saudara Kandung</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'pasangan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Data <?= $model['pegawai']->pasangan; ?>
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'nama_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'nama_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'nama_pasangan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'nama_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'nama_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tempat_lahir_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tempat_lahir_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tempat_lahir_pasangan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'tempat_lahir_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tempat_lahir_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'tanggal_lahir_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'tanggal_lahir_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'tanggal_lahir_pasangan', ['class' => 'form-text', 'maxlength' => true, 'placeholder' => 'cth: 18/12/1992']); ?>
                                        <?= Html::error($model['pegawai'], 'tanggal_lahir_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'tanggal_lahir_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'agama_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'agama_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'agama_pasangan', $pilihanAgama, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'agama_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'agama_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'jenjang_pendidikan_terakhir_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'jenjang_pendidikan_terakhir_pasangan', $pilihanJenjangPendidikan, ['prompt' => '- pilih jenjang pendidikan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'jenjang_pendidikan_terakhir_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'jenjang_pendidikan_terakhir_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'pekerjaan_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'pekerjaan_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'pekerjaan_pasangan', $pilihanPekerjaan, ['prompt' => '- pilih pekerjaan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'pekerjaan_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'pekerjaan_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'status_hidup_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'status_hidup_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeDropDownList($model['pegawai'], 'status_hidup_pasangan', $pilihanStatusHidupPasangan, ['prompt' => '- pilih status hidup pasangan -', 'class' => 'form-dropdown select2']); ?>
                                        <?= Html::error($model['pegawai'], 'status_hidup_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'status_hidup_pasangan')->end(); ?>

                                <?= $form->field($model['pegawai'], 'telpon_pasangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'telpon_pasangan', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'telpon_pasangan', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'telpon_pasangan', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'telpon_pasangan')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'anak') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Data Anak
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_anak'])) foreach ($model['pegawai_anak'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]nama_anak")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]nama_anak")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]jenis_kelamin")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]jenis_kelamin")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]tempat_lahir")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]tempat_lahir")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]tanggal_lahir")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]tanggal_lahir")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]jenjang_pendidikan_terakhir")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]jenjang_pendidikan_terakhir")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]pekerjaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]pekerjaan")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_anak")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_anak")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_hidup")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_hidup")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_tunjangan")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]status_tunjangan")->end(); ?>

                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]virtual_akta_lahir_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_anak'][$key], "[$key]virtual_akta_lahir_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiAnaks == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiAnaks">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaianak-' + key + '-id'" v-bind:name="'PegawaiAnak[' + key + '][id]'" type="text" v-model="pegawai.pegawaiAnaks[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-nama_anak'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-nama_anak'" class="form-label">Nama Anak</label>
                                                        <input v-bind:id="'pegawaianak-' + key + '-nama_anak'" v-bind:name="'PegawaiAnak[' + key + '][nama_anak]'" class="form-text" type="text" v-model="pegawai.pegawaiAnaks[key].nama_anak">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-jenis_kelamin'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-jenis_kelamin'" class="form-label">Jenis Kelamin</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-jenis_kelamin'" v-bind:name="'PegawaiAnak[' + key + '][jenis_kelamin]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].jenis_kelamin">
                                                            <option value="">- pilih jenis kelamin -</option>
                                                            <?php foreach ($pilihanJenisKelamin as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-tempat_lahir'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-tempat_lahir'" class="form-label">Tempat Lahir</label>
                                                        <input v-bind:id="'pegawaianak-' + key + '-tempat_lahir'" v-bind:name="'PegawaiAnak[' + key + '][tempat_lahir]'" class="form-text" type="text" v-model="pegawai.pegawaiAnaks[key].tempat_lahir">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-tanggal_lahir'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-tanggal_lahir'" class="form-label">Tanggal Lahir</label>
                                                        <input v-bind:id="'pegawaianak-' + key + '-tanggal_lahir'" v-bind:name="'PegawaiAnak[' + key + '][tanggal_lahir]'" class="form-text" type="text" v-model="pegawai.pegawaiAnaks[key].tanggal_lahir" placeholder="cth: 18/12/1992">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-jenjang_pendidikan_terakhir'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-jenjang_pendidikan_terakhir'" class="form-label">Jenjang Pendidikan Terakhir</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-jenjang_pendidikan_terakhir'" v-bind:name="'PegawaiAnak[' + key + '][jenjang_pendidikan_terakhir]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].jenjang_pendidikan_terakhir">
                                                            <option value="">- pilih jenjang pendidikan terakhir -</option>
                                                            <?php foreach ($pilihanJenjangPendidikan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-pekerjaan'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-pekerjaan'" class="form-label">Pekerjaan</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-pekerjaan'" v-bind:name="'PegawaiAnak[' + key + '][pekerjaan]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].pekerjaan">
                                                            <option value="">- pilih pekerjaan -</option>
                                                            <?php foreach ($pilihanPekerjaan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-status_anak'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-status_anak'" class="form-label">Status Anak</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-status_anak'" v-bind:name="'PegawaiAnak[' + key + '][status_anak]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].status_anak">
                                                            <option value="">- pilih status anak -</option>
                                                            <?php foreach ($pilihanStatusAnak as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-status_hidup'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-status_hidup'" class="form-label">Status Hidup</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-status_hidup'" v-bind:name="'PegawaiAnak[' + key + '][status_hidup]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].status_hidup">
                                                            <option value="">- pilih status hidup -</option>
                                                            <?php foreach ($pilihanStatusHidupPasangan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-status_tunjangan'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-status_tunjangan'" class="form-label">Status Tunjangan</label>
                                                        <select v-bind:id="'pegawaianak-' + key + '-status_tunjangan'" v-bind:name="'PegawaiAnak[' + key + '][status_tunjangan]'" class="form-dropdown" v-model="pegawai.pegawaiAnaks[key].status_tunjangan">
                                                            <option value="">- pilih status hidup -</option>
                                                            <?php foreach ($pilihanStatusTunjangan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaianak-' + key + '-virtual_akta_lahir_upload'">
                                                        <label v-bind:for="'pegawaianak-' + key + '-virtual_akta_lahir_upload'" class="form-label">Akta Lahir</label>
                                                        <input v-bind:id="'pegawaianak-' + key + '-virtual_akta_lahir_upload'" v-bind:name="'PegawaiAnak[' + key + '][virtual_akta_lahir_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiAnaks[key].virtual_akta_lahir_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeAnak(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addAnak" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Data Anak</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'pendidikan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Pendidikan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_pendidikan'])) foreach ($model['pegawai_pendidikan'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]nama_sekolah")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]nama_sekolah")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]jenjang_pendidikan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]jenjang_pendidikan")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]jurusan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]jurusan")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]nilai")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]nilai")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]skala_nilai")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]skala_nilai")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]alamat_sekolah")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]alamat_sekolah")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]tahun_masuk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]tahun_masuk")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]tahun_lulus")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]tahun_lulus")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]keterangan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]keterangan")->end(); ?>

                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]virtual_ijazah_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_pendidikan'][$key], "[$key]virtual_ijazah_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiPendidikans == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiPendidikans">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaipendidikan-' + key + '-id'" v-bind:name="'PegawaiPendidikan[' + key + '][id]'" type="text" v-model="pegawai.pegawaiPendidikans[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-nama_sekolah'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-nama_sekolah'" class="form-label">Nama Sekolah</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-nama_sekolah'" v-bind:name="'PegawaiPendidikan[' + key + '][nama_sekolah]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].nama_sekolah">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-jenjang_pendidikan'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-jenjang_pendidikan'" class="form-label">Jenjang Pendidikan</label>
                                                        <select v-bind:id="'pegawaipendidikan-' + key + '-jenjang_pendidikan'" v-bind:name="'PegawaiPendidikan[' + key + '][jenjang_pendidikan]'" class="form-dropdown" v-model="pegawai.pegawaiPendidikans[key].jenjang_pendidikan">
                                                            <option value="">- pilih jenjang pendidikan -</option>
                                                            <?php foreach ($pilihanJenjangPendidikan as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-jurusan'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-jurusan'" class="form-label">Jurusan</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-jurusan'" v-bind:name="'PegawaiPendidikan[' + key + '][jurusan]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].jurusan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-nilai'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-nilai'" class="form-label">Nilai</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-nilai'" v-bind:name="'PegawaiPendidikan[' + key + '][nilai]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].nilai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-skala_nilai'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-skala_nilai'" class="form-label">Skala Nilai</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-skala_nilai'" v-bind:name="'PegawaiPendidikan[' + key + '][skala_nilai]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].skala_nilai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-alamat_sekolah'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-alamat_sekolah'" class="form-label">Alamat Sekolah</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-alamat_sekolah'" v-bind:name="'PegawaiPendidikan[' + key + '][alamat_sekolah]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].alamat_sekolah">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-tahun_masuk'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-tahun_masuk'" class="form-label">Tahun Masuk</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-tahun_masuk'" v-bind:name="'PegawaiPendidikan[' + key + '][tahun_masuk]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].tahun_masuk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-tahun_lulus'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-tahun_lulus'" class="form-label">Tahun Lulus</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-tahun_lulus'" v-bind:name="'PegawaiPendidikan[' + key + '][tahun_lulus]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].tahun_lulus">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-keterangan'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-keterangan'" class="form-label">Keterangan</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-keterangan'" v-bind:name="'PegawaiPendidikan[' + key + '][keterangan]'" class="form-text" type="text" v-model="pegawai.pegawaiPendidikans[key].keterangan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipendidikan-' + key + '-virtual_ijazah_upload'">
                                                        <label v-bind:for="'pegawaipendidikan-' + key + '-virtual_ijazah_upload'" class="form-label">Ijazah</label>
                                                        <input v-bind:id="'pegawaipendidikan-' + key + '-virtual_ijazah_upload'" v-bind:name="'PegawaiPendidikan[' + key + '][virtual_ijazah_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiPendidikans[key].virtual_ijazah_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removePendidikan(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addPendidikan" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Pendidikan</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'penghargaan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Daftar Penghargaan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_penghargaan'])) foreach ($model['pegawai_penghargaan'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]jenis_penghargaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]jenis_penghargaan")->end(); ?>

                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]nama_penghargaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]nama_penghargaan")->end(); ?>

                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]tahun_penghargaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]tahun_penghargaan")->end(); ?>

                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]instansi_pemberi_penghargaan")->begin(); ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]instansi_pemberi_penghargaan")->end(); ?>

                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]virtual_sertifikat_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_penghargaan'][$key], "[$key]virtual_sertifikat_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiPenghargaans == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiPenghargaans">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaipenghargaan-' + key + '-id'" v-bind:name="'PegawaiPenghargaan[' + key + '][id]'" type="text" v-model="pegawai.pegawaiPenghargaans[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenghargaan-' + key + '-jenis_penghargaan'">
                                                        <label v-bind:for="'pegawaipenghargaan-' + key + '-jenis_penghargaan'" class="form-label">Jenis Penghargaan</label>
                                                        <input v-bind:id="'pegawaipenghargaan-' + key + '-jenis_penghargaan'" v-bind:name="'PegawaiPenghargaan[' + key + '][jenis_penghargaan]'" class="form-text" type="text" v-model="pegawai.pegawaiPenghargaans[key].jenis_penghargaan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenghargaan-' + key + '-nama_penghargaan'">
                                                        <label v-bind:for="'pegawaipenghargaan-' + key + '-nama_penghargaan'" class="form-label">Nama Penghargaan</label>
                                                        <input v-bind:id="'pegawaipenghargaan-' + key + '-nama_penghargaan'" v-bind:name="'PegawaiPenghargaan[' + key + '][nama_penghargaan]'" class="form-text" type="text" v-model="pegawai.pegawaiPenghargaans[key].nama_penghargaan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenghargaan-' + key + '-tahun_penghargaan'">
                                                        <label v-bind:for="'pegawaipenghargaan-' + key + '-tahun_penghargaan'" class="form-label">Tahun Penghargaan</label>
                                                        <input v-bind:id="'pegawaipenghargaan-' + key + '-tahun_penghargaan'" v-bind:name="'PegawaiPenghargaan[' + key + '][tahun_penghargaan]'" class="form-text" type="text" v-model="pegawai.pegawaiPenghargaans[key].tahun_penghargaan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenghargaan-' + key + '-instansi_pemberi_penghargaan'">
                                                        <label v-bind:for="'pegawaipenghargaan-' + key + '-instansi_pemberi_penghargaan'" class="form-label">Instansi Pemberi Penghargaan</label>
                                                        <input v-bind:id="'pegawaipenghargaan-' + key + '-instansi_pemberi_penghargaan'" v-bind:name="'PegawaiPenghargaan[' + key + '][instansi_pemberi_penghargaan]'" class="form-text" type="text" v-model="pegawai.pegawaiPenghargaans[key].instansi_pemberi_penghargaan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipenghargaan-' + key + '-virtual_sertifikat_upload'">
                                                        <label v-bind:for="'pegawaipenghargaan-' + key + '-virtual_sertifikat_upload'" class="form-label">Sertifikat</label>
                                                        <input v-bind:id="'pegawaipenghargaan-' + key + '-virtual_sertifikat_upload'" v-bind:name="'PegawaiPenghargaan[' + key + '][virtual_sertifikat_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiPenghargaans[key].virtual_sertifikat_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removePenghargaan(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addPenghargaan" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Penghargaan</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'diklat') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Diklat
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_diklat'])) foreach ($model['pegawai_diklat'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]jenis_diklat")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]jenis_diklat")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]nama_diklat")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]nama_diklat")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]penyelenggara")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]penyelenggara")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_mulai")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_mulai")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_selesai")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_selesai")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tempat")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tempat")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]keterangan")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]keterangan")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]virtual_sertifikat_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]virtual_sertifikat_upload")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]nomor_sttp")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]nomor_sttp")->end(); ?>

                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_sttp")->begin(); ?>
                                    <?= $form->field($model['pegawai_diklat'][$key], "[$key]tanggal_sttp")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiDiklats == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiDiklats">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaidiklat-' + key + '-id'" v-bind:name="'PegawaiDiklat[' + key + '][id]'" type="text" v-model="pegawai.pegawaiDiklats[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-jenis_diklat'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-jenis_diklat'" class="form-label">Jenis Diklat</label>
                                                        <select v-bind:id="'pegawaidiklat-' + key + '-jenis_diklat'" v-bind:name="'PegawaiDiklat[' + key + '][jenis_diklat]'" class="form-dropdown" v-model="pegawai.pegawaiDiklats[key].jenis_diklat">
                                                            <option value="">- pilih jenis diklat -</option>
                                                            <?php foreach ($pilihanJenisDiklat as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-nama_diklat'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-nama_diklat'" class="form-label">Nama Diklat</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-nama_diklat'" v-bind:name="'PegawaiDiklat[' + key + '][nama_diklat]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].nama_diklat">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-penyelenggara'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-penyelenggara'" class="form-label">Penyelenggara</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-penyelenggara'" v-bind:name="'PegawaiDiklat[' + key + '][penyelenggara]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].penyelenggara">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-tanggal_mulai'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-tanggal_mulai'" class="form-label">Tanggal Mulai</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-tanggal_mulai'" v-bind:name="'PegawaiDiklat[' + key + '][tanggal_mulai]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].tanggal_mulai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-tanggal_selesai'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-tanggal_selesai'" class="form-label">Tanggal Selesai</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-tanggal_selesai'" v-bind:name="'PegawaiDiklat[' + key + '][tanggal_selesai]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].tanggal_selesai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-tempat'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-tempat'" class="form-label">Tempat</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-tempat'" v-bind:name="'PegawaiDiklat[' + key + '][tempat]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].tempat">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-keterangan'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-keterangan'" class="form-label">Keterangan</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-keterangan'" v-bind:name="'PegawaiDiklat[' + key + '][keterangan]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].keterangan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-virtual_sertifikat_upload'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-virtual_sertifikat_upload'" class="form-label">Sertifikat</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-virtual_sertifikat_upload'" v-bind:name="'PegawaiDiklat[' + key + '][virtual_sertifikat_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiDiklats[key].virtual_sertifikat_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-nomor_sttp'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-nomor_sttp'" class="form-label">Nomor Sttp</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-nomor_sttp'" v-bind:name="'PegawaiDiklat[' + key + '][nomor_sttp]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].nomor_sttp">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaidiklat-' + key + '-tanggal_sttp'">
                                                        <label v-bind:for="'pegawaidiklat-' + key + '-tanggal_sttp'" class="form-label">Tanggal Sttp</label>
                                                        <input v-bind:id="'pegawaidiklat-' + key + '-tanggal_sttp'" v-bind:name="'PegawaiDiklat[' + key + '][tanggal_sttp]'" class="form-text" type="text" v-model="pegawai.pegawaiDiklats[key].tanggal_sttp">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeDiklat(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addDiklat" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Diklat</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'kursus-pelatihan') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Kursus Pelatihan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_kursus_pelatihan'])) foreach ($model['pegawai_kursus_pelatihan'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]nama_kursus_pelatihan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]nama_kursus_pelatihan")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]penyelenggara")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]penyelenggara")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]tanggal_mulai")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]tanggal_mulai")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]tanggal_selesai")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]tanggal_selesai")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]lokasi")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]lokasi")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]keterangan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]keterangan")->end(); ?>

                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]virtual_sertifikat_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_kursus_pelatihan'][$key], "[$key]virtual_sertifikat_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiKursusPelatihans == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiKursusPelatihans">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaikursuspelatihan-' + key + '-id'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][id]'" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-nama_kursus_pelatihan'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-nama_kursus_pelatihan'" class="form-label">Nama Kursus Pelatihan</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-nama_kursus_pelatihan'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][nama_kursus_pelatihan]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].nama_kursus_pelatihan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-penyelenggara'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-penyelenggara'" class="form-label">Penyelenggara</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-penyelenggara'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][penyelenggara]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].penyelenggara">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-tanggal_mulai'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-tanggal_mulai'" class="form-label">Tanggal Mulai</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-tanggal_mulai'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][tanggal_mulai]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].tanggal_mulai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-tanggal_selesai'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-tanggal_selesai'" class="form-label">Tanggal Selesai</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-tanggal_selesai'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][tanggal_selesai]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].tanggal_selesai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-lokasi'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-lokasi'" class="form-label">Lokasi</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-lokasi'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][lokasi]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].lokasi">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-keterangan'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-keterangan'" class="form-label">Keterangan</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-keterangan'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][keterangan]'" class="form-text" type="text" v-model="pegawai.pegawaiKursusPelatihans[key].keterangan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikursuspelatihan-' + key + '-virtual_sertifikat_upload'">
                                                        <label v-bind:for="'pegawaikursuspelatihan-' + key + '-virtual_sertifikat_upload'" class="form-label">Sertifikat</label>
                                                        <input v-bind:id="'pegawaikursuspelatihan-' + key + '-virtual_sertifikat_upload'" v-bind:name="'PegawaiKursusPelatihan[' + key + '][virtual_sertifikat_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiKursusPelatihans[key].virtual_sertifikat_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeKursusPelatihan(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addKursusPelatihan" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Kursus Pelatihan</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'kunjungan-luar-negeri' && false) : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Kunjungan Luar Negeri
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_kunjungan_luar_negeri'])) foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]negara_kunjungan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]negara_kunjungan")->end(); ?>

                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tujuan_kunjungan")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tujuan_kunjungan")->end(); ?>

                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tanggal_mulai")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tanggal_mulai")->end(); ?>

                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tanggal_selesai")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]tanggal_selesai")->end(); ?>

                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]sumber_biaya")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]sumber_biaya")->end(); ?>

                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]sertifikat")->begin(); ?>
                                    <?= $form->field($model['pegawai_kunjungan_luar_negeri'][$key], "[$key]sertifikat")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiKunjunganLuarNegeris == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiKunjunganLuarNegeris">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-id'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][id]'" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-negara_kunjungan'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-negara_kunjungan'" class="form-label">Negara Kunjungan</label>
                                                        <select v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-negara_kunjungan'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][negara_kunjungan]'" class="form-dropdown" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].negara_kunjungan">
                                                            <option value="">- pilih negara -</option>
                                                            <?php foreach ($pilihanNegara as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-tujuan_kunjungan'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-tujuan_kunjungan'" class="form-label">Tujuan Kunjungan</label>
                                                        <input v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-tujuan_kunjungan'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][tujuan_kunjungan]'" class="form-text" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].tujuan_kunjungan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-tanggal_mulai'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-tanggal_mulai'" class="form-label">Tanggal Mulai</label>
                                                        <input v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-tanggal_mulai'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][tanggal_mulai]'" class="form-text" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].tanggal_mulai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-tanggal_selesai'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-tanggal_selesai'" class="form-label">Tanggal Selesai</label>
                                                        <input v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-tanggal_selesai'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][tanggal_selesai]'" class="form-text" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].tanggal_selesai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-sumber_biaya'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-sumber_biaya'" class="form-label">Sumber Biaya</label>
                                                        <input v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-sumber_biaya'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][sumber_biaya]'" class="form-text" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].sumber_biaya">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div v-bind:class="'form-wrapper field-pegawaikunjunganluarnegeri-' + key + '-sertifikat'">
                                                        <label v-bind:for="'pegawaikunjunganluarnegeri-' + key + '-sertifikat'" class="form-label">Sertifikat</label>
                                                        <input v-bind:id="'pegawaikunjunganluarnegeri-' + key + '-sertifikat'" v-bind:name="'PegawaiKunjunganLuarNegeri[' + key + '][sertifikat]'" class="form-text" type="text" v-model="pegawai.pegawaiKunjunganLuarNegeris[key].sertifikat">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeKunjunganLuarNegeri(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addKunjunganLuarNegeri" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Kunjungan Luar Negeri</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'organisasi') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Organisasi
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_organisasi'])) foreach ($model['pegawai_organisasi'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]organisasi")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]organisasi")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]jabatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]jabatan")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]tahun_mulai")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]tahun_mulai")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]tahun_selesai")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]tahun_selesai")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]lokasi")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]lokasi")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]periode_organisasi")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]periode_organisasi")->end(); ?>

                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]virtual_sertifikat_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_organisasi'][$key], "[$key]virtual_sertifikat_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiOrganisasis == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiOrganisasis">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaiorganisasi-' + key + '-id'" v-bind:name="'PegawaiOrganisasi[' + key + '][id]'" type="text" v-model="pegawai.pegawaiOrganisasis[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-organisasi'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-organisasi'" class="form-label">Organisasi</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-organisasi'" v-bind:name="'PegawaiOrganisasi[' + key + '][organisasi]'" class="form-text" type="text" v-model="pegawai.pegawaiOrganisasis[key].organisasi">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-jabatan'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-jabatan'" class="form-label">Jabatan</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-jabatan'" v-bind:name="'PegawaiOrganisasi[' + key + '][jabatan]'" class="form-text" type="text" v-model="pegawai.pegawaiOrganisasis[key].jabatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-tahun_mulai'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-tahun_mulai'" class="form-label">Tahun Mulai</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-tahun_mulai'" v-bind:name="'PegawaiOrganisasi[' + key + '][tahun_mulai]'" class="form-text" type="text" v-model="pegawai.pegawaiOrganisasis[key].tahun_mulai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-tahun_selesai'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-tahun_selesai'" class="form-label">Tahun Selesai</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-tahun_selesai'" v-bind:name="'PegawaiOrganisasi[' + key + '][tahun_selesai]'" class="form-text" type="text" v-model="pegawai.pegawaiOrganisasis[key].tahun_selesai">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-lokasi'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-lokasi'" class="form-label">Lokasi</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-lokasi'" v-bind:name="'PegawaiOrganisasi[' + key + '][lokasi]'" class="form-text" type="text" v-model="pegawai.pegawaiOrganisasis[key].lokasi">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-periode_organisasi'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-periode_organisasi'" class="form-label">Periode Organisasi</label>
                                                        <select v-bind:id="'pegawaiorganisasi-' + key + '-periode_organisasi'" v-bind:name="'PegawaiOrganisasi[' + key + '][periode_organisasi]'" class="form-dropdown" v-model="pegawai.pegawaiOrganisasis[key].periode_organisasi">
                                                            <option value="">- pilih periode organisasi -</option>
                                                            <?php foreach ($pilihanPeriodeOrganisasi as $id => $nama) : ?>
                                                                <option value="<?= $id ?>"><?= $nama ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaiorganisasi-' + key + '-virtual_sertifikat_upload'">
                                                        <label v-bind:for="'pegawaiorganisasi-' + key + '-virtual_sertifikat_upload'" class="form-label">Sertifikat</label>
                                                        <input v-bind:id="'pegawaiorganisasi-' + key + '-virtual_sertifikat_upload'" v-bind:name="'PegawaiOrganisasi[' + key + '][virtual_sertifikat_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiOrganisasis[key].virtual_sertifikat_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeOrganisasi(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addOrganisasi" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Organisasi</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'hukuman-disiplin') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Hukuman Disiplin
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_hukuman_disiplin'])) foreach ($model['pegawai_hukuman_disiplin'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]jenis_hukuman_disiplin")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]jenis_hukuman_disiplin")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]penjelasan")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]penjelasan")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]terhitung_sampai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]terhitung_sampai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]jenis_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]nomor_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_hukuman_disiplin'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiHukumanDisiplins == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiHukumanDisiplins">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaihukumandisiplin-' + key + '-id'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][id]'" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-jenis_hukuman_disiplin'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-jenis_hukuman_disiplin'" class="form-label">Jenis Hukuman Disiplin</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-jenis_hukuman_disiplin'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][jenis_hukuman_disiplin]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].jenis_hukuman_disiplin">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-penjelasan'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-penjelasan'" class="form-label">Penjelasan</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-penjelasan'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][penjelasan]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].penjelasan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-terhitung_mulai_tanggal'" class="form-label">Terhitung Mulai Tanggal</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].terhitung_mulai_tanggal">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-terhitung_sampai_tanggal'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-terhitung_sampai_tanggal'" class="form-label">Terhitung Sampai Tanggal</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-terhitung_sampai_tanggal'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][terhitung_sampai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].terhitung_sampai_tanggal">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-jenis_sk'" class="form-label">Jenis Sk</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-jenis_sk'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][jenis_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].jenis_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-nomor_sk'" class="form-label">Nomor Sk</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-nomor_sk'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-tanggal_sk'" class="form-label">Tanggal Sk</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-tanggal_sk'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].tanggal_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap Sk</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiHukumanDisiplins[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaihukumandisiplin-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaihukumandisiplin-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip Sk</label>
                                                        <input v-bind:id="'pegawaihukumandisiplin-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiHukumanDisiplin[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiHukumanDisiplins[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeHukumanDisiplin(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addHukumanDisiplin" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Hukuman Disiplin</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'pemberhentian') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Pemberhentian
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_pemberhentian'])) foreach ($model['pegawai_pemberhentian'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenis_pemberhentian")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenis_pemberhentian")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>

                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]kepangkatan_saat_pemberhentian")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]kepangkatan_saat_pemberhentian")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]instansi_saat_pemberhentian")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]instansi_saat_pemberhentian")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]unit_kerja_saat_pemberhentian")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]unit_kerja_saat_pemberhentian")->end(); ?>

                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenjang_jabatan_saat_diberhentikan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenjang_jabatan_saat_diberhentikan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]nama_jabatan_saat_diberhentikan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]nama_jabatan_saat_diberhentikan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]catatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]catatan")->end(); ?>

                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]jenis_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]nomor_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]tanggal_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]pejabat_penetap_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_pemberhentian'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiPemberhentians == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiPemberhentians">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaipemberhentian-' + key + '-id'" v-bind:name="'PegawaiPemberhentian[' + key + '][id]'" type="text" v-model="pegawai.pegawaiPemberhentians[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-jenis_pemberhentian'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-jenis_pemberhentian'" class="form-label">Jenis Pemberhentian</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-jenis_pemberhentian'" v-bind:name="'PegawaiPemberhentian[' + key + '][jenis_pemberhentian]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].jenis_pemberhentian">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-terhitung_mulai_tanggal'" class="form-label">Terhitung Mulai Tanggal</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiPemberhentian[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].terhitung_mulai_tanggal">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-kepangkatan_saat_pemberhentian'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-kepangkatan_saat_pemberhentian'" class="form-label">Kepangkatan Saat Pemberhentian</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-kepangkatan_saat_pemberhentian'" v-bind:name="'PegawaiPemberhentian[' + key + '][kepangkatan_saat_pemberhentian]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].kepangkatan_saat_pemberhentian">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-instansi_saat_pemberhentian'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-instansi_saat_pemberhentian'" class="form-label">Instansi Saat Pemberhentian</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-instansi_saat_pemberhentian'" v-bind:name="'PegawaiPemberhentian[' + key + '][instansi_saat_pemberhentian]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].instansi_saat_pemberhentian">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-unit_kerja_saat_pemberhentian'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-unit_kerja_saat_pemberhentian'" class="form-label">Unit Kerja Saat Pemberhentian</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-unit_kerja_saat_pemberhentian'" v-bind:name="'PegawaiPemberhentian[' + key + '][unit_kerja_saat_pemberhentian]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].unit_kerja_saat_pemberhentian">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-jenjang_jabatan_saat_diberhentikan'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-jenjang_jabatan_saat_diberhentikan'" class="form-label">Jenjang Jabatan Saat Diberhentikan</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-jenjang_jabatan_saat_diberhentikan'" v-bind:name="'PegawaiPemberhentian[' + key + '][jenjang_jabatan_saat_diberhentikan]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].jenjang_jabatan_saat_diberhentikan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-nama_jabatan_saat_diberhentikan'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-nama_jabatan_saat_diberhentikan'" class="form-label">Nama Jabatan Saat Diberhentikan</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-nama_jabatan_saat_diberhentikan'" v-bind:name="'PegawaiPemberhentian[' + key + '][nama_jabatan_saat_diberhentikan]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].nama_jabatan_saat_diberhentikan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-catatan'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-catatan'" class="form-label">Catatan</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-catatan'" v-bind:name="'PegawaiPemberhentian[' + key + '][catatan]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].catatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-jenis_sk'" class="form-label">Jenis Sk</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-jenis_sk'" v-bind:name="'PegawaiPemberhentian[' + key + '][jenis_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].jenis_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-nomor_sk'" class="form-label">Nomor Sk</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-nomor_sk'" v-bind:name="'PegawaiPemberhentian[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-tanggal_sk'" class="form-label">Tanggal Sk</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-tanggal_sk'" v-bind:name="'PegawaiPemberhentian[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].tanggal_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap Sk</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiPemberhentian[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiPemberhentians[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaipemberhentian-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaipemberhentian-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip Sk</label>
                                                        <input v-bind:id="'pegawaipemberhentian-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiPemberhentian[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiPemberhentians[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removePemberhentian(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addPemberhentian" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Pemberhentian</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'masa-persiapan-pensiun') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Riwayat Masa Persiapan Pensiun
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if (isset($model['pegawai_masa_persiapan_pensiun'])) foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $value): ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]terhitung_mulai_tanggal")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]terhitung_mulai_tanggal")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]catatan")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]catatan")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]jenis_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]jenis_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]nomor_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]nomor_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]tanggal_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]tanggal_sk")->end(); ?>
                                    
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]pejabat_penetap_sk")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]pejabat_penetap_sk")->end(); ?>

                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]virtual_arsip_sk_upload")->begin(); ?>
                                    <?= $form->field($model['pegawai_masa_persiapan_pensiun'][$key], "[$key]virtual_arsip_sk_upload")->end(); ?>
                                <?php endforeach; ?>

                                <template v-if="typeof pegawai.pegawaiMasaPersiapanPensiuns == 'object'">
                                    <template v-for="(value, key, index) in pegawai.pegawaiMasaPersiapanPensiuns">
                                        <div v-show="!(value.id < 0)">
                                            <input type="hidden" v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-id'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][id]'" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].id">

                                            <div>
                                                <span class="fs-14 rounded-md border-azure text-azure padding-x-15 padding-y-5">Data ke-{{(key+1)}}</span>
                                            </div>

                                            <div class="margin-top-15"></div>

                                            <div class="box box-break-sm box-gutter box-equal">
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-terhitung_mulai_tanggal'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-terhitung_mulai_tanggal'" class="form-label">TMT</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-terhitung_mulai_tanggal'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][terhitung_mulai_tanggal]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].terhitung_mulai_tanggal">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-catatan'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-catatan'" class="form-label">Catatan</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-catatan'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][catatan]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].catatan">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-jenis_sk'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-jenis_sk'" class="form-label">Jenis Sk</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-jenis_sk'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][jenis_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].jenis_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-nomor_sk'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-nomor_sk'" class="form-label">Nomor Sk</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-nomor_sk'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][nomor_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].nomor_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-tanggal_sk'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-tanggal_sk'" class="form-label">Tanggal Sk</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-tanggal_sk'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][tanggal_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].tanggal_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-pejabat_penetap_sk'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-pejabat_penetap_sk'" class="form-label">Pejabat Penetap Sk</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-pejabat_penetap_sk'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][pejabat_penetap_sk]'" class="form-text" type="text" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].pejabat_penetap_sk">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-2">
                                                    <div v-bind:class="'form-wrapper field-pegawaimasapersiapanpensiun-' + key + '-virtual_arsip_sk_upload'">
                                                        <label v-bind:for="'pegawaimasapersiapanpensiun-' + key + '-virtual_arsip_sk_upload'" class="form-label">Arsip Sk</label>
                                                        <input v-bind:id="'pegawaimasapersiapanpensiun-' + key + '-virtual_arsip_sk_upload'" v-bind:name="'PegawaiMasaPersiapanPensiun[' + key + '][virtual_arsip_sk_upload]'" class="form-file" type="file" v-model="pegawai.pegawaiMasaPersiapanPensiuns[key].virtual_arsip_sk_upload">
                                                        <div class="form-info"></div>
                                                    </div>
                                                </div>
                                                <div class="box-1">
                                                    <div class="margin-bottom-5 hidden-sm-less">
                                                        &nbsp;
                                                    </div>
                                                    <div>
                                                        <i v-on:click="removeMasaPersiapanPensiun(key)" class="button m-button-block fa fa-trash-o bg-light-red border-red margin-0 hover-pointer"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </template>
                                </template>

                                <div class="clearfix">
                                    <a v-on:click="addMasaPersiapanPensiun" class="button border-azure bg-lightest text-azure hover-bg-azure pull-right m-pull-none m-button-block">Tambah Riwayat Masa Persiapan Pensiun</a>
                                </div>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => $tab], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php elseif ($tab == 'sisa-cuti') : ?>
                <div class="tab-content active">
                    <div class="padding-x-30 m-padding-x-15 padding-y-15">
                        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
                            <div>
                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>

                                <div class="fs-16 text-azure fs-italic">
                                    Cuti Tahunan
                                </div>

                                <hr class="margin-y-10 border-top border-light-orange">

                                <?= $form->field($model['pegawai'], 'cuti_N', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'cuti_N', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'cuti_N', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'cuti_N', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'cuti_N')->end(); ?>

                                <?= $form->field($model['pegawai'], 'cuti_N_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'cuti_N_1', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'cuti_N_1', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'cuti_N_1', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'cuti_N_1')->end(); ?>

                                <?= $form->field($model['pegawai'], 'cuti_N_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                    <div class="box-1 padding-x-0 padding-y-5">
                                        <?= Html::activeLabel($model['pegawai'], 'cuti_N_2', ['class' => 'form-label text-grayer']); ?>
                                    </div>
                                    <div class="box-11 m-padding-x-0">
                                        <?= Html::activeTextInput($model['pegawai'], 'cuti_N_2', ['class' => 'form-text', 'maxlength' => true]); ?>
                                        <?= Html::error($model['pegawai'], 'cuti_N_2', ['class' => 'form-info']); ?>
                                    </div>
                                <?= $form->field($model['pegawai'], 'cuti_N_2')->end(); ?>
                            </div>

                            <div class="margin-top-30"></div>

                            <div>
                                <hr class="margin-y-10 border-top border-light-orange">

                                <?php if ($errorMessage) : ?>
                                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                                        <?= $errorMessage ?>
                                    </div>
                                <?php endif; ?>
                                
                                <div class="form-wrapper">
                                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                                    <div class="margin-top-15"></div>
                                    <div class="clearfix">
                                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                        <div class="margin-5 pull-right m-pull-none"></div>
                                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                                    </div>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
