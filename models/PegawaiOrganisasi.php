<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_organisasi".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $organisasi
 * @property string $jabatan
 * @property string $tahun_mulai
 * @property string $tahun_selesai
 * @property string $lokasi
 * @property string $periode_organisasi
 * @property string $sertifikat
 *
 * @property Pegawai $pegawai0
 */
class PegawaiOrganisasi extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_sertifikat_upload;
    public $virtual_sertifikat_download;

    public static function tableName()
    {
        return 'pegawai_organisasi';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //organisasi
            [['organisasi'], 'required'],
            [['organisasi'], 'string', 'max' => 256],

            //jabatan
            [['jabatan'], 'required'],
            [['jabatan'], 'string', 'max' => 256],

            //tahun_mulai
            [['tahun_mulai'], 'required'],
            [['tahun_mulai'], 'safe'],

            //tahun_selesai
            [['tahun_selesai'], 'required'],
            [['tahun_selesai'], 'safe'],

            //lokasi
            [['lokasi'], 'required'],
            [['lokasi'], 'string', 'max' => 256],

            //periode_organisasi
            [['periode_organisasi'], 'required'],
            [['periode_organisasi'], 'string'],

            //sertifikat
            [['sertifikat'], 'string', 'max' => 256],
            
            //virtual_sertifikat_download
            [['virtual_sertifikat_download'], 'safe'],
            
            //virtual_sertifikat_upload
            [['virtual_sertifikat_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_organisasi-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_sertifikat_upload) {
            $path = Yii::getAlias('@upload-pegawai_organisasi-sertifikat') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_sertifikat_upload->saveAs($path . '/' . $this->sertifikat);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_organisasi-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->sertifikat) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_organisasi-sertifikat');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_sertifikat_download = $path . '/' . $this->sertifikat;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'organisasi' => 'Organisasi',
            'jabatan' => 'Jabatan',
            'tahun_mulai' => 'Tahun Mulai',
            'tahun_selesai' => 'Tahun Selesai',
            'lokasi' => 'Lokasi',
            'periode_organisasi' => 'Periode Organisasi',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
