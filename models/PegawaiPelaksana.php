<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_pelaksana".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $jenis_pelaksana
 * @property string $instansi
 * @property string $unit_kerja
 * @property integer $jenjang_jabatan
 * @property string $nama_jabatan
 * @property string $terhitung_mulai_tanggal
 * @property string $terhitung_sampai_tanggal
 * @property string $catatan
 * @property integer $jenis_sk
 * @property string $nomor_sk
 * @property string $tanggal_sk
 * @property string $pejabat_penetap_sk
 * @property string $arsip_sk
 *
 * @property Pegawai $pegawai0
 * @property JenjangJabatan $jenjangJabatan
 * @property JenisSk $jenisSk
 */
class PegawaiPelaksana extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;

    public static function tableName()
    {
        return 'pegawai_pelaksana';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //jenis_pelaksana
            [['jenis_pelaksana'], 'required'],
            [['jenis_pelaksana'], 'string'],

            //instansi
            [['instansi'], 'required'],
            [['instansi'], 'string', 'max' => 256],

            //unit_kerja
            [['unit_kerja'], 'required'],
            [['unit_kerja'], 'string', 'max' => 256],

            //jenjang_jabatan
            [['jenjang_jabatan'], 'required'],
            [['jenjang_jabatan'], 'integer'],
            [['jenjang_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangJabatan::className(), 'targetAttribute' => ['jenjang_jabatan' => 'id']],

            //nama_jabatan
            [['nama_jabatan'], 'required'],
            [['nama_jabatan'], 'string', 'max' => 256],

            //terhitung_mulai_tanggal
            [['terhitung_mulai_tanggal'], 'required'],
            [['terhitung_mulai_tanggal'], 'safe'],

            //terhitung_sampai_tanggal
            [['terhitung_sampai_tanggal'], 'required'],
            [['terhitung_sampai_tanggal'], 'safe'],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'integer'],
            [['jenis_sk'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSk::className(), 'targetAttribute' => ['jenis_sk' => 'id']],

            //nomor_sk
            [['nomor_sk'], 'required'],
            [['nomor_sk'], 'string', 'max' => 256],

            //tanggal_sk
            [['tanggal_sk'], 'required'],
            [['tanggal_sk'], 'safe'],

            //pejabat_penetap_sk
            [['pejabat_penetap_sk'], 'required'],
            [['pejabat_penetap_sk'], 'string', 'max' => 256],

            //arsip_sk
            // [['arsip_sk'], 'required'],
            [['arsip_sk'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'jenis_pelaksana' => 'Jenis Pelaksana',
            'instansi' => 'Instansi',
            'unit_kerja' => 'Unit Kerja',
            'jenjang_jabatan' => 'Jenjang Jabatan',
            'nama_jabatan' => 'Nama Jabatan',
            'terhitung_mulai_tanggal' => 'Terhitung Mulai Tanggal',
            'terhitung_sampai_tanggal' => 'Terhitung Sampai Tanggal',
            'catatan' => 'Catatan',
            'jenis_sk' => 'Jenis Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_sk' => 'Tanggal Sk',
            'pejabat_penetap_sk' => 'Pejabat Penetap Sk',
            'arsip_sk' => 'Arsip Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatan()
    {
        return $this->hasOne(JenjangJabatan::className(), ['id' => 'jenjang_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSk()
    {
        return $this->hasOne(JenisSk::className(), ['id' => 'jenis_sk']);
    }
}
