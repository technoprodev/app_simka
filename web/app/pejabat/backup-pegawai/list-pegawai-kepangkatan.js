$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai-kepangkatan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('pejabat/datatables-pegawai-kepangkatan', {idPegawai : idPegawai}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: {
                            _: 'k.pangkat',
                            display: 'pangkat',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.golongan',
                            display: 'golongan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.ruang',
                            display: 'ruang',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 's.status_kepegawaian',
                            display: 'status_kepegawaian',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.terhitung_mulai_tanggal',
                            display: 'terhitung_mulai_tanggal',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.terhitung_sampai_tanggal',
                            display: 'terhitung_sampai_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + (row.terhitung_sampai_sekarang == 'Ya' ? 'Sekarang' : data) + '</span>' +
                                '</div>' +
                                '';
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jenis_sk',
                            display: 'jenis_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nomor_sk',
                            display: 'nomor_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tanggal_sk',
                            display: 'tanggal_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.pejabat_penetap_sk',
                            display: 'pejabat_penetap_sk',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});