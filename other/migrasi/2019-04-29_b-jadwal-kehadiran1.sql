DELIMITER $$

DROP FUNCTION IF EXISTS get_unit_kerja_rec;
CREATE FUNCTION get_unit_kerja_rec(root_id int) RETURNS text READS SQL DATA
BEGIN
  return (
    select group_concat(id) from (
      select id from unit_kerja where id=root_id union all
      select id from (
        select * from unit_kerja order by parent, id
      ) unit_kerja_sorted, (select @pv := root_id) initialisation
      where find_in_set(parent, @pv) and length(@pv := concat(@pv, ',', id))
    ) t
  );
END$$

DROP FUNCTION IF EXISTS get_pegawai_rec;
CREATE FUNCTION get_pegawai_rec(unit_kerja_id int) RETURNS text READS SQL DATA
BEGIN
  return (
    select group_concat(id) from pegawai
    where find_in_set(unit_kerja, get_unit_kerja_rec(unit_kerja_id))
  );
END$$

DROP FUNCTION IF EXISTS get_jadwal_masuk;
DROP FUNCTION IF EXISTS get_jadwal_pulang;

DROP FUNCTION IF EXISTS get_time_before;
CREATE FUNCTION get_time_before(
  tanggal date,
  minggu_pembanding time, minggu_time time, minggu_libur boolean,
  senin_pembanding time, senin_time time, senin_libur boolean,
  selasa_pembanding time, selasa_time time, selasa_libur boolean,
  rabu_pembanding time, rabu_time time, rabu_libur boolean,
  kamis_pembanding time, kamis_time time, kamis_libur boolean,
  jumat_pembanding time, jumat_time time, jumat_libur boolean,
  sabtu_pembanding time, sabtu_time time, sabtu_libur boolean
) RETURNS datetime
BEGIN
  return (
    case
      when DAYOFWEEK(tanggal)=1 then
        case
          when minggu_libur then null
          when minggu_time > minggu_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), minggu_time)
          else timestamp(tanggal, minggu_time)
        end
      when DAYOFWEEK(tanggal)=2 then
        case
          when senin_libur then null
          when senin_time > senin_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), senin_time)
          else timestamp(tanggal, senin_time)
        end
      when DAYOFWEEK(tanggal)=3 then
        case
          when selasa_libur then null
          when selasa_time > selasa_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), selasa_time)
          else timestamp(tanggal, selasa_time)
        end
      when DAYOFWEEK(tanggal)=4 then
        case
          when rabu_libur then null
          when rabu_time > rabu_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), rabu_time)
          else timestamp(tanggal, rabu_time)
        end
      when DAYOFWEEK(tanggal)=5 then
        case
          when kamis_libur then null
          when kamis_time > kamis_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), kamis_time)
          else timestamp(tanggal, kamis_time)
        end
      when DAYOFWEEK(tanggal)=6 then
        case
          when jumat_libur then null
          when jumat_time > jumat_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), jumat_time)
          else timestamp(tanggal, jumat_time)
        end
      when DAYOFWEEK(tanggal)=7 then
        case
          when sabtu_libur then null
          when sabtu_time > sabtu_pembanding then timestamp(SUBDATE(tanggal, INTERVAL 1 DAY), sabtu_time)
          else timestamp(tanggal, sabtu_time)
        end
    end
  );
END$$

DROP FUNCTION IF EXISTS get_time;
CREATE FUNCTION get_time(
  tanggal date,
  minggu_time time, minggu_libur boolean,
  senin_time time, senin_libur boolean,
  selasa_time time, selasa_libur boolean,
  rabu_time time, rabu_libur boolean,
  kamis_time time, kamis_libur boolean,
  jumat_time time, jumat_libur boolean,
  sabtu_time time, sabtu_libur boolean
) RETURNS datetime
BEGIN
  return (
    case
      when DAYOFWEEK(tanggal)=1 then
        case when minggu_libur then null else timestamp(tanggal, minggu_time) end
      when DAYOFWEEK(tanggal)=2 then
        case when senin_libur then null else timestamp(tanggal, senin_time) end
      when DAYOFWEEK(tanggal)=3 then
        case when selasa_libur then null else timestamp(tanggal, selasa_time) end
      when DAYOFWEEK(tanggal)=4 then
        case when rabu_libur then null else timestamp(tanggal, rabu_time) end
      when DAYOFWEEK(tanggal)=5 then
        case when kamis_libur then null else timestamp(tanggal, kamis_time) end
      when DAYOFWEEK(tanggal)=6 then
        case when jumat_libur then null else timestamp(tanggal, jumat_time) end
      when DAYOFWEEK(tanggal)=7 then
        case when sabtu_libur then null else timestamp(tanggal, sabtu_time) end
    end
  );
END$$

DROP FUNCTION IF EXISTS get_time_after;
CREATE FUNCTION get_time_after(
  tanggal date,
  minggu_pembanding time, minggu_time time, minggu_libur boolean,
  senin_pembanding time, senin_time time, senin_libur boolean,
  selasa_pembanding time, selasa_time time, selasa_libur boolean,
  rabu_pembanding time, rabu_time time, rabu_libur boolean,
  kamis_pembanding time, kamis_time time, kamis_libur boolean,
  jumat_pembanding time, jumat_time time, jumat_libur boolean,
  sabtu_pembanding time, sabtu_time time, sabtu_libur boolean
) RETURNS datetime
BEGIN
  return (
    case
      when DAYOFWEEK(tanggal)=1 then
        case
          when minggu_libur then null
          when minggu_time >= minggu_pembanding then timestamp(tanggal, minggu_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), minggu_time)
        end
      when DAYOFWEEK(tanggal)=2 then
        case
          when senin_libur then null
          when senin_time >= senin_pembanding then timestamp(tanggal, senin_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), senin_time)
        end
      when DAYOFWEEK(tanggal)=3 then
        case
          when selasa_libur then null
          when selasa_time >= selasa_pembanding then timestamp(tanggal, selasa_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), selasa_time)
        end
      when DAYOFWEEK(tanggal)=4 then
        case
          when rabu_libur then null
          when rabu_time >= rabu_pembanding then timestamp(tanggal, rabu_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), rabu_time)
        end
      when DAYOFWEEK(tanggal)=5 then
        case
          when kamis_libur then null
          when kamis_time >= kamis_pembanding then timestamp(tanggal, kamis_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), kamis_time)
        end
      when DAYOFWEEK(tanggal)=6 then
        case
          when jumat_libur then null
          when jumat_time >= jumat_pembanding then timestamp(tanggal, jumat_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), jumat_time)
        end
      when DAYOFWEEK(tanggal)=7 then
        case
          when sabtu_libur then null
          when sabtu_time >= sabtu_pembanding then timestamp(tanggal, sabtu_time)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), sabtu_time)
        end
    end
  );
END$$

/*DROP PROCEDURE IF EXISTS apply_jk_global;
CREATE PROCEDURE apply_jk_global(id int)
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_fleksi_time int;
  declare v_waktu_mulai_istirahat time;
  declare v_waktu_selesai_istirahat time;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_awal_masuk time;
  declare v_senin_batas_akhir_masuk time;
  declare v_senin_batas_awal_pulang time;
  declare v_senin_batas_akhir_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_awal_masuk time;
  declare v_selasa_batas_akhir_masuk time;
  declare v_selasa_batas_awal_pulang time;
  declare v_selasa_batas_akhir_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_awal_masuk time;
  declare v_rabu_batas_akhir_masuk time;
  declare v_rabu_batas_awal_pulang time;
  declare v_rabu_batas_akhir_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_awal_masuk time;
  declare v_kamis_batas_akhir_masuk time;
  declare v_kamis_batas_awal_pulang time;
  declare v_kamis_batas_akhir_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_awal_masuk time;
  declare v_jumat_batas_akhir_masuk time;
  declare v_jumat_batas_awal_pulang time;
  declare v_jumat_batas_akhir_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_awal_masuk time;
  declare v_sabtu_batas_akhir_masuk time;
  declare v_sabtu_batas_awal_pulang time;
  declare v_sabtu_batas_akhir_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_awal_masuk time;
  declare v_minggu_batas_akhir_masuk time;
  declare v_minggu_batas_awal_pulang time;
  declare v_minggu_batas_akhir_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      p.id as pegawai, jk.berlaku_dari, jk.berlaku_sampai, jk.fleksi_time, jk.waktu_mulai_istirahat, jk.waktu_selesai_istirahat,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_awal_masuk, jk.senin_batas_akhir_masuk, jk.senin_batas_awal_pulang, jk.senin_batas_akhir_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_awal_masuk, jk.selasa_batas_akhir_masuk, jk.selasa_batas_awal_pulang, jk.selasa_batas_akhir_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_awal_masuk, jk.rabu_batas_akhir_masuk, jk.rabu_batas_awal_pulang, jk.rabu_batas_akhir_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_awal_masuk, jk.kamis_batas_akhir_masuk, jk.kamis_batas_awal_pulang, jk.kamis_batas_akhir_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_awal_masuk, jk.jumat_batas_akhir_masuk, jk.jumat_batas_awal_pulang, jk.jumat_batas_akhir_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_awal_masuk, jk.sabtu_batas_akhir_masuk, jk.sabtu_batas_awal_pulang, jk.sabtu_batas_akhir_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_awal_masuk, jk.minggu_batas_akhir_masuk, jk.minggu_batas_awal_pulang, jk.minggu_batas_akhir_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    left join pegawai p on true
    where jk.berlaku_bagi='Seluruh Unit Kerja' and jk.id = id;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Seluruh Unit Kerja'
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
    and jk.id = id
  set
    k.fleksi_time=jk.fleksi_time,
    jadwal_masuk=get_time(
      k.tanggal,
      minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_masuk=get_time_before(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_masuk=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_pulang, sabtu_libur='Ya'
    ),
    keterangan=case
      when keterangan is null then
      case
        when DAYOFWEEK(tanggal)=1 and minggu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=2 and senin_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=3 and selasa_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=4 and rabu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=5 and kamis_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=6 and jumat_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=7 and sabtu_libur='Ya' then 7
      end
      else keterangan
    end,
    k.waktu_mulai_istirahat=timestamp(k.tanggal, jk.waktu_mulai_istirahat),
    k.waktu_selesai_istirahat=timestamp(k.tanggal, jk.waktu_selesai_istirahat)
  ;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_fleksi_time, v_waktu_mulai_istirahat, v_waktu_selesai_istirahat,
      v_senin_masuk, v_senin_pulang, v_senin_batas_awal_masuk, v_senin_batas_akhir_masuk, v_senin_batas_awal_pulang, v_senin_batas_akhir_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_awal_masuk, v_selasa_batas_akhir_masuk, v_selasa_batas_awal_pulang, v_selasa_batas_akhir_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_awal_masuk, v_rabu_batas_akhir_masuk, v_rabu_batas_awal_pulang, v_rabu_batas_akhir_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_awal_masuk, v_kamis_batas_akhir_masuk, v_kamis_batas_awal_pulang, v_kamis_batas_akhir_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_awal_masuk, v_jumat_batas_akhir_masuk, v_jumat_batas_awal_pulang, v_jumat_batas_akhir_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_awal_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_batas_awal_pulang, v_sabtu_batas_akhir_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_awal_masuk, v_minggu_batas_akhir_masuk, v_minggu_batas_awal_pulang, v_minggu_batas_akhir_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, fleksi_time, waktu_mulai_istirahat, waktu_selesai_istirahat, jadwal_masuk, jadwal_pulang, jadwal_batas_awal_masuk, jadwal_batas_akhir_masuk, jadwal_batas_awal_pulang, jadwal_batas_akhir_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          v_fleksi_time, timestamp(v_tanggal, v_waktu_mulai_istirahat), timestamp(v_tanggal, v_waktu_selesai_istirahat),
          get_time(
            v_tanggal,
            v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_time_before(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_pulang, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 7
              else 1
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DROP PROCEDURE IF EXISTS apply_jk_pegawai;
CREATE PROCEDURE apply_jk_pegawai()
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_fleksi_time int;
  declare v_waktu_mulai_istirahat time;
  declare v_waktu_selesai_istirahat time;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_awal_masuk time;
  declare v_senin_batas_akhir_masuk time;
  declare v_senin_batas_awal_pulang time;
  declare v_senin_batas_akhir_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_awal_masuk time;
  declare v_selasa_batas_akhir_masuk time;
  declare v_selasa_batas_awal_pulang time;
  declare v_selasa_batas_akhir_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_awal_masuk time;
  declare v_rabu_batas_akhir_masuk time;
  declare v_rabu_batas_awal_pulang time;
  declare v_rabu_batas_akhir_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_awal_masuk time;
  declare v_kamis_batas_akhir_masuk time;
  declare v_kamis_batas_awal_pulang time;
  declare v_kamis_batas_akhir_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_awal_masuk time;
  declare v_jumat_batas_akhir_masuk time;
  declare v_jumat_batas_awal_pulang time;
  declare v_jumat_batas_akhir_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_awal_masuk time;
  declare v_sabtu_batas_akhir_masuk time;
  declare v_sabtu_batas_awal_pulang time;
  declare v_sabtu_batas_akhir_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_awal_masuk time;
  declare v_minggu_batas_akhir_masuk time;
  declare v_minggu_batas_awal_pulang time;
  declare v_minggu_batas_akhir_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      pegawai, jk.berlaku_dari, jk.berlaku_sampai, jk.fleksi_time, jk.waktu_mulai_istirahat, jk.waktu_selesai_istirahat,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_awal_masuk, jk.senin_batas_akhir_masuk, jk.senin_batas_awal_pulang, jk.senin_batas_akhir_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_awal_masuk, jk.selasa_batas_akhir_masuk, jk.selasa_batas_awal_pulang, jk.selasa_batas_akhir_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_awal_masuk, jk.rabu_batas_akhir_masuk, jk.rabu_batas_awal_pulang, jk.rabu_batas_akhir_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_awal_masuk, jk.kamis_batas_akhir_masuk, jk.kamis_batas_awal_pulang, jk.kamis_batas_akhir_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_awal_masuk, jk.jumat_batas_akhir_masuk, jk.jumat_batas_awal_pulang, jk.jumat_batas_akhir_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_awal_masuk, jk.sabtu_batas_akhir_masuk, jk.sabtu_batas_awal_pulang, jk.sabtu_batas_akhir_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_awal_masuk, jk.minggu_batas_akhir_masuk, jk.minggu_batas_awal_pulang, jk.minggu_batas_akhir_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    inner join pegawai p on p.id=jk.pegawai
    where jk.berlaku_bagi='Pegawai Tertentu'
      and jk.pegawai is not null and jk.id = id;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Pegawai Tertentu'
    and jk.pegawai is not null
    and k.pegawai=jk.pegawai
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
  set
    k.fleksi_time=jk.fleksi_time,
    jadwal_masuk=get_time(
      k.tanggal,
      minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_masuk=get_time_before(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_masuk=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_pulang, sabtu_libur='Ya'
    ),
    keterangan=case
      when keterangan is null then
      case
        when DAYOFWEEK(tanggal)=1 and minggu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=2 and senin_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=3 and selasa_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=4 and rabu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=5 and kamis_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=6 and jumat_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=7 and sabtu_libur='Ya' then 7
      end
      else keterangan
    end,
    k.waktu_mulai_istirahat=timestamp(k.tanggal, jk.waktu_mulai_istirahat),
    k.waktu_selesai_istirahat=timestamp(k.tanggal, jk.waktu_selesai_istirahat)
  ;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_fleksi_time, v_waktu_mulai_istirahat, v_waktu_selesai_istirahat,
      v_senin_masuk, v_senin_pulang, v_senin_batas_awal_masuk, v_senin_batas_akhir_masuk, v_senin_batas_awal_pulang, v_senin_batas_akhir_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_awal_masuk, v_selasa_batas_akhir_masuk, v_selasa_batas_awal_pulang, v_selasa_batas_akhir_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_awal_masuk, v_rabu_batas_akhir_masuk, v_rabu_batas_awal_pulang, v_rabu_batas_akhir_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_awal_masuk, v_kamis_batas_akhir_masuk, v_kamis_batas_awal_pulang, v_kamis_batas_akhir_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_awal_masuk, v_jumat_batas_akhir_masuk, v_jumat_batas_awal_pulang, v_jumat_batas_akhir_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_awal_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_batas_awal_pulang, v_sabtu_batas_akhir_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_awal_masuk, v_minggu_batas_akhir_masuk, v_minggu_batas_awal_pulang, v_minggu_batas_akhir_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, fleksi_time, waktu_mulai_istirahat, waktu_selesai_istirahat, jadwal_masuk, jadwal_pulang, jadwal_batas_awal_masuk, jadwal_batas_akhir_masuk, jadwal_batas_awal_pulang, jadwal_batas_akhir_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          v_fleksi_time, timestamp(v_tanggal, v_waktu_mulai_istirahat), timestamp(v_tanggal, v_waktu_selesai_istirahat),
          get_time(
            v_tanggal,
            v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_time_before(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_pulang, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 7
              else 1
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DROP PROCEDURE IF EXISTS apply_jk_unit_kerja;
CREATE PROCEDURE apply_jk_unit_kerja()
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_fleksi_time int;
  declare v_waktu_mulai_istirahat time;
  declare v_waktu_selesai_istirahat time;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_awal_masuk time;
  declare v_senin_batas_akhir_masuk time;
  declare v_senin_batas_awal_pulang time;
  declare v_senin_batas_akhir_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_awal_masuk time;
  declare v_selasa_batas_akhir_masuk time;
  declare v_selasa_batas_awal_pulang time;
  declare v_selasa_batas_akhir_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_awal_masuk time;
  declare v_rabu_batas_akhir_masuk time;
  declare v_rabu_batas_awal_pulang time;
  declare v_rabu_batas_akhir_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_awal_masuk time;
  declare v_kamis_batas_akhir_masuk time;
  declare v_kamis_batas_awal_pulang time;
  declare v_kamis_batas_akhir_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_awal_masuk time;
  declare v_jumat_batas_akhir_masuk time;
  declare v_jumat_batas_awal_pulang time;
  declare v_jumat_batas_akhir_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_awal_masuk time;
  declare v_sabtu_batas_akhir_masuk time;
  declare v_sabtu_batas_awal_pulang time;
  declare v_sabtu_batas_akhir_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_awal_masuk time;
  declare v_minggu_batas_akhir_masuk time;
  declare v_minggu_batas_awal_pulang time;
  declare v_minggu_batas_akhir_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      p.id as pegawai, jk.berlaku_dari, jk.berlaku_sampai, jk.fleksi_time, jk.waktu_mulai_istirahat, jk.waktu_selesai_istirahat,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_awal_masuk, jk.senin_batas_akhir_masuk, jk.senin_batas_awal_pulang, jk.senin_batas_akhir_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_awal_masuk, jk.selasa_batas_akhir_masuk, jk.selasa_batas_awal_pulang, jk.selasa_batas_akhir_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_awal_masuk, jk.rabu_batas_akhir_masuk, jk.rabu_batas_awal_pulang, jk.rabu_batas_akhir_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_awal_masuk, jk.kamis_batas_akhir_masuk, jk.kamis_batas_awal_pulang, jk.kamis_batas_akhir_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_awal_masuk, jk.jumat_batas_akhir_masuk, jk.jumat_batas_awal_pulang, jk.jumat_batas_akhir_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_awal_masuk, jk.sabtu_batas_akhir_masuk, jk.sabtu_batas_awal_pulang, jk.sabtu_batas_akhir_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_awal_masuk, jk.minggu_batas_akhir_masuk, jk.minggu_batas_awal_pulang, jk.minggu_batas_akhir_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    inner join unit_kerja uk on uk.id=jk.unit_kerja
    inner join pegawai p on find_in_set(p.unit_kerja, get_unit_kerja_rec(jk.unit_kerja))
    where jk.berlaku_bagi='Unit Kerja Tertentu'
      and jk.unit_kerja is not null and jk.id = id;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Unit Kerja Tertentu'
    and jk.unit_kerja is not null
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
  inner join pegawai p on p.id=k.pegawai
    and find_in_set(p.unit_kerja, get_unit_kerja_rec(jk.unit_kerja))
  set
    k.fleksi_time=jk.fleksi_time,
    jadwal_masuk=get_time(
      k.tanggal,
      minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_masuk=get_time_before(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_masuk=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_awal_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_awal_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_awal_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_awal_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_awal_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_awal_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_awal_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_awal_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_akhir_pulang=get_time_after(
      k.tanggal,
      minggu_masuk, minggu_batas_akhir_pulang, minggu_libur='Ya',
      senin_masuk, senin_batas_akhir_pulang, senin_libur='Ya',
      selasa_masuk, selasa_batas_akhir_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_batas_akhir_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_batas_akhir_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_batas_akhir_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_akhir_pulang, sabtu_libur='Ya'
    ),
    keterangan=case
      when keterangan is null then
      case
        when DAYOFWEEK(tanggal)=1 and minggu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=2 and senin_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=3 and selasa_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=4 and rabu_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=5 and kamis_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=6 and jumat_libur='Ya' then 7
        when DAYOFWEEK(tanggal)=7 and sabtu_libur='Ya' then 7
      end
      else keterangan
    end,
    k.waktu_mulai_istirahat=timestamp(k.tanggal, jk.waktu_mulai_istirahat),
    k.waktu_selesai_istirahat=timestamp(k.tanggal, jk.waktu_selesai_istirahat)
  ;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_fleksi_time, v_waktu_mulai_istirahat, v_waktu_selesai_istirahat,
      v_senin_masuk, v_senin_pulang, v_senin_batas_awal_masuk, v_senin_batas_akhir_masuk, v_senin_batas_awal_pulang, v_senin_batas_akhir_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_awal_masuk, v_selasa_batas_akhir_masuk, v_selasa_batas_awal_pulang, v_selasa_batas_akhir_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_awal_masuk, v_rabu_batas_akhir_masuk, v_rabu_batas_awal_pulang, v_rabu_batas_akhir_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_awal_masuk, v_kamis_batas_akhir_masuk, v_kamis_batas_awal_pulang, v_kamis_batas_akhir_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_awal_masuk, v_jumat_batas_akhir_masuk, v_jumat_batas_awal_pulang, v_jumat_batas_akhir_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_awal_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_batas_awal_pulang, v_sabtu_batas_akhir_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_awal_masuk, v_minggu_batas_akhir_masuk, v_minggu_batas_awal_pulang, v_minggu_batas_akhir_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, fleksi_time, waktu_mulai_istirahat, waktu_selesai_istirahat, jadwal_masuk, jadwal_pulang, jadwal_batas_awal_masuk, jadwal_batas_akhir_masuk, jadwal_batas_awal_pulang, jadwal_batas_akhir_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          v_fleksi_time, timestamp(v_tanggal, v_waktu_mulai_istirahat), timestamp(v_tanggal, v_waktu_selesai_istirahat),
          get_time(
            v_tanggal,
            v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_time_before(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_masuk, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_awal_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_awal_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_awal_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_awal_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_awal_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_awal_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_awal_pulang, v_sabtu_libur='Ya'
          ),
          get_time_after(
            v_tanggal,
            v_minggu_masuk, v_minggu_batas_akhir_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_akhir_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_akhir_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_akhir_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_akhir_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_akhir_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_akhir_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 7
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 7
              else 1
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$*/

DROP PROCEDURE IF EXISTS apply_jk;
CREATE PROCEDURE apply_jk()
BEGIN
  call apply_jk_global();
  call apply_jk_unit_kerja();
  call apply_jk_pegawai();
END$$

DELIMITER ;