<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\BuktiKehadiran;
use app_simka\models\BuktiKehadiranPegawai;
use app_simka\models\BuktiCctv;
use app_simka\models\FilterKehadiran;

class AdministrasiController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['administrasi']]
            ]),
        ];
    }

    public function actionDatatablesBuktiKehadiran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'bk.id',
                'bk.status_pengajuan',
                'kh.keterangan AS keterangan',
                'DATE_FORMAT(bk.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(bk.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'bk.nomor_surat',
                'DATE_FORMAT(bk.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
            ])
            ->from('bukti_kehadiran bk')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = bk.keterangan_kehadiran')
            ->where([
                'bk.diajukan' => Yii::$app->user->identity->id,
                'kh.diajukan_administrasi' => 'Ya',
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), BuktiKehadiran::getDb());
    }

    public function actionBuktiKehadiran($id = null)
    {
        if (!$id) {
            return $this->render('list-bukti-kehadiran', [
                'title' => 'Status Pengajuan Bukti Kehadiran',
            ]);
        }

        if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-bukti-kehadiran', [
            'model' => $model,
            'title' => 'Detail Bukti Kehadiran',
        ]);
    }

    public function actionBuktiKehadiranCreate()
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_kehadiran'] = new BuktiKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bukti_kehadiran']->load($post);
            if (isset($post['BuktiKehadiranPegawai'])) {
                foreach ($post['BuktiKehadiranPegawai'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']])->one();
                        $buktiKehadiranPegawai->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']*-1])->one();
                        $buktiKehadiranPegawai->isDeleted = true;
                    } else {
                        $buktiKehadiranPegawai = new BuktiKehadiranPegawai();
                        $buktiKehadiranPegawai->setAttributes($value);
                    }
                    $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
                }
            }

            $transaction['bukti_kehadiran'] = BuktiKehadiran::getDb()->beginTransaction();

            try {
                $model['bukti_kehadiran']->status_pengajuan = 'Diajukan';
                $model['bukti_kehadiran']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_kehadiran']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if (isset($model['bukti_kehadiran_pegawai']) and is_array($model['bukti_kehadiran_pegawai'])) {
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        $buktiKehadiranPegawai->bukti_kehadiran = $model['bukti_kehadiran']->id;
                        if (!$buktiKehadiranPegawai->isDeleted && !$buktiKehadiranPegawai->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        if ($buktiKehadiranPegawai->isDeleted) {
                            if (!$buktiKehadiranPegawai->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$buktiKehadiranPegawai->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                if (YII_ENV == 'prod' && false) {
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-kehadiran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Kehadiran')->send();
                } else if (YII_ENV == 'dev' && false) {
                    /*return $this->render('//email/verifikasi-pengajuan-bukti-kehadiran', [
                        'model' => $model,
                        'title' => 'Segera Verifikasi Pengajuan Bukti Kehadiran',
                    ]);*/
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-kehadiran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Kehadiran')->send();
                }


                $transaction['bukti_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_kehadiran']->isNewRecord) {
            }

            foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai)
                $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
        }

        if ($error)
            return $this->render('form-bukti-kehadiran', [
                'model' => $model,
                'title' => 'Unggah Bukti Kehadiran',
            ]);
        else
            return $this->redirect(['bukti-kehadiran']);
    }

    public function actionBuktiKehadiranUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_kehadiran'] = new BuktiKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            // ddx($post);

            $model['bukti_kehadiran']->load($post);
            if (isset($post['BuktiKehadiranPegawai'])) {
                foreach ($post['BuktiKehadiranPegawai'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']])->one();
                        $buktiKehadiranPegawai->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']*-1])->one();
                        $buktiKehadiranPegawai->isDeleted = true;
                    } else {
                        $buktiKehadiranPegawai = new BuktiKehadiranPegawai();
                        $buktiKehadiranPegawai->setAttributes($value);
                    }
                    $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
                }
            }

            $transaction['bukti_kehadiran'] = BuktiKehadiran::getDb()->beginTransaction();

            try {
                $model['bukti_kehadiran']->status_pengajuan = 'Diajukan';
                $model['bukti_kehadiran']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_kehadiran']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if (isset($model['bukti_kehadiran_pegawai']) and is_array($model['bukti_kehadiran_pegawai'])) {
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        $buktiKehadiranPegawai->bukti_kehadiran = $model['bukti_kehadiran']->id;
                        if (!$buktiKehadiranPegawai->isDeleted && !$buktiKehadiranPegawai->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        if ($buktiKehadiranPegawai->isDeleted) {
                            if (!$buktiKehadiranPegawai->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$buktiKehadiranPegawai->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['bukti_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_kehadiran']->isNewRecord) {
            }

            foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai)
                $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
        }

        if ($error)
            return $this->render('form-bukti-kehadiran', [
                'model' => $model,
                'title' => 'Revisi Bukti Kehadiran',
            ]);
        else
            return $this->redirect(['bukti-kehadiran']);
    }

    public function actionBuktiKehadiranDelete($id)
    {
        $transaction['bukti_kehadiran'] = BuktiKehadiran::getDb()->beginTransaction();

        try {
            foreach (BuktiKehadiranPegawai::find()->where(['bukti_kehadiran' => $id])->all() as $key => $value) {
                $value->delete();
            }
            BuktiKehadiran::find()->where(['id' => $id])->one()->delete();
            
            $transaction['bukti_kehadiran']->commit();
            Yii::$app->session->setFlash('success', 'Data has been deleted.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['bukti_kehadiran']->rollBack();
            if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
        }

        return $this->redirect(['bukti-kehadiran']);
    }

    ///

    public function datatablesKehadiran($idPegawai, $bulan, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'k.id',
                'DATE_FORMAT(k.tanggal, "%d %b") AS tanggal',
                'DATE_FORMAT(k.jadwal_masuk, "%H:%i:%s") AS jadwal_masuk',
                'DATE_FORMAT(k.jadwal_pulang, "%H:%i:%s") AS jadwal_pulang',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_masuk, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_masuk',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_pulang, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_pulang',
                'DATE_FORMAT(k.waktu_terakhir_diupdate, "%H:%i:%s") AS waktu_terakhir_diupdate',
                '@telat_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS telat_detik',
                '@kecepetan_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kecepetan_detik',
                'TIME_FORMAT(SEC_TO_TIME(@telat_detik), "%H:%i:%s") AS telat',
                'CASE WHEN @kecepetan_detik > 0 THEN TIME_FORMAT(SEC_TO_TIME(@kecepetan_detik), "%H:%i:%s") ELSE NULL END AS kecepetan',
                'LEAST(
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END +
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END
                , 100) AS potongan_tunkin',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(TIMEDIFF(TIMEDIFF(k.waktu_pulang, k.waktu_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)), "%H:%i:%s")
                    ELSE NULL
                END AS jumlah_jam',
                '@kurang_detik := CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'k.keterangan as idKeterangan',
                'kh.color as color',
                'kh.keterangan as keterangan',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
        ;

        return $query;
    }

    public function datatablesKehadiranTahunan($idPegawai, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN 0
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE 0
                END) AS kurang_detik',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->andWhere([
                'between', 'MONTH(k.tanggal)', '01', date('m'),
            ])
            ->groupBy(['k.pegawai'])
        ;

        return $query;
    }

    public function actionDatatablesKehadiran($idPegawai, $bulan, $tahun)
    {
        return $this->datatables($this->datatablesKehadiran($idPegawai, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function datatablesKehadiranBulanan($idPegawai = null, $bulan, $tahun)
    {
        $subQuery = new \yii\db\Query();
        $subQuery
            ->select([
                'p.id',
                'p.nama',
                'p.nip',
                'p.pin',
                'uk.unit_kerja',
                'LEAST(
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END) +
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END)
                , 100) AS potongan_tunkin',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS telat',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS kecepetan',
                'TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(k.waktu_pulang, k.waktu_masuk)))), "%H:%i:%s") AS jumlah_jam',
                '@kurang_detik := SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END) AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'count(case when kh.dihitung_hadir = "Hadir" then k.id end) AS hadir',
                'count(case when kh.dihitung_hadir = "Tidak Hadir" then k.id end) AS tidak_hadir',
                'count(case when kh.id = 1 then k.id end) AS belum_absen',
                'count(case when kh.id = 7 then k.id end) AS libur',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'kehadiran k', 'k.pegawai = p.id')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->where([
                // 'p.id' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->orderBy(['uk.level' => SORT_ASC])
            ->groupBy(['p.id', 'p.nama', 'p.nip', 'p.pin'])
        ;
        $uk = Yii::$app->request->isPost ? Yii::$app->request->post()['columns'][4]['search']['value'] : '';
        if ($uk) {
            $ukIdMentah = (new \yii\db\Query())->select('id')->from('unit_kerja uk')->where(['like', 'unit_kerja', $uk])->orderBy(['level' => SORT_ASC])->scalar();
            $result = (new \yii\db\Query())->select('get_pegawai_rec(' . $ukIdMentah . ')')->scalar();
            $pegawais = $result ? explode(',', $result) : [];
            if ($pegawais) {
                $subQuery->andWhere(['p.id' => $pegawais]);
            }
        }
        if ($idPegawai) {
            $subQuery->andWhere(['p.id' => $idPegawai]);
        }
        if ($uka = Yii::$app->user->identity->pegawai->unit_kerja_administrasi) {
            $result = (new \yii\db\Query())->select('get_pegawai_rec(' . $uka . ')')->scalar();
            $pegawais = $result ? explode(',', $result) : [];
            if ($pegawais) {
                $subQuery->andWhere(['p.id' => $pegawais]);
            }
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'nama',
                'nip',
                'pin',
                'unit_kerja',
                'potongan_tunkin',
                'telat',
                'kecepetan',
                'jumlah_jam',
                'kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(kurang_detik), "%H:%i:%s") AS kurang_jam',
                '(hadir + tidak_hadir + belum_absen) AS hari_kerja',
                'hadir',
                'tidak_hadir',
                'belum_absen',
                'libur',
            ])
            ->from(['sq' => $subQuery])
        ;

        return $query;
        // return $this->json($query->all());
    }

    public function actionDatatablesKehadiranBulanan(/*$idPegawai, */$bulan, $tahun)
    {
        return $this->datatables($this->datatablesKehadiranBulanan(null, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionKehadiran($id = null, $bulan = null, $tahun = null)
    {
        $model['filter_kehadiran'] = new FilterKehadiran();
        if ($bulan) {
            $model['filter_kehadiran']->bulan = $bulan;
        }
        if ($tahun) {
            $model['filter_kehadiran']->tahun = $tahun;
        }
        
        if (!$id) {
            return $this->render('list-kehadiran-bulanan', [
                'model' => $model,
                'title' => 'Daftar Kehadiran Pegawai',
            ]);
        }

        if (($model['pegawai'] = Pegawai::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $model['kehadiran_bulanan'] = $this->datatablesKehadiranBulanan($model['pegawai']->id, $model['filter_kehadiran']->bulan, $model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_bulanan'] = [
            'hari_kerja' => $model['kehadiran_bulanan']['hari_kerja'],
            'hadir' => $model['kehadiran_bulanan']['hadir'],
            'tidak_hadir' => $model['kehadiran_bulanan']['tidak_hadir'],
            'belum_absen' => $model['kehadiran_bulanan']['belum_absen'],
            'libur' => $model['kehadiran_bulanan']['libur'],
            'potongan_tunkin' => $model['kehadiran_bulanan']['potongan_tunkin'],
            'uang_makan' => $model['kehadiran_bulanan']['hadir'],
            'disiplin' => $model['kehadiran_bulanan']['kurang_detik'],
        ];

        $model['kehadiran_tahunan'] = $this->datatablesKehadiranTahunan($model['pegawai']->id,$model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_tahunan'] = [
            'disiplin' => $model['kehadiran_tahunan']['kurang_detik'],
        ];

        return $this->render('list-kehadiran', [
            'model' => $model,
            'title' => 'Daftar Kehadiran : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
        ]);
    }

    ///
    public function actionDatatablesBuktiCctv()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'bc.id',
                'bc.status_pengajuan',
                'bc.jenis AS jenis',
                'DATE_FORMAT(bc.tanggal, "%d/%m/%Y") AS tanggal',
                'DATE_FORMAT(bc.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
            ])
            ->from('bukti_cctv bc')
            ->where([
                'bc.diajukan' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), BuktiCctv::getDb());
    }

    public function actionBuktiCctv($id = null)
    {
        if (!$id) {
            return $this->render('list-bukti-cctv', [
                'title' => 'Status Pengajuan Bukti Cctv',
            ]);
        }

        if (($model['bukti_cctv'] = BuktiCctv::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-bukti-cctv', [
            'model' => $model,
            'title' => 'Detail Bukti Cctv',
        ]);
    }

    public function actionBuktiCctvCreate()
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_cctv'] = BuktiCctv::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_cctv'] = new BuktiCctv();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bukti_cctv']->load($post);

            $transaction['bukti_cctv'] = BuktiCctv::getDb()->beginTransaction();

            try {
                $model['bukti_cctv']->status_pengajuan = 'Diajukan';
                $model['bukti_cctv']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_cctv']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_cctv']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if (YII_ENV == 'prod' && false) {
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-cctv', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Cctv')->send();
                } else if (YII_ENV == 'dev' && false) {
                    /*return $this->render('//email/verifikasi-pengajuan-bukti-cctv', [
                        'model' => $model,
                        'title' => 'Segera Verifikasi Pengajuan Bukti Cctv',
                    ]);*/
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-cctv', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Cctv')->send();
                }


                $transaction['bukti_cctv']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_cctv']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_cctv']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-bukti-cctv', [
                'model' => $model,
                'title' => 'Unggah Bukti Cctv',
            ]);
        else
            return $this->redirect(['bukti-cctv']);
    }

    public function actionBuktiCctvUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_cctv'] = BuktiCctv::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_cctv'] = new BuktiCctv();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bukti_cctv']->load($post);

            $transaction['bukti_cctv'] = BuktiCctv::getDb()->beginTransaction();

            try {
                $model['bukti_cctv']->status_pengajuan = 'Diajukan';
                $model['bukti_cctv']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_cctv']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_cctv']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['bukti_cctv']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_cctv']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_cctv']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-bukti-cctv', [
                'model' => $model,
                'title' => 'Revisi Bukti Cctv',
            ]);
        else
            return $this->redirect(['bukti-cctv']);
    }

    ///
}
