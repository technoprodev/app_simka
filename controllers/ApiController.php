<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\UnitKerja;
use app_simka\models\Login;

class ApiController extends Controller
{
    public function validate($token)
    {
        switch ($token) {
            case '7va9dfnf9v7df9av8sd7f9':
                return true;
                break;
            
            default:
                return false;
                break;
        }
    }

    public function actionLogin($token, $username, $password)
    {
        if (!$this->validate($token)) exit;

        $model['login'] = new Login(['scenario' => 'using-login']);
        $model['login']->login = $username;
        $model['login']->password = $password;

        $login = $model['login']->login();

        if ($login) {
            $user = User::findByUsername($username);

            $query = '
                SELECT
                    p.nama,
                    p.nip,
                    p.pin,
                    sk.status_kepegawaian ,
                    CONCAT(k.pangkat, " (", k.golongan, " / ", k.ruang, ")") AS kepangkatan,
                    jj.jenjang_jabatan ,
                    p.nama_jabatan,
                    uk.unit_kerja AS unit_kerja ,
                    p.unit_kerja AS id_unit_kerja,
                    e.eselon ,
                    g.grade ,
                    kp.kedudukan_pegawai ,
                    p.mendapat_tunkin,
                    p.nama_panggilan,
                    p.jenis_kelamin,
                    p.tempat_lahir,
                    p.tanggal_lahir,
                    a.agama ,
                    sm.status_marital ,
                    p.golongan_darah,
                    jp.jenjang_pendidikan ,
                    p.gelar_depan,
                    p.gelar_belakang,
                    p.telpon,
                    p.email_kantor,
                    p.email_pribadi,
                    p.nik,
                    p.npwp,
                    p.karpeg,
                    p.karis_atau_karsu,
                    p.taspen,
                    p.poto,
                    p.alamat_sekarang,
                    p.kelurahan_sekarang,
                    p.kode_pos_sekarang,
                    p.alamat_ktp,
                    p.kelurahan_ktp,
                    p.kode_pos_ktp,
                    p.tinggi_badan_cm,
                    p.berat_badan_kg,
                    mr.model_rambut ,
                    bw.bentuk_wajah ,
                    wk.warna_kulit ,
                    p.hobi,
                    p.nama_pasangan,
                    p.tempat_lahir_pasangan,
                    p.tanggal_lahir_pasangan,
                    a1.agama AS agama_pasangan ,
                    jp1.jenjang_pendidikan AS agama_pasangan ,
                    p1.pekerjaan AS pekerjaan_ngan ,
                    p.status_hidup_pasangan,
                    p.telpon_pasangan,
                    p.nama_ayah,
                    p.tempat_lahir_ayah,
                    p.tanggal_lahir_ayah,
                    a2.agama AS agama_ayah ,
                    jp2.jenjang_pendidikan AS agama_ayah ,
                    p2.pekerjaan AS pekerjaan_ayah ,
                    p.status_hidup_ayah,
                    p.telpon_ayah,
                    p.alamat_ayah,
                    p.kelurahan_ayah,
                    p.kode_pos_ayah,
                    p.nama_ibu,
                    p.tempat_lahir_ibu,
                    p.tanggal_lahir_ibu,
                    a3.agama AS agama_ibu ,
                    jp3.jenjang_pendidikan AS agama_ibu ,
                    p3.pekerjaan AS pekerjaan__ibu ,
                    p.status_hidup_ibu,
                    p.telpon_ibu,
                    p.alamat_ibu,
                    p.kelurahan_ibu,
                    p.kode_pos_ibu,
                    p.pimpinan_unit_kerja,
                    uk1.unit_kerja AS unit_kerja_administrasi ,
                    p.tunkin_nama,
                    NULL AS tanda_tangan,
                    p.tunkin_nip
                FROM pegawai p
                JOIN user u ON u.id = p.id
                LEFT JOIN status_kepegawaian sk ON sk.id = p.status_kepegawaian
                LEFT JOIN kepangkatan k ON k.id = p.kepangkatan
                LEFT JOIN jenjang_jabatan jj ON jj.id = p.jenjang_jabatan
                LEFT JOIN unit_kerja uk ON uk.id = p.unit_kerja
                LEFT JOIN eselon e ON e.id = p.eselon
                LEFT JOIN grade g ON g.id = p.grade
                LEFT JOIN kedudukan_pegawai kp ON kp.id = p.kedudukan_pegawai
                LEFT JOIN agama a ON a.id = p.agama
                LEFT JOIN status_marital sm ON sm.id = p.status_marital
                LEFT JOIN jenjang_pendidikan jp ON jp.id = p.jenjang_pendidikan_terakhir
                LEFT JOIN model_rambut mr ON mr.id = p.model_rambut
                LEFT JOIN bentuk_wajah bw ON bw.id = p.bentuk_wajah
                LEFT JOIN warna_kulit wk ON wk.id = p.warna_kulit
                LEFT JOIN agama a1 ON a1.id = p.agama_pasangan
                LEFT JOIN jenjang_pendidikan jp1 ON jp1.id = p.jenjang_pendidikan_terakhir_pasangan
                LEFT JOIN pekerjaan p1 ON p1.id = p.pekerjaan_pasangan
                LEFT JOIN agama a2 ON a2.id = p.agama_ayah
                LEFT JOIN jenjang_pendidikan jp2 ON jp2.id = p.jenjang_pendidikan_terakhir_ayah
                LEFT JOIN pekerjaan p2 ON p2.id = p.pekerjaan_ayah
                LEFT JOIN agama a3 ON a3.id = p.agama_ibu
                LEFT JOIN jenjang_pendidikan jp3 ON jp3.id = p.jenjang_pendidikan_terakhir_ibu
                LEFT JOIN pekerjaan p3 ON p3.id = p.pekerjaan_ibu
                LEFT JOIN unit_kerja uk1 ON uk1.id = p.unit_kerja_administrasi
            ';

            $pegawai = \Yii::$app->db->createCommand($query . ' WHERE p.id = :id',
                [
                    'id' => $user->id,
                ]
            )->queryOne();

            return $this->json([
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'login' => true,
                    'pegawai' => $pegawai,
                ],
            ]);
        } else {
            return $this->json([
                'code' => 400,
                'message' => 'Data Not Found',
                'data' => [
                    'login' => false,
                ],
            ]);
        }
    }

    public function actionPegawai($token, $pin = null)
    {
        if (!$this->validate($token)) exit;

        $query = '
            SELECT
                p.nama,
                p.nip,
                p.pin,
                sk.status_kepegawaian ,
                CONCAT(k.pangkat, " (", k.golongan, " / ", k.ruang, ")") AS kepangkatan,
                jj.jenjang_jabatan ,
                p.nama_jabatan,
                uk.unit_kerja AS unit_kerja ,
                p.unit_kerja AS id_unit_kerja,
                e.eselon ,
                g.grade ,
                kp.kedudukan_pegawai ,
                p.mendapat_tunkin,
                p.nama_panggilan,
                p.jenis_kelamin,
                p.tempat_lahir,
                p.tanggal_lahir,
                a.agama ,
                sm.status_marital ,
                p.golongan_darah,
                jp.jenjang_pendidikan ,
                p.gelar_depan,
                p.gelar_belakang,
                p.telpon,
                p.email_kantor,
                p.email_pribadi,
                p.nik,
                p.npwp,
                p.karpeg,
                p.karis_atau_karsu,
                p.taspen,
                p.poto,
                p.alamat_sekarang,
                p.kelurahan_sekarang,
                p.kode_pos_sekarang,
                p.alamat_ktp,
                p.kelurahan_ktp,
                p.kode_pos_ktp,
                p.tinggi_badan_cm,
                p.berat_badan_kg,
                mr.model_rambut ,
                bw.bentuk_wajah ,
                wk.warna_kulit ,
                p.hobi,
                p.nama_pasangan,
                p.tempat_lahir_pasangan,
                p.tanggal_lahir_pasangan,
                a1.agama AS agama_pasangan ,
                jp1.jenjang_pendidikan AS agama_pasangan ,
                p1.pekerjaan AS pekerjaan_ngan ,
                p.status_hidup_pasangan,
                p.telpon_pasangan,
                p.nama_ayah,
                p.tempat_lahir_ayah,
                p.tanggal_lahir_ayah,
                a2.agama AS agama_ayah ,
                jp2.jenjang_pendidikan AS agama_ayah ,
                p2.pekerjaan AS pekerjaan_ayah ,
                p.status_hidup_ayah,
                p.telpon_ayah,
                p.alamat_ayah,
                p.kelurahan_ayah,
                p.kode_pos_ayah,
                p.nama_ibu,
                p.tempat_lahir_ibu,
                p.tanggal_lahir_ibu,
                a3.agama AS agama_ibu ,
                jp3.jenjang_pendidikan AS agama_ibu ,
                p3.pekerjaan AS pekerjaan__ibu ,
                p.status_hidup_ibu,
                p.telpon_ibu,
                p.alamat_ibu,
                p.kelurahan_ibu,
                p.kode_pos_ibu,
                p.pimpinan_unit_kerja,
                uk1.unit_kerja AS unit_kerja_administrasi ,
                p.tunkin_nama,
                NULL AS tanda_tangan,
                p.tunkin_nip
            FROM pegawai p
            JOIN user u ON u.id = p.id
            LEFT JOIN status_kepegawaian sk ON sk.id = p.status_kepegawaian
            LEFT JOIN kepangkatan k ON k.id = p.kepangkatan
            LEFT JOIN jenjang_jabatan jj ON jj.id = p.jenjang_jabatan
            LEFT JOIN unit_kerja uk ON uk.id = p.unit_kerja
            LEFT JOIN eselon e ON e.id = p.eselon
            LEFT JOIN grade g ON g.id = p.grade
            LEFT JOIN kedudukan_pegawai kp ON kp.id = p.kedudukan_pegawai
            LEFT JOIN agama a ON a.id = p.agama
            LEFT JOIN status_marital sm ON sm.id = p.status_marital
            LEFT JOIN jenjang_pendidikan jp ON jp.id = p.jenjang_pendidikan_terakhir
            LEFT JOIN model_rambut mr ON mr.id = p.model_rambut
            LEFT JOIN bentuk_wajah bw ON bw.id = p.bentuk_wajah
            LEFT JOIN warna_kulit wk ON wk.id = p.warna_kulit
            LEFT JOIN agama a1 ON a1.id = p.agama_pasangan
            LEFT JOIN jenjang_pendidikan jp1 ON jp1.id = p.jenjang_pendidikan_terakhir_pasangan
            LEFT JOIN pekerjaan p1 ON p1.id = p.pekerjaan_pasangan
            LEFT JOIN agama a2 ON a2.id = p.agama_ayah
            LEFT JOIN jenjang_pendidikan jp2 ON jp2.id = p.jenjang_pendidikan_terakhir_ayah
            LEFT JOIN pekerjaan p2 ON p2.id = p.pekerjaan_ayah
            LEFT JOIN agama a3 ON a3.id = p.agama_ibu
            LEFT JOIN jenjang_pendidikan jp3 ON jp3.id = p.jenjang_pendidikan_terakhir_ibu
            LEFT JOIN pekerjaan p3 ON p3.id = p.pekerjaan_ibu
            LEFT JOIN unit_kerja uk1 ON uk1.id = p.unit_kerja_administrasi
        ';

        $uk = $ukParent = $atasan = null;
        $ukChild = $bawahan = [];
        if ($pin) {
            $pegawai = \Yii::$app->db->createCommand($query . ' WHERE p.pin = :pin',
                [
                    'pin' => $pin,
                ]
            )->queryOne();

            $uk = UnitKerja::find()->where(['id' => $pegawai['id_unit_kerja']])->one();
            if ($uk) $ukParent = UnitKerja::find()->where(['id' => $uk->parent])->one();
            if ($ukParent) $atasan = \Yii::$app->db->createCommand('SELECT pin FROM pegawai p WHERE p.unit_kerja = :unit_kerja',
                [
                    'unit_kerja' => $ukParent->id,
                ]
            )->queryAll();

            $ukChild = UnitKerja::find()->where(['parent' => $pegawai['id_unit_kerja']])->all();
            if ($ukChild) foreach ($ukChild as $key => $value) {
                $temp = \Yii::$app->db->createCommand('SELECT pin FROM pegawai p WHERE p.unit_kerja = :unit_kerja',
                    [
                        'unit_kerja' => $value->id,
                    ]
                )->queryAll();
                $bawahan = array_merge($bawahan, $temp);
            }
        } else {
            $pegawai = \Yii::$app->db->createCommand($query)->queryAll();
        }

        if ($pegawai) {
            return $this->json([
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'pegawai' => $pegawai,
                    'atasan' => $atasan,
                    'bawahan' => $bawahan,
                ],
            ]);
        } else {
            return $this->json([
                'code' => 400,
                'message' => 'Data Not Found',
            ]);
        }
    }
}
