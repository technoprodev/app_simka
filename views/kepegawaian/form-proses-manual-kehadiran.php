<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FlatpickrAsset::register($this);

//
$errorMessage = '';
if ($model['filter_proses_manual_kehadiran']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['filter_proses_manual_kehadiran'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Pilih Rentang Waktu
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['filter_proses_manual_kehadiran'], 'dari_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['filter_proses_manual_kehadiran'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['filter_proses_manual_kehadiran'], 'dari_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['filter_proses_manual_kehadiran'], 'dari_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['filter_proses_manual_kehadiran'], 'dari_tanggal')->end(); ?>

                <?= $form->field($model['filter_proses_manual_kehadiran'], 'sampai_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['filter_proses_manual_kehadiran'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['filter_proses_manual_kehadiran'], 'sampai_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['filter_proses_manual_kehadiran'], 'sampai_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['filter_proses_manual_kehadiran'], 'sampai_tanggal')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
