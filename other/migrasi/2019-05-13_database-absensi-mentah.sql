SET NAMES utf8;
SET time_zone = '+00:00';

CREATE DATABASE `absensi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `absensi`;

DROP TABLE IF EXISTS `habsen`;
CREATE TABLE `habsen` (
  `tr_no` decimal(20,0) unsigned NOT NULL,
  `loc_code` varchar(5) NOT NULL,
  `remoteno` smallint(6) NOT NULL,
  `tr_date` date NOT NULL,
  `tr_time` time NOT NULL,
  `empl_code` varchar(10) NOT NULL,
  `acc_code` varchar(10) DEFAULT NULL,
  `transfered` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tr_no`,`loc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- selain tr_date, tr_time dan empl_code bisa diisi asal-asalan untuk keperluan development

USE `simka`;

ALTER TABLE `pegawai`
ADD `pin` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `nip`;

-- pin sama dengan empl_code