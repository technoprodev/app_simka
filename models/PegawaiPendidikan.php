<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_pendidikan".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $nama_sekolah
 * @property integer $jenjang_pendidikan
 * @property string $jurusan
 * @property string $nilai
 * @property string $skala_nilai
 * @property string $alamat_sekolah
 * @property string $tahun_masuk
 * @property string $tahun_lulus
 * @property string $keterangan
 * @property string $ijazah
 *
 * @property Pegawai $pegawai0
 * @property JenjangPendidikan $jenjangPendidikan
 */
class PegawaiPendidikan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_ijazah_upload;
    public $virtual_ijazah_download;

    public static function tableName()
    {
        return 'pegawai_pendidikan';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //nama_sekolah
            [['nama_sekolah'], 'required'],
            [['nama_sekolah'], 'string', 'max' => 256],

            //jenjang_pendidikan
            [['jenjang_pendidikan'], 'required'],
            [['jenjang_pendidikan'], 'integer'],
            [['jenjang_pendidikan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan' => 'id']],

            //jurusan
            [['jurusan'], 'required'],
            [['jurusan'], 'string', 'max' => 256],

            //nilai
            [['nilai'], 'required'],
            [['nilai'], 'string', 'max' => 256],

            //skala_nilai
            [['skala_nilai'], 'required'],
            [['skala_nilai'], 'string', 'max' => 256],

            //alamat_sekolah
            [['alamat_sekolah'], 'required'],
            [['alamat_sekolah'], 'string', 'max' => 256],

            //tahun_masuk
            [['tahun_masuk'], 'required'],
            [['tahun_masuk'], 'safe'],

            //tahun_lulus
            [['tahun_lulus'], 'required'],
            [['tahun_lulus'], 'safe'],

            //keterangan
            [['keterangan'], 'string', 'max' => 256],

            //ijazah
            // [['ijazah'], 'required'],
            [['ijazah'], 'string', 'max' => 256],
            
            //virtual_ijazah_download
            [['virtual_ijazah_download'], 'safe'],
            
            //virtual_ijazah_upload
            [['virtual_ijazah_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_ijazah_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_ijazah_upload");
        if ($this->virtual_ijazah_upload) {
            $this->ijazah = $this->virtual_ijazah_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_ijazah_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_ijazah_upload");
        if ($this->virtual_ijazah_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_pendidikan-ijazah') . '/' . $this->id . '/' . $this->ijazah;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->ijazah = $this->virtual_ijazah_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_ijazah_upload) {
            $path = Yii::getAlias('@upload-pegawai_pendidikan-ijazah') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_ijazah_upload->saveAs($path . '/' . $this->ijazah);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_pendidikan-ijazah') . '/' . $this->id . '/' . $this->ijazah;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->ijazah) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_pendidikan-ijazah');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_ijazah_download = $path . '/' . $this->ijazah;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'nama_sekolah' => 'Nama Sekolah',
            'jenjang_pendidikan' => 'Jenjang Pendidikan',
            'jurusan' => 'Jurusan',
            'nilai' => 'Nilai',
            'skala_nilai' => 'Skala Nilai',
            'alamat_sekolah' => 'Alamat Sekolah',
            'tahun_masuk' => 'Tahun Masuk',
            'tahun_lulus' => 'Tahun Lulus',
            'keterangan' => 'Keterangan',
            'ijazah' => 'Ijazah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikan()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan']);
    }
}
