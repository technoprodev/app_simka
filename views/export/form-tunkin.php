<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

/*$this->registerJsFile('@web/app/kepegawaian/form-pegawai.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);*/

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$pegawaiAnaks = [];
if (isset($model['pegawai_anak']))
    foreach ($model['pegawai_anak'] as $key => $pegawaiAnak)
        $pegawaiAnaks[] = $pegawaiAnak->attributes;
$pegawaiDiklats = [];
if (isset($model['pegawai_diklat']))
    foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat)
        $pegawaiDiklats[] = $pegawaiDiklat->attributes;
$pegawaiHukumanDisiplins = [];
if (isset($model['pegawai_hukuman_disiplin']))
    foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin)
        $pegawaiHukumanDisiplins[] = $pegawaiHukumanDisiplin->attributes;
$pegawaiKenaikanGajiBerkalas = [];
if (isset($model['pegawai_kenaikan_gaji_berkala']))
    foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala)
        $pegawaiKenaikanGajiBerkalas[] = $pegawaiKenaikanGajiBerkala->attributes;
$pegawaiKepangkatans = [];
if (isset($model['pegawai_kepangkatan']))
    foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan)
        $pegawaiKepangkatans[] = $pegawaiKepangkatan->attributes;
$pegawaiKunjunganLuarNegeris = [];
if (isset($model['pegawai_kunjungan_luar_negeri']))
    foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri)
        $pegawaiKunjunganLuarNegeris[] = $pegawaiKunjunganLuarNegeri->attributes;
$pegawaiKursusPelatihans = [];
if (isset($model['pegawai_kursus_pelatihan']))
    foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan)
        $pegawaiKursusPelatihans[] = $pegawaiKursusPelatihan->attributes;
$pegawaiMasaPersiapanPensiuns = [];
if (isset($model['pegawai_masa_persiapan_pensiun']))
    foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun)
        $pegawaiMasaPersiapanPensiuns[] = $pegawaiMasaPersiapanPensiun->attributes;
$pegawaiMutasis = [];
if (isset($model['pegawai_mutasi']))
    foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi)
        $pegawaiMutasis[] = $pegawaiMutasi->attributes;
$pegawaiOrganisasis = [];
if (isset($model['pegawai_organisasi']))
    foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi)
        $pegawaiOrganisasis[] = $pegawaiOrganisasi->attributes;
$pegawaiPelaksanas = [];
if (isset($model['pegawai_pelaksana']))
    foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana)
        $pegawaiPelaksanas[] = $pegawaiPelaksana->attributes;
$pegawaiPemberhentians = [];
if (isset($model['pegawai_pemberhentian']))
    foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian)
        $pegawaiPemberhentians[] = $pegawaiPemberhentian->attributes;
$pegawaiPendidikans = [];
if (isset($model['pegawai_pendidikan']))
    foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan)
        $pegawaiPendidikans[] = $pegawaiPendidikan->attributes;
$pegawaiPenetapanAngkaKredits = [];
if (isset($model['pegawai_penetapan_angka_kredit']))
    foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit)
        $pegawaiPenetapanAngkaKredits[] = $pegawaiPenetapanAngkaKredit->attributes;
$pegawaiPenghargaans = [];
if (isset($model['pegawai_penghargaan']))
    foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan)
        $pegawaiPenghargaans[] = $pegawaiPenghargaan->attributes;
$pegawaiSaudaraKandungs = [];
if (isset($model['pegawai_saudara_kandung']))
    foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung)
        $pegawaiSaudaraKandungs[] = $pegawaiSaudaraKandung->attributes;

$this->registerJs(
    // 'vm.$data.pegawai.virtual_category = ' . json_encode($model['pegawai']->virtual_category) . ';' .
    // 'vm.$data.pegawai.pegawaiAnaks = vm.$data.pegawai.pegawaiAnaks.concat(' . json_encode($pegawaiAnaks) . ');' .
    // 'vm.$data.pegawai.pegawaiDiklats = vm.$data.pegawai.pegawaiDiklats.concat(' . json_encode($pegawaiDiklats) . ');' .
    // 'vm.$data.pegawai.pegawaiHukumanDisiplins = vm.$data.pegawai.pegawaiHukumanDisiplins.concat(' . json_encode($pegawaiHukumanDisiplins) . ');' .
    // 'vm.$data.pegawai.pegawaiKenaikanGajiBerkalas = vm.$data.pegawai.pegawaiKenaikanGajiBerkalas.concat(' . json_encode($pegawaiKenaikanGajiBerkalas) . ');' .
    // 'vm.$data.pegawai.pegawaiKepangkatans = vm.$data.pegawai.pegawaiKepangkatans.concat(' . json_encode($pegawaiKepangkatans) . ');' .
    // 'vm.$data.pegawai.pegawaiKunjunganLuarNegeris = vm.$data.pegawai.pegawaiKunjunganLuarNegeris.concat(' . json_encode($pegawaiKunjunganLuarNegeris) . ');' .
    // 'vm.$data.pegawai.pegawaiKursusPelatihans = vm.$data.pegawai.pegawaiKursusPelatihans.concat(' . json_encode($pegawaiKursusPelatihans) . ');' .
    // 'vm.$data.pegawai.pegawaiMasaPersiapanPensiuns = vm.$data.pegawai.pegawaiMasaPersiapanPensiuns.concat(' . json_encode($pegawaiMasaPersiapanPensiuns) . ');' .
    // 'vm.$data.pegawai.pegawaiMutasis = vm.$data.pegawai.pegawaiMutasis.concat(' . json_encode($pegawaiMutasis) . ');' .
    // 'vm.$data.pegawai.pegawaiOrganisasis = vm.$data.pegawai.pegawaiOrganisasis.concat(' . json_encode($pegawaiOrganisasis) . ');' .
    // 'vm.$data.pegawai.pegawaiPelaksanas = vm.$data.pegawai.pegawaiPelaksanas.concat(' . json_encode($pegawaiPelaksanas) . ');' .
    // 'vm.$data.pegawai.pegawaiPemberhentians = vm.$data.pegawai.pegawaiPemberhentians.concat(' . json_encode($pegawaiPemberhentians) . ');' .
    // 'vm.$data.pegawai.pegawaiPendidikans = vm.$data.pegawai.pegawaiPendidikans.concat(' . json_encode($pegawaiPendidikans) . ');' .
    // 'vm.$data.pegawai.pegawaiPenetapanAngkaKredits = vm.$data.pegawai.pegawaiPenetapanAngkaKredits.concat(' . json_encode($pegawaiPenetapanAngkaKredits) . ');' .
    // 'vm.$data.pegawai.pegawaiPenghargaans = vm.$data.pegawai.pegawaiPenghargaans.concat(' . json_encode($pegawaiPenghargaans) . ');' .
    // 'vm.$data.pegawai.pegawaiSaudaraKandungs = vm.$data.pegawai.pegawaiSaudaraKandungs.concat(' . json_encode($pegawaiSaudaraKandungs) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['pegawai']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pegawai'], ['class' => '']);
}

if (isset($model['pegawai_anak'])) foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
    if ($pegawaiAnak->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiAnak, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_diklat'])) foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
    if ($pegawaiDiklat->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiDiklat, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_hukuman_disiplin'])) foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
    if ($pegawaiHukumanDisiplin->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiHukumanDisiplin, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kenaikan_gaji_berkala'])) foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
    if ($pegawaiKenaikanGajiBerkala->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKenaikanGajiBerkala, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kepangkatan'])) foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
    if ($pegawaiKepangkatan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKepangkatan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kunjungan_luar_negeri'])) foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
    if ($pegawaiKunjunganLuarNegeri->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKunjunganLuarNegeri, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_kursus_pelatihan'])) foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
    if ($pegawaiKursusPelatihan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiKursusPelatihan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_masa_persiapan_pensiun'])) foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
    if ($pegawaiMasaPersiapanPensiun->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMasaPersiapanPensiun, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_mutasi'])) foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
    if ($pegawaiMutasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiMutasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_organisasi'])) foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
    if ($pegawaiOrganisasi->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiOrganisasi, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pelaksana'])) foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
    if ($pegawaiPelaksana->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPelaksana, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pemberhentian'])) foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
    if ($pegawaiPemberhentian->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPemberhentian, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_pendidikan'])) foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
    if ($pegawaiPendidikan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPendidikan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penetapan_angka_kredit'])) foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
    if ($pegawaiPenetapanAngkaKredit->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenetapanAngkaKredit, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_penghargaan'])) foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
    if ($pegawaiPenghargaan->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiPenghargaan, ['class' => '']);
        $errorVue = true; 
    }
}
if (isset($model['pegawai_saudara_kandung'])) foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
    if ($pegawaiSaudaraKandung->hasErrors()) {
        $errorMessage .= Html::errorSummary($pegawaiSaudaraKandung, ['class' => '']);
        $errorVue = true; 
    }
}
/*if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}*/

$pilihanPekerjaan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT p.id, p.pekerjaan FROM pekerjaan p
            order by pekerjaan
        ", []
    )->queryAll(), 'id', 'pekerjaan'
);
$pilihanAgama = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT a.id, a.agama FROM agama a
            order by agama
        ", []
    )->queryAll(), 'id', 'agama'
);
$pilihanEselon = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT e.id, e.eselon FROM eselon e
            order by eselon
        ", []
    )->queryAll(), 'id', 'eselon'
);
$pilihanGrade = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT g.id, g.grade FROM grade g
            order by grade
        ", []
    )->queryAll(), 'id', 'grade'
);
$pilihanKedudukanPegawai = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kp.id, kp.kedudukan_pegawai FROM kedudukan_pegawai kp
            order by kedudukan_pegawai
        ", []
    )->queryAll(), 'id', 'kedudukan_pegawai'
);
$pilihanKepangkatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT k.id, CONCAT(k.pangkat, ' (', k.golongan, ' / ', k.ruang, ')') AS kepangkatan FROM kepangkatan k
            order by kepangkatan
        ", []
    )->queryAll(), 'id', 'kepangkatan'
);
$pilihanStatusKepegawaian = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sk.id, sk.status_kepegawaian FROM status_kepegawaian sk
            order by status_kepegawaian
        ", []
    )->queryAll(), 'id', 'status_kepegawaian'
);
$pilihanJenisSk = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT js.id, js.jenis_sk FROM jenis_sk js
            order by jenis_sk
        ", []
    )->queryAll(), 'id', 'jenis_sk'
);
$pilihanJenjangJabatan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jj.id, CONCAT(jjj.jenis_jabatan, ' - ', jj.jenjang_jabatan) AS jenjang_jabatan FROM jenjang_jabatan jj
            JOIN jenis_jabatan jjj ON jjj.id = jj.jenis_jabatan
            order by jenjang_jabatan
        ", []
    )->queryAll(), 'id', 'jenjang_jabatan'
);
$pilihanUnitKerja = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanUnitKerjaAdministrasi = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT uk.id, CONCAT(uk.unit_kerja) AS unit_kerja FROM unit_kerja uk
            LEFT JOIN jenis_unit_kerja juk ON juk.id = uk.jenis_unit_kerja
            order by unit_kerja
        ", []
    )->queryAll(), 'id', 'unit_kerja'
);
$pilihanStatusPernikahan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT sm.id, sm.status_marital FROM status_marital sm
            order by status_marital
        ", []
    )->queryAll(), 'id', 'status_marital'
);
$pilihanPeriodeOrganisasi = (new \app_simka\models\PegawaiOrganisasi())->getEnum('periode_organisasi');
$pilihanStatusHidupAyah = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ayah');
$pilihanStatusHidupIbu = (new \app_simka\models\Pegawai())->getEnum('status_hidup_ibu');
$pilihanStatusHidupPasangan = (new \app_simka\models\Pegawai())->getEnum('status_hidup_pasangan');
$pilihanGolonganDarah = (new \app_simka\models\Pegawai())->getEnum('golongan_darah');
$pilihanJenisKelamin = (new \app_simka\models\Pegawai())->getEnum('jenis_kelamin');
$pilihanJenisPelaksana = (new \app_simka\models\PegawaiPelaksana())->getEnum('jenis_pelaksana');
$pilihanStatusAnak = (new \app_simka\models\PegawaiAnak())->getEnum('status_anak');
$pilihanStatusTunjangan = (new \app_simka\models\PegawaiAnak())->getEnum('status_tunjangan');
$pilihanJenjangPendidikan = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT jp.id, jp.jenjang_pendidikan FROM jenjang_pendidikan jp
        ", []
    )->queryAll(), 'id', 'jenjang_pendidikan'
);
$pilihanJenisDiklat = (new \app_simka\models\PegawaiDiklat())->getEnum('jenis_diklat');
$pilihanNegara = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT n.id, n.nama FROM negara n
            order by nama
        ", []
    )->queryAll(), 'id', 'nama'
);
$pilihanModelRambut = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, model_rambut FROM model_rambut
            order by model_rambut
        ", []
    )->queryAll(), 'id', 'model_rambut'
);
$pilihanBentukWajah = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, bentuk_wajah FROM bentuk_wajah
            order by bentuk_wajah
        ", []
    )->queryAll(), 'id', 'bentuk_wajah'
);
$pilihanWarnaKulit = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT id, warna_kulit FROM warna_kulit
            order by warna_kulit
        ", []
    )->queryAll(), 'id', 'warna_kulit'
);
$pilihanMendapatTunkin = (new \app_simka\models\Pegawai())->getEnum('mendapat_tunkin');
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <div class="fs-16 text-azure fs-italic">
                    Data Tunkin
                </div>

                <hr class="margin-y-10 border-top border-light-orange">

                <?= $form->field($model['pegawai'], 'tunkin_nama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'tunkin_nama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'tunkin_nama', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'tunkin_nama', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'tunkin_nama')->end(); ?>

                <?= $form->field($model['pegawai'], 'tunkin_nip', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'tunkin_nip', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'tunkin_nip', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'tunkin_nip', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'tunkin_nip')->end(); ?>

                <?= $form->field($model['pegawai'], 'tunkin_nomor_rekening', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'tunkin_nomor_rekening', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['pegawai'], 'tunkin_nomor_rekening', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['pegawai'], 'tunkin_nomor_rekening', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'tunkin_nomor_rekening')->end(); ?>

                <?= $form->field($model['pegawai'], 'status_kepegawaian', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'status_kepegawaian', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'status_kepegawaian', $pilihanStatusKepegawaian, ['prompt' => '- pilih status kepegawaian -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'status_kepegawaian', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'status_kepegawaian')->end(); ?>

                <?= $form->field($model['pegawai'], 'kepangkatan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'kepangkatan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'kepangkatan', $pilihanKepangkatan, ['prompt' => '- pilih kepangkatan -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'kepangkatan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'kepangkatan')->end(); ?>

                <?= $form->field($model['pegawai'], 'grade', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['pegawai'], 'grade', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeDropDownList($model['pegawai'], 'grade', $pilihanGrade, ['prompt' => '- pilih grade -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['pegawai'], 'grade', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['pegawai'], 'grade')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                    <div class="margin-top-15"></div>
                    <div class="clearfix">
                        <?= Html::a('Kembali ke Export Tunjangan Kinerja', ['tunkin'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                        <div class="margin-5 pull-right m-pull-none"></div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
