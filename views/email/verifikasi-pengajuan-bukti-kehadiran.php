<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #3376b8;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;">
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;"><b style="box-sizing:border-box;font-weight:bolder;">PERHATIAN !</b> Terdapat pengajuan bukti kehadiran baru pada aplikasi <?= Yii::$app->params['app.name'] ?>. Harap segera verifikasi pengajuan tersebut dengan membuka aplikasi <?= Yii::$app->params['app.name'] ?> pada link <a href="https://srikandi.bekraf.go.id" class="fw-bold" style="box-sizing:border-box;background-color:transparent;color:#3376b8;text-decoration:none;font-weight:700;">https://srikandi.bekraf.go.id</a></p>

        </div>
    </div>
</div>