<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "model_rambut".
 *
 * @property integer $id
 * @property string $model_rambut
 *
 * @property Pegawai[] $pegawais
 */
class ModelRambut extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'model_rambut';
    }

    public function rules()
    {
        return [
            //id

            //model_rambut
            [['model_rambut'], 'required'],
            [['model_rambut'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_rambut' => 'Model Rambut',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['model_rambut' => 'id']);
    }
}
