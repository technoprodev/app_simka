<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-6 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Detail Kehadiran
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Tanggal Kehadiran
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['tanggal'] ? $model['kehadiran']['tanggal'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Jadwal Masuk
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['jadwal_masuk'] ? $model['kehadiran']['jadwal_masuk'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Jadwal Pulang
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['jadwal_pulang'] ? $model['kehadiran']['jadwal_pulang'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Waktu Masuk
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['waktu_masuk'] ? $model['kehadiran']['waktu_masuk'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Waktu Pulang
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['waktu_pulang'] ? $model['kehadiran']['waktu_pulang'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Waktu Terakhir Diupdate
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['waktu_terakhir_diupdate'] ? $model['kehadiran']['waktu_terakhir_diupdate'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Telat Masuk
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['telat'] ? $model['kehadiran']['telat'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Pulang Cepat
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['kecepetan'] ? $model['kehadiran']['kecepetan'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Jumlah Jam
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['jumlah_jam'] ? $model['kehadiran']['jumlah_jam'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Kurang Jam
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['kurang_jam'] ? $model['kehadiran']['kurang_jam'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Keterangan
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['keterangan'] ? $model['kehadiran']['keterangan'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Potongan Tunkin
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['potongan_tunkin'] ? $model['kehadiran']['potongan_tunkin'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Disiplin
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['kehadiran']['kurang_jam'] ? $model['kehadiran']['kurang_jam'] : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-6 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Detail Bukti Kehadiran
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <?php if ($model['bukti_kehadiran']) : ?>

            <div class="fs-italic text-azure">Ditemukan <?= count($model['bukti_kehadiran']) ?> pengajuan bukti kehadiran</div>

            <div class="margin-top-15"></div>

            <?php foreach ($model['bukti_kehadiran'] as $key => $buktiKehadiran) : ?>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Status Pengajuan
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->status_pengajuan ? $buktiKehadiran->status_pengajuan : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Diajukan Oleh
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->diajukan ? $buktiKehadiran->diajukan0->nama : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Waktu Pengajuan
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->waktu_pengajuan ? $buktiKehadiran->waktu_pengajuan : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Dari Tanggal
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->dari_tanggal ? $buktiKehadiran->dari_tanggal : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Sampai Tanggal
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->sampai_tanggal ? $buktiKehadiran->dari_tanggal : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Utama
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_utama ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_utama_download . '">' . $buktiKehadiran->bukti_utama . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Tambahan 1
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_tambahan_1 ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_tambahan_1_download . '">' . $buktiKehadiran->bukti_tambahan_1 . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Tambahan 2
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_tambahan_2 ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_tambahan_2_download . '">' . $buktiKehadiran->bukti_tambahan_2 . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Tambahan 3
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_tambahan_3 ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_tambahan_3_download . '">' . $buktiKehadiran->bukti_tambahan_3 . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Tambahan 4
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_tambahan_4 ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_tambahan_4_download . '">' . $buktiKehadiran->bukti_tambahan_4 . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Bukti Tambahan 5
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiran->bukti_tambahan_5 ? '<a target="_blank" href="' . $buktiKehadiran->virtual_bukti_tambahan_5_download . '">' . $buktiKehadiran->bukti_tambahan_5 . '</a>' : '-' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <?php foreach ($buktiKehadiran->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai) : ?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 padding-y-5">
                    Pegawai <?= ($key + 1) ?>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiranPegawai->pegawai ? $buktiKehadiranPegawai->pegawai0->nama . ' - ' . $buktiKehadiranPegawai->pegawai0->nip : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endforeach; ?>

            <hr class="margin-y-10 border-top border-light-orange">

            <?php endforeach; ?>

            <?php else : ?>

            <div class="fs-italic text-red">Bukti kehadiran tidak ditemukan</div>

            <?php endif; ?>
        </div>
    </div>
</div>
