<?php
namespace app_simka\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_simka/assets';
    
    public $css = [
        'css/custom.css',
    ];
    
    public $js = [
        'js/custom.js',
    ];
    
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}