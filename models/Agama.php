<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "agama".
 *
 * @property integer $id
 * @property string $agama
 *
 * @property Pegawai[] $pegawais
 * @property Pegawai[] $pegawais0
 * @property Pegawai[] $pegawais1
 * @property Pegawai[] $pegawais2
 */
class Agama extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'agama';
    }

    public function rules()
    {
        return [
            //id

            //agama
            [['agama'], 'required'],
            [['agama'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agama' => 'Agama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['agama_ayah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais0()
    {
        return $this->hasMany(Pegawai::className(), ['agama_ibu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais1()
    {
        return $this->hasMany(Pegawai::className(), ['agama_pasangan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais2()
    {
        return $this->hasMany(Pegawai::className(), ['agama' => 'id']);
    }
}
