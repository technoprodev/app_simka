<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_pemberhentian".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property integer $jenis_pemberhentian
 * @property string $terhitung_mulai_tanggal
 * @property integer $kepangkatan_saat_pemberhentian
 * @property string $instansi_saat_pemberhentian
 * @property string $unit_kerja_saat_pemberhentian
 * @property integer $jenjang_jabatan_saat_diberhentikan
 * @property string $nama_jabatan_saat_diberhentikan
 * @property string $catatan
 * @property integer $jenis_sk
 * @property string $nomor_sk
 * @property string $tanggal_sk
 * @property string $pejabat_penetap_sk
 * @property string $arsip_sk
 *
 * @property Pegawai $pegawai0
 * @property JenisPemberhentian $jenisPemberhentian
 * @property Kepangkatan $kepangkatanSaatPemberhentian
 * @property JenjangJabatan $jenjangJabatanSaatDiberhentikan
 * @property JenisSk $jenisSk
 */
class PegawaiPemberhentian extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_arsip_sk_upload;
    public $virtual_arsip_sk_download;

    public static function tableName()
    {
        return 'pegawai_pemberhentian';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //jenis_pemberhentian
            [['jenis_pemberhentian'], 'required'],
            [['jenis_pemberhentian'], 'integer'],
            [['jenis_pemberhentian'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPemberhentian::className(), 'targetAttribute' => ['jenis_pemberhentian' => 'id']],

            //terhitung_mulai_tanggal
            [['terhitung_mulai_tanggal'], 'required'],
            [['terhitung_mulai_tanggal'], 'safe'],

            //kepangkatan_saat_pemberhentian
            [['kepangkatan_saat_pemberhentian'], 'required'],
            [['kepangkatan_saat_pemberhentian'], 'integer'],
            [['kepangkatan_saat_pemberhentian'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan_saat_pemberhentian' => 'id']],

            //instansi_saat_pemberhentian
            [['instansi_saat_pemberhentian'], 'required'],
            [['instansi_saat_pemberhentian'], 'string', 'max' => 256],

            //unit_kerja_saat_pemberhentian
            [['unit_kerja_saat_pemberhentian'], 'required'],
            [['unit_kerja_saat_pemberhentian'], 'string', 'max' => 256],

            //jenjang_jabatan_saat_diberhentikan
            [['jenjang_jabatan_saat_diberhentikan'], 'required'],
            [['jenjang_jabatan_saat_diberhentikan'], 'integer'],
            [['jenjang_jabatan_saat_diberhentikan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangJabatan::className(), 'targetAttribute' => ['jenjang_jabatan_saat_diberhentikan' => 'id']],

            //nama_jabatan_saat_diberhentikan
            [['nama_jabatan_saat_diberhentikan'], 'required'],
            [['nama_jabatan_saat_diberhentikan'], 'string', 'max' => 256],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'integer'],
            [['jenis_sk'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSk::className(), 'targetAttribute' => ['jenis_sk' => 'id']],

            //nomor_sk
            [['nomor_sk'], 'required'],
            [['nomor_sk'], 'string', 'max' => 256],

            //tanggal_sk
            [['tanggal_sk'], 'required'],
            [['tanggal_sk'], 'safe'],

            //pejabat_penetap_sk
            [['pejabat_penetap_sk'], 'required'],
            [['pejabat_penetap_sk'], 'string', 'max' => 256],

            //arsip_sk
            [['arsip_sk'], 'required'],
            [['arsip_sk'], 'string', 'max' => 256],
            
            //virtual_arsip_sk_download
            [['virtual_arsip_sk_download'], 'safe'],
            
            //virtual_arsip_sk_upload
            [['virtual_arsip_sk_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_pemberhentian-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_arsip_sk_upload) {
            $path = Yii::getAlias('@upload-pegawai_pemberhentian-arsip_sk') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_arsip_sk_upload->saveAs($path . '/' . $this->arsip_sk);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_pemberhentian-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->arsip_sk) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_pemberhentian-arsip_sk');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_arsip_sk_download = $path . '/' . $this->arsip_sk;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'jenis_pemberhentian' => 'Jenis Pemberhentian',
            'terhitung_mulai_tanggal' => 'Terhitung Mulai Tanggal',
            'kepangkatan_saat_pemberhentian' => 'Kepangkatan Saat Pemberhentian',
            'instansi_saat_pemberhentian' => 'Instansi Saat Pemberhentian',
            'unit_kerja_saat_pemberhentian' => 'Unit Kerja Saat Pemberhentian',
            'jenjang_jabatan_saat_diberhentikan' => 'Jenjang Jabatan Saat Diberhentikan',
            'nama_jabatan_saat_diberhentikan' => 'Nama Jabatan Saat Diberhentikan',
            'catatan' => 'Catatan',
            'jenis_sk' => 'Jenis Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_sk' => 'Tanggal Sk',
            'pejabat_penetap_sk' => 'Pejabat Penetap Sk',
            'arsip_sk' => 'Arsip Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPemberhentian()
    {
        return $this->hasOne(JenisPemberhentian::className(), ['id' => 'jenis_pemberhentian']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatanSaatPemberhentian()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan_saat_pemberhentian']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatanSaatDiberhentikan()
    {
        return $this->hasOne(JenjangJabatan::className(), ['id' => 'jenjang_jabatan_saat_diberhentikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSk()
    {
        return $this->hasOne(JenisSk::className(), ['id' => 'jenis_sk']);
    }
}
