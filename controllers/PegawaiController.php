<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\Pegawai;
use app_simka\models\PegawaiAnak;
use app_simka\models\PegawaiDiklat;
use app_simka\models\PegawaiHukumanDisiplin;
use app_simka\models\PegawaiKenaikanGajiBerkala;
use app_simka\models\PegawaiKepangkatan;
use app_simka\models\PegawaiKunjunganLuarNegeri;
use app_simka\models\PegawaiKursusPelatihan;
use app_simka\models\PegawaiMasaPersiapanPensiun;
use app_simka\models\PegawaiMutasi;
use app_simka\models\PegawaiOrganisasi;
use app_simka\models\PegawaiPelaksana;
use app_simka\models\PegawaiPemberhentian;
use app_simka\models\PegawaiPendidikan;
use app_simka\models\PegawaiPenetapanAngkaKredit;
use app_simka\models\PegawaiPenghargaan;
use app_simka\models\PegawaiSaudaraKandung;
use app_simka\models\User;
use app_simka\models\BuktiKehadiran;
use app_simka\models\BuktiKehadiranPegawai;
use app_simka\models\Cuti;
use app_simka\models\KeteranganKehadiran;
use app_simka\models\FilterKehadiran;

class PegawaiController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['pegawai']]
            ]),
        ];
    }

    public function actionTest()
    {
        \Yii::$app->mail->compose('email/test', ['value' => 'value'])
            ->setFrom(['sadewa@bekraf.go.id' => 'Sadewa Bekraf (no-reply)'])
            ->setTo('pradana.fandy@gmail.com')
            ->setSubject('Judul')
            ->send();
    }

    public function datatablesKehadiran($idPegawai, $bulan, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'k.id',
                'DATE_FORMAT(k.tanggal, "%d %b") AS tanggal',
                'DATE_FORMAT(k.jadwal_masuk, "%H:%i:%s") AS jadwal_masuk',
                'DATE_FORMAT(k.jadwal_pulang, "%H:%i:%s") AS jadwal_pulang',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_masuk, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_masuk',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_pulang, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_pulang',
                'DATE_FORMAT(k.waktu_terakhir_diupdate, "%H:%i:%s") AS waktu_terakhir_diupdate',
                '@telat_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS telat_detik',
                '@kecepetan_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kecepetan_detik',
                'TIME_FORMAT(SEC_TO_TIME(@telat_detik), "%H:%i:%s") AS telat',
                'CASE WHEN @kecepetan_detik > 0 THEN TIME_FORMAT(SEC_TO_TIME(@kecepetan_detik), "%H:%i:%s") ELSE NULL END AS kecepetan',
                'LEAST(
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END +
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END
                , 100) AS potongan_tunkin',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(TIMEDIFF(TIMEDIFF(k.waktu_pulang, k.waktu_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)), "%H:%i:%s")
                    ELSE NULL
                END AS jumlah_jam',
                '@kurang_detik := CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'k.keterangan as idKeterangan',
                'kh.color as color',
                'kh.keterangan as keterangan',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
        ;

        return $query;
    }

    public function datatablesKehadiranBulanan($idPegawai = null, $bulan, $tahun)
    {
        $subQuery = new \yii\db\Query();
        $subQuery
            ->select([
                'p.id',
                'p.nama',
                'p.nip',
                'p.pin',
                'uk.unit_kerja',
                'LEAST(
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END) +
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END)
                , 100) AS potongan_tunkin',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS telat',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS kecepetan',
                'TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(k.waktu_pulang, k.waktu_masuk)))), "%H:%i:%s") AS jumlah_jam',
                '@kurang_detik := SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END) AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'count(case when kh.dihitung_hadir = "Hadir" then k.id end) AS hadir',
                'count(case when kh.dihitung_hadir = "Tidak Hadir" then k.id end) AS tidak_hadir',
                'count(case when kh.id = 1 then k.id end) AS belum_absen',
                'count(case when kh.id = 7 then k.id end) AS libur',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'kehadiran k', 'k.pegawai = p.id')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->where([
                // 'p.id' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->orderBy(['uk.level' => SORT_ASC])
            ->groupBy(['p.id', 'p.nama', 'p.nip', 'p.pin'])
        ;
        $uk = Yii::$app->request->isPost ? Yii::$app->request->post()['columns'][4]['search']['value'] : '';
        if ($uk) {
            $ukIdMentah = (new \yii\db\Query())->select('id')->from('unit_kerja uk')->where(['like', 'unit_kerja', $uk])->orderBy(['level' => SORT_ASC])->scalar();
            $result = (new \yii\db\Query())->select('get_pegawai_rec(' . $ukIdMentah . ')')->scalar();
            $pegawais = $result ? explode(',', $result) : [];
            $subQuery->andWhere(['p.id' => $pegawais]);
        }
        if ($idPegawai) {
            $subQuery->andWhere(['p.id' => $idPegawai]);
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'nama',
                'nip',
                'pin',
                'unit_kerja',
                'potongan_tunkin',
                'telat',
                'kecepetan',
                'jumlah_jam',
                'kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(kurang_detik), "%H:%i:%s") AS kurang_jam',
                '(hadir + tidak_hadir + belum_absen) AS hari_kerja',
                'hadir',
                'tidak_hadir',
                'belum_absen',
                'libur',
            ])
            ->from(['sq' => $subQuery])
        ;

        return $query;
        // return $this->json($query->all());
    }

    public function datatablesKehadiranTahunan($idPegawai, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN 0
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE 0
                END) AS kurang_detik',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->andWhere([
                'between', 'MONTH(k.tanggal)', '01', date('m'),
            ])
            ->groupBy(['k.pegawai'])
        ;

        return $query;
    }
    
    public function actionIndex($tab = 'jabatan')
    {
        // $result = (new \yii\db\Query())->select('get_pegawai_rec(2)')->scalar();
        // $pegawais = $result ? explode(',', $result) : null;

        // $result = (new \yii\db\Query())->select('get_unit_kerja_rec(1)')->scalar();
        // $unitKerjas = $result ? explode(',', $result) : null;

        // ddx($unitKerjas);
        
        $model['filter_kehadiran'] = new FilterKehadiran();

        if (($model['pegawai'] = Pegawai::find()->where(['id' => Yii::$app->user->identity->id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $model['kehadiran_bulanan'] = $this->datatablesKehadiranBulanan($model['pegawai']->id, $model['filter_kehadiran']->bulan, $model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_bulanan'] = [
            'hari_kerja' => $model['kehadiran_bulanan']['hari_kerja'],
            'hadir' => $model['kehadiran_bulanan']['hadir'],
            'tidak_hadir' => $model['kehadiran_bulanan']['tidak_hadir'],
            'belum_absen' => $model['kehadiran_bulanan']['belum_absen'],
            'libur' => $model['kehadiran_bulanan']['libur'],
            'potongan_tunkin' => $model['kehadiran_bulanan']['potongan_tunkin'],
            'uang_makan' => $model['kehadiran_bulanan']['hadir'],
            'disiplin' => $model['kehadiran_bulanan']['kurang_detik'],
        ];

        $model['kehadiran_tahunan'] = $this->datatablesKehadiranTahunan($model['pegawai']->id,$model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_tahunan'] = [
            'disiplin' => $model['kehadiran_tahunan']['kurang_detik'],
        ];

        return $this->render('index', [
            'model' => $model,
            'tab' => $tab,
            'title' => 'Dashbor Pegawai',
        ]);
    }

    public function actionDatatablesPegawaiKepangkatan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'k.pangkat',
                'k.golongan',
                'k.ruang',
                's.status_kepegawaian',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_kepangkatan-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_kepangkatan p')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan')
            ->join('LEFT JOIN', 'status_kepegawaian s', 's.id = p.status_kepegawaian')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKenaikanGajiBerkala()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.gaji_pokok_lama',
                'p.gaji_pokok_baru',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'CONCAT(k.pangkat, " (", k.golongan, "/", k.golongan, ")") AS kepangkatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_kenaikan_gaji_berkala-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_kenaikan_gaji_berkala p')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan_saat_kenaikan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiJabatan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.instansi',
                'p.unit_kerja',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_mutasi-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_mutasi p')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPelaksana()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_pelaksana',
                'p.instansi',
                'p.unit_kerja',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
            ])
            ->from('pegawai_pelaksana p')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPenetapanAngkaKredit()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.periode_penilaian_awal',
                'p.periode_penilaian_akhir',
                'p.unsur_utama',
                'p.unsur_penunjang',
                'p.total_angkat_kredit',
                'CONCAT("upload/pegawai_penetapan_angka_kredit-arsip_pak/", p.id, "/", p.arsip_pak) AS arsip_pak',
            ])
            ->from('pegawai_penetapan_angka_kredit p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiSaudaraKandung()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama',
                'p.jenis_kelamin',
                'p.tempat_lahir',
                'p.tanggal_lahir',
                'pk.pekerjaan',
                'p.anak_ke',
            ])
            ->from('pegawai_saudara_kandung p')
            ->join('LEFT JOIN', 'pekerjaan pk', 'pk.id = p.pekerjaan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiAnak()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_anak',
                'p.jenis_kelamin',
                'p.tempat_lahir',
                'p.tanggal_lahir',
                'jp.jenjang_pendidikan',
                'pk.pekerjaan',
                'p.status_anak',
                'p.status_hidup',
                'p.status_tunjangan',
                'CONCAT("upload/pegawai_anak-akta_lahir/", p.id, "/", p.akta_lahir) AS akta_lahir',
            ])
            ->from('pegawai_anak p')
            ->join('LEFT JOIN', 'jenjang_pendidikan jp', 'jp.id = p.jenjang_pendidikan_terakhir')
            ->join('LEFT JOIN', 'pekerjaan pk', 'pk.id = p.pekerjaan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPendidikan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_sekolah',
                'jp.jenjang_pendidikan',
                'p.jurusan',
                'p.nilai',
                'p.skala_nilai',
                'p.alamat_sekolah',
                'p.tahun_masuk',
                'p.tahun_lulus',
                'CONCAT("upload/pegawai_pendidikan-ijazah/", p.id, "/", p.ijazah) AS ijazah',
            ])
            ->from('pegawai_pendidikan p')
            ->join('LEFT JOIN', 'jenjang_pendidikan jp', 'jp.id = p.jenjang_pendidikan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPenghargaan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_penghargaan',
                'p.nama_penghargaan',
                'p.tahun_penghargaan',
                'p.instansi_pemberi_penghargaan',
                'CONCAT("upload/pegawai_penghargaan-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_penghargaan p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiDiklat()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_diklat',
                'p.nama_diklat',
                'p.penyelenggara',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.tempat',
                'p.keterangan',
                'CONCAT("upload/pegawai_diklat-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
                'p.nomor_sttp',
                'p.tanggal_sttp',
            ])
            ->from('pegawai_diklat p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKursusPelatihan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_kursus_pelatihan',
                'p.penyelenggara',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.lokasi',
                'CONCAT("upload/pegawai_kursus_pelatihan-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_kursus_pelatihan p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKunjunganLuarNegeri()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'n.nama AS negara_kunjungan',
                'p.tujuan_kunjungan',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.sumber_biaya',
            ])
            ->from('pegawai_kunjungan_luar_negeri p')
            ->join('LEFT JOIN', 'negara n', 'n.id = p.negara_kunjungan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiOrganisasi()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.organisasi',
                'p.jabatan',
                'p.tahun_mulai',
                'p.tahun_selesai',
                'p.lokasi',
                'p.periode_organisasi',
                'CONCAT("upload/pegawai_organisasi-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_organisasi p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiHukumanDisiplin()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'jhd.jenis_hukuman_disiplin',
                'p.penjelasan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_hukuman_disiplin-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_hukuman_disiplin p')
            ->join('LEFT JOIN', 'jenis_hukuman_disiplin jhd', 'jhd.id = p.jenis_hukuman_disiplin')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPemberhentian()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'jp.jenis_pemberhentian',
                'p.terhitung_mulai_tanggal',
                'k.pangkat',
                'k.golongan',
                'k.ruang',
                'p.instansi_saat_pemberhentian',
                'p.unit_kerja_saat_pemberhentian',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan_saat_diberhentikan',
                'p.catatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_pemberhentian-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_pemberhentian p')
            ->join('LEFT JOIN', 'jenis_pemberhentian jp', 'jp.id = p.jenis_pemberhentian')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan_saat_pemberhentian')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan_saat_diberhentikan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiMasaPersiapanPensiun()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.terhitung_mulai_tanggal',
                'p.catatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_masa_persiapan_pensiun-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_masa_persiapan_pensiun p')
            ->where([
                'p.pegawai' => Yii::$app->user->identity->id,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }
    
    public function actionDrh($tab = 'jabatan')
    {
        if (($model['pegawai'] = Pegawai::find()->where(['id' => Yii::$app->user->identity->id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('drh', [
            'model' => $model,
            'tab' => $tab,
            'title' => 'Daftar Riwayat Hidup Pegawai',
        ]);
    }

    public function actionDrhPrint()
    {
        if (($model['pegawai'] = Pegawai::find()->where(['id' => Yii::$app->user->identity->id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $title = 'DRH ' . $model['pegawai']->nama;
        $this->layout = 'download';
        // return $this->render('print-drh', ['model' => $model, 'title' => $title]);

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->render('print-drh', ['model' => $model, 'title' => $title]));
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($title . '.pdf');
        exit;

    }

    public function actionPegawaiUpdate($tab)
    {
        $error = true;

        if (true) {
            if (($model['pegawai'] = Pegawai::find()->where(['id' => Yii::$app->user->identity->id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['user'] = new User(['scenario' => 'password']);
            $model['pegawai'] = new Pegawai();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pegawai']->load($post);
            if (true) {
                if (isset($post['PegawaiAnak'])) {
                    foreach ($post['PegawaiAnak'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiAnak = PegawaiAnak::find()->where(['id' => $value['id']])->one();
                            $pegawaiAnak->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiAnak = PegawaiAnak::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiAnak->isDeleted = true;
                        } else {
                            $pegawaiAnak = new PegawaiAnak();
                            $pegawaiAnak->setAttributes($value);
                        }
                        $pegawaiAnak->index = $key;
                        $model['pegawai_anak'][] = $pegawaiAnak;
                    }
                }
                if (isset($post['PegawaiDiklat'])) {
                    foreach ($post['PegawaiDiklat'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiDiklat = PegawaiDiklat::find()->where(['id' => $value['id']])->one();
                            $pegawaiDiklat->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiDiklat = PegawaiDiklat::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiDiklat->isDeleted = true;
                        } else {
                            $pegawaiDiklat = new PegawaiDiklat();
                            $pegawaiDiklat->setAttributes($value);
                        }
                        $pegawaiDiklat->index = $key;
                        $model['pegawai_diklat'][] = $pegawaiDiklat;
                    }
                }
                if (isset($post['PegawaiHukumanDisiplin'])) {
                    foreach ($post['PegawaiHukumanDisiplin'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiHukumanDisiplin = PegawaiHukumanDisiplin::find()->where(['id' => $value['id']])->one();
                            $pegawaiHukumanDisiplin->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiHukumanDisiplin = PegawaiHukumanDisiplin::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiHukumanDisiplin->isDeleted = true;
                        } else {
                            $pegawaiHukumanDisiplin = new PegawaiHukumanDisiplin();
                            $pegawaiHukumanDisiplin->setAttributes($value);
                        }
                        $pegawaiHukumanDisiplin->index = $key;
                        $model['pegawai_hukuman_disiplin'][] = $pegawaiHukumanDisiplin;
                    }
                }
                if (isset($post['PegawaiKenaikanGajiBerkala'])) {
                    foreach ($post['PegawaiKenaikanGajiBerkala'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKenaikanGajiBerkala = PegawaiKenaikanGajiBerkala::find()->where(['id' => $value['id']])->one();
                            $pegawaiKenaikanGajiBerkala->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKenaikanGajiBerkala = PegawaiKenaikanGajiBerkala::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKenaikanGajiBerkala->isDeleted = true;
                        } else {
                            $pegawaiKenaikanGajiBerkala = new PegawaiKenaikanGajiBerkala();
                            $pegawaiKenaikanGajiBerkala->setAttributes($value);
                        }
                        $pegawaiKenaikanGajiBerkala->index = $key;
                        $model['pegawai_kenaikan_gaji_berkala'][] = $pegawaiKenaikanGajiBerkala;
                    }
                }
                if (isset($post['PegawaiKepangkatan'])) {
                    foreach ($post['PegawaiKepangkatan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKepangkatan = PegawaiKepangkatan::find()->where(['id' => $value['id']])->one();
                            $pegawaiKepangkatan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKepangkatan = PegawaiKepangkatan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKepangkatan->isDeleted = true;
                        } else {
                            $pegawaiKepangkatan = new PegawaiKepangkatan();
                            $pegawaiKepangkatan->setAttributes($value);
                        }
                        $pegawaiKepangkatan->index = $key;
                        $model['pegawai_kepangkatan'][] = $pegawaiKepangkatan;
                    }
                }
                if (isset($post['PegawaiKunjunganLuarNegeri'])) {
                    foreach ($post['PegawaiKunjunganLuarNegeri'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKunjunganLuarNegeri = PegawaiKunjunganLuarNegeri::find()->where(['id' => $value['id']])->one();
                            $pegawaiKunjunganLuarNegeri->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKunjunganLuarNegeri = PegawaiKunjunganLuarNegeri::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKunjunganLuarNegeri->isDeleted = true;
                        } else {
                            $pegawaiKunjunganLuarNegeri = new PegawaiKunjunganLuarNegeri();
                            $pegawaiKunjunganLuarNegeri->setAttributes($value);
                        }
                        $pegawaiKunjunganLuarNegeri->index = $key;
                        $model['pegawai_kunjungan_luar_negeri'][] = $pegawaiKunjunganLuarNegeri;
                    }
                }
                if (isset($post['PegawaiKursusPelatihan'])) {
                    foreach ($post['PegawaiKursusPelatihan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKursusPelatihan = PegawaiKursusPelatihan::find()->where(['id' => $value['id']])->one();
                            $pegawaiKursusPelatihan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKursusPelatihan = PegawaiKursusPelatihan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKursusPelatihan->isDeleted = true;
                        } else {
                            $pegawaiKursusPelatihan = new PegawaiKursusPelatihan();
                            $pegawaiKursusPelatihan->setAttributes($value);
                        }
                        $pegawaiKursusPelatihan->index = $key;
                        $model['pegawai_kursus_pelatihan'][] = $pegawaiKursusPelatihan;
                    }
                }
                if (isset($post['PegawaiMasaPersiapanPensiun'])) {
                    foreach ($post['PegawaiMasaPersiapanPensiun'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiMasaPersiapanPensiun = PegawaiMasaPersiapanPensiun::find()->where(['id' => $value['id']])->one();
                            $pegawaiMasaPersiapanPensiun->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiMasaPersiapanPensiun = PegawaiMasaPersiapanPensiun::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiMasaPersiapanPensiun->isDeleted = true;
                        } else {
                            $pegawaiMasaPersiapanPensiun = new PegawaiMasaPersiapanPensiun();
                            $pegawaiMasaPersiapanPensiun->setAttributes($value);
                        }
                        $pegawaiMasaPersiapanPensiun->index = $key;
                        $model['pegawai_masa_persiapan_pensiun'][] = $pegawaiMasaPersiapanPensiun;
                    }
                }
                if (isset($post['PegawaiMutasi'])) {
                    foreach ($post['PegawaiMutasi'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiMutasi = PegawaiMutasi::find()->where(['id' => $value['id']])->one();
                            $pegawaiMutasi->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiMutasi = PegawaiMutasi::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiMutasi->isDeleted = true;
                        } else {
                            $pegawaiMutasi = new PegawaiMutasi();
                            $pegawaiMutasi->setAttributes($value);
                        }
                        $pegawaiMutasi->index = $key;
                        $model['pegawai_mutasi'][] = $pegawaiMutasi;
                    }
                }
                if (isset($post['PegawaiOrganisasi'])) {
                    foreach ($post['PegawaiOrganisasi'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiOrganisasi = PegawaiOrganisasi::find()->where(['id' => $value['id']])->one();
                            $pegawaiOrganisasi->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiOrganisasi = PegawaiOrganisasi::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiOrganisasi->isDeleted = true;
                        } else {
                            $pegawaiOrganisasi = new PegawaiOrganisasi();
                            $pegawaiOrganisasi->setAttributes($value);
                        }
                        $pegawaiOrganisasi->index = $key;
                        $model['pegawai_organisasi'][] = $pegawaiOrganisasi;
                    }
                }
                if (isset($post['PegawaiPelaksana'])) {
                    foreach ($post['PegawaiPelaksana'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPelaksana = PegawaiPelaksana::find()->where(['id' => $value['id']])->one();
                            $pegawaiPelaksana->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPelaksana = PegawaiPelaksana::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPelaksana->isDeleted = true;
                        } else {
                            $pegawaiPelaksana = new PegawaiPelaksana();
                            $pegawaiPelaksana->setAttributes($value);
                        }
                        $pegawaiPelaksana->index = $key;
                        $model['pegawai_pelaksana'][] = $pegawaiPelaksana;
                    }
                }
                if (isset($post['PegawaiPemberhentian'])) {
                    foreach ($post['PegawaiPemberhentian'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPemberhentian = PegawaiPemberhentian::find()->where(['id' => $value['id']])->one();
                            $pegawaiPemberhentian->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPemberhentian = PegawaiPemberhentian::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPemberhentian->isDeleted = true;
                        } else {
                            $pegawaiPemberhentian = new PegawaiPemberhentian();
                            $pegawaiPemberhentian->setAttributes($value);
                        }
                        $pegawaiPemberhentian->index = $key;
                        $model['pegawai_pemberhentian'][] = $pegawaiPemberhentian;
                    }
                }
                if (isset($post['PegawaiPendidikan'])) {
                    foreach ($post['PegawaiPendidikan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPendidikan = PegawaiPendidikan::find()->where(['id' => $value['id']])->one();
                            $pegawaiPendidikan->load($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPendidikan = PegawaiPendidikan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPendidikan->isDeleted = true;
                        } else {
                            $pegawaiPendidikan = new PegawaiPendidikan();
                            $pegawaiPendidikan->setAttributes($value);
                        }
                        $pegawaiPendidikan->index = $key;
                        $model['pegawai_pendidikan'][] = $pegawaiPendidikan;
                    }
                }
                if (isset($post['PegawaiPenetapanAngkaKredit'])) {
                    foreach ($post['PegawaiPenetapanAngkaKredit'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPenetapanAngkaKredit = PegawaiPenetapanAngkaKredit::find()->where(['id' => $value['id']])->one();
                            $pegawaiPenetapanAngkaKredit->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPenetapanAngkaKredit = PegawaiPenetapanAngkaKredit::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPenetapanAngkaKredit->isDeleted = true;
                        } else {
                            $pegawaiPenetapanAngkaKredit = new PegawaiPenetapanAngkaKredit();
                            $pegawaiPenetapanAngkaKredit->setAttributes($value);
                        }
                        $pegawaiPenetapanAngkaKredit->index = $key;
                        $model['pegawai_penetapan_angka_kredit'][] = $pegawaiPenetapanAngkaKredit;
                    }
                }
                if (isset($post['PegawaiPenghargaan'])) {
                    foreach ($post['PegawaiPenghargaan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPenghargaan = PegawaiPenghargaan::find()->where(['id' => $value['id']])->one();
                            $pegawaiPenghargaan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPenghargaan = PegawaiPenghargaan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPenghargaan->isDeleted = true;
                        } else {
                            $pegawaiPenghargaan = new PegawaiPenghargaan();
                            $pegawaiPenghargaan->setAttributes($value);
                        }
                        $pegawaiPenghargaan->index = $key;
                        $model['pegawai_penghargaan'][] = $pegawaiPenghargaan;
                    }
                }
                if (isset($post['PegawaiSaudaraKandung'])) {
                    foreach ($post['PegawaiSaudaraKandung'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiSaudaraKandung = PegawaiSaudaraKandung::find()->where(['id' => $value['id']])->one();
                            $pegawaiSaudaraKandung->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiSaudaraKandung = PegawaiSaudaraKandung::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiSaudaraKandung->isDeleted = true;
                        } else {
                            $pegawaiSaudaraKandung = new PegawaiSaudaraKandung();
                            $pegawaiSaudaraKandung->setAttributes($value);
                        }
                        $pegawaiSaudaraKandung->index = $key;
                        $model['pegawai_saudara_kandung'][] = $pegawaiSaudaraKandung;
                    }
                }
            }

            $transaction['pegawai'] = Pegawai::getDb()->beginTransaction();

            try {
                $model['pegawai']->updated_at = new \yii\db\Expression('NOW()');
                $model['pegawai']->updated_by = Yii::$app->user->identity->id;
                if (!$model['pegawai']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (true) {
                    if (isset($model['pegawai_anak']) and is_array($model['pegawai_anak'])) {
                        foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
                            $pegawaiAnak->pegawai = $model['pegawai']->id;
                            if (!$pegawaiAnak->isDeleted && !$pegawaiAnak->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_diklat']) and is_array($model['pegawai_diklat'])) {
                        foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
                            $pegawaiDiklat->pegawai = $model['pegawai']->id;
                            if (!$pegawaiDiklat->isDeleted && !$pegawaiDiklat->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_hukuman_disiplin']) and is_array($model['pegawai_hukuman_disiplin'])) {
                        foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
                            $pegawaiHukumanDisiplin->pegawai = $model['pegawai']->id;
                            if (!$pegawaiHukumanDisiplin->isDeleted && !$pegawaiHukumanDisiplin->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kenaikan_gaji_berkala']) and is_array($model['pegawai_kenaikan_gaji_berkala'])) {
                        foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
                            $pegawaiKenaikanGajiBerkala->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKenaikanGajiBerkala->isDeleted && !$pegawaiKenaikanGajiBerkala->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kepangkatan']) and is_array($model['pegawai_kepangkatan'])) {
                        foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
                            $pegawaiKepangkatan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKepangkatan->isDeleted && !$pegawaiKepangkatan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kunjungan_luar_negeri']) and is_array($model['pegawai_kunjungan_luar_negeri'])) {
                        foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
                            $pegawaiKunjunganLuarNegeri->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKunjunganLuarNegeri->isDeleted && !$pegawaiKunjunganLuarNegeri->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kursus_pelatihan']) and is_array($model['pegawai_kursus_pelatihan'])) {
                        foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
                            $pegawaiKursusPelatihan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKursusPelatihan->isDeleted && !$pegawaiKursusPelatihan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_masa_persiapan_pensiun']) and is_array($model['pegawai_masa_persiapan_pensiun'])) {
                        foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
                            $pegawaiMasaPersiapanPensiun->pegawai = $model['pegawai']->id;
                            if (!$pegawaiMasaPersiapanPensiun->isDeleted && !$pegawaiMasaPersiapanPensiun->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_mutasi']) and is_array($model['pegawai_mutasi'])) {
                        foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
                            $pegawaiMutasi->pegawai = $model['pegawai']->id;
                            if (!$pegawaiMutasi->isDeleted && !$pegawaiMutasi->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_organisasi']) and is_array($model['pegawai_organisasi'])) {
                        foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
                            $pegawaiOrganisasi->pegawai = $model['pegawai']->id;
                            if (!$pegawaiOrganisasi->isDeleted && !$pegawaiOrganisasi->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pelaksana']) and is_array($model['pegawai_pelaksana'])) {
                        foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
                            $pegawaiPelaksana->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPelaksana->isDeleted && !$pegawaiPelaksana->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pemberhentian']) and is_array($model['pegawai_pemberhentian'])) {
                        foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
                            $pegawaiPemberhentian->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPemberhentian->isDeleted && !$pegawaiPemberhentian->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pendidikan']) and is_array($model['pegawai_pendidikan'])) {
                        foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
                            $pegawaiPendidikan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPendidikan->isDeleted && !$pegawaiPendidikan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_penetapan_angka_kredit']) and is_array($model['pegawai_penetapan_angka_kredit'])) {
                        foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
                            $pegawaiPenetapanAngkaKredit->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPenetapanAngkaKredit->isDeleted && !$pegawaiPenetapanAngkaKredit->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_penghargaan']) and is_array($model['pegawai_penghargaan'])) {
                        foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
                            $pegawaiPenghargaan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPenghargaan->isDeleted && !$pegawaiPenghargaan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_saudara_kandung']) and is_array($model['pegawai_saudara_kandung'])) {
                        foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
                            $pegawaiSaudaraKandung->pegawai = $model['pegawai']->id;
                            if (!$pegawaiSaudaraKandung->isDeleted && !$pegawaiSaudaraKandung->validate()) $error = true;
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                if (true) {
                    if (isset($model['pegawai_anak']) and is_array($model['pegawai_anak'])) {
                        foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
                            if ($pegawaiAnak->isDeleted) {
                                if (!$pegawaiAnak->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiAnak->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_diklat']) and is_array($model['pegawai_diklat'])) {
                        foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
                            if ($pegawaiDiklat->isDeleted) {
                                if (!$pegawaiDiklat->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiDiklat->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_hukuman_disiplin']) and is_array($model['pegawai_hukuman_disiplin'])) {
                        foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
                            if ($pegawaiHukumanDisiplin->isDeleted) {
                                if (!$pegawaiHukumanDisiplin->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiHukumanDisiplin->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kenaikan_gaji_berkala']) and is_array($model['pegawai_kenaikan_gaji_berkala'])) {
                        foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
                            if ($pegawaiKenaikanGajiBerkala->isDeleted) {
                                if (!$pegawaiKenaikanGajiBerkala->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKenaikanGajiBerkala->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kepangkatan']) and is_array($model['pegawai_kepangkatan'])) {
                        foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
                            if ($pegawaiKepangkatan->isDeleted) {
                                if (!$pegawaiKepangkatan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKepangkatan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kunjungan_luar_negeri']) and is_array($model['pegawai_kunjungan_luar_negeri'])) {
                        foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
                            if ($pegawaiKunjunganLuarNegeri->isDeleted) {
                                if (!$pegawaiKunjunganLuarNegeri->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKunjunganLuarNegeri->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kursus_pelatihan']) and is_array($model['pegawai_kursus_pelatihan'])) {
                        foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
                            if ($pegawaiKursusPelatihan->isDeleted) {
                                if (!$pegawaiKursusPelatihan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKursusPelatihan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_masa_persiapan_pensiun']) and is_array($model['pegawai_masa_persiapan_pensiun'])) {
                        foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
                            if ($pegawaiMasaPersiapanPensiun->isDeleted) {
                                if (!$pegawaiMasaPersiapanPensiun->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiMasaPersiapanPensiun->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_mutasi']) and is_array($model['pegawai_mutasi'])) {
                        foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
                            if ($pegawaiMutasi->isDeleted) {
                                if (!$pegawaiMutasi->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiMutasi->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_organisasi']) and is_array($model['pegawai_organisasi'])) {
                        foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
                            if ($pegawaiOrganisasi->isDeleted) {
                                if (!$pegawaiOrganisasi->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiOrganisasi->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pelaksana']) and is_array($model['pegawai_pelaksana'])) {
                        foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
                            if ($pegawaiPelaksana->isDeleted) {
                                if (!$pegawaiPelaksana->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPelaksana->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pemberhentian']) and is_array($model['pegawai_pemberhentian'])) {
                        foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
                            if ($pegawaiPemberhentian->isDeleted) {
                                if (!$pegawaiPemberhentian->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPemberhentian->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pendidikan']) and is_array($model['pegawai_pendidikan'])) {
                        foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
                            if ($pegawaiPendidikan->isDeleted) {
                                if (!$pegawaiPendidikan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPendidikan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_penetapan_angka_kredit']) and is_array($model['pegawai_penetapan_angka_kredit'])) {
                        foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
                            if ($pegawaiPenetapanAngkaKredit->isDeleted) {
                                if (!$pegawaiPenetapanAngkaKredit->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPenetapanAngkaKredit->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_penghargaan']) and is_array($model['pegawai_penghargaan'])) {
                        foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
                            if ($pegawaiPenghargaan->isDeleted) {
                                if (!$pegawaiPenghargaan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPenghargaan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_saudara_kandung']) and is_array($model['pegawai_saudara_kandung'])) {
                        foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
                            if ($pegawaiSaudaraKandung->isDeleted) {
                                if (!$pegawaiSaudaraKandung->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiSaudaraKandung->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['pegawai']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pegawai']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if (true) {
                foreach ($model['pegawai']->pegawaiAnaks as $key => $pegawaiAnak)
                    $model['pegawai_anak'][] = $pegawaiAnak;
                foreach ($model['pegawai']->pegawaiDiklats as $key => $pegawaiDiklat)
                    $model['pegawai_diklat'][] = $pegawaiDiklat;
                foreach ($model['pegawai']->pegawaiHukumanDisiplins as $key => $pegawaiHukumanDisiplin)
                    $model['pegawai_hukuman_disiplin'][] = $pegawaiHukumanDisiplin;
                foreach ($model['pegawai']->pegawaiKenaikanGajiBerkalas as $key => $pegawaiKenaikanGajiBerkala)
                    $model['pegawai_kenaikan_gaji_berkala'][] = $pegawaiKenaikanGajiBerkala;
                foreach ($model['pegawai']->pegawaiKepangkatans as $key => $pegawaiKepangkatan)
                    $model['pegawai_kepangkatan'][] = $pegawaiKepangkatan;
                foreach ($model['pegawai']->pegawaiKunjunganLuarNegeris as $key => $pegawaiKunjunganLuarNegeri)
                    $model['pegawai_kunjungan_luar_negeri'][] = $pegawaiKunjunganLuarNegeri;
                foreach ($model['pegawai']->pegawaiKursusPelatihans as $key => $pegawaiKursusPelatihan)
                    $model['pegawai_kursus_pelatihan'][] = $pegawaiKursusPelatihan;
                foreach ($model['pegawai']->pegawaiMasaPersiapanPensiuns as $key => $pegawaiMasaPersiapanPensiun)
                    $model['pegawai_masa_persiapan_pensiun'][] = $pegawaiMasaPersiapanPensiun;
                foreach ($model['pegawai']->pegawaiMutasis as $key => $pegawaiMutasi)
                    $model['pegawai_mutasi'][] = $pegawaiMutasi;
                foreach ($model['pegawai']->pegawaiOrganisasis as $key => $pegawaiOrganisasi)
                    $model['pegawai_organisasi'][] = $pegawaiOrganisasi;
                foreach ($model['pegawai']->pegawaiPelaksanas as $key => $pegawaiPelaksana)
                    $model['pegawai_pelaksana'][] = $pegawaiPelaksana;
                foreach ($model['pegawai']->pegawaiPemberhentians as $key => $pegawaiPemberhentian)
                    $model['pegawai_pemberhentian'][] = $pegawaiPemberhentian;
                foreach ($model['pegawai']->pegawaiPendidikans as $key => $pegawaiPendidikan)
                    $model['pegawai_pendidikan'][] = $pegawaiPendidikan;
                foreach ($model['pegawai']->pegawaiPenetapanAngkaKredits as $key => $pegawaiPenetapanAngkaKredit)
                    $model['pegawai_penetapan_angka_kredit'][] = $pegawaiPenetapanAngkaKredit;
                foreach ($model['pegawai']->pegawaiPenghargaans as $key => $pegawaiPenghargaan)
                    $model['pegawai_penghargaan'][] = $pegawaiPenghargaan;
                foreach ($model['pegawai']->pegawaiSaudaraKandungs as $key => $pegawaiSaudaraKandung)
                $model['pegawai_saudara_kandung'][] = $pegawaiSaudaraKandung;
            }

            if ($model['pegawai']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-pegawai', [
                'model' => $model,
                'tab' => $tab,
                'title' => 'Ubah Data Saya : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
            ]);
        else
            return $this->redirect(['drh', 'tab' => $tab == 'personal' || $tab == 'pegawai' ? 'jabatan' : $tab]);
    }

    ///

    public function actionDatatablesKehadiran($bulan, $tahun)
    {
        return $this->datatables($this->datatablesKehadiran(Yii::$app->user->identity->id, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionKehadiran($bulan = null, $tahun = null)
    {
        $model['filter_kehadiran'] = new FilterKehadiran();
        if ($bulan) {
            $model['filter_kehadiran']->bulan = $bulan;
        }
        if ($tahun) {
            $model['filter_kehadiran']->tahun = $tahun;
        }

        if (($model['pegawai'] = Pegawai::find()->where(['id' => Yii::$app->user->identity->id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $model['kehadiran_bulanan'] = $this->datatablesKehadiranBulanan($model['pegawai']->id, $model['filter_kehadiran']->bulan, $model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_bulanan'] = [
            'hari_kerja' => $model['kehadiran_bulanan']['hari_kerja'],
            'hadir' => $model['kehadiran_bulanan']['hadir'],
            'tidak_hadir' => $model['kehadiran_bulanan']['tidak_hadir'],
            'belum_absen' => $model['kehadiran_bulanan']['belum_absen'],
            'libur' => $model['kehadiran_bulanan']['libur'],
            'potongan_tunkin' => $model['kehadiran_bulanan']['potongan_tunkin'],
            'uang_makan' => $model['kehadiran_bulanan']['hadir'],
            'disiplin' => $model['kehadiran_bulanan']['kurang_detik'],
        ];

        $model['kehadiran_tahunan'] = $this->datatablesKehadiranTahunan($model['pegawai']->id,$model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_tahunan'] = [
            'disiplin' => $model['kehadiran_tahunan']['kurang_detik'],
        ];

        return $this->render('kehadiran', [
            'model' => $model,
            'title' => 'Daftar Kehadiran Pegawai',
        ]);
    }

    public function actionKehadiranDetail($id)
    {
        if (($model['kehadiran'] = $this->datatablesKehadiran(null, null, null)->addSelect(['k.tanggal AS tanggal_original', 'k.pegawai AS pegawai'])->where(['k.id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        if (($model['pegawai'] = Pegawai::find()->where(['id' => $model['kehadiran']['pegawai']])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'bk.id',
            ])
            ->from('bukti_kehadiran bk')
            ->join('LEFT JOIN', 'bukti_kehadiran_pegawai bkp', 'bkp.bukti_kehadiran = bk.id')
            ->where([
                'bkp.pegawai' => $model['kehadiran']['pegawai'],
            ])
            ->andWhere('"' . $model['kehadiran']['tanggal_original'] . '" between bk.dari_tanggal and bk.sampai_tanggal')
            ->orderBy(['waktu_disetujui' => SORT_DESC])
        ;
        // ddx($query->createCommand()->sql);
        // ddx($query->all());

        $model['bukti_kehadiran'] = [];
        foreach ($query->all() as $key => $value) {
            $buktiKehadiran = BuktiKehadiran::find()->where(['id' => $value['id']])->one();
            if (!$buktiKehadiran) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
            if ($buktiKehadiran->keterangan_kehadiran == 1111) {
                break;
            } else if ($buktiKehadiran->status_pengajuan == 'Diterima') {
                $model['bukti_kehadiran'][] = $buktiKehadiran;
            }
        }

        return $this->render('detail-kehadiran', [
            'model' => $model,
            'title' => 'Detail Kehadiran : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
        ]);
    }

    ///

    public function actionDatatablesBuktiKehadiran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'bk.id',
                'bk.status_pengajuan',
                'kh.keterangan AS keterangan',
                'DATE_FORMAT(bk.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(bk.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(bk.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
            ])
            ->from('bukti_kehadiran bk')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = bk.keterangan_kehadiran')
            ->where([
                'bk.diajukan' => Yii::$app->user->identity->id,
                'kh.diajukan_pegawai' => 'Ya',
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), BuktiKehadiran::getDb());
    }

    public function actionBuktiKehadiran($id = null)
    {
        if (!$id) {
            return $this->render('list-bukti-kehadiran', [
                'title' => 'Status Pengajuan Sakit / Izin',
            ]);
        }

        if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-bukti-kehadiran', [
            'model' => $model,
            'title' => 'Detail Pengajuan Sakit / Izin',
        ]);
    }

    public function actionBuktiKehadiranCreate()
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_kehadiran'] = new BuktiKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bukti_kehadiran']->load($post);
            $buktiKehadiranPegawai = new BuktiKehadiranPegawai();
            $buktiKehadiranPegawai->pegawai = Yii::$app->user->identity->id;
            $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;

            $transaction['bukti_kehadiran'] = BuktiKehadiran::getDb()->beginTransaction();

            try {
                $model['bukti_kehadiran']->nomor_surat = '-';
                $model['bukti_kehadiran']->status_pengajuan = 'Diajukan';
                $model['bukti_kehadiran']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_kehadiran']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if (isset($model['bukti_kehadiran_pegawai']) and is_array($model['bukti_kehadiran_pegawai'])) {
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        $buktiKehadiranPegawai->bukti_kehadiran = $model['bukti_kehadiran']->id;
                        if (!$buktiKehadiranPegawai->isDeleted && !$buktiKehadiranPegawai->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        if ($buktiKehadiranPegawai->isDeleted) {
                            if (!$buktiKehadiranPegawai->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$buktiKehadiranPegawai->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                if (YII_ENV == 'prod' && false) {
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-kehadiran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Kehadiran')->send();
                } else if (YII_ENV == 'dev' && false) {
                    /*return $this->render('//email/verifikasi-pengajuan-bukti-kehadiran', [
                        'model' => $model,
                        'title' => 'Segera Verifikasi Pengajuan Bukti Kehadiran',
                    ]);*/
                    \Yii::$app->mail->compose('email/verifikasi-pengajuan-bukti-kehadiran', ['model' => $model])
                        ->setFrom(['pradana.fandy@gmail.com' => 'Admin'])->setTo('zardranta04@gmail.com')
                        ->setSubject('Segera Verifikasi Pengajuan Bukti Kehadiran')->send();
                }


                $transaction['bukti_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_kehadiran']->isNewRecord) {
            }

            foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai)
                $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
        }

        if ($error)
            return $this->render('form-bukti-kehadiran', [
                'model' => $model,
                'title' => 'Unggah Pengajuan Sakit / Izin',
            ]);
        else
            return $this->redirect(['bukti-kehadiran']);
    }

    public function actionBuktiKehadiranUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['bukti_kehadiran'] = BuktiKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['bukti_kehadiran'] = new BuktiKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['bukti_kehadiran']->load($post);
            if (isset($post['BuktiKehadiranPegawai'])) {
                foreach ($post['BuktiKehadiranPegawai'] as $key => $value) {
                    if ($value['id'] > 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']])->one();
                        $buktiKehadiranPegawai->setAttributes($value);
                    } else if($value['id'] < 0) {
                        $buktiKehadiranPegawai = BuktiKehadiranPegawai::find()->where(['id' => $value['id']*-1])->one();
                        $buktiKehadiranPegawai->isDeleted = true;
                    } else {
                        $buktiKehadiranPegawai = new BuktiKehadiranPegawai();
                        $buktiKehadiranPegawai->setAttributes($value);
                    }
                    $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
                }
            }

            $transaction['bukti_kehadiran'] = BuktiKehadiran::getDb()->beginTransaction();

            try {
                $model['bukti_kehadiran']->status_pengajuan = 'Diajukan';
                $model['bukti_kehadiran']->diajukan = Yii::$app->user->identity->id;
                $model['bukti_kehadiran']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['bukti_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if (isset($model['bukti_kehadiran_pegawai']) and is_array($model['bukti_kehadiran_pegawai'])) {
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        $buktiKehadiranPegawai->bukti_kehadiran = $model['bukti_kehadiran']->id;
                        if (!$buktiKehadiranPegawai->isDeleted && !$buktiKehadiranPegawai->validate()) $error = true;
                    }

                    if ($error) {
                        throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                    }
                
                    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
                        if ($buktiKehadiranPegawai->isDeleted) {
                            if (!$buktiKehadiranPegawai->delete()) {
                                $error = true;
                            }
                        } else {
                            if (!$buktiKehadiranPegawai->save()) {
                                $error = true;
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['bukti_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['bukti_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['bukti_kehadiran']->isNewRecord) {
            }

            foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai)
                $model['bukti_kehadiran_pegawai'][] = $buktiKehadiranPegawai;
        }

        if ($error)
            return $this->render('form-bukti-kehadiran', [
                'model' => $model,
                'title' => 'Revisi Pengajuan Sakit / Izin',
            ]);
        else
            return $this->redirect(['bukti-kehadiran']);
    }

    ///

    public function actionDatatablesCuti()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'c.pegawai' => Yii::$app->user->identity->id,
                'kh.diajukan_pegawai_online' => 'Ya',
            ])
        ;

        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionCuti($id = null)
    {
        if (!$id) {
            return $this->render('list-cuti', [
                'title' => 'Status Pengajuan Cuti Online',
            ]);
        }

        if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-cuti', [
            'model' => $model,
            'title' => 'Detail Cuti Online',
        ]);
    }

    public function actionCutiCreate()
    {
        $error = true;

        if (isset($id)) {
            if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['cuti'] = new Cuti();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['cuti']->load($post);

            $transaction['cuti'] = Cuti::getDb()->beginTransaction();

            try {
                $model['cuti']->status_pengajuan = 'Diajukan';
                $model['cuti']->pegawai = Yii::$app->user->identity->id;
                $model['cuti']->waktu_pengajuan = new \yii\db\Expression('NOW()');
                if (!$model['cuti']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['cuti']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['cuti']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            $model['cuti']->pegawai = Yii::$app->user->identity->id;
        }

        if ($error)
            return $this->render('form-cuti', [
                'model' => $model,
                'title' => 'Unggah Cuti Online',
            ]);
        else
            return $this->redirect(['cuti']);
    }

    public function actionCutiUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['cuti'] = new Cuti();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['cuti']->load($post);

            $transaction['cuti'] = Cuti::getDb()->beginTransaction();

            try {
                $model['cuti']->status_pengajuan = 'Diajukan';
                $model['cuti']->waktu_pengajuan = new \yii\db\Expression('NOW()');

                $model['cuti']->waktu_atasan_1 = null;
                $model['cuti']->catatan_atasan_1 = null;
                $model['cuti']->waktu_atasan_2 = null;
                $model['cuti']->catatan_atasan_2 = null;
                if (!$model['cuti']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['cuti']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['cuti']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-cuti', [
                'model' => $model,
                'title' => 'Revisi Cuti Online',
            ]);
        else
            return $this->redirect(['cuti']);
    }

    ///

    public function actionDatatablesCutiVerifikasi()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'kh.diajukan_pegawai_online' => 'Ya',
            ])
            ->andWhere(['or',
                ['and', ['c.status_pengajuan' => 'Diajukan', 'c.atasan_1' => Yii::$app->user->identity->id]],
                ['and', ['c.status_pengajuan' => 'Diterima Atasan 1', 'c.atasan_2' => Yii::$app->user->identity->id]],
            ])
        ;

        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionDatatablesCutiVerifikasiMonitoring()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'kh.diajukan_pegawai_online' => 'Ya',
            ])
            ->andWhere(['not', ['or',
                ['and', ['c.status_pengajuan' => 'Diajukan', 'c.atasan_1' => Yii::$app->user->identity->id]],
                ['and', ['c.status_pengajuan' => 'Diterima Atasan 1', 'c.atasan_2' => Yii::$app->user->identity->id]],
            ]])
        ;

        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionCutiVerifikasi($id = null)
    {
        if (!$id) {
            return $this->render('list-cuti-verifikasi', [
                'title' => 'Verifikasi Pengajuan Cuti Online',
            ]);
        }

        if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-cuti-verifikasi', [
            'model' => $model,
            'title' => 'Detail Cuti Online',
        ]);
    }

    public function actionCutiVerifikasi1($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['cuti'] = new Cuti();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['cuti']->load($post);

            $transaction['cuti'] = Cuti::getDb()->beginTransaction();

            try {
                $model['cuti']->waktu_atasan_1 = new \yii\db\Expression('NOW()');
                if (!$model['cuti']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['cuti']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['cuti']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-cuti-verifikasi-1', [
                'model' => $model,
                'title' => 'Verifikasi Cuti Online',
            ]);
        else
            return $this->redirect(['cuti-verifikasi']);
    }

    public function actionCutiVerifikasi2($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['cuti'] = new Cuti();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['cuti']->load($post);

            $transaction['cuti'] = Cuti::getDb()->beginTransaction();

            try {
                $model['cuti']->waktu_atasan_2 = new \yii\db\Expression('NOW()');
                if (!$model['cuti']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['cuti']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['cuti']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-cuti-verifikasi-2', [
                'model' => $model,
                'title' => 'Verifikasi Cuti Online',
            ]);
        else
            return $this->redirect(['cuti-verifikasi']);
    }

    ///

    public function actionDatatablesKeteranganKehadiran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'kk.id',
                'kk.kode',
                'kk.keterangan',
                'kk.dihitung_hadir',
                'kk.potong_tunkin',
                'IFNULL(kk.potongan_tunkin_persen, "") AS potongan_tunkin_persen',
                'kk.potong_uang_makan',
                'kk.potong_disiplin',
                'IFNULL(kk.batas_awal_unggah, "") AS batas_awal_unggah',
                'IFNULL(kk.batas_akhir_unggah, "") AS batas_akhir_unggah',
            ])
            ->from('keterangan_kehadiran kk')
        ;

        return $this->datatables($query, Yii::$app->request->post(), KeteranganKehadiran::getDb());
    }

    public function actionKeteranganKehadiran($id = null)
    {
        if (!$id) {
            return $this->render('list-keterangan-kehadiran', [
                'title' => 'Keterangan Kehadiran',
            ]);
        }

        if (($model['keterangan_kehadiran'] = KeteranganKehadiran::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-keterangan-kehadiran', [
            'model' => $model,
            'title' => 'Detail Keterangan Kehadiran',
        ]);
    }

    ///

    public function actionGantiPassword()
    {
        $error = true;

        $id = Yii::$app->user->identity->id;

        if (($model['pegawai'] = Pegawai::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
        if (($model['user'] = User::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
        $model['user']->scenario = 'password-change';

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['user']->load($post);

            $transaction['user'] = User::getDb()->beginTransaction();

            try {
                $model['user']->setPassword($model['user']->password);
                $model['user']->generateAuthKey();
                if (!$model['user']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                $transaction['user']->commit();
                Yii::$app->session->setFlash('info', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            } catch (\Exception $e) {
                $error = true;
                $transaction['user']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('info', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-ganti-password', [
                'model' => $model,
                'title' => 'Ganti Password',
            ]);
        else
            return $this->redirect(['index']);
    }
}