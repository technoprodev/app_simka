<?php

use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['general/reset-password', 'token' => $user->password_reset_token]);
?>

<div style="box-sizing:border-box;padding:15px!important;color: #595959;background-color: #fafafa">
    <div style="box-sizing:border-box;width:100%;max-width:600px;margin:30px auto!important;background:#fff;border:1px solid #3376b8;border-radius: 8px">
        <div style="box-sizing:border-box;padding:15px!important;">
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;"><b style="box-sizing:border-box;font-weight:bolder;">PERHATIAN !</b> Klik link dibawah ini untuk reset password Anda.</p>
            <p style="box-sizing:border-box;max-width:100%;margin-top:0;margin-bottom:15px;margin-right:0;margin-left:0;font-weight:inherit;word-break:normal;word-wrap:break-word;text-align:center;"><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
        </div>
    </div>
</div>