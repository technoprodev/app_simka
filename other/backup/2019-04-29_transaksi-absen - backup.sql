alter table kehadiran add column jadwal_batas_masuk datetime;
alter table kehadiran add column jadwal_batas_pulang datetime;

DELIMITER $$

DROP PROCEDURE IF EXISTS assign_kehadiran;
CREATE PROCEDURE assign_kehadiran(tgl_dari date, tgl_sampai date)
BEGIN
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update waktu masuk
  update kehadiran k
  inner join (
    select id, substring_index(min(diff_order), '/', -1) as tr_time from (
      select
        k.id,
        @tr_time:=timestamp(a.tr_date, a.tr_time),
        @diff_val:=timestampdiff(minute, k.jadwal_masuk, timestamp(a.tr_date, a.tr_time)) as diff_val,
        @priority:=@diff_val >= 0,
        @diff:=abs(@diff_val),
        concat(@priority, lpad(@diff, 4, '0'), '/', @tr_time) as diff_order
      from kehadiran k
      inner join pegawai p on p.id=k.pegawai
      inner join `absensi`.`habsen` a on p.pin=a.empl_code and k.tanggal=a.tr_date
      where
        (
          case
            when tgl_dari is not null and tgl_sampai is not null then a.tr_date between tgl_dari and tgl_sampai
            when tgl_dari is not null then a.tr_date >= tgl_dari
            when tgl_sampai is not null then a.tr_date <= tgl_sampai
            else true
          end
        )
        and k.jadwal_masuk is not null and k.jadwal_pulang is not null
        and timestamp(a.tr_date, a.tr_time) between date_sub(k.jadwal_batas_pulang, interval 1 day) and k.jadwal_batas_masuk
      order by diff_order
    ) as t1
    group by id
  ) as t2 on t2.id=k.id
  set waktu_masuk=t2.tr_time;

  -- update waktu pulang
  update kehadiran k
  inner join (
    select id, substring_index(max(diff_order), '/', -1) as tr_time from (
      select
        k.id,
        @tr_time:=timestamp(a.tr_date, a.tr_time),
        @diff_val:=timestampdiff(minute, k.jadwal_pulang, timestamp(a.tr_date, a.tr_time)) as diff_val,
        @priority:=@diff_val > 0,
        @diff:=abs(@diff_val),
        concat(@priority, lpad(@diff, 4, '0'), '/', @tr_time) as diff_order
      from kehadiran k
      inner join pegawai p on p.id=k.pegawai
      inner join `absensi`.`habsen` a on p.pin=a.empl_code and k.tanggal=a.tr_date
      where
        (
          case
            when tgl_dari is not null and tgl_sampai is not null then a.tr_date between tgl_dari and tgl_sampai
            when tgl_dari is not null then a.tr_date >= tgl_dari
            when tgl_sampai is not null then a.tr_date <= tgl_sampai
            else true
          end
        )
        and k.jadwal_pulang is not null and k.jadwal_pulang is not null
        and timestamp(a.tr_date, a.tr_time) between k.jadwal_batas_masuk and k.jadwal_batas_pulang
      order by diff_order desc
    ) as t1
    group by id
  ) as t2 on t2.id=k.id
  set waktu_pulang=t2.tr_time;

  -- update status kehadiran
  update kehadiran set keterangan = (
    case
      when waktu_masuk is null and waktu_pulang is null then 'Tanpa Keterangan'
      when timestampdiff(minute, jadwal_masuk, jadwal_pulang) > timestampdiff(minute, waktu_masuk, waktu_pulang) then 'Kurang Jam'
      else 'Hadir'
    end
  )
  where
    (
      case
        when tgl_dari is not null and tgl_sampai is not null then tanggal between tgl_dari and tgl_sampai
        when tgl_dari is not null then tanggal >= tgl_dari
        when tgl_sampai is not null then tanggal <= tgl_sampai
        else true
      end
    )
    and jadwal_masuk is not null and jadwal_pulang is not null;

  commit;
END$$

DELIMITER ;
