<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_saudara_kandung".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $nama
 * @property string $jenis_kelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $pekerjaan
 * @property integer $anak_ke
 *
 * @property Pegawai $pegawai0
 * @property Pekerjaan $pekerjaan0
 */
class PegawaiSaudaraKandung extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;

    public static function tableName()
    {
        return 'pegawai_saudara_kandung';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],

            //jenis_kelamin
            [['jenis_kelamin'], 'required'],
            [['jenis_kelamin'], 'string'],

            //tempat_lahir
            [['tempat_lahir'], 'required'],
            [['tempat_lahir'], 'string', 'max' => 256],

            //tanggal_lahir
            [['tanggal_lahir'], 'required'],
            [['tanggal_lahir'], 'safe'],

            //pekerjaan
            [['pekerjaan'], 'required'],
            [['pekerjaan'], 'integer'],
            [['pekerjaan'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan' => 'id']],

            //anak_ke
            [['anak_ke'], 'required'],
            [['anak_ke'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'nama' => 'Nama',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'pekerjaan' => 'Pekerjaan',
            'anak_ke' => 'Anak Ke',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaan0()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan']);
    }
}
