ALTER TABLE `pegawai`
ADD `eselon` int(11) NULL AFTER `unit_kerja`,
ADD FOREIGN KEY (`eselon`) REFERENCES `eselon` (`id`);

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrasi',	1,	'Administrasi',	NULL,	NULL,	1461048991,	1461048991);

DROP TABLE IF EXISTS `bukti_kehadiran`;
CREATE TABLE `bukti_kehadiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_pengajuan` enum('Diajukan','Diterima Atasan','Direvisi Atasan','Ditolak Atasan','Diterima','Direvisi','Ditolak') NOT NULL,
  `diajukan` int(11) NOT NULL,
  `waktu_pengajuan` datetime NOT NULL,
  `catatan_pengajuan` varchar(256) DEFAULT NULL,
  `disetujui_atasan` int(11) DEFAULT NULL,
  `waktu_disetujui_atasan` datetime DEFAULT NULL,
  `catatan_disetujui_atasan` varchar(256) DEFAULT NULL,
  `disetujui` int(11) DEFAULT NULL,
  `waktu_disetujui` datetime DEFAULT NULL,
  `catatan_disetujui` varchar(256) DEFAULT NULL,
  `keterangan_kehadiran` int(11) NOT NULL,
  `dari_tanggal` date NOT NULL,
  `sampai_tanggal` date NOT NULL,
  `bukti_utama` varchar(256) NOT NULL,
  `bukti_tambahan_1` varchar(256) DEFAULT NULL,
  `bukti_tambahan_2` varchar(256) DEFAULT NULL,
  `bukti_tambahan_3` varchar(256) DEFAULT NULL,
  `bukti_tambahan_4` varchar(256) DEFAULT NULL,
  `bukti_tambahan_5` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diajukan` (`diajukan`),
  KEY `disetujui` (`disetujui`),
  KEY `disetujui_atasan` (`disetujui_atasan`),
  KEY `keterangan_kehadiran` (`keterangan_kehadiran`),
  CONSTRAINT `bukti_kehadiran_ibfk_1` FOREIGN KEY (`diajukan`) REFERENCES `pegawai` (`id`),
  CONSTRAINT `bukti_kehadiran_ibfk_2` FOREIGN KEY (`disetujui`) REFERENCES `pegawai` (`id`),
  CONSTRAINT `bukti_kehadiran_ibfk_4` FOREIGN KEY (`disetujui_atasan`) REFERENCES `pegawai` (`id`),
  CONSTRAINT `bukti_kehadiran_ibfk_5` FOREIGN KEY (`keterangan_kehadiran`) REFERENCES `keterangan_kehadiran` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `bukti_kehadiran_pegawai`;
CREATE TABLE `bukti_kehadiran_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bukti_kehadiran` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bukti_kehadiran` (`bukti_kehadiran`),
  KEY `pegawai` (`pegawai`),
  CONSTRAINT `bukti_kehadiran_pegawai_ibfk_1` FOREIGN KEY (`bukti_kehadiran`) REFERENCES `bukti_kehadiran` (`id`),
  CONSTRAINT `bukti_kehadiran_pegawai_ibfk_2` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `grade` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `grade` int NOT NULL,
  `nominal_tunkin` int NOT NULL
);

ALTER TABLE `pegawai`
ADD `grade` int(11) NULL AFTER `eselon`,
ADD FOREIGN KEY (`grade`) REFERENCES `grade` (`id`);

--

ALTER TABLE `keterangan_kehadiran`
ADD `diajukan_administrasi` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'Tidak',
ADD `diajukan_pegawai` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'Tidak' AFTER `diajukan_administrasi`,
ADD `diajukan_kepegawaian` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'Tidak' AFTER `diajukan_pegawai`;

UPDATE `keterangan_kehadiran` SET
`id` = '8',
`kode` = 'DL',
`keterangan` = 'Dinas Luar',
`default_sistem` = 2,
`dihitung_hadir` = 2,
`potong_tunkin` = 2,
`potong_uang_makan` = 1,
`potong_disiplin` = 2,
`diajukan_administrasi` = 1,
`diajukan_pegawai` = 2,
`diajukan_kepegawaian` = 1
WHERE `id` = '8';

UPDATE `keterangan_kehadiran` SET
`id` = '11',
`kode` = 'D',
`keterangan` = 'Diklat',
`default_sistem` = 2,
`dihitung_hadir` = 2,
`potong_tunkin` = 2,
`potong_uang_makan` = 1,
`potong_disiplin` = 2,
`diajukan_administrasi` = 1,
`diajukan_pegawai` = 2,
`diajukan_kepegawaian` = 1
WHERE `id` = '11';

UPDATE `keterangan_kehadiran` SET
`id` = '9',
`kode` = 'I',
`keterangan` = 'Izin',
`default_sistem` = 2,
`dihitung_hadir` = 2,
`potong_tunkin` = 2,
`potong_uang_makan` = 1,
`potong_disiplin` = 2,
`diajukan_administrasi` = 2,
`diajukan_pegawai` = 1,
`diajukan_kepegawaian` = 1
WHERE `id` = '9';

UPDATE `keterangan_kehadiran` SET
`id` = '10',
`kode` = 'C',
`keterangan` = 'Cuti',
`default_sistem` = 2,
`dihitung_hadir` = 2,
`potong_tunkin` = 2,
`potong_uang_makan` = 1,
`potong_disiplin` = 2,
`diajukan_administrasi` = 2,
`diajukan_pegawai` = 1,
`diajukan_kepegawaian` = 1
WHERE `id` = '10';

ALTER TABLE `kepangkatan`
ADD `nominal_uang_makan` int NULL DEFAULT '0';

UPDATE `kepangkatan` SET `nominal_uang_makan` = '41000' WHERE `id` = '1';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '41000' WHERE `id` = '2';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '41000' WHERE `id` = '3';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '41000' WHERE `id` = '4';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '41000' WHERE `id` = '5';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '37000' WHERE `id` = '6';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '37000' WHERE `id` = '7';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '37000' WHERE `id` = '8';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '37000' WHERE `id` = '9';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '10';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '11';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '12';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '13';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '14';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '15';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '16';
UPDATE `kepangkatan` SET `nominal_uang_makan` = '35000' WHERE `id` = '17';

ALTER TABLE `jadwal_kehadiran`
ADD `waktu_mulai_istirahat` time NULL AFTER `fleksi_time`,
ADD `waktu_selesai_istirahat` time NULL AFTER `waktu_mulai_istirahat`;

ALTER TABLE `kehadiran`
ADD `waktu_mulai_istirahat` time NULL AFTER `fleksi_time`,
ADD `waktu_selesai_istirahat` time NULL AFTER `waktu_mulai_istirahat`;

ALTER TABLE `keterangan_kehadiran`
ADD `tampilkan_waktu_absen` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'Ya' AFTER `default_sistem`;

ALTER TABLE `kehadiran`
ADD `keterangan_diupdate` enum('Ya','Tidak') NULL DEFAULT 'Tidak' AFTER `keterangan`;

update kehadiran set keterangan_diupdate = 'Ya' where keterangan_default <> keterangan;

ALTER TABLE `kehadiran`
DROP FOREIGN KEY `kehadiran_ibfk_2`;

ALTER TABLE `kehadiran`
DROP INDEX `keterangan_default`;

ALTER TABLE `kehadiran`
DROP `keterangan_default`,
DROP `waktu_masuk`,
DROP `waktu_pulang`;

ALTER TABLE `kehadiran`
CHANGE `waktu_masuk_default` `waktu_masuk` datetime NULL AFTER `jadwal_batas_akhir_pulang`,
CHANGE `waktu_pulang_default` `waktu_pulang` datetime NULL AFTER `waktu_masuk`;

ALTER TABLE `pegawai_kepangkatan`
ADD `status_kepegawaian` int(11) NULL AFTER `kepangkatan`,
ADD FOREIGN KEY (`status_kepegawaian`) REFERENCES `status_kepegawaian` (`id`);

update pegawai_kepangkatan set status_kepegawaian = 1;

ALTER TABLE `pegawai_kepangkatan`
CHANGE `status_kepegawaian` `status_kepegawaian` int(11) NOT NULL AFTER `kepangkatan`;

ALTER TABLE `pegawai`
CHANGE `keterangan_fisik_tambahan` `hobi` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `warna_kulit`;

ALTER TABLE `keterangan_kehadiran`
ADD `color` varchar(256) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'grayest';

ALTER TABLE `kehadiran`
ADD `waktu_terakhir_diupdate` timestamp NULL ON UPDATE CURRENT_TIMESTAMP AFTER `waktu_pulang`;

ALTER TABLE `pegawai`
ADD `unit_kerja_administrasi` int(11) NULL,
ADD FOREIGN KEY (`unit_kerja_administrasi`) REFERENCES `unit_kerja` (`id`);

INSERT INTO `keterangan_kehadiran` (`id`, `kode`, `keterangan`, `default_sistem`, `tampilkan_waktu_absen`, `dihitung_hadir`, `potong_tunkin`, `potong_uang_makan`, `potong_disiplin`, `diajukan_administrasi`, `diajukan_pegawai`, `diajukan_kepegawaian`, `color`) VALUES
(1111,  'X',  'Pembatalan', 'Ya', 'Tidak',  'Tidak Dihitung', 'Tidak',  'Tidak',  'Tidak',  'Tidak',  'Tidak',  'Ya', 'grayest');

ALTER TABLE `pegawai`
ADD UNIQUE `pin` (`pin`);

ALTER TABLE `pegawai`
ADD `pimpinan_unit_kerja` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NULL AFTER `kode_pos_ibu`;

ALTER TABLE `unit_kerja`
CHANGE `jenis_unit_kerja` `jenis_unit_kerja` int(11) NULL AFTER `id`;

ALTER TABLE `unit_kerja`
CHANGE `jenis_unit_kerja` `jenis_unit_kerja` int(11) NULL AFTER `id`;

ALTER TABLE `bukti_kehadiran`
CHANGE `status_pengajuan` `status_pengajuan` enum('Diajukan','Diajukan Pegawai','Diterima Atasan','Direvisi Atasan','Ditolak Atasan','Diterima','Direvisi','Ditolak') COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

ALTER TABLE `pegawai`
ADD `tunkin_nama` varchar(256) NULL,
ADD `tunkin_nomor_rekening` varchar(256) NULL AFTER `tunkin_nama`;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('export',  1,  'Export', NULL, NULL, 1461048991, 1461048991);

ALTER TABLE `kehadiran`
CHANGE `waktu_mulai_istirahat` `waktu_mulai_istirahat` datetime NULL AFTER `fleksi_time`,
CHANGE `waktu_selesai_istirahat` `waktu_selesai_istirahat` datetime NULL AFTER `waktu_mulai_istirahat`;

ALTER TABLE `kehadiran`
CHANGE `fleksi_time` `fleksi_time` int(11) NULL DEFAULT '0' AFTER `tanggal`;

ALTER TABLE `pegawai`
ADD `tunkin_nip` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `tunkin_nama`;

ALTER TABLE `unit_kerja`
ADD `level` int(11) NULL;

ALTER TABLE `grade`
ADD `pajak_tunkin` int(11) NULL;

ALTER TABLE `status_kepegawaian`
ADD `pajak_tunkin` int NULL;

ALTER TABLE `bukti_kehadiran`
CHANGE `catatan_disetujui` `catatan_verifikasi` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `waktu_disetujui`;

CREATE TABLE `bukti_cctv` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pegawai` int(11) NOT NULL,
  `status_pengajuan` enum('Diajukan','Diajukan Pegawai','Diterima Atasan','Direvisi Atasan','Ditolak Atasan','Diterima','Direvisi','Ditolak') NOT NULL,
  `diajukan` int(11) NOT NULL,
  `waktu_pengajuan` datetime NOT NULL,
  `catatan_pengajuan` varchar(256) NULL,
  `disetujui` int(11) NULL,
  `waktu_disetujui` datetime NULL,
  `catatan_verifikasi` varchar(256) NULL,
  `tanggal` date NOT NULL,
  `bukti_utama` varchar(256) NOT NULL,
  `bukti_tambahan_1` varchar(256) NULL,
  `bukti_tambahan_2` varchar(256) NULL,
  `bukti_tambahan_3` varchar(256) NULL,
  `bukti_tambahan_4` varchar(256) NULL,
  `bukti_tambahan_5` varchar(256) NULL,
  FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`),
  FOREIGN KEY (`diajukan`) REFERENCES `pegawai` (`id`),
  FOREIGN KEY (`disetujui`) REFERENCES `pegawai` (`id`)
);

ALTER TABLE `kehadiran`
ADD `waktu_masuk_diupdate` enum('Ya','Tidak') NULL DEFAULT 'Tidak' AFTER `waktu_pulang`,
ADD `waktu_pulang_diupdate` enum('Ya','Tidak') NULL DEFAULT 'Tidak' AFTER `waktu_masuk_diupdate`;

ALTER TABLE `bukti_cctv`
ADD `jenis` enum('Waktu Masuk','Waktu Pulang','Pembatalan') COLLATE 'latin1_swedish_ci' NOT NULL AFTER `catatan_verifikasi`,
CHANGE `bukti_utama` `bukti_utama` varchar(256) COLLATE 'latin1_swedish_ci' NULL AFTER `tanggal`;

ALTER TABLE `bukti_cctv`
ADD `waktu` time NOT NULL AFTER `tanggal`;

ALTER TABLE `bukti_kehadiran`
ADD `nomor_surat` varchar(256) NOT NULL DEFAULT '-' AFTER `sampai_tanggal`;

ALTER TABLE `keterangan_kehadiran`
ADD `batas_awal_unggah` datetime NULL AFTER `diajukan_kepegawaian`,
ADD `batas_akhir_unggah` datetime NULL AFTER `batas_awal_unggah`;

ALTER TABLE `keterangan_kehadiran`
ADD `potongan_tunkin_persen` double NULL AFTER `potong_tunkin`;

ALTER TABLE `keterangan_kehadiran`
CHANGE `potong_tunkin` `potong_tunkin` enum('Ya','Tidak') COLLATE 'latin1_swedish_ci' NULL AFTER `dihitung_hadir`;

ALTER TABLE `jadwal_kehadiran`
ADD `prioritas` tinyint NULL AFTER `pegawai`,
ADD `status` enum('Sedang Berlaku','Tidak Berlaku') NULL AFTER `prioritas`;

ALTER TABLE `jenjang_jabatan`
ADD `batas_pensiun` int NULL AFTER `jenjang_jabatan`;

ALTER TABLE `jadwal_kehadiran`
CHANGE `waktu_mulai_istirahat` `senin_waktu_mulai_istirahat` time NULL AFTER `fleksi_time`,
ADD `selasa_waktu_mulai_istirahat` time NULL AFTER `senin_waktu_mulai_istirahat`,
ADD `rabu_waktu_mulai_istirahat` time NULL AFTER `selasa_waktu_mulai_istirahat`,
ADD `kamis_waktu_mulai_istirahat` time NULL AFTER `rabu_waktu_mulai_istirahat`,
ADD `jumat_waktu_mulai_istirahat` time NULL AFTER `kamis_waktu_mulai_istirahat`,
ADD `sabtu_waktu_mulai_istirahat` time NULL AFTER `jumat_waktu_mulai_istirahat`,
ADD `minggu_waktu_mulai_istirahat` time NULL AFTER `sabtu_waktu_mulai_istirahat`,
CHANGE `waktu_selesai_istirahat` `senin_waktu_selesai_istirahat` time NULL AFTER `minggu_waktu_mulai_istirahat`,
ADD `selasa_waktu_selesai_istirahat` time NULL AFTER `senin_waktu_selesai_istirahat`,
ADD `rabu_waktu_selesai_istirahat` time NULL AFTER `selasa_waktu_selesai_istirahat`,
ADD `kamis_waktu_selesai_istirahat` time NULL AFTER `rabu_waktu_selesai_istirahat`,
ADD `jumat_waktu_selesai_istirahat` time NULL AFTER `kamis_waktu_selesai_istirahat`,
ADD `sabtu_waktu_selesai_istirahat` time NULL AFTER `jumat_waktu_selesai_istirahat`,
ADD `minggu_waktu_selesai_istirahat` time NULL AFTER `sabtu_waktu_selesai_istirahat`;

