<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FlatpickrAsset::register($this);

//
$errorMessage = '';
if ($model['bukti_cctv']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['bukti_cctv'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Verifikasi Bukti Cctv
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'pegawai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->pegawai ? $model['bukti_cctv']->pegawai0->nama : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <?= $form->field($model['bukti_cctv'], 'jenis', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'jenis', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->jenis ? $model['bukti_cctv']->jenis : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'jenis')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'diajukan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'diajukan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->diajukan ? $model['bukti_cctv']->diajukan0->nama : '(kosong)'?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'diajukan')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'catatan_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->catatan_pengajuan ? $model['bukti_cctv']->catatan_pengajuan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'catatan_pengajuan')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->tanggal ? $model['bukti_cctv']->tanggal : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'tanggal')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'waktu', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'waktu', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->waktu ? $model['bukti_cctv']->waktu : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'waktu')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_utama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_utama ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_utama_download . '">' . $model['bukti_cctv']->bukti_utama . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_utama')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_tambahan_1 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_1_download . '">' . $model['bukti_cctv']->bukti_tambahan_1 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_1')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_tambahan_2 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_2_download . '">' . $model['bukti_cctv']->bukti_tambahan_2 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_2')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_3', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_tambahan_3 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_3_download . '">' . $model['bukti_cctv']->bukti_tambahan_3 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_3')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_4', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_tambahan_4 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_4_download . '">' . $model['bukti_cctv']->bukti_tambahan_4 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_4')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_5', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_cctv']->bukti_tambahan_5 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_5_download . '">' . $model['bukti_cctv']->bukti_tambahan_5 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_cctv'], 'bukti_tambahan_5')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'catatan_verifikasi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'catatan_verifikasi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['bukti_cctv'], 'catatan_verifikasi', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['bukti_cctv'], 'catatan_verifikasi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_cctv'], 'catatan_verifikasi')->end(); ?>

                <?= $form->field($model['bukti_cctv'], 'status_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_cctv'], 'status_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['bukti_cctv'], 'status_pengajuan', ['Diterima' => 'Diterima', 'Direvisi' => 'Direvisi', 'Ditolak' => 'Ditolak'], ['prompt' => '- pilih verifikasi -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['bukti_cctv'], 'status_pengajuan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_cctv'], 'status_pengajuan')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
