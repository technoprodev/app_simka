<?php
namespace app_simka\models;

use Yii;

class FilterKehadiran extends \yii\base\Model
{
    public $bulan;
    public $tahun;

    public function __construct ()
    {
        $this->bulan = date('m');
        $this->tahun = date('Y');
    }
}
