if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    // Vue.directive('init', {
    //     inserted: function(el) {
    //         pluginInit(el);
    //     },
    //     componentUpdated: function(el) {
    //         pluginInit(el);
    //     },
    // }); 

    var vm = new Vue({
        el: '#app',
        data: {
            buktiKehadiran: {
                buktiKehadiranPegawais: [],
                buktiKehadiranPegawaisTemp: [],
            },
        },
        methods: {
            addBuktiKehadiranPegawai: function() {
                this.buktiKehadiran.buktiKehadiranPegawais.push({
                    id: null,
                    bukti_kehadiran: null,
                    pegawai: '',
                });
            },
            updateBuktiKehadiranPegawai: function(i) {
                var bkp = $.extend({}, this.buktiKehadiran.buktiKehadiranPegawais[i]);
            },
            removeBuktiKehadiranPegawai: function(i, isNew) {
                if (this.buktiKehadiran.buktiKehadiranPegawais[i].id == null)
                    this.buktiKehadiran.buktiKehadiranPegawais.splice(i, 1);
                else
                    this.buktiKehadiran.buktiKehadiranPegawais[i].id *= -1;
            },
        },
        // beforeUpdated: function() {
        //     this.buktiKehadiran.buktiKehadiranPegawaisTemp = this.buktiKehadiran.buktiKehadiranPegawais;
        // },
        updated: function() {
            /*var temp = this.buktiKehadiran.buktiKehadiranPegawais;
            console.log(temp[0].pegawai);
            if (temp[1]) {
                console.log(this.buktiKehadiran.buktiKehadiranPegawais[1].pegawai);
                console.log(temp[1].pegawai);
            }*/
            pluginInit("#app");
            // this.buktiKehadiran.buktiKehadiranPegawais = temp;
        },
    });
}