<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "keterangan_kehadiran".
 *
 * @property integer $id
 * @property string $kode
 * @property string $keterangan
 * @property string $jenis
 * @property string $default_sistem
 * @property string $tampilkan_waktu_absen
 * @property string $dihitung_hadir
 * @property string $potong_tunkin
 * @property double $potongan_tunkin_persen
 * @property string $potong_uang_makan
 * @property string $potong_disiplin
 * @property string $diajukan_administrasi
 * @property string $diajukan_pegawai
 * @property string $diajukan_pegawai_online
 * @property string $diajukan_kepegawaian
 * @property string $batas_awal_unggah
 * @property string $batas_akhir_unggah
 * @property string $color
 * @property string $mengurangi_sisa_cuti
 *
 * @property BuktiKehadiran[] $buktiKehadirans
 * @property Cuti[] $cutis
 * @property Kehadiran[] $kehadirans
 */
class KeteranganKehadiran extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'keterangan_kehadiran';
    }

    public function rules()
    {
        return [
            //id

            //kode
            [['kode'], 'required'],
            [['kode'], 'string', 'max' => 256],

            //keterangan
            [['keterangan'], 'required'],
            [['keterangan'], 'string', 'max' => 256],

            //jenis
            [['jenis'], 'string', 'max' => 256],

            //default_sistem
            [['default_sistem'], 'string'],

            //tampilkan_waktu_absen
            [['tampilkan_waktu_absen'], 'string'],

            //dihitung_hadir
            [['dihitung_hadir'], 'required'],
            [['dihitung_hadir'], 'string'],

            //potong_tunkin
            [['potong_tunkin'], 'string'],

            //potongan_tunkin_persen
            [['potongan_tunkin_persen'], 'number'],

            //potong_uang_makan
            [['potong_uang_makan'], 'required'],
            [['potong_uang_makan'], 'string'],

            //potong_disiplin
            [['potong_disiplin'], 'required'],
            [['potong_disiplin'], 'string'],

            //diajukan_administrasi
            [['diajukan_administrasi'], 'string'],

            //diajukan_pegawai
            [['diajukan_pegawai'], 'string'],

            //diajukan_pegawai_online
            [['diajukan_pegawai_online'], 'required'],
            [['diajukan_pegawai_online'], 'string'],

            //diajukan_kepegawaian
            [['diajukan_kepegawaian'], 'string'],

            //batas_awal_unggah
            [['batas_awal_unggah'], 'safe'],

            //batas_akhir_unggah
            [['batas_akhir_unggah'], 'safe'],

            //color
            [['color'], 'string', 'max' => 256],

            //mengurangi_sisa_cuti
            [['mengurangi_sisa_cuti'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'keterangan' => 'Keterangan',
            'jenis' => 'Jenis',
            'default_sistem' => 'Default Sistem',
            'tampilkan_waktu_absen' => 'Tampilkan Waktu Absen',
            'dihitung_hadir' => 'Dihitung Hadir',
            'potong_tunkin' => 'Potong Tunkin',
            'potongan_tunkin_persen' => 'Potongan Tunkin Persen',
            'potong_uang_makan' => 'Potong Uang Makan',
            'potong_disiplin' => 'Potong Disiplin',
            'diajukan_administrasi' => 'Diajukan Administrasi',
            'diajukan_pegawai' => 'Diajukan Pegawai',
            'diajukan_pegawai_online' => 'Diajukan Pegawai Online',
            'diajukan_kepegawaian' => 'Diajukan Kepegawaian',
            'batas_awal_unggah' => 'Batas Awal Unggah',
            'batas_akhir_unggah' => 'Batas Akhir Unggah',
            'color' => 'Color',
            'mengurangi_sisa_cuti' => 'Mengurangi Sisa Cuti',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadirans()
    {
        return $this->hasMany(BuktiKehadiran::className(), ['keterangan_kehadiran' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutis()
    {
        return $this->hasMany(Cuti::className(), ['keterangan_kehadiran' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKehadirans()
    {
        return $this->hasMany(Kehadiran::className(), ['keterangan' => 'id']);
    }
}
