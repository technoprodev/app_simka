$(document).ready(function() {
    if (true) {
        window.datatablesDefaultOptions = {
            autoWidth: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            dom: '<"clearfix margin-bottom-30"lBf><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
            lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
            orderCellsTop: true,
            // stateSave: true,
            // ordering: false,
            // order: [[1, 'asc'], [2, 'asc']],
            order: [],
            buttons: {
                dom: {
                    container: {
                        className: 'dt-buttons pull-left hidden-sm-less'
                    },
                    button: {
                        className: 'margin-left-5 margin-bottom-5 button',
                        active: 'bg-azure border-azure'
                    }
                },
                buttons: [
                    {
                        extend: 'colvis',
                        title: 'Data show/hide',
                        text: 'Show/hide <i class="fa fa-angle-down"></i>'
                    },
                    {
                        extend: 'copy',
                        title: 'Data export',
                        text: 'Copy'
                    },
                    {
                        extend: 'csv',
                        title: 'Data export',
                        text: 'Csv'
                    },
                    {
                        extend: 'excel',
                        title: 'Data export',
                        text: 'Excel',
                    },
                    {
                        extend: 'pdf',
                        title: 'Data export',
                        text: 'Pdf'
                    },
                    {
                        extend: 'print',
                        title: 'Data export',
                        text: 'Print'
                    }
                ]
            },
            language: {
                lengthMenu : '_MENU_',
                search: '',
                searchPlaceholder: 'search all here...',
                buttons: {
                    copyTitle: 'Title',
                    copyKeys: 'copy keys',
                    copySuccess: {
                        _: '%d rows copied',
                        1: '1 row copied'
                    }
                }
            },
            colReorder: true
        }
    }
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-pegawai'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('kepegawaian/pegawai/' + data, {tab: 'jabatan'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail Pegawai"></i></a>' +
                                '    <a href="' + fn.urlTo('kepegawaian/pegawai-update/' + data, {tab: 'personal'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Update Pegawai"></i></a>' +
                                '    <a href="' + fn.urlTo('kepegawaian/pegawai-role/' + data, {tab: 'personal'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-users cdexsw text-spring xswzaq" data-toggle="tooltip" data-placement="top" title="Role Pegawai"></i></a>' +
                                '    <a href="' + fn.urlTo('kepegawaian/pegawai-reset-password/' + data, {tab: 'jabatan'}) + '" class="inline-block" modal-md="" modal-title="Reset Password"><i style="width:20px;" class="fa fa-lock cdexsw text-orange xswzaq" data-toggle="tooltip" data-placement="bottom" title="Reset Password"></i></a>' +
                                '    <a href="' + fn.urlTo('kepegawaian/pegawai-delete/' + data, {tab: 'jabatan'}) + '" class="inline-block" data-confirm="Apakah Anda yakin menghapus pegawai ini ?" data-method="post"><i style="width:20px;" class="fa fa-trash-o cdexsw text-red xswzaq" data-toggle="tooltip" data-placement="bottom" title="Hapus Pegawai"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'nama',
                        },
                        render: function ( data, type, row ) {
                            firstLetter = row.nama.match(/\b(\w)/g)[0].toUpperCase();
                            color = '';
                            if (firstLetter >= 'A' && firstLetter <= 'C') {
                                color = 'azure';
                            } else if (firstLetter >= 'D' && firstLetter <= 'F') {
                                color = 'spring';
                            } else if (firstLetter >= 'G' && firstLetter <= 'H') {
                                color = 'red';
                            } else if (firstLetter >= 'I' && firstLetter <= 'J') {
                                color = 'yellow';
                            } else if (firstLetter >= 'K' && firstLetter <= 'L') {
                                color = 'orange';
                            } else if (firstLetter >= 'M' && firstLetter <= 'N') {
                                color = 'chartreuse';
                            } else if (firstLetter >= 'O' && firstLetter <= 'P') {
                                color = 'green';
                            } else if (firstLetter >= 'Q' && firstLetter <= 'R') {
                                color = 'cyan';
                            } else if (firstLetter >= 'S' && firstLetter <= 'T') {
                                color = 'blue';
                            } else if (firstLetter >= 'U' && firstLetter <= 'V') {
                                color = 'violet';
                            } else if (firstLetter >= 'W' && firstLetter <= 'X') {
                                color = 'magenta';
                            } else if (firstLetter >= 'Y' && firstLetter <= 'Z') {
                                color = 'rose';
                            }
                            return '' +
                                '        <span class="circle-icon margin-right-5 fs-11 bg-' + color + ' border-transparent" style="width: 30px; height: 30px; line-height: 28px;">' + row.nama.match(/\b(\w)/g).slice(0, 2).join('').toUpperCase() + '</span>' +
                                '        <span class="fs-14 text-dark">' + row.nama + '</span>' +
                                
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nip',
                            display: 'nip',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-orange">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.pin',
                            display: 'pin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-orange">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.pangkat',
                            display: 'kepangkatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jj.jenjang_jabatan',
                            display: 'jenjang_jabatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_jabatan',
                            display: 'nama_jabatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'uk.unit_kerja',
                            display: 'unit_kerja',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'e.eselon',
                            display: 'eselon',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'g.grade',
                            display: 'grade',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kp.kedudukan_pegawai',
                            display: 'kedudukan_pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.mendapat_tunkin',
                            display: 'mendapat_tunkin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_panggilan',
                            display: 'nama_panggilan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jenis_kelamin',
                            display: 'jenis_kelamin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.ttl',
                            display: 'ttl',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.agama',
                            display: 'agama',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_marital',
                            display: 'status_marital',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.golongan_darah',
                            display: 'golongan_darah',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'sisa_pensiun',
                            display: 'sisa_pensiun',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.updated_at',
                            display: 'updated_at',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p1.nama',
                            display: 'updated_by',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});

/*$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [ {
            extend: 'excelHtml5',
            customize: function ( xlsx ){
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row c[r*="10"]', sheet).attr( 's', '25' );
            }
        }]
    });
});*/