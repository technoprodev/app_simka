<?php
namespace app_simka\models;

use Yii;

class FilterProsesManualKehadiran extends \yii\base\Model
{
    public $dari_tanggal;
    public $sampai_tanggal;

    public function __construct ()
    {
    }

    public function rules()
    {
        return [
            //dari_tanggal
            [['dari_tanggal'], 'required'],

            //sampai_tanggal
            [['sampai_tanggal'], 'required'],
        ];
    }
}
