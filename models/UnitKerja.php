<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "unit_kerja".
 *
 * @property integer $id
 * @property integer $jenis_unit_kerja
 * @property string $unit_kerja
 * @property integer $parent
 *
 * @property JadwalKehadiran[] $jadwalKehadirans
 * @property Pegawai[] $pegawais
 * @property JenisUnitKerja $jenisUnitKerja
 * @property UnitKerja $parent0
 * @property UnitKerja[] $unitKerjas
 */
class UnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'unit_kerja';
    }

    public function rules()
    {
        return [
            //id

            //jenis_unit_kerja
            [['jenis_unit_kerja'], 'integer'],
            [['jenis_unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => JenisUnitKerja::className(), 'targetAttribute' => ['jenis_unit_kerja' => 'id']],

            //unit_kerja
            [['unit_kerja'], 'required'],
            [['unit_kerja'], 'string', 'max' => 256],

            //parent
            [['parent'], 'integer'],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_unit_kerja' => 'Jenis Unit Kerja',
            'unit_kerja' => 'Unit Kerja',
            'parent' => 'Parent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwalKehadirans()
    {
        return $this->hasMany(JadwalKehadiran::className(), ['unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['unit_kerja' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisUnitKerja()
    {
        return $this->hasOne(JenisUnitKerja::className(), ['id' => 'jenis_unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjas()
    {
        return $this->hasMany(UnitKerja::className(), ['parent' => 'id']);
    }
}
