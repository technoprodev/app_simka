DROP TABLE IF EXISTS `keterangan_kehadiran`;
CREATE TABLE `keterangan_kehadiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(256) NOT NULL,
  `keterangan` varchar(256) NOT NULL,
  `default_sistem` enum('Ya','Tidak') NOT NULL DEFAULT 'Tidak',
  `dihitung_hadir` enum('Hadir','Tidak Hadir','Tidak Dihitung') NOT NULL,
  `potong_tunkin` enum('Ya','Tidak') NOT NULL,
  `potong_uang_makan` enum('Ya','Tidak') NOT NULL,
  `potong_disiplin` enum('Ya','Tidak') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `keterangan_kehadiran` (`id`, `kode`, `keterangan`, `default_sistem`, `dihitung_hadir`, `potong_tunkin`, `potong_uang_makan`, `potong_disiplin`) VALUES
(1,	'BA',	'Belum Absen',	'Ya',	'Tidak Dihitung',	'Tidak',	'Tidak',	'Tidak'),
(2,	'H',	'Hadir',	'Ya',	'Hadir',	'Tidak',	'Tidak',	'Tidak'),
(3,	'HK',	'Hadir dan Kurang Jam',	'Ya',	'Hadir',	'Ya',	'Tidak',	'Ya'),
(4,	'TH',	'Tidak Hadir',	'Ya',	'Tidak Hadir',	'Ya',	'Ya',	'Ya'),
(5,	'TAM',	'Tanpa Absen Masuk',	'Ya',	'Hadir',	'Ya',	'Tidak',	'Ya'),
(6,	'TAP',	'Tanpa Absen Pulang',	'Ya',	'Hadir',	'Ya',	'Tidak',	'Ya'),
(7,	'L',	'Libur',	'Ya',	'Tidak Dihitung',	'Tidak',	'Tidak',	'Tidak'),
(8,	'DL',	'Dinas Luar',	'Tidak',	'Tidak Hadir',	'Tidak',	'Ya',	'Tidak'),
(9,	'I',	'Izin',	'Tidak',	'Tidak Hadir',	'Tidak',	'Ya',	'Tidak'),
(10,	'C',	'Cuti',	'Tidak',	'Tidak Hadir',	'Tidak',	'Ya',	'Tidak'),
(11,	'D',	'Diklat',	'Tidak',	'Tidak Hadir',	'Tidak',	'Ya',	'Tidak');

ALTER TABLE `kehadiran`
CHANGE `jadwal_batas_awal_masuk` `jadwal_batas_awal_masuk` datetime NULL AFTER `jadwal_masuk`,
CHANGE `jadwal_batas_akhir_masuk` `jadwal_batas_akhir_masuk` datetime NULL AFTER `jadwal_batas_awal_masuk`,
CHANGE `jadwal_pulang` `jadwal_pulang` datetime NULL AFTER `jadwal_batas_akhir_masuk`,
CHANGE `jadwal_batas_awal_pulang` `jadwal_batas_awal_pulang` datetime NULL AFTER `jadwal_pulang`,
CHANGE `jadwal_batas_akhir_pulang` `jadwal_batas_akhir_pulang` datetime NULL AFTER `jadwal_batas_awal_pulang`,
CHANGE `waktu_masuk` `waktu_masuk` datetime NULL AFTER `jadwal_batas_akhir_pulang`,
CHANGE `waktu_pulang` `waktu_pulang` datetime NULL AFTER `waktu_masuk`,
CHANGE `keterangan` `keterangan` enum('Belum Absen','Hari Libur','Hadir','Kurang Jam','Tanpa Keterangan') COLLATE 'latin1_swedish_ci' NOT NULL AFTER `waktu_pulang`;

ALTER TABLE `kehadiran`
ADD `waktu_masuk_default` datetime NULL AFTER `jadwal_batas_akhir_pulang`,
ADD `waktu_pulang_default` datetime NULL AFTER `waktu_masuk_default`,
ADD `keterangan_default` int(11) NULL AFTER `waktu_pulang_default`,
ADD `keterangan_revisi` int(11) NULL AFTER `waktu_pulang`,
ADD FOREIGN KEY (`keterangan_default`) REFERENCES `keterangan_kehadiran` (`id`),
ADD FOREIGN KEY (`keterangan_revisi`) REFERENCES `keterangan_kehadiran` (`id`);

ALTER TABLE `kehadiran`
CHANGE `keterangan_revisi` `keterangan` int(11) NULL AFTER `waktu_pulang`,
DROP `keterangan`;

ALTER TABLE `jadwal_kehadiran`
ADD `fleksi_time` int NULL AFTER `berlaku_sampai`;

update jadwal_kehadiran set fleksi_time = 90;

ALTER TABLE `jadwal_kehadiran`
CHANGE `fleksi_time` `fleksi_time` int(11) NOT NULL AFTER `berlaku_sampai`;

ALTER TABLE `kehadiran`
ADD `fleksi_time` int NOT NULL DEFAULT '90' AFTER `tanggal`;

ALTER TABLE `kehadiran`
CHANGE `fleksi_time` `fleksi_time` int(11) NOT NULL AFTER `tanggal`;

ALTER TABLE `kehadiran`
CHANGE `fleksi_time` `fleksi_time` int(11) NOT NULL DEFAULT '0' AFTER `tanggal`;

ALTER TABLE `jadwal_kehadiran`
ADD `catatan` varchar(256) NOT NULL AFTER `berlaku_sampai`;