<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

//
$errorMessage = '';
$errorVue = false;
if ($model['pegawai']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['pegawai'], ['class' => '']);
}

$roles = Yii::$app->authManager->getRoles();
foreach ($roles as $key => $value) $roles[$key] = $value->description;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <div class="fs-16 text-azure fs-italic">
                    Update Role Pegawai
                </div>

                <hr class="margin-y-10 border-top border-light-orange">

                <div class="form-wrapper box box-break-sm">
                    <div class="box-1 padding-x-0 padding-y-5">
                        <label class="form-label">Roles</label>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <div class="form-checkbox">
                                <?= Html::checkboxList(
                                    'assignments[]',
                                    $model['assignments'],
                                    $roles,
                                    ['tag' => false]
                                ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                    <div class="margin-top-15"></div>
                    <div class="clearfix">
                        <?= Html::a('Kembali ke Daftar Pegawai', ['pegawai'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                        <div class="margin-5 pull-right m-pull-none"></div>
                        <?= Html::a('Kembali ke Detail Pegawai', ['pegawai', 'id' => $model['pegawai']->id, 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
