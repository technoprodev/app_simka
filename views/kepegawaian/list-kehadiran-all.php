<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/list-kehadiran-all.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$this->registerJs(
    'var tahun = ' . json_encode($model['filter_kehadiran']->tahun) . ';' .
    'var bulan = ' . json_encode($model['filter_kehadiran']->bulan) . ';' .
    '',
    3
);

$bulans = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', ];
$tahuns = ['2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024', ];
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-16 text-azure fs-italic">
                Daftar Hadir
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app'], 'method' => 'get', 'action' => Url::to(['kepegawaian/kehadiran-all'])]); ?>
                <div class="box box-break-md box-gutter">
                    <div class="box-2">
                        <?= $form->field($model['filter_kehadiran'], 'bulan', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                            <?= Html::activeDropDownList($model['filter_kehadiran'], 'bulan', $bulans, ['class' => 'form-dropdown', 'name' => 'bulan']); ?>
                        <?= $form->field($model['filter_kehadiran'], 'bulan')->end(); ?>
                    </div>
                    <div class="box-2">
                        <?= $form->field($model['filter_kehadiran'], 'tahun', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                            <?= Html::activeDropDownList($model['filter_kehadiran'], 'tahun', $tahuns, ['class' => 'form-dropdown', 'name' => 'tahun']); ?>
                        <?= $form->field($model['filter_kehadiran'], 'tahun')->end(); ?>
                    </div>
                    <div class="box-2">
                        <div class="form-wrapper margin-0">
                            <button class="button button-block border-azure bg-azure m-margin-top-5" type="submit">Tampilkan</button>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="fs-13 fw-bold">
                Rincian Bulan <?= $bulans[$model['filter_kehadiran']->bulan] ?>
            </div>
            <div>*Geser kekanan untuk melihat tunkin dan disiplin. Tombol scroll untuk menggeser terletak paling bawah</div>
            <div class="margin-top-15"></div>

            <table class="datatables-kehadiran-all table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th>Nama<br>Pegawai</th>
                        <th>Tanggal<br>Kehadiran</th>
                        <th>Jadwal<br>Masuk</th>
                        <th>Jadwal<br>Pulang</th>
                        <th>Waktu<br>Masuk</th>
                        <th>Waktu<br>Pulang</th>
                        <th>Telat<br>Masuk</th>
                        <th>Pulang<br>Cepat</th>
                        <th>Jumlah<br>Jam</th>
                        <th>Kurang<br>Jam</th>
                        <th>Keterangan<br>Kehadiran</th>
                        <th>Tunkin<br>(%)</th>
                        <th>Disiplin<br>(Waktu)</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>