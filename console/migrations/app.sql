-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `configuration_file` (`target`, `alias_upload_root`, `alias_download_base_url`) VALUES
('dev-file',  '@upload-dev-file', '@download-dev-file');

INSERT INTO `sequence` (`target`, `last_sequence`, `first`, `second`, `third`) VALUES
('dev-id_combination',  '0A', '0',  'A',  NULL);

DROP TABLE IF EXISTS `user_extend`;
CREATE TABLE `user_extend` (
  `id` int(11) NOT NULL,
  `sex` enum('Male','Female') NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `user_extend_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_extend` (`id`, `sex`) VALUES
(1, 'Male');

--

DROP TABLE IF EXISTS `dev`;
CREATE TABLE `dev` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_combination` varchar(32) DEFAULT NULL,
  `id_user_defined` varchar(32) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` varchar(32) DEFAULT NULL,
  `link` int(11) DEFAULT NULL,
  `enum` enum('male','female') DEFAULT NULL,
  `set` set('technology','innovation','professional') DEFAULT NULL,
  `checklist` tinyint(1) DEFAULT NULL,
  `file` varchar(128) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_combination` (`id_combination`),
  UNIQUE KEY `id_user_defined` (`id_user_defined`),
  KEY `link` (`link`),
  CONSTRAINT `dev_ibfk_1` FOREIGN KEY (`link`) REFERENCES `dev_category_option` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `dev` (`id`, `id_combination`, `id_user_defined`, `created_at`, `updated_at`, `text`, `link`, `enum`, `set`, `checklist`, `file`, `time`, `date`, `datetime`, `year`) VALUES
(1,	'88',	NULL,	'2017-11-30 16:55:00',	NULL,	NULL,	NULL,	NULL,	NULL,	0,	'Android Development Component.PNG', '16:55:00', '2017-11-30', '2017-11-30 16:55:00',  '2017');

DROP TABLE IF EXISTS `dev_category`;
CREATE TABLE `dev_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dev` int(11) NOT NULL,
  `id_dev_category_option` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dev_category_option` (`id_dev_category_option`),
  KEY `id_dev` (`id_dev`),
  CONSTRAINT `dev_category_ibfk_1` FOREIGN KEY (`id_dev_category_option`) REFERENCES `dev_category_option` (`id`),
  CONSTRAINT `dev_category_ibfk_2` FOREIGN KEY (`id_dev`) REFERENCES `dev` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `dev_category` (`id`, `id_dev`, `id_dev_category_option`) VALUES
(1, 1,  1),
(2, 1,  2),
(3,	1,	3);

DROP TABLE IF EXISTS `dev_category_option`;
CREATE TABLE `dev_category_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `parent` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO `dev_category_option` (`id`, `name`, `parent`) VALUES
(1,	'Alpha',	'first'),
(2,	'Bravo',	'first'),
(3,	'Charlie',	'first'),
(4,	'Delta',	'first'),
(5,	'Echo',	'first'),
(6,	'Fanta',	'first'),
(7,	'Golf',	'first'),
(8,	'Hotel',	'first'),
(9,	'India',	'first'),
(10,	'Juliet',	'first'),
(11,	'Kilo',	'first'),
(12,	'London',	'first'),
(13,	'Mama',	'first'),
(14,	'Nano',	'second'),
(15,	'Oscar',	'second'),
(16,	'Papa',	'second'),
(17,	'Queen',	'second'),
(18,	'Romeo',	'second'),
(19,	'Sierra',	'second'),
(20,	'Tango',	'second'),
(21,	'Ultra',	'second'),
(22,	'Victor',	'second'),
(23,	'Whiskey',	'second'),
(24,	'Xray',	'second'),
(25,	'Yankee',	'second'),
(26,	'Zebra',	'second');

DROP TABLE IF EXISTS `dev_child`;
CREATE TABLE `dev_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dev` int(11) NOT NULL,
  `child` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dev` (`id_dev`),
  CONSTRAINT `dev_child_ibfk_1` FOREIGN KEY (`id_dev`) REFERENCES `dev` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `dev_child` (`id`, `id_dev`, `child`) VALUES
(1, 1,  1),
(2, 1,  2),
(3, 1,  3),
(4, 1,  4),
(5,	1,	5);

DROP TABLE IF EXISTS `dev_extend`;
CREATE TABLE `dev_extend` (
  `id_dev` int(11) NOT NULL,
  `extend` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_dev`),
  CONSTRAINT `dev_extend_ibfk_1` FOREIGN KEY (`id_dev`) REFERENCES `dev` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `dev_extend` (`id_dev`, `extend`) VALUES
(1, 'a');