tabControl();

/*
We also apply the switch when a viewport change is detected on the fly
(e.g. when you resize the browser window or flip your device from 
portrait mode to landscape). We set a timer with a small delay to run 
it only once when the resizing ends. It's not perfect, but it's better
than have it running constantly during the action of resizing.
*/
var resizeTimer;
$(window).on('resize', function(e) {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function() {
    tabControl();
  }, 250);
});

/*
The function below is responsible for switching the tabMenu when clicked.
It switches both the tabMenu and the accordion buttons even if 
only the one or the other can be visible on a screen. We prefer
that in order to have a consistent selection in case the viewport
changes (e.g. when you esize the browser window or flip your 
device from portrait mode to landscape).
*/
function tabControl() {
  var tabMenu = $('.tab-menu-selector');
  tabMenu.find('a').on('click', function(event) {
    event.preventDefault();
    var target = $(this).attr('href'),
        tabMenu = $(this).parents('.tab-menu'),
        buttons = tabMenu.find('a'),
        tabContent = tabMenu.parents('.tab-wrapper').find('.tab-content');
    buttons.removeClass('active');
    tabContent.removeClass('active');
    $(this).addClass('active');
    $(target).addClass('active');
  });
  
  var tabActive = tabMenu.find('a.active');
  var target = tabActive.attr('href'),
      tabMenu = tabActive.parents('.tab-menu'),
      buttons = tabMenu.find('a'),
      tabContent = tabMenu.parents('.tab-wrapper').find('.tab-content');
  buttons.removeClass('active');
  tabContent.removeClass('active');
  tabActive.addClass('active');
  $(target).addClass('active');
}