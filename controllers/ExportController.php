<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\PegawaiAnak;
use app_simka\models\PegawaiDiklat;
use app_simka\models\PegawaiHukumanDisiplin;
use app_simka\models\PegawaiKenaikanGajiBerkala;
use app_simka\models\PegawaiKepangkatan;
use app_simka\models\PegawaiKunjunganLuarNegeri;
use app_simka\models\PegawaiKursusPelatihan;
use app_simka\models\PegawaiMasaPersiapanPensiun;
use app_simka\models\PegawaiMutasi;
use app_simka\models\PegawaiOrganisasi;
use app_simka\models\PegawaiPelaksana;
use app_simka\models\PegawaiPemberhentian;
use app_simka\models\PegawaiPendidikan;
use app_simka\models\PegawaiPenetapanAngkaKredit;
use app_simka\models\PegawaiPenghargaan;
use app_simka\models\PegawaiSaudaraKandung;
use app_simka\models\Kehadiran;
use app_simka\models\Agama;
use app_simka\models\BentukWajah;
use app_simka\models\Eselon;
use app_simka\models\Grade;
use app_simka\models\JenisHukumanDisiplin;
use app_simka\models\JenisJabatan;
use app_simka\models\JenisPemberhentian;
use app_simka\models\JenisSk;
use app_simka\models\JenisUnitKerja;
use app_simka\models\JenjangJabatan;
use app_simka\models\JenjangPendidikan;
use app_simka\models\Kecamatan;
use app_simka\models\KedudukanPegawai;
use app_simka\models\Kelurahan;
use app_simka\models\Kepangkatan;
use app_simka\models\KotaKabupaten;
use app_simka\models\ModelRambut;
use app_simka\models\Negara;
use app_simka\models\Pekerjaan;
use app_simka\models\Provinsi;
use app_simka\models\StatusKepegawaian;
use app_simka\models\StatusMarital;
use app_simka\models\TingkatHukumanDisiplin;
use app_simka\models\UnitKerja;
use app_simka\models\WarnaKulit;
use app_simka\models\FilterKehadiran;
use app_simka\models\BuktiKehadiran;
use app_simka\models\BuktiKehadiranPegawai;
use app_simka\models\FilterProsesManualKehadiran;

class ExportController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['export']]
            ]),
        ];
    }

    public function actionDatatablesTunkin($bulan, $tahun)
    {
        $subQuery = new \yii\db\Query();
        $subQuery
            ->select([
                'p.id',
                'sk.status_kepegawaian as status_kepegawaian',
                'CONCAT("", p.tunkin_nip) as nip',
                'p.tunkin_nama',
                'CONCAT(kp.golongan, "/", kp.ruang) AS kepangkatan',
                'CONCAT("") AS keterangan',
                'g.grade',
                '(g.nominal_tunkin * sk.pajak_tunkin / 100) as nominal_tunkin',
                'g.pajak_tunkin',
                'LEAST(
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END) +
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END)
                , 100) AS potongan_tunkin',
                'CONCAT("", p.tunkin_nomor_rekening) as tunkin_nomor_rekening',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'kepangkatan kp', 'kp.id = p.kepangkatan')
            ->join('LEFT JOIN', 'grade g', 'g.id = p.grade')
            ->join('LEFT JOIN', 'status_kepegawaian sk', 'sk.id = p.status_kepegawaian')
            ->join('LEFT JOIN', 'kehadiran k', 'k.pegawai = p.id')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->where([
                // 'p.id' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->andWhere(['or',
                ['p.status_kepegawaian' => 1],
                ['p.status_kepegawaian' => 2],
                ['p.status_kepegawaian' => 3],
                ['p.status_kepegawaian' => 5],
                ['p.status_kepegawaian' => 6],
            ])
            ->groupBy(['p.id'])
        ;

        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'status_kepegawaian',
                'nip',
                'tunkin_nama',
                'kepangkatan',
                'keterangan',
                'grade',
                'round(nominal_tunkin) AS nominal_tunkin',
                'potongan_tunkin',
                'round(@nilai_pemotongan := (potongan_tunkin / 100 * nominal_tunkin)) AS nilai_pemotongan',
                'round(@jumlah_pemotongan := (nominal_tunkin - @nilai_pemotongan)) AS jumlah_pemotongan',
                'round(@pajak_pegawai := (@jumlah_pemotongan * pajak_tunkin / 100)) AS pajak_pegawai',
                'round(@bruto := (@pajak_pegawai + @jumlah_pemotongan)) AS bruto',
                'round(@pajak_pegawai) AS potongan_pajak',
                'round(@bruto - @pajak_pegawai) AS netto',
                'tunkin_nomor_rekening',
            ])
            ->from(['sq' => $subQuery])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionTunkin($id = null, $tab = 'jabatan', $bulan = null, $tahun = null)
    {
        $model['filter_kehadiran'] = new FilterKehadiran();
        if ($bulan) {
            $model['filter_kehadiran']->bulan = $bulan;
        }
        if ($tahun) {
            $model['filter_kehadiran']->tahun = $tahun;
        }

        if (!$id) {
            return $this->render('list-tunkin', [
                'model' => $model,
                'title' => 'Daftar Tunjangan Kinerja Pegawai',
            ]);
        }
    }

    public function actionTunkinUpdate($id, $tab = 'tunkin')
    {
        $error = true;

        if (isset($id)) {
            if (($model['pegawai'] = Pegawai::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['user'] = new User(['scenario' => 'password']);
            $model['pegawai'] = new Pegawai();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['pegawai']->load($post);
            if (false) {
                if (isset($post['PegawaiAnak'])) {
                    foreach ($post['PegawaiAnak'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiAnak = PegawaiAnak::find()->where(['id' => $value['id']])->one();
                            $pegawaiAnak->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiAnak = PegawaiAnak::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiAnak->isDeleted = true;
                        } else {
                            $pegawaiAnak = new PegawaiAnak();
                            $pegawaiAnak->setAttributes($value);
                        }
                        $pegawaiAnak->index = $key;
                        $model['pegawai_anak'][] = $pegawaiAnak;
                    }
                }
                if (isset($post['PegawaiDiklat'])) {
                    foreach ($post['PegawaiDiklat'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiDiklat = PegawaiDiklat::find()->where(['id' => $value['id']])->one();
                            $pegawaiDiklat->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiDiklat = PegawaiDiklat::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiDiklat->isDeleted = true;
                        } else {
                            $pegawaiDiklat = new PegawaiDiklat();
                            $pegawaiDiklat->setAttributes($value);
                        }
                        $pegawaiDiklat->index = $key;
                        $model['pegawai_diklat'][] = $pegawaiDiklat;
                    }
                }
                if (isset($post['PegawaiHukumanDisiplin'])) {
                    foreach ($post['PegawaiHukumanDisiplin'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiHukumanDisiplin = PegawaiHukumanDisiplin::find()->where(['id' => $value['id']])->one();
                            $pegawaiHukumanDisiplin->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiHukumanDisiplin = PegawaiHukumanDisiplin::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiHukumanDisiplin->isDeleted = true;
                        } else {
                            $pegawaiHukumanDisiplin = new PegawaiHukumanDisiplin();
                            $pegawaiHukumanDisiplin->setAttributes($value);
                        }
                        $pegawaiHukumanDisiplin->index = $key;
                        $model['pegawai_hukuman_disiplin'][] = $pegawaiHukumanDisiplin;
                    }
                }
                if (isset($post['PegawaiKenaikanGajiBerkala'])) {
                    foreach ($post['PegawaiKenaikanGajiBerkala'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKenaikanGajiBerkala = PegawaiKenaikanGajiBerkala::find()->where(['id' => $value['id']])->one();
                            $pegawaiKenaikanGajiBerkala->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKenaikanGajiBerkala = PegawaiKenaikanGajiBerkala::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKenaikanGajiBerkala->isDeleted = true;
                        } else {
                            $pegawaiKenaikanGajiBerkala = new PegawaiKenaikanGajiBerkala();
                            $pegawaiKenaikanGajiBerkala->setAttributes($value);
                        }
                        $pegawaiKenaikanGajiBerkala->index = $key;
                        $model['pegawai_kenaikan_gaji_berkala'][] = $pegawaiKenaikanGajiBerkala;
                    }
                }
                if (isset($post['PegawaiKepangkatan'])) {
                    foreach ($post['PegawaiKepangkatan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKepangkatan = PegawaiKepangkatan::find()->where(['id' => $value['id']])->one();
                            $pegawaiKepangkatan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKepangkatan = PegawaiKepangkatan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKepangkatan->isDeleted = true;
                        } else {
                            $pegawaiKepangkatan = new PegawaiKepangkatan();
                            $pegawaiKepangkatan->setAttributes($value);
                        }
                        $pegawaiKepangkatan->index = $key;
                        $model['pegawai_kepangkatan'][] = $pegawaiKepangkatan;
                    }
                }
                if (isset($post['PegawaiKunjunganLuarNegeri'])) {
                    foreach ($post['PegawaiKunjunganLuarNegeri'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKunjunganLuarNegeri = PegawaiKunjunganLuarNegeri::find()->where(['id' => $value['id']])->one();
                            $pegawaiKunjunganLuarNegeri->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKunjunganLuarNegeri = PegawaiKunjunganLuarNegeri::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKunjunganLuarNegeri->isDeleted = true;
                        } else {
                            $pegawaiKunjunganLuarNegeri = new PegawaiKunjunganLuarNegeri();
                            $pegawaiKunjunganLuarNegeri->setAttributes($value);
                        }
                        $pegawaiKunjunganLuarNegeri->index = $key;
                        $model['pegawai_kunjungan_luar_negeri'][] = $pegawaiKunjunganLuarNegeri;
                    }
                }
                if (isset($post['PegawaiKursusPelatihan'])) {
                    foreach ($post['PegawaiKursusPelatihan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiKursusPelatihan = PegawaiKursusPelatihan::find()->where(['id' => $value['id']])->one();
                            $pegawaiKursusPelatihan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiKursusPelatihan = PegawaiKursusPelatihan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiKursusPelatihan->isDeleted = true;
                        } else {
                            $pegawaiKursusPelatihan = new PegawaiKursusPelatihan();
                            $pegawaiKursusPelatihan->setAttributes($value);
                        }
                        $pegawaiKursusPelatihan->index = $key;
                        $model['pegawai_kursus_pelatihan'][] = $pegawaiKursusPelatihan;
                    }
                }
                if (isset($post['PegawaiMasaPersiapanPensiun'])) {
                    foreach ($post['PegawaiMasaPersiapanPensiun'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiMasaPersiapanPensiun = PegawaiMasaPersiapanPensiun::find()->where(['id' => $value['id']])->one();
                            $pegawaiMasaPersiapanPensiun->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiMasaPersiapanPensiun = PegawaiMasaPersiapanPensiun::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiMasaPersiapanPensiun->isDeleted = true;
                        } else {
                            $pegawaiMasaPersiapanPensiun = new PegawaiMasaPersiapanPensiun();
                            $pegawaiMasaPersiapanPensiun->setAttributes($value);
                        }
                        $pegawaiMasaPersiapanPensiun->index = $key;
                        $model['pegawai_masa_persiapan_pensiun'][] = $pegawaiMasaPersiapanPensiun;
                    }
                }
                if (isset($post['PegawaiMutasi'])) {
                    foreach ($post['PegawaiMutasi'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiMutasi = PegawaiMutasi::find()->where(['id' => $value['id']])->one();
                            $pegawaiMutasi->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiMutasi = PegawaiMutasi::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiMutasi->isDeleted = true;
                        } else {
                            $pegawaiMutasi = new PegawaiMutasi();
                            $pegawaiMutasi->setAttributes($value);
                        }
                        $pegawaiMutasi->index = $key;
                        $model['pegawai_mutasi'][] = $pegawaiMutasi;
                    }
                }
                if (isset($post['PegawaiOrganisasi'])) {
                    foreach ($post['PegawaiOrganisasi'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiOrganisasi = PegawaiOrganisasi::find()->where(['id' => $value['id']])->one();
                            $pegawaiOrganisasi->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiOrganisasi = PegawaiOrganisasi::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiOrganisasi->isDeleted = true;
                        } else {
                            $pegawaiOrganisasi = new PegawaiOrganisasi();
                            $pegawaiOrganisasi->setAttributes($value);
                        }
                        $pegawaiOrganisasi->index = $key;
                        $model['pegawai_organisasi'][] = $pegawaiOrganisasi;
                    }
                }
                if (isset($post['PegawaiPelaksana'])) {
                    foreach ($post['PegawaiPelaksana'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPelaksana = PegawaiPelaksana::find()->where(['id' => $value['id']])->one();
                            $pegawaiPelaksana->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPelaksana = PegawaiPelaksana::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPelaksana->isDeleted = true;
                        } else {
                            $pegawaiPelaksana = new PegawaiPelaksana();
                            $pegawaiPelaksana->setAttributes($value);
                        }
                        $pegawaiPelaksana->index = $key;
                        $model['pegawai_pelaksana'][] = $pegawaiPelaksana;
                    }
                }
                if (isset($post['PegawaiPemberhentian'])) {
                    foreach ($post['PegawaiPemberhentian'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPemberhentian = PegawaiPemberhentian::find()->where(['id' => $value['id']])->one();
                            $pegawaiPemberhentian->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPemberhentian = PegawaiPemberhentian::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPemberhentian->isDeleted = true;
                        } else {
                            $pegawaiPemberhentian = new PegawaiPemberhentian();
                            $pegawaiPemberhentian->setAttributes($value);
                        }
                        $pegawaiPemberhentian->index = $key;
                        $model['pegawai_pemberhentian'][] = $pegawaiPemberhentian;
                    }
                }
                if (isset($post['PegawaiPendidikan'])) {
                    foreach ($post['PegawaiPendidikan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPendidikan = PegawaiPendidikan::find()->where(['id' => $value['id']])->one();
                            $pegawaiPendidikan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPendidikan = PegawaiPendidikan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPendidikan->isDeleted = true;
                        } else {
                            $pegawaiPendidikan = new PegawaiPendidikan();
                            $pegawaiPendidikan->setAttributes($value);
                        }
                        $pegawaiPendidikan->index = $key;
                        $model['pegawai_pendidikan'][] = $pegawaiPendidikan;
                    }
                }
                if (isset($post['PegawaiPenetapanAngkaKredit'])) {
                    foreach ($post['PegawaiPenetapanAngkaKredit'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPenetapanAngkaKredit = PegawaiPenetapanAngkaKredit::find()->where(['id' => $value['id']])->one();
                            $pegawaiPenetapanAngkaKredit->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPenetapanAngkaKredit = PegawaiPenetapanAngkaKredit::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPenetapanAngkaKredit->isDeleted = true;
                        } else {
                            $pegawaiPenetapanAngkaKredit = new PegawaiPenetapanAngkaKredit();
                            $pegawaiPenetapanAngkaKredit->setAttributes($value);
                        }
                        $pegawaiPenetapanAngkaKredit->index = $key;
                        $model['pegawai_penetapan_angka_kredit'][] = $pegawaiPenetapanAngkaKredit;
                    }
                }
                if (isset($post['PegawaiPenghargaan'])) {
                    foreach ($post['PegawaiPenghargaan'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiPenghargaan = PegawaiPenghargaan::find()->where(['id' => $value['id']])->one();
                            $pegawaiPenghargaan->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiPenghargaan = PegawaiPenghargaan::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiPenghargaan->isDeleted = true;
                        } else {
                            $pegawaiPenghargaan = new PegawaiPenghargaan();
                            $pegawaiPenghargaan->setAttributes($value);
                        }
                        $pegawaiPenghargaan->index = $key;
                        $model['pegawai_penghargaan'][] = $pegawaiPenghargaan;
                    }
                }
                if (isset($post['PegawaiSaudaraKandung'])) {
                    foreach ($post['PegawaiSaudaraKandung'] as $key => $value) {
                        if ($value['id'] > 0) {
                            $pegawaiSaudaraKandung = PegawaiSaudaraKandung::find()->where(['id' => $value['id']])->one();
                            $pegawaiSaudaraKandung->setAttributes($value);
                        } else if($value['id'] < 0) {
                            $pegawaiSaudaraKandung = PegawaiSaudaraKandung::find()->where(['id' => $value['id']*-1])->one();
                            $pegawaiSaudaraKandung->isDeleted = true;
                        } else {
                            $pegawaiSaudaraKandung = new PegawaiSaudaraKandung();
                            $pegawaiSaudaraKandung->setAttributes($value);
                        }
                        $pegawaiSaudaraKandung->index = $key;
                        $model['pegawai_saudara_kandung'][] = $pegawaiSaudaraKandung;
                    }
                }
            }

            $transaction['pegawai'] = Pegawai::getDb()->beginTransaction();

            try {
                if (!$model['pegawai']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                if (false) {
                    if (isset($model['pegawai_anak']) and is_array($model['pegawai_anak'])) {
                        foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
                            $pegawaiAnak->pegawai = $model['pegawai']->id;
                            if (!$pegawaiAnak->isDeleted && !$pegawaiAnak->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_diklat']) and is_array($model['pegawai_diklat'])) {
                        foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
                            $pegawaiDiklat->pegawai = $model['pegawai']->id;
                            if (!$pegawaiDiklat->isDeleted && !$pegawaiDiklat->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_hukuman_disiplin']) and is_array($model['pegawai_hukuman_disiplin'])) {
                        foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
                            $pegawaiHukumanDisiplin->pegawai = $model['pegawai']->id;
                            if (!$pegawaiHukumanDisiplin->isDeleted && !$pegawaiHukumanDisiplin->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kenaikan_gaji_berkala']) and is_array($model['pegawai_kenaikan_gaji_berkala'])) {
                        foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
                            $pegawaiKenaikanGajiBerkala->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKenaikanGajiBerkala->isDeleted && !$pegawaiKenaikanGajiBerkala->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kepangkatan']) and is_array($model['pegawai_kepangkatan'])) {
                        foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
                            $pegawaiKepangkatan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKepangkatan->isDeleted && !$pegawaiKepangkatan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kunjungan_luar_negeri']) and is_array($model['pegawai_kunjungan_luar_negeri'])) {
                        foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
                            $pegawaiKunjunganLuarNegeri->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKunjunganLuarNegeri->isDeleted && !$pegawaiKunjunganLuarNegeri->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_kursus_pelatihan']) and is_array($model['pegawai_kursus_pelatihan'])) {
                        foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
                            $pegawaiKursusPelatihan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiKursusPelatihan->isDeleted && !$pegawaiKursusPelatihan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_masa_persiapan_pensiun']) and is_array($model['pegawai_masa_persiapan_pensiun'])) {
                        foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
                            $pegawaiMasaPersiapanPensiun->pegawai = $model['pegawai']->id;
                            if (!$pegawaiMasaPersiapanPensiun->isDeleted && !$pegawaiMasaPersiapanPensiun->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_mutasi']) and is_array($model['pegawai_mutasi'])) {
                        foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
                            $pegawaiMutasi->pegawai = $model['pegawai']->id;
                            if (!$pegawaiMutasi->isDeleted && !$pegawaiMutasi->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_organisasi']) and is_array($model['pegawai_organisasi'])) {
                        foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
                            $pegawaiOrganisasi->pegawai = $model['pegawai']->id;
                            if (!$pegawaiOrganisasi->isDeleted && !$pegawaiOrganisasi->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pelaksana']) and is_array($model['pegawai_pelaksana'])) {
                        foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
                            $pegawaiPelaksana->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPelaksana->isDeleted && !$pegawaiPelaksana->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pemberhentian']) and is_array($model['pegawai_pemberhentian'])) {
                        foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
                            $pegawaiPemberhentian->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPemberhentian->isDeleted && !$pegawaiPemberhentian->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_pendidikan']) and is_array($model['pegawai_pendidikan'])) {
                        foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
                            $pegawaiPendidikan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPendidikan->isDeleted && !$pegawaiPendidikan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_penetapan_angka_kredit']) and is_array($model['pegawai_penetapan_angka_kredit'])) {
                        foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
                            $pegawaiPenetapanAngkaKredit->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPenetapanAngkaKredit->isDeleted && !$pegawaiPenetapanAngkaKredit->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_penghargaan']) and is_array($model['pegawai_penghargaan'])) {
                        foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
                            $pegawaiPenghargaan->pegawai = $model['pegawai']->id;
                            if (!$pegawaiPenghargaan->isDeleted && !$pegawaiPenghargaan->validate()) $error = true;
                        }
                    }
                    if (isset($model['pegawai_saudara_kandung']) and is_array($model['pegawai_saudara_kandung'])) {
                        foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
                            $pegawaiSaudaraKandung->pegawai = $model['pegawai']->id;
                            if (!$pegawaiSaudaraKandung->isDeleted && !$pegawaiSaudaraKandung->validate()) $error = true;
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                if (false) {
                    if (isset($model['pegawai_anak']) and is_array($model['pegawai_anak'])) {
                        foreach ($model['pegawai_anak'] as $key => $pegawaiAnak) {
                            if ($pegawaiAnak->isDeleted) {
                                if (!$pegawaiAnak->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiAnak->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_diklat']) and is_array($model['pegawai_diklat'])) {
                        foreach ($model['pegawai_diklat'] as $key => $pegawaiDiklat) {
                            if ($pegawaiDiklat->isDeleted) {
                                if (!$pegawaiDiklat->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiDiklat->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_hukuman_disiplin']) and is_array($model['pegawai_hukuman_disiplin'])) {
                        foreach ($model['pegawai_hukuman_disiplin'] as $key => $pegawaiHukumanDisiplin) {
                            if ($pegawaiHukumanDisiplin->isDeleted) {
                                if (!$pegawaiHukumanDisiplin->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiHukumanDisiplin->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kenaikan_gaji_berkala']) and is_array($model['pegawai_kenaikan_gaji_berkala'])) {
                        foreach ($model['pegawai_kenaikan_gaji_berkala'] as $key => $pegawaiKenaikanGajiBerkala) {
                            if ($pegawaiKenaikanGajiBerkala->isDeleted) {
                                if (!$pegawaiKenaikanGajiBerkala->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKenaikanGajiBerkala->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kepangkatan']) and is_array($model['pegawai_kepangkatan'])) {
                        foreach ($model['pegawai_kepangkatan'] as $key => $pegawaiKepangkatan) {
                            if ($pegawaiKepangkatan->isDeleted) {
                                if (!$pegawaiKepangkatan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKepangkatan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kunjungan_luar_negeri']) and is_array($model['pegawai_kunjungan_luar_negeri'])) {
                        foreach ($model['pegawai_kunjungan_luar_negeri'] as $key => $pegawaiKunjunganLuarNegeri) {
                            if ($pegawaiKunjunganLuarNegeri->isDeleted) {
                                if (!$pegawaiKunjunganLuarNegeri->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKunjunganLuarNegeri->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_kursus_pelatihan']) and is_array($model['pegawai_kursus_pelatihan'])) {
                        foreach ($model['pegawai_kursus_pelatihan'] as $key => $pegawaiKursusPelatihan) {
                            if ($pegawaiKursusPelatihan->isDeleted) {
                                if (!$pegawaiKursusPelatihan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiKursusPelatihan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_masa_persiapan_pensiun']) and is_array($model['pegawai_masa_persiapan_pensiun'])) {
                        foreach ($model['pegawai_masa_persiapan_pensiun'] as $key => $pegawaiMasaPersiapanPensiun) {
                            if ($pegawaiMasaPersiapanPensiun->isDeleted) {
                                if (!$pegawaiMasaPersiapanPensiun->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiMasaPersiapanPensiun->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_mutasi']) and is_array($model['pegawai_mutasi'])) {
                        foreach ($model['pegawai_mutasi'] as $key => $pegawaiMutasi) {
                            if ($pegawaiMutasi->isDeleted) {
                                if (!$pegawaiMutasi->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiMutasi->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_organisasi']) and is_array($model['pegawai_organisasi'])) {
                        foreach ($model['pegawai_organisasi'] as $key => $pegawaiOrganisasi) {
                            if ($pegawaiOrganisasi->isDeleted) {
                                if (!$pegawaiOrganisasi->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiOrganisasi->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pelaksana']) and is_array($model['pegawai_pelaksana'])) {
                        foreach ($model['pegawai_pelaksana'] as $key => $pegawaiPelaksana) {
                            if ($pegawaiPelaksana->isDeleted) {
                                if (!$pegawaiPelaksana->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPelaksana->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pemberhentian']) and is_array($model['pegawai_pemberhentian'])) {
                        foreach ($model['pegawai_pemberhentian'] as $key => $pegawaiPemberhentian) {
                            if ($pegawaiPemberhentian->isDeleted) {
                                if (!$pegawaiPemberhentian->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPemberhentian->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_pendidikan']) and is_array($model['pegawai_pendidikan'])) {
                        foreach ($model['pegawai_pendidikan'] as $key => $pegawaiPendidikan) {
                            if ($pegawaiPendidikan->isDeleted) {
                                if (!$pegawaiPendidikan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPendidikan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_penetapan_angka_kredit']) and is_array($model['pegawai_penetapan_angka_kredit'])) {
                        foreach ($model['pegawai_penetapan_angka_kredit'] as $key => $pegawaiPenetapanAngkaKredit) {
                            if ($pegawaiPenetapanAngkaKredit->isDeleted) {
                                if (!$pegawaiPenetapanAngkaKredit->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPenetapanAngkaKredit->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_penghargaan']) and is_array($model['pegawai_penghargaan'])) {
                        foreach ($model['pegawai_penghargaan'] as $key => $pegawaiPenghargaan) {
                            if ($pegawaiPenghargaan->isDeleted) {
                                if (!$pegawaiPenghargaan->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiPenghargaan->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                    if (isset($model['pegawai_saudara_kandung']) and is_array($model['pegawai_saudara_kandung'])) {
                        foreach ($model['pegawai_saudara_kandung'] as $key => $pegawaiSaudaraKandung) {
                            if ($pegawaiSaudaraKandung->isDeleted) {
                                if (!$pegawaiSaudaraKandung->delete()) {
                                    $error = true;
                                }
                            } else {
                                if (!$pegawaiSaudaraKandung->save()) {
                                    $error = true;
                                }
                            }
                        }
                    }
                }

                if ($error) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $transaction['pegawai']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pegawai']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if (false) {
                foreach ($model['pegawai']->pegawaiAnaks as $key => $pegawaiAnak)
                    $model['pegawai_anak'][] = $pegawaiAnak;
                foreach ($model['pegawai']->pegawaiDiklats as $key => $pegawaiDiklat)
                    $model['pegawai_diklat'][] = $pegawaiDiklat;
                foreach ($model['pegawai']->pegawaiHukumanDisiplins as $key => $pegawaiHukumanDisiplin)
                    $model['pegawai_hukuman_disiplin'][] = $pegawaiHukumanDisiplin;
                foreach ($model['pegawai']->pegawaiKenaikanGajiBerkalas as $key => $pegawaiKenaikanGajiBerkala)
                    $model['pegawai_kenaikan_gaji_berkala'][] = $pegawaiKenaikanGajiBerkala;
                foreach ($model['pegawai']->pegawaiKepangkatans as $key => $pegawaiKepangkatan)
                    $model['pegawai_kepangkatan'][] = $pegawaiKepangkatan;
                foreach ($model['pegawai']->pegawaiKunjunganLuarNegeris as $key => $pegawaiKunjunganLuarNegeri)
                    $model['pegawai_kunjungan_luar_negeri'][] = $pegawaiKunjunganLuarNegeri;
                foreach ($model['pegawai']->pegawaiKursusPelatihans as $key => $pegawaiKursusPelatihan)
                    $model['pegawai_kursus_pelatihan'][] = $pegawaiKursusPelatihan;
                foreach ($model['pegawai']->pegawaiMasaPersiapanPensiuns as $key => $pegawaiMasaPersiapanPensiun)
                    $model['pegawai_masa_persiapan_pensiun'][] = $pegawaiMasaPersiapanPensiun;
                foreach ($model['pegawai']->pegawaiMutasis as $key => $pegawaiMutasi)
                    $model['pegawai_mutasi'][] = $pegawaiMutasi;
                foreach ($model['pegawai']->pegawaiOrganisasis as $key => $pegawaiOrganisasi)
                    $model['pegawai_organisasi'][] = $pegawaiOrganisasi;
                foreach ($model['pegawai']->pegawaiPelaksanas as $key => $pegawaiPelaksana)
                    $model['pegawai_pelaksana'][] = $pegawaiPelaksana;
                foreach ($model['pegawai']->pegawaiPemberhentians as $key => $pegawaiPemberhentian)
                    $model['pegawai_pemberhentian'][] = $pegawaiPemberhentian;
                foreach ($model['pegawai']->pegawaiPendidikans as $key => $pegawaiPendidikan)
                    $model['pegawai_pendidikan'][] = $pegawaiPendidikan;
                foreach ($model['pegawai']->pegawaiPenetapanAngkaKredits as $key => $pegawaiPenetapanAngkaKredit)
                    $model['pegawai_penetapan_angka_kredit'][] = $pegawaiPenetapanAngkaKredit;
                foreach ($model['pegawai']->pegawaiPenghargaans as $key => $pegawaiPenghargaan)
                    $model['pegawai_penghargaan'][] = $pegawaiPenghargaan;
                foreach ($model['pegawai']->pegawaiSaudaraKandungs as $key => $pegawaiSaudaraKandung)
                $model['pegawai_saudara_kandung'][] = $pegawaiSaudaraKandung;
            }

            if ($model['pegawai']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-tunkin', [
                'model' => $model,
                'tab' => $tab,
                'title' => 'Ubah Data Tunkin Pegawai : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
            ]);
        else
            return $this->redirect(['tunkin-update', 'id' => $model['pegawai']->id/*, 'tab' => $tab == 'personal' || $tab == 'pegawai' ? 'jabatan' : $tab*/]);
    }

    ///

    public function actionDatatablesMasterStatusKepegawaian()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'm.id',
                'm.status_kepegawaian',
            ])
            ->from('status_kepegawaian m')
            // ->join('LEFT JOIN', 'asdf j', 'j.id = m.asdf')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesMasterKepangkatan()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'm.id',
                'm.pangkat',
                'm.golongan',
                'm.ruang',
            ])
            ->from('kepangkatan m')
            // ->join('LEFT JOIN', 'asdf j', 'j.id = m.asdf')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesMasterGrade()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'g.id',
                'g.grade',
            ])
            ->from('grade g')
            // ->join('LEFT JOIN', 'asdf j', 'j.id = m.asdf')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionMaster($tab = 'grade')
    {
        return $this->render('list-master', [
            'tab' => $tab,
            'title' => 'Data Master',
        ]);
    }

    public function actionMasterForm($id = null, $tab)
    {
        $error = true;

        switch ($tab) {
            case 'grade':
                if (isset($id)) {
                    if (($model['grade'] = Grade::find()->where(['id' => $id])->one()) == null) {
                        throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                    }
                } else {
                    $model['grade'] = new Grade();
                }
                break;
            case 'kepangkatan':
                if (isset($id)) {
                    if (($model['kepangkatan'] = Kepangkatan::find()->where(['id' => $id])->one()) == null) {
                        throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                    }
                } else {
                    $model['kepangkatan'] = new Kepangkatan();
                }
                break;
            case 'status-kepegawaian':
                if (isset($id)) {
                    if (($model['status_kepegawaian'] = StatusKepegawaian::find()->where(['id' => $id])->one()) == null) {
                        throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                    }
                } else {
                    $model['status_kepegawaian'] = new StatusKepegawaian();
                }
                break;
            default:
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                break;
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model[str_replace('-', '_', $tab)]->load($post);

            $transaction['pegawai'] = Pegawai::getDb()->beginTransaction();

            try {
                if (!$model[str_replace('-', '_', $tab)]->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $error = false;

                $transaction['pegawai']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['pegawai']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-master', [
                'model' => $model,
                'tab' => $tab,
                'title' => 'Ubah Data Master : <span class="text-lightest">' . ucfirst(str_replace('_', ' ', $tab)) . '</span>',
            ]);
        else
            return $this->redirect(['master', 'tab' => $tab]);
    }

    public function actionMasterDelete($id, $tab)
    {
        $error = true;

        $transaction['pegawai'] = Pegawai::getDb()->beginTransaction();

        try {
            switch ($tab) {
                case 'grade':
                    Grade::find()->where(['id' => $id])->one()->delete();
                    break;
                case 'kepangkatan':
                    Kepangkatan::find()->where(['id' => $id])->one()->delete();
                    break;
                case 'status-kepegawaian':
                    StatusKepegawaian::find()->where(['id' => $id])->one()->delete();
                    break;
                default:
                    throw new \yii\web\HttpException(404, 'The requested page does not exist.');
                    break;
            }
            
            $error = false;

            $transaction['pegawai']->commit();
            Yii::$app->session->setFlash('success', 'Data has been deleted.');
        } catch (\Throwable $e) {
            $error = true;
            $transaction['pegawai']->rollBack();
            if (get_class($e) == 'yii\db\IntegrityException') Yii::$app->session->setFlash('error', 'Data tidak dapat dihapus karena direferensikan ke data lain.');
            else if (get_class($e) == 'yii\web\HttpException' || get_class($e) == 'yii\db\IntegrityException') Yii::$app->session->setFlash('error', $e->getMessage());
            else throw $e;
        }

        return $this->redirect(['master', 'tab' => $tab]);
    }

    ///

    public function datatabless($query, $post, $db)
    {
        if (isset($post['draw'])) {
            $select = $query->select;

            $query->select('count(*)');
            $countWhere = $query->where === null ? 0 : count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if (isset($value['data']['equal']) && $value['data']['equal'] == 'true') {
                        $query->andFilterWhere([$column => $value['search']['value']]);
                    }
                    else if ($value['search']['regex'] == 'false') {
                        if ($key != 2) {
                            $query->andFilterWhere(['like', $column, $value['search']['value']]);
                        }
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (($allWhere === null ? 0 : count($allWhere)) > 1)
                $query->andFilterWhere($allWhere);
            if (($query->where === null ? 0 : count($query->where)) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select($select);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            ($order === null ? 0 : count($order)) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function datatablesKehadiranBulanan($idPegawai = null, $bulan, $tahun)
    {
        $subQuery = new \yii\db\Query();
        $subQuery
            ->select([
                'p.id',
                'p.nama',
                'CONCAT("\'", p.tunkin_nip) as nip',
                'uk.unit_kerja',
                'count(case when kh.dihitung_hadir = "Hadir" then k.id end) AS hadir',
                'count(case when kh.dihitung_hadir = "Tidak Hadir" then k.id end) AS tidak_hadir',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'kehadiran k', 'k.pegawai = p.id')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->where([
                // 'p.id' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->orderBy(['uk.level' => SORT_ASC])
            ->groupBy(['p.id', 'p.nama', 'p.nip', 'p.pin'])
        ;
        $uk = Yii::$app->request->isPost ? Yii::$app->request->post()['columns'][2]['search']['value'] : '';
        if ($uk) {
            $ukIdMentah = (new \yii\db\Query())->select('id')->from('unit_kerja uk')->where(['like', 'unit_kerja', $uk])->orderBy(['level' => SORT_ASC])->scalar();
            $result = (new \yii\db\Query())->select('get_pegawai_rec(' . $ukIdMentah . ')')->scalar();
            $pegawais = $result ? explode(',', $result) : [];
            $subQuery->andWhere(['p.id' => $pegawais]);
        }
        if ($idPegawai) {
            $subQuery->andWhere(['p.id' => $idPegawai]);
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'nama',
                'nip',
                'unit_kerja',
                'hadir',
                'tidak_hadir',
            ])
            ->from(['sq' => $subQuery])
        ;

        return $query;
        // return $this->json($query->all());
    }

    public function actionDatatablesKehadiranBulanan(/*$idPegawai, */$bulan, $tahun)
    {
        return $this->datatabless($this->datatablesKehadiranBulanan(null, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionUangMakan($id = null, $bulan = null, $tahun = null)
    {
        $model['filter_kehadiran'] = new FilterKehadiran();
        if ($bulan) {
            $model['filter_kehadiran']->bulan = $bulan;
        }
        if ($tahun) {
            $model['filter_kehadiran']->tahun = $tahun;
        }
        
        if (!$id) {
            return $this->render('list-uang-makan', [
                'model' => $model,
                'title' => 'Daftar Uang Makan Pegawai',
            ]);
        }
    }
}