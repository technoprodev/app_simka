<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "cuti".
 *
 * @property integer $id
 * @property integer $keterangan_kehadiran
 * @property string $status_pengajuan
 * @property integer $pegawai
 * @property string $waktu_pengajuan
 * @property string $catatan_pengajuan
 * @property integer $atasan_1
 * @property string $waktu_atasan_1
 * @property string $catatan_atasan_1
 * @property integer $atasan_2
 * @property string $waktu_atasan_2
 * @property string $catatan_atasan_2
 * @property integer $disetujui
 * @property string $waktu_disetujui
 * @property string $catatan_verifikasi
 * @property string $dari_tanggal
 * @property string $sampai_tanggal
 * @property string $alasan_cuti
 * @property string $alamat
 * @property string $telp
 * @property string $nomor_surat
 * @property string $bukti_utama
 * @property string $bukti_tambahan_1
 * @property string $bukti_tambahan_2
 * @property string $bukti_tambahan_3
 * @property string $bukti_tambahan_4
 * @property string $bukti_tambahan_5
 *
 * @property Pegawai $pegawai0
 * @property Pegawai $atasan1
 * @property Pegawai $atasan2
 * @property Pegawai $disetujui0
 * @property KeteranganKehadiran $keteranganKehadiran
 */
class Cuti extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_bukti_utama_upload;
    public $virtual_bukti_utama_download;
    public $virtual_bukti_tambahan_1_upload;
    public $virtual_bukti_tambahan_1_download;
    public $virtual_bukti_tambahan_2_upload;
    public $virtual_bukti_tambahan_2_download;
    public $virtual_bukti_tambahan_3_upload;
    public $virtual_bukti_tambahan_3_download;
    public $virtual_bukti_tambahan_4_upload;
    public $virtual_bukti_tambahan_4_download;
    public $virtual_bukti_tambahan_5_upload;
    public $virtual_bukti_tambahan_5_download;
    
    public static function tableName()
    {
        return 'cuti';
    }

    public function rules()
    {
        return [
            //id

            //keterangan_kehadiran
            [['keterangan_kehadiran'], 'required'],
            [['keterangan_kehadiran'], 'integer'],
            [['keterangan_kehadiran'], 'exist', 'skipOnError' => true, 'targetClass' => KeteranganKehadiran::className(), 'targetAttribute' => ['keterangan_kehadiran' => 'id']],

            //status_pengajuan
            [['status_pengajuan'], 'required'],
            [['status_pengajuan'], 'string'],

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //waktu_pengajuan
            [['waktu_pengajuan'], 'required'],
            [['waktu_pengajuan'], 'safe'],

            //catatan_pengajuan
            [['catatan_pengajuan'], 'required'],
            [['catatan_pengajuan'], 'string', 'max' => 256],

            //atasan_1
            [['atasan_1'], 'integer'],
            [['atasan_1'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['atasan_1' => 'id']],

            //waktu_atasan_1
            [['waktu_atasan_1'], 'safe'],

            //catatan_atasan_1
            [['catatan_atasan_1'], 'string', 'max' => 256],

            //atasan_2
            [['atasan_2'], 'integer'],
            [['atasan_2'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['atasan_2' => 'id']],

            //waktu_atasan_2
            [['waktu_atasan_2'], 'safe'],

            //catatan_atasan_2
            [['catatan_atasan_2'], 'string', 'max' => 256],

            //disetujui
            [['disetujui'], 'integer'],
            [['disetujui'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['disetujui' => 'id']],

            //waktu_disetujui
            [['waktu_disetujui'], 'safe'],

            //catatan_verifikasi
            [['catatan_verifikasi'], 'string', 'max' => 256],

            //dari_tanggal
            [['dari_tanggal'], 'required'],
            [['dari_tanggal'], 'safe'],

            //sampai_tanggal
            [['sampai_tanggal'], 'required'],
            [['sampai_tanggal'], 'safe'],

            //alasan_cuti
            [['alasan_cuti'], 'required'],
            [['alasan_cuti'], 'string'],

            //alamat
            [['alamat'], 'required'],
            [['alamat'], 'string'],

            //telp
            [['telp'], 'required'],
            [['telp'], 'string', 'max' => 32],

            //nomor_surat
            [['nomor_surat'], 'string', 'max' => 32],

            //bukti_utama
            [['bukti_utama'], 'string', 'max' => 256],

            //bukti_tambahan_1
            [['bukti_tambahan_1'], 'string', 'max' => 256],

            //bukti_tambahan_2
            [['bukti_tambahan_2'], 'string', 'max' => 256],

            //bukti_tambahan_3
            [['bukti_tambahan_3'], 'string', 'max' => 256],

            //bukti_tambahan_4
            [['bukti_tambahan_4'], 'string', 'max' => 256],

            //bukti_tambahan_5
            [['bukti_tambahan_5'], 'string', 'max' => 256],

            //custom
            
            //virtual_bukti_utama_download
            [['virtual_bukti_utama_download'], 'safe'],
            
            //virtual_bukti_utama_upload
            [['virtual_bukti_utama_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_1_download
            [['virtual_bukti_tambahan_1_download'], 'safe'],
            
            //virtual_bukti_tambahan_1_upload
            [['virtual_bukti_tambahan_1_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_2_download
            [['virtual_bukti_tambahan_2_download'], 'safe'],
            
            //virtual_bukti_tambahan_2_upload
            [['virtual_bukti_tambahan_2_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_3_download
            [['virtual_bukti_tambahan_3_download'], 'safe'],
            
            //virtual_bukti_tambahan_3_upload
            [['virtual_bukti_tambahan_3_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_4_download
            [['virtual_bukti_tambahan_4_download'], 'safe'],
            
            //virtual_bukti_tambahan_4_upload
            [['virtual_bukti_tambahan_4_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_5_download
            [['virtual_bukti_tambahan_5_download'], 'safe'],
            
            //virtual_bukti_tambahan_5_upload
            [['virtual_bukti_tambahan_5_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_bukti_utama_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_utama_upload');
        if ($this->virtual_bukti_utama_upload) {
            $this->bukti_utama = $this->virtual_bukti_utama_upload->name;
        }

        $this->virtual_bukti_tambahan_1_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_1_upload');
        if ($this->virtual_bukti_tambahan_1_upload) {
            $this->bukti_tambahan_1 = $this->virtual_bukti_tambahan_1_upload->name;
        }

        $this->virtual_bukti_tambahan_2_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_2_upload');
        if ($this->virtual_bukti_tambahan_2_upload) {
            $this->bukti_tambahan_2 = $this->virtual_bukti_tambahan_2_upload->name;
        }

        $this->virtual_bukti_tambahan_3_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_3_upload');
        if ($this->virtual_bukti_tambahan_3_upload) {
            $this->bukti_tambahan_3 = $this->virtual_bukti_tambahan_3_upload->name;
        }

        $this->virtual_bukti_tambahan_4_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_4_upload');
        if ($this->virtual_bukti_tambahan_4_upload) {
            $this->bukti_tambahan_4 = $this->virtual_bukti_tambahan_4_upload->name;
        }

        $this->virtual_bukti_tambahan_5_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_5_upload');
        if ($this->virtual_bukti_tambahan_5_upload) {
            $this->bukti_tambahan_5 = $this->virtual_bukti_tambahan_5_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_bukti_utama_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_utama_upload');
        if ($this->virtual_bukti_utama_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_utama') . '/' . $this->id . '/' . $this->bukti_utama;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_utama = $this->virtual_bukti_utama_upload->name;
        }

        $this->virtual_bukti_tambahan_1_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_1_upload');
        if ($this->virtual_bukti_tambahan_1_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_1') . '/' . $this->id . '/' . $this->bukti_tambahan_1;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_1 = $this->virtual_bukti_tambahan_1_upload->name;
        }

        $this->virtual_bukti_tambahan_2_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_2_upload');
        if ($this->virtual_bukti_tambahan_2_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_2') . '/' . $this->id . '/' . $this->bukti_tambahan_2;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_2 = $this->virtual_bukti_tambahan_2_upload->name;
        }

        $this->virtual_bukti_tambahan_3_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_3_upload');
        if ($this->virtual_bukti_tambahan_3_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_3') . '/' . $this->id . '/' . $this->bukti_tambahan_3;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_3 = $this->virtual_bukti_tambahan_3_upload->name;
        }

        $this->virtual_bukti_tambahan_4_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_4_upload');
        if ($this->virtual_bukti_tambahan_4_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_4') . '/' . $this->id . '/' . $this->bukti_tambahan_4;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_4 = $this->virtual_bukti_tambahan_4_upload->name;
        }

        $this->virtual_bukti_tambahan_5_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_5_upload');
        if ($this->virtual_bukti_tambahan_5_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_5') . '/' . $this->id . '/' . $this->bukti_tambahan_5;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_5 = $this->virtual_bukti_tambahan_5_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_bukti_utama_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_utama') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_utama_upload->saveAs($path . '/' . $this->bukti_utama);
        }
        
        if ($this->virtual_bukti_tambahan_1_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_1') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_1_upload->saveAs($path . '/' . $this->bukti_tambahan_1);
        }
        
        if ($this->virtual_bukti_tambahan_2_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_2') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_2_upload->saveAs($path . '/' . $this->bukti_tambahan_2);
        }
        
        if ($this->virtual_bukti_tambahan_3_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_3') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_3_upload->saveAs($path . '/' . $this->bukti_tambahan_3);
        }
        
        if ($this->virtual_bukti_tambahan_4_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_4') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_4_upload->saveAs($path . '/' . $this->bukti_tambahan_4);
        }
        
        if ($this->virtual_bukti_tambahan_5_upload) {
            $path = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_5') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_5_upload->saveAs($path . '/' . $this->bukti_tambahan_5);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_utama') . '/' . $this->id . '/' . $this->bukti_utama;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_1') . '/' . $this->id . '/' . $this->bukti_tambahan_1;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_2') . '/' . $this->id . '/' . $this->bukti_tambahan_2;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_3') . '/' . $this->id . '/' . $this->bukti_tambahan_3;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_4') . '/' . $this->id . '/' . $this->bukti_tambahan_4;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_kehadiran-bukti_tambahan_5') . '/' . $this->id . '/' . $this->bukti_tambahan_5;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->bukti_utama) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_utama');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_utama_download = $path . '/' . $this->bukti_utama;
        }
        
        if($this->bukti_tambahan_1) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_tambahan_1');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_1_download = $path . '/' . $this->bukti_tambahan_1;
        }
        
        if($this->bukti_tambahan_2) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_tambahan_2');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_2_download = $path . '/' . $this->bukti_tambahan_2;
        }
        
        if($this->bukti_tambahan_3) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_tambahan_3');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_3_download = $path . '/' . $this->bukti_tambahan_3;
        }
        
        if($this->bukti_tambahan_4) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_tambahan_4');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_4_download = $path . '/' . $this->bukti_tambahan_4;
        }
        
        if($this->bukti_tambahan_5) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_kehadiran-bukti_tambahan_5');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_5_download = $path . '/' . $this->bukti_tambahan_5;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan_kehadiran' => 'Keterangan Kehadiran',
            'status_pengajuan' => 'Status Pengajuan',
            'pegawai' => 'Pegawai',
            'waktu_pengajuan' => 'Waktu Pengajuan',
            'catatan_pengajuan' => 'Catatan Pengajuan',
            'atasan_1' => 'Atasan 1',
            'waktu_atasan_1' => 'Waktu Atasan 1',
            'catatan_atasan_1' => 'Catatan Atasan 1',
            'atasan_2' => 'Atasan 2',
            'waktu_atasan_2' => 'Waktu Atasan 2',
            'catatan_atasan_2' => 'Catatan Atasan 2',
            'disetujui' => 'Disetujui',
            'waktu_disetujui' => 'Waktu Disetujui',
            'catatan_verifikasi' => 'Catatan Verifikasi',
            'dari_tanggal' => 'Dari Tanggal',
            'sampai_tanggal' => 'Sampai Tanggal',
            'alasan_cuti' => 'Alasan Cuti', 
            'alamat' => 'Alamat Saat Cuti', 
            'telp' => 'Telp', 
            'nomor_surat' => 'Nomor Surat', 
            'bukti_utama' => 'Bukti Utama',
            'bukti_tambahan_1' => 'Bukti Tambahan 1',
            'bukti_tambahan_2' => 'Bukti Tambahan 2',
            'bukti_tambahan_3' => 'Bukti Tambahan 3',
            'bukti_tambahan_4' => 'Bukti Tambahan 4',
            'bukti_tambahan_5' => 'Bukti Tambahan 5',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtasan1()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'atasan_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtasan2()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'atasan_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujui0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'disetujui']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeteranganKehadiran()
    {
        return $this->hasOne(KeteranganKehadiran::className(), ['id' => 'keterangan_kehadiran']);
    }
}
