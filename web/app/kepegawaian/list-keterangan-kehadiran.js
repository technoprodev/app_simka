$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-keterangan-kehadiran');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-keterangan-kehadiran'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            var action = '<a href="' + fn.urlTo('kepegawaian/keterangan-kehadiran/' + data) + '" class=""><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>';
                            return '' +
                                '<div class="fs-14 text-center">' +
                                action +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.kode',
                            display: 'kode',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.dihitung_hadir',
                            display: 'dihitung_hadir',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.potong_tunkin',
                            display: 'potong_tunkin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.potongan_tunkin_persen',
                            display: 'potongan_tunkin_persen',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.potong_uang_makan',
                            display: 'potong_uang_makan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.potong_disiplin',
                            display: 'potong_disiplin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.batas_awal_unggah',
                            display: 'batas_awal_unggah',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kk.batas_akhir_unggah',
                            display: 'batas_akhir_unggah',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});