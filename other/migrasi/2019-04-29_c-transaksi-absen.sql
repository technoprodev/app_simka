DELIMITER $$

DROP PROCEDURE IF EXISTS assign_kehadiran;
CREATE PROCEDURE assign_kehadiran(tgl_dari date, tgl_sampai date)
BEGIN
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update waktu masuk
  update kehadiran k
  inner join (
    select id, min(tr_time) as tr_time from (
      select
        k.id,
        @tr_time:=timestamp(a.tr_date, a.tr_time) as tr_time
      from kehadiran k
      inner join pegawai p on p.id=k.pegawai
      inner join `absensi`.`habsen` a on p.pin=a.empl_code and k.tanggal=a.tr_date
      where
        (
          case
            when tgl_dari is not null and tgl_sampai is not null then a.tr_date between tgl_dari and tgl_sampai
            when tgl_dari is not null then a.tr_date >= tgl_dari
            when tgl_sampai is not null then a.tr_date <= tgl_sampai
            else true
          end
        )
        and k.jadwal_masuk is not null and k.jadwal_pulang is not null
        and timestamp(a.tr_date, a.tr_time) between k.jadwal_batas_awal_masuk and k.jadwal_batas_akhir_masuk
    ) as t1
    group by id
  ) as t2 on t2.id=k.id
  set
    waktu_masuk=case
      when waktu_masuk_diupdate = 'Tidak' then t2.tr_time
      else waktu_masuk
    end
  ;

  -- update waktu pulang
  update kehadiran k
  inner join (
    select id, max(tr_time) as tr_time from (
      select
        k.id,
        @tr_time:=timestamp(a.tr_date, a.tr_time) as tr_time
      from kehadiran k
      inner join pegawai p on p.id=k.pegawai
      inner join `absensi`.`habsen` a on p.pin=a.empl_code and k.tanggal=a.tr_date
      where
        (
          case
            when tgl_dari is not null and tgl_sampai is not null then a.tr_date between tgl_dari and tgl_sampai
            when tgl_dari is not null then a.tr_date >= tgl_dari
            when tgl_sampai is not null then a.tr_date <= tgl_sampai
            else true
          end
        )
        and k.jadwal_masuk is not null and k.jadwal_pulang is not null
        and timestamp(a.tr_date, a.tr_time) between k.jadwal_batas_awal_pulang and k.jadwal_batas_akhir_pulang
    ) as t1
    group by id
  ) as t2 on t2.id=k.id
  set
    waktu_pulang=case
      when waktu_pulang_diupdate = 'Tidak' then t2.tr_time
      else waktu_pulang
    end
  where (
    case
      when k.waktu_masuk is not null and t2.tr_time is not null then t2.tr_time <> k.waktu_masuk
      else k.waktu_masuk is null
    end
  );

  -- update status kehadiran
  update kehadiran
  set
    keterangan=case
      when tanggal <= CURDATE() and keterangan_diupdate = 'Tidak' then
        case
          when waktu_masuk is not null and waktu_pulang is null then 6
          when waktu_masuk is null and waktu_pulang is not null then 5
          when waktu_masuk is null and waktu_pulang is null then 4
          when TIME_TO_SEC(
              GREATEST(
                  TIMEDIFF(
                      TIMEDIFF(jadwal_pulang, jadwal_masuk),
                      IFNULL(TIMEDIFF(
                          CASE WHEN DATE_FORMAT(waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(waktu_pulang, "%Y-%m-%d %H:%i") END,
                          DATE_FORMAT(GREATEST(waktu_masuk, jadwal_masuk), "%Y-%m-%d %H:%i")
                      ), 0)
                  ),
                  "00:00:00"
              )
          ) > 0 then 3
          else 2
        end
      when tanggal > CURDATE() and keterangan_diupdate = 'Tidak' then 1
      else keterangan
    end
  where
    (
      case
        when tgl_dari is not null and tgl_sampai is not null then tanggal between tgl_dari and tgl_sampai
        when tgl_dari is not null then tanggal >= tgl_dari
        when tgl_sampai is not null then tanggal <= tgl_sampai
        else true
      end
    )
    and jadwal_masuk is not null and jadwal_pulang is not null;

  commit;
END$$

DELIMITER ;