<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_kepangkatan".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property integer $kepangkatan
 * @property integer $status_kepegawaian
 * @property string $terhitung_mulai_tanggal
 * @property string $terhitung_sampai_tanggal
 * @property string $terhitung_sampai_sekarang
 * @property string $catatan
 * @property integer $jenis_sk
 * @property string $nomor_sk
 * @property string $tanggal_sk
 * @property string $pejabat_penetap_sk
 * @property string $arsip_sk
 *
 * @property Pegawai $pegawai0
 * @property Kepangkatan $kepangkatan0
 * @property JenisSk $jenisSk
 * @property StatusKepegawaian $statusKepegawaian
 */
class PegawaiKepangkatan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_arsip_sk_upload;
    public $virtual_arsip_sk_download;
    
    public static function tableName()
    {
        return 'pegawai_kepangkatan';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //kepangkatan
            [['kepangkatan'], 'required'],
            [['kepangkatan'], 'integer'],
            [['kepangkatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan' => 'id']],

            //status_kepegawaian
            [['status_kepegawaian'], 'required'],
            [['status_kepegawaian'], 'integer'],
            [['status_kepegawaian'], 'exist', 'skipOnError' => true, 'targetClass' => StatusKepegawaian::className(), 'targetAttribute' => ['status_kepegawaian' => 'id']],

            //terhitung_mulai_tanggal
            [['terhitung_mulai_tanggal'], 'required'],
            [['terhitung_mulai_tanggal'], 'safe'],

            //terhitung_sampai_tanggal
            [['terhitung_sampai_tanggal'], 'safe'],

            //terhitung_sampai_sekarang
            [['terhitung_sampai_sekarang'], 'string'],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'integer'],
            [['jenis_sk'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSk::className(), 'targetAttribute' => ['jenis_sk' => 'id']],

            //nomor_sk
            [['nomor_sk'], 'required'],
            [['nomor_sk'], 'string', 'max' => 256],

            //tanggal_sk
            [['tanggal_sk'], 'required'],
            [['tanggal_sk'], 'safe'],

            //pejabat_penetap_sk
            [['pejabat_penetap_sk'], 'required'],
            [['pejabat_penetap_sk'], 'string', 'max' => 256],

            //arsip_sk
            // [['arsip_sk'], 'required'],
            [['arsip_sk'], 'string', 'max' => 256],
            
            //virtual_arsip_sk_download
            [['virtual_arsip_sk_download'], 'safe'],
            
            //virtual_arsip_sk_upload
            [['virtual_arsip_sk_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_kepangkatan-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_arsip_sk_upload) {
            $path = Yii::getAlias('@upload-pegawai_kepangkatan-arsip_sk') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_arsip_sk_upload->saveAs($path . '/' . $this->arsip_sk);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_kepangkatan-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->arsip_sk) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_kepangkatan-arsip_sk');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_arsip_sk_download = $path . '/' . $this->arsip_sk;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'kepangkatan' => 'Kepangkatan',
            'status_kepegawaian' => 'Status Kepegawaian',
            'terhitung_mulai_tanggal' => 'Terhitung Mulai Tanggal',
            'terhitung_sampai_tanggal' => 'Terhitung Sampai Tanggal',
            'terhitung_sampai_sekarang' => 'Terhitung Sampai Sekarang',
            'catatan' => 'Catatan',
            'jenis_sk' => 'Jenis Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_sk' => 'Tanggal Sk',
            'pejabat_penetap_sk' => 'Pejabat Penetap Sk',
            'arsip_sk' => 'Arsip Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatan0()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSk()
    {
        return $this->hasOne(JenisSk::className(), ['id' => 'jenis_sk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusKepegawaian()
    {
        return $this->hasOne(StatusKepegawaian::className(), ['id' => 'status_kepegawaian']);
    }
}
