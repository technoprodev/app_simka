<?php
$params['app.name'] = 'Sadewa';
$params['app.description'] = 'Sistem Aplikasi Data Kepegawaian dan Absensi';
$params['app.keywords'] = 'sadewa, bekraf';
$params['app.owner'] = 'Bekraf';
$params['app.author'] = 'Technopro';
$params['enable.permission-checking'] = true;
$params['enable.permission-init'] = false;
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app_simka',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_simka\controllers',
    'bootstrap' => [
        'log',
        function(){
            Yii::setAlias('@download-pegawai-poto', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai-poto');
            Yii::setAlias('@upload-pegawai-poto', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai-poto');
            Yii::setAlias('@download-pegawai_mutasi-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_mutasi-arsip_sk');
            Yii::setAlias('@upload-pegawai_mutasi-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_mutasi-arsip_sk');
            Yii::setAlias('@download-pegawai_kepangkatan-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_kepangkatan-arsip_sk');
            Yii::setAlias('@upload-pegawai_kepangkatan-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_kepangkatan-arsip_sk');
            Yii::setAlias('@download-pegawai_kenaikan_gaji_berkala-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_kenaikan_gaji_berkala-arsip_sk');
            Yii::setAlias('@upload-pegawai_kenaikan_gaji_berkala-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_kenaikan_gaji_berkala-arsip_sk');
            Yii::setAlias('@download-pegawai_penetapan_angka_kredit-arsip_pak', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_penetapan_angka_kredit-arsip_pak');
            Yii::setAlias('@upload-pegawai_penetapan_angka_kredit-arsip_pak', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_penetapan_angka_kredit-arsip_pak');
            Yii::setAlias('@download-pegawai_anak-akta_lahir', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_anak-akta_lahir');
            Yii::setAlias('@upload-pegawai_anak-akta_lahir', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_anak-akta_lahir');
            Yii::setAlias('@download-pegawai_pendidikan-ijazah', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_pendidikan-ijazah');
            Yii::setAlias('@upload-pegawai_pendidikan-ijazah', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_pendidikan-ijazah');
            Yii::setAlias('@download-pegawai_penghargaan-sertifikat', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_penghargaan-sertifikat');
            Yii::setAlias('@upload-pegawai_penghargaan-sertifikat', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_penghargaan-sertifikat');
            Yii::setAlias('@download-pegawai_diklat-sertifikat', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_diklat-sertifikat');
            Yii::setAlias('@upload-pegawai_diklat-sertifikat', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_diklat-sertifikat');
            Yii::setAlias('@download-pegawai_kursus_pelatihan-sertifikat', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_kursus_pelatihan-sertifikat');
            Yii::setAlias('@upload-pegawai_kursus_pelatihan-sertifikat', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_kursus_pelatihan-sertifikat');
            Yii::setAlias('@download-pegawai_organisasi-sertifikat', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_organisasi-sertifikat');
            Yii::setAlias('@upload-pegawai_organisasi-sertifikat', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_organisasi-sertifikat');
            Yii::setAlias('@download-pegawai_hukuman_disiplin-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_hukuman_disiplin-arsip_sk');
            Yii::setAlias('@upload-pegawai_hukuman_disiplin-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_hukuman_disiplin-arsip_sk');
            Yii::setAlias('@download-pegawai_pemberhentian-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_pemberhentian-arsip_sk');
            Yii::setAlias('@upload-pegawai_pemberhentian-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_pemberhentian-arsip_sk');
            Yii::setAlias('@download-pegawai_masa_persiapan_pensiun-arsip_sk', Yii::$app->getRequest()->getBaseUrl() . '/upload/pegawai_masa_persiapan_pensiun-arsip_sk');
            Yii::setAlias('@upload-pegawai_masa_persiapan_pensiun-arsip_sk', dirname(dirname(__DIR__)) . '/app_simka/web/upload/pegawai_masa_persiapan_pensiun-arsip_sk');

            Yii::setAlias('@download-bukti_kehadiran-bukti_utama', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_utama');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_utama', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_utama');
            Yii::setAlias('@download-bukti_kehadiran-bukti_tambahan_1', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_tambahan_1');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_tambahan_1', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_tambahan_1');
            Yii::setAlias('@download-bukti_kehadiran-bukti_tambahan_2', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_tambahan_2');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_tambahan_2', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_tambahan_2');
            Yii::setAlias('@download-bukti_kehadiran-bukti_tambahan_3', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_tambahan_3');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_tambahan_3', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_tambahan_3');
            Yii::setAlias('@download-bukti_kehadiran-bukti_tambahan_4', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_tambahan_4');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_tambahan_4', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_tambahan_4');
            Yii::setAlias('@download-bukti_kehadiran-bukti_tambahan_5', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_kehadiran-bukti_tambahan_5');
            Yii::setAlias('@upload-bukti_kehadiran-bukti_tambahan_5', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_kehadiran-bukti_tambahan_5');

            Yii::setAlias('@download-bukti_cctv-bukti_utama', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_utama');
            Yii::setAlias('@upload-bukti_cctv-bukti_utama', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_utama');
            Yii::setAlias('@download-bukti_cctv-bukti_tambahan_1', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_tambahan_1');
            Yii::setAlias('@upload-bukti_cctv-bukti_tambahan_1', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_tambahan_1');
            Yii::setAlias('@download-bukti_cctv-bukti_tambahan_2', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_tambahan_2');
            Yii::setAlias('@upload-bukti_cctv-bukti_tambahan_2', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_tambahan_2');
            Yii::setAlias('@download-bukti_cctv-bukti_tambahan_3', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_tambahan_3');
            Yii::setAlias('@upload-bukti_cctv-bukti_tambahan_3', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_tambahan_3');
            Yii::setAlias('@download-bukti_cctv-bukti_tambahan_4', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_tambahan_4');
            Yii::setAlias('@upload-bukti_cctv-bukti_tambahan_4', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_tambahan_4');
            Yii::setAlias('@download-bukti_cctv-bukti_tambahan_5', Yii::$app->getRequest()->getBaseUrl() . '/upload/bukti_cctv-bukti_tambahan_5');
            Yii::setAlias('@upload-bukti_cctv-bukti_tambahan_5', dirname(dirname(__DIR__)) . '/app_simka/web/upload/bukti_cctv-bukti_tambahan_5');

            Yii::setAlias('@download-cuti-bukti_utama', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_utama');
            Yii::setAlias('@upload-cuti-bukti_utama', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_utama');
            Yii::setAlias('@download-cuti-bukti_tambahan_1', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_tambahan_1');
            Yii::setAlias('@upload-cuti-bukti_tambahan_1', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_tambahan_1');
            Yii::setAlias('@download-cuti-bukti_tambahan_2', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_tambahan_2');
            Yii::setAlias('@upload-cuti-bukti_tambahan_2', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_tambahan_2');
            Yii::setAlias('@download-cuti-bukti_tambahan_3', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_tambahan_3');
            Yii::setAlias('@upload-cuti-bukti_tambahan_3', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_tambahan_3');
            Yii::setAlias('@download-cuti-bukti_tambahan_4', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_tambahan_4');
            Yii::setAlias('@upload-cuti-bukti_tambahan_4', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_tambahan_4');
            Yii::setAlias('@download-cuti-bukti_tambahan_5', Yii::$app->getRequest()->getBaseUrl() . '/upload/cuti-bukti_tambahan_5');
            Yii::setAlias('@upload-cuti-bukti_tambahan_5', dirname(dirname(__DIR__)) . '/app_simka/web/upload/cuti-bukti_tambahan_5');

            // $result = \Yii::$app->db->createCommand("call assign_kehadiran(:tanggalAwal, :tanggalAkhir);")
            //     ->bindValue(':tanggalAwal', date('Y-m-d', strtotime('-3 days')))
            //     ->bindValue(':tanggalAkhir', date('Y-m-d'))
            //     ->execute();
        },
    ],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_simka',
        ],
        'user' => [
            'identityClass' => 'app_simka\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_simka', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_simka',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;