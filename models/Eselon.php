<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "eselon".
 *
 * @property integer $id
 * @property string $eselon
 *
 * @property JenjangJabatan[] $jenjangJabatans
 * @property JenjangJabatan[] $jenjangJabatans0
 * @property Pegawai[] $pegawais
 */
class Eselon extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'eselon';
    }

    public function rules()
    {
        return [
            //id

            //eselon
            [['eselon'], 'required'],
            [['eselon'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eselon' => 'Eselon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatans()
    {
        return $this->hasMany(JenjangJabatan::className(), ['eselon_tertinggi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatans0()
    {
        return $this->hasMany(JenjangJabatan::className(), ['eselon_terendah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['eselon' => 'id']);
    }
}
