<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\FileInputAsset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);

//
$errorMessage = '';
$errorVue = false;
if ($model['kehadiran']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['kehadiran'], ['class' => '']);
}

$pilihanKeterangan = (new \app_simka\models\Kehadiran())->getEnum('keterangan');
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div class="fs-16 text-azure fs-italic">
                Form Kehadiran
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="margin-top-30"></div>

            <?php if ($errorMessage) : ?>
                <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div>
                <?= $form->field($model['kehadiran'], 'tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['kehadiran']->tanggal ? $model['kehadiran']->tanggal : '<span class="text-grayer">(kosong)</span>' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['kehadiran'], 'tanggal')->end(); ?>

                <?= $form->field($model['kehadiran'], 'jadwal_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'jadwal_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['kehadiran']->jadwal_masuk ? $model['kehadiran']->jadwal_masuk : '<span class="text-grayer">(kosong)</span>' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['kehadiran'], 'jadwal_masuk')->end(); ?>

                <?= $form->field($model['kehadiran'], 'waktu_masuk', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'waktu_masuk', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['kehadiran'], 'waktu_masuk', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['kehadiran'], 'waktu_masuk', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['kehadiran'], 'waktu_masuk')->end(); ?>

                <?= $form->field($model['kehadiran'], 'jadwal_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'jadwal_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['kehadiran']->jadwal_pulang ? $model['kehadiran']->jadwal_pulang : '<span class="text-grayer">(kosong)</span>' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['kehadiran'], 'jadwal_pulang')->end(); ?>

                <?= $form->field($model['kehadiran'], 'waktu_pulang', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'waktu_pulang', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <?= Html::activeTextInput($model['kehadiran'], 'waktu_pulang', ['class' => 'form-text', 'maxlength' => true]); ?>
                        <?= Html::error($model['kehadiran'], 'waktu_pulang', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['kehadiran'], 'waktu_pulang')->end(); ?>

                <?= $form->field($model['kehadiran'], 'keterangan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-1 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['kehadiran'], 'keterangan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-11 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= Html::activeRadioList($model['kehadiran'], 'keterangan', $pilihanKeterangan, ['class' => 'form-radio', 'unselect' => null,
                                'item' => function($index, $label, $name, $checked, $value){
                                    $checked = $checked ? 'checked' : '';
                                    $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                                    return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                                }]); ?>
                            <?= Html::error($model['kehadiran'], 'keterangan', ['class' => 'form-info']); ?>
                        </div>
                    </div>
                <?= $form->field($model['kehadiran'], 'keterangan')->end(); ?>
            </div>

            <div>
                <div class="margin-top-30"></div>

                <hr class="margin-y-10 border-top border-light-orange">
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                    <div class="margin-top-15"></div>
                    <div class="clearfix">
                        <?= Html::a('Kembali ke Daftar Hadir', ['kehadiran', 'id' => $model['kehadiran']->pegawai], ['class' => 'button pull-right m-pull-none m-button-block']) ?>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
