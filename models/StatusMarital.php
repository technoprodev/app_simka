<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "status_marital".
 *
 * @property integer $id
 * @property string $status_marital
 *
 * @property Pegawai[] $pegawais
 */
class StatusMarital extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'status_marital';
    }

    public function rules()
    {
        return [
            //id

            //status_marital
            [['status_marital'], 'required'],
            [['status_marital'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_marital' => 'Status Pernikahan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['status_marital' => 'id']);
    }
}
