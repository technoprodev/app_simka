<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "bentuk_wajah".
 *
 * @property integer $id
 * @property string $bentuk_wajah
 *
 * @property Pegawai[] $pegawais
 */
class BentukWajah extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bentuk_wajah';
    }

    public function rules()
    {
        return [
            //id

            //bentuk_wajah
            [['bentuk_wajah'], 'required'],
            [['bentuk_wajah'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bentuk_wajah' => 'Bentuk Wajah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['bentuk_wajah' => 'id']);
    }
}
