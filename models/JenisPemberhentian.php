<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenis_pemberhentian".
 *
 * @property integer $id
 * @property string $jenis_pemberhentian
 *
 * @property PegawaiPemberhentian[] $pegawaiPemberhentians
 */
class JenisPemberhentian extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_pemberhentian';
    }

    public function rules()
    {
        return [
            //id

            //jenis_pemberhentian
            [['jenis_pemberhentian'], 'required'],
            [['jenis_pemberhentian'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_pemberhentian' => 'Jenis Pemberhentian',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPemberhentians()
    {
        return $this->hasMany(PegawaiPemberhentian::className(), ['jenis_pemberhentian' => 'id']);
    }
}
