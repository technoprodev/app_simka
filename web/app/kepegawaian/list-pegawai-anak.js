$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai-anak');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-pegawai-anak', {idPegawai : idPegawai}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: {
                            _: 'p.nama_anak',
                            display: 'nama_anak',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jenis_kelamin',
                            display: 'jenis_kelamin',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tempat_lahir',
                            display: 'tempat_lahir',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tanggal_lahir',
                            display: 'tanggal_lahir',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jp.jenjang_pendidikan',
                            display: 'jenjang_pendidikan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pk.pekerjaan',
                            display: 'pekerjaan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_anak',
                            display: 'status_anak',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_hidup',
                            display: 'status_hidup',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_tunjangan',
                            display: 'status_tunjangan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.akta_lahir',
                            display: 'akta_lahir',
                        },
                        render: function ( data, type, row ) {
                            if (data) {
                                return '' +
                                    '<a href="' +
                                    baseUrl +
                                    data +
                                    '">Download</a>' +
                                    '';
                            } else {
                                return 'tidak tersedia';
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});