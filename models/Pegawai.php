<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai".
 *
 * @property integer $id
 * @property string $nama
 * @property string $nip
 * @property string $pin
 * @property integer $status_kepegawaian
 * @property integer $kepangkatan
 * @property integer $jenjang_jabatan
 * @property string $nama_jabatan
 * @property integer $unit_kerja
 * @property integer $eselon
 * @property integer $grade
 * @property integer $kedudukan_pegawai
 * @property string $mendapat_tunkin
 * @property string $nama_panggilan
 * @property string $jenis_kelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $agama
 * @property integer $status_marital
 * @property string $golongan_darah
 * @property integer $jenjang_pendidikan_terakhir
 * @property string $gelar_depan
 * @property string $gelar_belakang
 * @property string $telpon
 * @property string $email_kantor
 * @property string $email_pribadi
 * @property string $nik
 * @property string $npwp
 * @property string $karpeg
 * @property string $karis_atau_karsu
 * @property string $taspen
 * @property string $poto
 * @property string $alamat_sekarang
 * @property string $kelurahan_sekarang
 * @property string $kode_pos_sekarang
 * @property string $alamat_ktp
 * @property string $kelurahan_ktp
 * @property string $kode_pos_ktp
 * @property integer $tinggi_badan_cm
 * @property integer $berat_badan_kg
 * @property integer $model_rambut
 * @property integer $bentuk_wajah
 * @property integer $warna_kulit
 * @property string $hobi
 * @property string $kemampuan_berbahasa
 * @property string $nama_pasangan
 * @property string $tempat_lahir_pasangan
 * @property string $tanggal_lahir_pasangan
 * @property integer $agama_pasangan
 * @property integer $jenjang_pendidikan_terakhir_pasangan
 * @property integer $pekerjaan_pasangan
 * @property string $status_hidup_pasangan
 * @property string $telpon_pasangan
 * @property string $nama_ayah
 * @property string $tempat_lahir_ayah
 * @property string $tanggal_lahir_ayah
 * @property integer $agama_ayah
 * @property integer $jenjang_pendidikan_terakhir_ayah
 * @property integer $pekerjaan_ayah
 * @property string $status_hidup_ayah
 * @property string $telpon_ayah
 * @property string $alamat_ayah
 * @property string $kelurahan_ayah
 * @property string $kode_pos_ayah
 * @property string $nama_ibu
 * @property string $tempat_lahir_ibu
 * @property string $tanggal_lahir_ibu
 * @property integer $agama_ibu
 * @property integer $jenjang_pendidikan_terakhir_ibu
 * @property integer $pekerjaan_ibu
 * @property string $status_hidup_ibu
 * @property string $telpon_ibu
 * @property string $alamat_ibu
 * @property string $kelurahan_ibu
 * @property string $kode_pos_ibu
 * @property string $pimpinan_unit_kerja
 * @property integer $unit_kerja_administrasi
 * @property string $tunkin_nama
 * @property string $tunkin_nip
 * @property string $tunkin_nomor_rekening
 * @property integer $cuti_N
 * @property integer $cuti_N_1
 * @property integer $cuti_N_2
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property BuktiCctv[] $buktiCctvs
 * @property BuktiCctv[] $buktiCctvs0
 * @property BuktiCctv[] $buktiCctvs1
 * @property BuktiKehadiran[] $buktiKehadirans
 * @property BuktiKehadiran[] $buktiKehadirans0
 * @property BuktiKehadiran[] $buktiKehadirans1
 * @property BuktiKehadiranPegawai[] $buktiKehadiranPegawais
 * @property Cuti[] $cutis
 * @property Cuti[] $cutis0
 * @property Cuti[] $cutis1
 * @property Cuti[] $cutis2
 * @property JadwalKehadiran[] $jadwalKehadirans
 * @property Kehadiran[] $kehadirans
 * @property User $id0
 * @property ModelRambut $modelRambut
 * @property BentukWajah $bentukWajah
 * @property WarnaKulit $warnaKulit
 * @property Agama $agamaAyah
 * @property JenjangPendidikan $jenjangPendidikanTerakhirAyah
 * @property Pekerjaan $pekerjaanAyah
 * @property Agama $agamaIbu
 * @property StatusKepegawaian $statusKepegawaian
 * @property JenjangPendidikan $jenjangPendidikanTerakhirIbu
 * @property Pekerjaan $pekerjaanIbu
 * @property Agama $agamaPasangan
 * @property JenjangPendidikan $jenjangPendidikanTerakhirPasangan
 * @property Pekerjaan $pekerjaanPasangan
 * @property KedudukanPegawai $kedudukanPegawai
 * @property Eselon $eselon0
 * @property Grade $grade0
 * @property UnitKerja $unitKerjaAdministrasi
 * @property JenjangJabatan $jenjangJabatan
 * @property User $createdBy
 * @property User $updatedBy
 * @property Kepangkatan $kepangkatan0
 * @property UnitKerja $unitKerja
 * @property Agama $agama0
 * @property StatusMarital $statusMarital
 * @property JenjangPendidikan $jenjangPendidikanTerakhir
 * @property PegawaiAnak[] $pegawaiAnaks
 * @property PegawaiDiklat[] $pegawaiDiklats
 * @property PegawaiHukumanDisiplin[] $pegawaiHukumanDisiplins
 * @property PegawaiKenaikanGajiBerkala[] $pegawaiKenaikanGajiBerkalas
 * @property PegawaiKepangkatan[] $pegawaiKepangkatans
 * @property PegawaiKunjunganLuarNegeri[] $pegawaiKunjunganLuarNegeris
 * @property PegawaiKursusPelatihan[] $pegawaiKursusPelatihans
 * @property PegawaiMasaPersiapanPensiun[] $pegawaiMasaPersiapanPensiuns
 * @property PegawaiMutasi[] $pegawaiMutasis
 * @property PegawaiOrganisasi[] $pegawaiOrganisasis
 * @property PegawaiPelaksana[] $pegawaiPelaksanas
 * @property PegawaiPemberhentian[] $pegawaiPemberhentians
 * @property PegawaiPendidikan[] $pegawaiPendidikans
 * @property PegawaiPenetapanAngkaKredit[] $pegawaiPenetapanAngkaKredits
 * @property PegawaiPenghargaan[] $pegawaiPenghargaans
 * @property PegawaiSaudaraKandung[] $pegawaiSaudaraKandungs
 */
class Pegawai extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_poto_upload;
    public $virtual_poto_download;

    public static function tableName()
    {
        return 'pegawai';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'unique'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],

            //nama
            [['nama'], 'string', 'max' => 256],

            //nip
            [['nip'], 'string', 'max' => 256],

            //pin
            [['pin'], 'string', 'max' => 256],
            [['pin'], 'unique'],

            //status_kepegawaian
            [['status_kepegawaian'], 'integer'],
            [['status_kepegawaian'], 'exist', 'skipOnError' => true, 'targetClass' => StatusKepegawaian::className(), 'targetAttribute' => ['status_kepegawaian' => 'id']],

            //kepangkatan
            [['kepangkatan'], 'integer'],
            [['kepangkatan'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan' => 'id']],

            //jenjang_jabatan
            [['jenjang_jabatan'], 'integer'],
            [['jenjang_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangJabatan::className(), 'targetAttribute' => ['jenjang_jabatan' => 'id']],

            //nama_jabatan
            [['nama_jabatan'], 'string', 'max' => 256],

            //unit_kerja
            [['unit_kerja'], 'integer'],
            [['unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['unit_kerja' => 'id']],

            //eselon
            [['eselon'], 'integer'],
            [['eselon'], 'exist', 'skipOnError' => true, 'targetClass' => Eselon::className(), 'targetAttribute' => ['eselon' => 'id']],

            //grade
            [['grade'], 'integer'],
            [['grade'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['grade' => 'id']],

            //kedudukan_pegawai
            [['kedudukan_pegawai'], 'integer'],
            [['kedudukan_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => KedudukanPegawai::className(), 'targetAttribute' => ['kedudukan_pegawai' => 'id']],

            //mendapat_tunkin
            [['mendapat_tunkin'], 'string'],

            //nama_panggilan
            [['nama_panggilan'], 'string', 'max' => 256],

            //jenis_kelamin
            [['jenis_kelamin'], 'string'],

            //tempat_lahir
            [['tempat_lahir'], 'string', 'max' => 256],

            //tanggal_lahir
            [['tanggal_lahir'], 'safe'],

            //agama
            [['agama'], 'integer'],
            [['agama'], 'exist', 'skipOnError' => true, 'targetClass' => Agama::className(), 'targetAttribute' => ['agama' => 'id']],

            //status_marital
            [['status_marital'], 'integer'],
            [['status_marital'], 'exist', 'skipOnError' => true, 'targetClass' => StatusMarital::className(), 'targetAttribute' => ['status_marital' => 'id']],

            //golongan_darah
            [['golongan_darah'], 'string'],

            //jenjang_pendidikan_terakhir
            [['jenjang_pendidikan_terakhir'], 'integer'],
            [['jenjang_pendidikan_terakhir'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan_terakhir' => 'id']],

            //gelar_depan
            [['gelar_depan'], 'string', 'max' => 256],

            //gelar_belakang
            [['gelar_belakang'], 'string', 'max' => 256],

            //telpon
            [['telpon'], 'string', 'max' => 256],

            //email_kantor
            [['email_kantor'], 'string', 'max' => 256],

            //email_pribadi
            [['email_pribadi'], 'string', 'max' => 256],

            //nik
            [['nik'], 'string', 'max' => 256],

            //npwp
            [['npwp'], 'string', 'max' => 256],

            //karpeg
            [['karpeg'], 'string', 'max' => 256],

            //karis_atau_karsu
            [['karis_atau_karsu'], 'string', 'max' => 256],

            //taspen
            [['taspen'], 'string', 'max' => 256],

            //poto
            [['poto'], 'string', 'max' => 256],

            //alamat_sekarang
            [['alamat_sekarang'], 'string'],

            //kelurahan_sekarang
            [['kelurahan_sekarang'], 'string', 'max' => 256],

            //kode_pos_sekarang
            [['kode_pos_sekarang'], 'string', 'max' => 256],

            //alamat_ktp
            [['alamat_ktp'], 'string'],

            //kelurahan_ktp
            [['kelurahan_ktp'], 'string', 'max' => 256],

            //kode_pos_ktp
            [['kode_pos_ktp'], 'string', 'max' => 256],

            //tinggi_badan_cm
            [['tinggi_badan_cm'], 'integer'],

            //berat_badan_kg
            [['berat_badan_kg'], 'integer'],

            //model_rambut
            [['model_rambut'], 'integer'],
            [['model_rambut'], 'exist', 'skipOnError' => true, 'targetClass' => ModelRambut::className(), 'targetAttribute' => ['model_rambut' => 'id']],

            //bentuk_wajah
            [['bentuk_wajah'], 'integer'],
            [['bentuk_wajah'], 'exist', 'skipOnError' => true, 'targetClass' => BentukWajah::className(), 'targetAttribute' => ['bentuk_wajah' => 'id']],

            //warna_kulit
            [['warna_kulit'], 'integer'],
            [['warna_kulit'], 'exist', 'skipOnError' => true, 'targetClass' => WarnaKulit::className(), 'targetAttribute' => ['warna_kulit' => 'id']],

            //hobi
            [['hobi'], 'string', 'max' => 256],

            //kemampuan_berbahasa
            [['kemampuan_berbahasa'], 'string', 'max' => 256],

            //nama_pasangan
            [['nama_pasangan'], 'string', 'max' => 256],

            //tempat_lahir_pasangan
            [['tempat_lahir_pasangan'], 'string', 'max' => 256],

            //tanggal_lahir_pasangan
            [['tanggal_lahir_pasangan'], 'safe'],

            //agama_pasangan
            [['agama_pasangan'], 'integer'],
            [['agama_pasangan'], 'exist', 'skipOnError' => true, 'targetClass' => Agama::className(), 'targetAttribute' => ['agama_pasangan' => 'id']],

            //jenjang_pendidikan_terakhir_pasangan
            [['jenjang_pendidikan_terakhir_pasangan'], 'integer'],
            [['jenjang_pendidikan_terakhir_pasangan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan_terakhir_pasangan' => 'id']],

            //pekerjaan_pasangan
            [['pekerjaan_pasangan'], 'integer'],
            [['pekerjaan_pasangan'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan_pasangan' => 'id']],

            //status_hidup_pasangan
            [['status_hidup_pasangan'], 'string'],

            //telpon_pasangan
            [['telpon_pasangan'], 'string', 'max' => 256],

            //nama_ayah
            [['nama_ayah'], 'string', 'max' => 256],

            //tempat_lahir_ayah
            [['tempat_lahir_ayah'], 'string', 'max' => 256],

            //tanggal_lahir_ayah
            [['tanggal_lahir_ayah'], 'safe'],

            //agama_ayah
            [['agama_ayah'], 'integer'],
            [['agama_ayah'], 'exist', 'skipOnError' => true, 'targetClass' => Agama::className(), 'targetAttribute' => ['agama_ayah' => 'id']],

            //jenjang_pendidikan_terakhir_ayah
            [['jenjang_pendidikan_terakhir_ayah'], 'integer'],
            [['jenjang_pendidikan_terakhir_ayah'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan_terakhir_ayah' => 'id']],

            //pekerjaan_ayah
            [['pekerjaan_ayah'], 'integer'],
            [['pekerjaan_ayah'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan_ayah' => 'id']],

            //status_hidup_ayah
            [['status_hidup_ayah'], 'string'],

            //telpon_ayah
            [['telpon_ayah'], 'string', 'max' => 256],

            //alamat_ayah
            [['alamat_ayah'], 'string', 'max' => 256],

            //kelurahan_ayah
            [['kelurahan_ayah'], 'string', 'max' => 256],

            //kode_pos_ayah
            [['kode_pos_ayah'], 'string', 'max' => 256],

            //nama_ibu
            [['nama_ibu'], 'string', 'max' => 256],

            //tempat_lahir_ibu
            [['tempat_lahir_ibu'], 'string', 'max' => 256],

            //tanggal_lahir_ibu
            [['tanggal_lahir_ibu'], 'safe'],

            //agama_ibu
            [['agama_ibu'], 'integer'],
            [['agama_ibu'], 'exist', 'skipOnError' => true, 'targetClass' => Agama::className(), 'targetAttribute' => ['agama_ibu' => 'id']],

            //jenjang_pendidikan_terakhir_ibu
            [['jenjang_pendidikan_terakhir_ibu'], 'integer'],
            [['jenjang_pendidikan_terakhir_ibu'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan_terakhir_ibu' => 'id']],

            //pekerjaan_ibu
            [['pekerjaan_ibu'], 'integer'],
            [['pekerjaan_ibu'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan_ibu' => 'id']],

            //status_hidup_ibu
            [['status_hidup_ibu'], 'string'],

            //telpon_ibu
            [['telpon_ibu'], 'string', 'max' => 256],

            //alamat_ibu
            [['alamat_ibu'], 'string', 'max' => 256],

            //kelurahan_ibu
            [['kelurahan_ibu'], 'string', 'max' => 256],

            //kode_pos_ibu
            [['kode_pos_ibu'], 'string', 'max' => 256],

            //pimpinan_unit_kerja
            [['pimpinan_unit_kerja'], 'string'],

            //unit_kerja_administrasi
            [['unit_kerja_administrasi'], 'integer'],
            [['unit_kerja_administrasi'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['unit_kerja_administrasi' => 'id']],

            //tunkin_nama
            [['tunkin_nama'], 'string', 'max' => 256],

            //tunkin_nip
            [['tunkin_nip'], 'string', 'max' => 256],

            //tunkin_nomor_rekening
            [['tunkin_nomor_rekening'], 'string', 'max' => 256],

            //cuti_N
            [['cuti_N'], 'integer'],

            //cuti_N_1
            [['cuti_N_1'], 'integer'],

            //cuti_N_2
            [['cuti_N_2'], 'integer'],

            //created_at
            [['created_at'], 'required'],
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //created_by
            [['created_by'], 'required'],
            [['created_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],

            //updated_by
            [['updated_by'], 'integer'],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],

            //custom
            
            //virtual_poto_download
            [['virtual_poto_download'], 'safe'],
            
            //virtual_poto_upload
            [['virtual_poto_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            $this->poto = $this->virtual_poto_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_poto_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_poto_upload');
        if ($this->virtual_poto_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai-poto') . '/' . $this->id . '/' . $this->poto;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->poto = $this->virtual_poto_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_poto_upload) {
            $path = Yii::getAlias('@upload-pegawai-poto') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_poto_upload->saveAs($path . '/' . $this->poto);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai-poto') . '/' . $this->id . '/' . $this->poto;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->poto) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai-poto');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_poto_download = $path . '/' . $this->poto;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'pin' => 'Pin',
            'status_kepegawaian' => 'Status Kepegawaian',
            'kepangkatan' => 'Kepangkatan',
            'jenjang_jabatan' => 'Jenjang Jabatan',
            'nama_jabatan' => 'Nama Jabatan',
            'unit_kerja' => 'Unit Kerja',
            'eselon' => 'Eselon',
            'grade' => 'Grade',
            'kedudukan_pegawai' => 'Kedudukan Pegawai',
            'mendapat_tunkin' => 'Mendapat Tunkin',
            'nama_panggilan' => 'Nama Panggilan',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'agama' => 'Agama',
            'status_marital' => 'Status Pernikahan',
            'golongan_darah' => 'Golongan Darah',
            'jenjang_pendidikan_terakhir' => 'Jenjang Pendidikan Terakhir',
            'gelar_depan' => 'Gelar Depan',
            'gelar_belakang' => 'Gelar Belakang',
            'telpon' => 'Telpon',
            'email_kantor' => 'Email Kantor',
            'email_pribadi' => 'Email Pribadi',
            'nik' => 'Nik',
            'npwp' => 'Npwp',
            'karpeg' => 'Karpeg',
            'karis_atau_karsu' => 'Karis Atau Karsu',
            'taspen' => 'Taspen',
            'poto' => 'Poto',
            'alamat_sekarang' => 'Alamat Sekarang',
            'kelurahan_sekarang' => 'Kelurahan Sekarang',
            'kode_pos_sekarang' => 'Kode Pos Sekarang',
            'alamat_ktp' => 'Alamat Ktp',
            'kelurahan_ktp' => 'Kelurahan Ktp',
            'kode_pos_ktp' => 'Kode Pos Ktp',
            'tinggi_badan_cm' => 'Tinggi Badan Cm',
            'berat_badan_kg' => 'Berat Badan Kg',
            'model_rambut' => 'Model Rambut',
            'bentuk_wajah' => 'Bentuk Wajah',
            'warna_kulit' => 'Warna Kulit',
            'hobi' => 'Hobi',
            'kemampuan_berbahasa' => 'Kemampuan Berbahasa',
            'nama_pasangan' => 'Nama ' . $this->pasangan,
            'tempat_lahir_pasangan' => 'Tempat Lahir ' . $this->pasangan,
            'tanggal_lahir_pasangan' => 'Tanggal Lahir ' . $this->pasangan,
            'agama_pasangan' => 'Agama ' . $this->pasangan,
            'jenjang_pendidikan_terakhir_pasangan' => 'Jenjang Pendidikan Terakhir ' . $this->pasangan,
            'pekerjaan_pasangan' => 'Pekerjaan ' . $this->pasangan,
            'status_hidup_pasangan' => 'Status Hidup ' . $this->pasangan,
            'telpon_pasangan' => 'Telpon ' . $this->pasangan,
            'nama_ayah' => 'Nama Ayah',
            'tempat_lahir_ayah' => 'Tempat Lahir Ayah',
            'tanggal_lahir_ayah' => 'Tanggal Lahir Ayah',
            'agama_ayah' => 'Agama Ayah',
            'jenjang_pendidikan_terakhir_ayah' => 'Jenjang Pendidikan Terakhir Ayah',
            'pekerjaan_ayah' => 'Pekerjaan Ayah',
            'status_hidup_ayah' => 'Status Hidup Ayah',
            'telpon_ayah' => 'Telpon Ayah',
            'alamat_ayah' => 'Alamat Ayah',
            'kelurahan_ayah' => 'Kelurahan Ayah',
            'kode_pos_ayah' => 'Kode Pos Ayah',
            'nama_ibu' => 'Nama Ibu',
            'tempat_lahir_ibu' => 'Tempat Lahir Ibu',
            'tanggal_lahir_ibu' => 'Tanggal Lahir Ibu',
            'agama_ibu' => 'Agama Ibu',
            'jenjang_pendidikan_terakhir_ibu' => 'Jenjang Pendidikan Terakhir Ibu',
            'pekerjaan_ibu' => 'Pekerjaan Ibu',
            'status_hidup_ibu' => 'Status Hidup Ibu',
            'telpon_ibu' => 'Telpon Ibu',
            'alamat_ibu' => 'Alamat Ibu',
            'kelurahan_ibu' => 'Kelurahan Ibu',
            'kode_pos_ibu' => 'Kode Pos Ibu',
            'pimpinan_unit_kerja' => 'Pimpinan Unit Kerja',
            'unit_kerja_administrasi' => 'Unit Kerja Administrasi',
            'tunkin_nama' => 'Tunkin Nama',
            'tunkin_nip' => 'Tunkin Nip',
            'tunkin_nomor_rekening' => 'Tunkin Nomor Rekening',
            'cuti_N' => 'Cuti N',
            'cuti_N_1' => 'Cuti N 1',
            'cuti_N_2' => 'Cuti N 2',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiCctvs()
    {
        return $this->hasMany(BuktiCctv::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiCctvs0()
    {
        return $this->hasMany(BuktiCctv::className(), ['diajukan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiCctvs1()
    {
        return $this->hasMany(BuktiCctv::className(), ['disetujui' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadirans()
    {
        return $this->hasMany(BuktiKehadiran::className(), ['diajukan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadirans0()
    {
        return $this->hasMany(BuktiKehadiran::className(), ['disetujui' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadirans1()
    {
        return $this->hasMany(BuktiKehadiran::className(), ['disetujui_atasan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadiranPegawais()
    {
        return $this->hasMany(BuktiKehadiranPegawai::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutis()
    {
        return $this->hasMany(Cuti::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutis0()
    {
        return $this->hasMany(Cuti::className(), ['atasan_1' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutis1()
    {
        return $this->hasMany(Cuti::className(), ['atasan_2' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCutis2()
    {
        return $this->hasMany(Cuti::className(), ['disetujui' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwalKehadirans()
    {
        return $this->hasMany(JadwalKehadiran::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKehadirans()
    {
        return $this->hasMany(Kehadiran::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModelRambut()
    {
        return $this->hasOne(ModelRambut::className(), ['id' => 'model_rambut']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBentukWajah()
    {
        return $this->hasOne(BentukWajah::className(), ['id' => 'bentuk_wajah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWarnaKulit()
    {
        return $this->hasOne(WarnaKulit::className(), ['id' => 'warna_kulit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgamaAyah()
    {
        return $this->hasOne(Agama::className(), ['id' => 'agama_ayah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikanTerakhirAyah()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan_terakhir_ayah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanAyah()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan_ayah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgamaIbu()
    {
        return $this->hasOne(Agama::className(), ['id' => 'agama_ibu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusKepegawaian()
    {
        return $this->hasOne(StatusKepegawaian::className(), ['id' => 'status_kepegawaian']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikanTerakhirIbu()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan_terakhir_ibu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanIbu()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan_ibu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgamaPasangan()
    {
        return $this->hasOne(Agama::className(), ['id' => 'agama_pasangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikanTerakhirPasangan()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan_terakhir_pasangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanPasangan()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan_pasangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKedudukanPegawai()
    {
        return $this->hasOne(KedudukanPegawai::className(), ['id' => 'kedudukan_pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEselon0()
    {
        return $this->hasOne(Eselon::className(), ['id' => 'eselon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade0()
    {
        return $this->hasOne(Grade::className(), ['id' => 'grade']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjaAdministrasi()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'unit_kerja_administrasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatan()
    {
        return $this->hasOne(JenjangJabatan::className(), ['id' => 'jenjang_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatan0()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgama0()
    {
        return $this->hasOne(Agama::className(), ['id' => 'agama']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusMarital()
    {
        return $this->hasOne(StatusMarital::className(), ['id' => 'status_marital']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikanTerakhir()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan_terakhir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiAnaks()
    {
        return $this->hasMany(PegawaiAnak::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiDiklats()
    {
        return $this->hasMany(PegawaiDiklat::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiHukumanDisiplins()
    {
        return $this->hasMany(PegawaiHukumanDisiplin::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKenaikanGajiBerkalas()
    {
        return $this->hasMany(PegawaiKenaikanGajiBerkala::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKepangkatans()
    {
        return $this->hasMany(PegawaiKepangkatan::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKunjunganLuarNegeris()
    {
        return $this->hasMany(PegawaiKunjunganLuarNegeri::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKursusPelatihans()
    {
        return $this->hasMany(PegawaiKursusPelatihan::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMasaPersiapanPensiuns()
    {
        return $this->hasMany(PegawaiMasaPersiapanPensiun::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMutasis()
    {
        return $this->hasMany(PegawaiMutasi::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiOrganisasis()
    {
        return $this->hasMany(PegawaiOrganisasi::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPelaksanas()
    {
        return $this->hasMany(PegawaiPelaksana::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPemberhentians()
    {
        return $this->hasMany(PegawaiPemberhentian::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPendidikans()
    {
        return $this->hasMany(PegawaiPendidikan::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPenetapanAngkaKredits()
    {
        return $this->hasMany(PegawaiPenetapanAngkaKredit::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPenghargaans()
    {
        return $this->hasMany(PegawaiPenghargaan::className(), ['pegawai' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiSaudaraKandungs()
    {
        return $this->hasMany(PegawaiSaudaraKandung::className(), ['pegawai' => 'id']);
    }

    public function getPasangan()
    {
        if ($this->jenis_kelamin == 'Laki-laki') {
            return 'Istri';
        } elseif ($this->jenis_kelamin == 'Perempuan') {
            return 'Suami';
        } else {
            return 'Suami / Istri';
        }
    }
}
