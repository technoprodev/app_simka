<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\Login;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionRedirectSlash($url = '')
    {
        $newUrl = rtrim($url, '/');
        if ($newUrl) $newUrl = '/' . $newUrl;
        return $this->redirect(Yii::$app->getRequest()->getBaseUrl() . $newUrl, 301);
    }
    
    //

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['pegawai'])) {
            return $this->redirect(['pegawai/index']);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['kepegawaian'])) {
            return $this->redirect(['kepegawaian/pegawai']);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['pejabat'])) {
            return $this->redirect(['pejabat/pegawai']);
        }
        throw new \yii\web\HttpException(403, 'User Anda belum di set rolenya oleh Admin. Harap hubungi Admin agar role user Anda di set terlebih dahulu.');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest)
            return $this->goHome();

        $model['login'] = new Login(['scenario' => 'using-login']);
        
        if ($model['login']->load(Yii::$app->request->post()) && $model['login']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'empty';
            return $this->render('login', [
                'model' => $model,
                'title' => 'Login',
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}