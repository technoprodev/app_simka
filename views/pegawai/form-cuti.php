<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FlatpickrAsset::register($this);
technosmart\assets_manager\Select2Asset::register($this);

$pilihanKehadiran = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kh.id, CONCAT(kh.kode, ' - ', kh.keterangan) as keterangan FROM keterangan_kehadiran kh
            where kh.diajukan_pegawai_online = 'Ya' order by kh.kode
        ", []
    )->queryAll(), 'id', 'keterangan'
);

$pilihanPegawai = [];
if (true || $unitKerjaAdministrasi = Yii::$app->user->identity->pegawai->unit_kerja_administrasi) {
    /*$result = (new \yii\db\Query())->select('get_pegawai_rec(' . Yii::$app->user->identity->pegawai->unit_kerja_administrasi . ')')->scalar();
    $pegawais = $result ? explode(',', $result) : null;*/

    $query = new \yii\db\Query();
    $query
        ->select([
            'p.id',
            'CONCAT(p.nama, \' - \', IFNULL(p.nip, \'\')) AS nama',
        ])
        ->from('pegawai p')
        /*->where([
            'p.id' => $pegawais,
        ])*/
    ;

    $pilihanPegawai = ArrayHelper::map($query->all(), 'id', 'nama');
}

//
$errorMessage = '';
if ($model['cuti']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['cuti'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Verifikasi Cuti Online
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['cuti'], 'keterangan_kehadiran', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'keterangan_kehadiran', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['cuti'], 'keterangan_kehadiran', $pilihanKehadiran, ['prompt' => '- pilih jenis bukti -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['cuti'], 'keterangan_kehadiran', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'keterangan_kehadiran')->end(); ?>

                <?= $form->field($model['cuti'], 'pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'pegawai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nama : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['cuti'], 'pegawai')->end(); ?>

                <?= $form->field($model['cuti'], 'alasan_cuti', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'alasan_cuti', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['cuti'], 'alasan_cuti', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['cuti'], 'alasan_cuti', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'alasan_cuti')->end(); ?>

                <?= $form->field($model['cuti'], 'alamat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'alamat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['cuti'], 'alamat', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['cuti'], 'alamat', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'alamat')->end(); ?>

                <?= $form->field($model['cuti'], 'telp', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'telp', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['cuti'], 'telp', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['cuti'], 'telp', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'telp')->end(); ?>

                <?= $form->field($model['cuti'], 'catatan_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['cuti'], 'catatan_pengajuan', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['cuti'], 'catatan_pengajuan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'catatan_pengajuan')->end(); ?>

                <?= $form->field($model['cuti'], 'atasan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'atasan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['cuti'], 'atasan_1', $pilihanPegawai, ['prompt' => '- pilih atasan 1 -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['cuti'], 'atasan_1', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'atasan_1')->end(); ?>

                <?= $form->field($model['cuti'], 'atasan_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'atasan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['cuti'], 'atasan_2', $pilihanPegawai, ['prompt' => '- pilih atasan 2 -', 'class' => 'form-dropdown select2']); ?>
                        <?= Html::error($model['cuti'], 'atasan_2', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'atasan_2')->end(); ?>

                <?= $form->field($model['cuti'], 'dari_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['cuti'], 'dari_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['cuti'], 'dari_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'dari_tanggal')->end(); ?>

                <?= $form->field($model['cuti'], 'sampai_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['cuti'], 'sampai_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['cuti'], 'sampai_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'sampai_tanggal')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_utama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_utama_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_utama', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_utama')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_tambahan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_tambahan_1_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_tambahan_1', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_tambahan_1')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_tambahan_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_tambahan_2_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_tambahan_2', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_tambahan_2')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_tambahan_3', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_tambahan_3_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_tambahan_3', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_tambahan_3')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_tambahan_4', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_tambahan_4_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_tambahan_4', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_tambahan_4')->end(); ?>

                <?= $form->field($model['cuti'], 'bukti_tambahan_5', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['cuti'], 'virtual_bukti_tambahan_5_upload'); ?>
                        <?= Html::error($model['cuti'], 'bukti_tambahan_5', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'bukti_tambahan_5')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
