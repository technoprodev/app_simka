if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            pegawai: {
                virtual_category: [],
                pegawaiAnaks: [],
                pegawaiDiklats: [],
                pegawaiHukumanDisiplins: [],
                pegawaiKenaikanGajiBerkalas: [],
                pegawaiKepangkatans: [],
                pegawaiKunjunganLuarNegeris: [],
                pegawaiKursusPelatihans: [],
                pegawaiMasaPersiapanPensiuns: [],
                pegawaiMutasis: [],
                pegawaiOrganisasis: [],
                pegawaiPelaksanas: [],
                pegawaiPemberhentians: [],
                pegawaiPendidikans: [],
                pegawaiPenetapanAngkaKredits: [],
                pegawaiPenghargaans: [],
                pegawaiSaudaraKandungs: [],
            },
        },
        methods: {
            addAnak: function() {
                this.pegawai.pegawaiAnaks.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeAnak: function(i, isNew) {
                if (this.pegawai.pegawaiAnaks[i].id == null)
                    this.pegawai.pegawaiAnaks.splice(i, 1);
                else
                    this.pegawai.pegawaiAnaks[i].id*=-1;
            },

            addDiklat: function() {
                this.pegawai.pegawaiDiklats.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeDiklat: function(i, isNew) {
                if (this.pegawai.pegawaiDiklats[i].id == null)
                    this.pegawai.pegawaiDiklats.splice(i, 1);
                else
                    this.pegawai.pegawaiDiklats[i].id*=-1;
            },

            addHukumanDisiplin: function() {
                this.pegawai.pegawaiHukumanDisiplins.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeHukumanDisiplin: function(i, isNew) {
                if (this.pegawai.pegawaiHukumanDisiplins[i].id == null)
                    this.pegawai.pegawaiHukumanDisiplins.splice(i, 1);
                else
                    this.pegawai.pegawaiHukumanDisiplins[i].id*=-1;
            },

            addKenaikanGajiBerkala: function() {
                this.pegawai.pegawaiKenaikanGajiBerkalas.push({
                    id: null,
                    id_pegawai: null,
                    gaji_pokok_lama: null,
                    gaji_pokok_baru: null,
                    terhitung_mulai_tanggal: null,
                    terhitung_sampai_tanggal: null,
                    terhitung_sampai_sekarang: null,
                    kepangkatan_saat_kenaikan: '',
                    catatan: null,
                    jenis_sk: '',
                    nomor_sk: null,
                    tanggal_sk: null,
                    pejabat_penetap_sk: null,
                    arsip_sk: null,
                });
            },
            removeKenaikanGajiBerkala: function(i, isNew) {
                if (this.pegawai.pegawaiKenaikanGajiBerkalas[i].id == null)
                    this.pegawai.pegawaiKenaikanGajiBerkalas.splice(i, 1);
                else
                    this.pegawai.pegawaiKenaikanGajiBerkalas[i].id*=-1;
            },

            addKepangkatan: function() {
                this.pegawai.pegawaiKepangkatans.push({
                    id: null,
                    id_pegawai: null,
                    kepangkatan: '',
                    status: '',
                    terhitung_mulai_tanggal: null,
                    terhitung_sampai_tanggal: null,
                    jenis_sk: '',
                    nomor_sk: null,
                    tanggal_sk: null,
                    pejabat_penetap_sk: null,
                    arsip_sk: null,
                });
            },
            removeKepangkatan: function(i, isNew) {
                if (this.pegawai.pegawaiKepangkatans[i].id == null)
                    this.pegawai.pegawaiKepangkatans.splice(i, 1);
                else
                    this.pegawai.pegawaiKepangkatans[i].id*=-1;
            },

            addKunjunganLuarNegeri: function() {
                this.pegawai.pegawaiKunjunganLuarNegeris.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeKunjunganLuarNegeri: function(i, isNew) {
                if (this.pegawai.pegawaiKunjunganLuarNegeris[i].id == null)
                    this.pegawai.pegawaiKunjunganLuarNegeris.splice(i, 1);
                else
                    this.pegawai.pegawaiKunjunganLuarNegeris[i].id*=-1;
            },

            addKursusPelatihan: function() {
                this.pegawai.pegawaiKursusPelatihans.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeKursusPelatihan: function(i, isNew) {
                if (this.pegawai.pegawaiKursusPelatihans[i].id == null)
                    this.pegawai.pegawaiKursusPelatihans.splice(i, 1);
                else
                    this.pegawai.pegawaiKursusPelatihans[i].id*=-1;
            },

            addMasaPersiapanPensiun: function() {
                this.pegawai.pegawaiMasaPersiapanPensiuns.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeMasaPersiapanPensiun: function(i, isNew) {
                if (this.pegawai.pegawaiMasaPersiapanPensiuns[i].id == null)
                    this.pegawai.pegawaiMasaPersiapanPensiuns.splice(i, 1);
                else
                    this.pegawai.pegawaiMasaPersiapanPensiuns[i].id*=-1;
            },

            addMutasi: function() {
                this.pegawai.pegawaiMutasis.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeMutasi: function(i, isNew) {
                if (this.pegawai.pegawaiMutasis[i].id == null)
                    this.pegawai.pegawaiMutasis.splice(i, 1);
                else
                    this.pegawai.pegawaiMutasis[i].id*=-1;
            },

            addOrganisasi: function() {
                this.pegawai.pegawaiOrganisasis.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeOrganisasi: function(i, isNew) {
                if (this.pegawai.pegawaiOrganisasis[i].id == null)
                    this.pegawai.pegawaiOrganisasis.splice(i, 1);
                else
                    this.pegawai.pegawaiOrganisasis[i].id*=-1;
            },

            addPelaksana: function() {
                this.pegawai.pegawaiPelaksanas.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removePelaksana: function(i, isNew) {
                if (this.pegawai.pegawaiPelaksanas[i].id == null)
                    this.pegawai.pegawaiPelaksanas.splice(i, 1);
                else
                    this.pegawai.pegawaiPelaksanas[i].id*=-1;
            },

            addPemberhentian: function() {
                this.pegawai.pegawaiPemberhentians.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removePemberhentian: function(i, isNew) {
                if (this.pegawai.pegawaiPemberhentians[i].id == null)
                    this.pegawai.pegawaiPemberhentians.splice(i, 1);
                else
                    this.pegawai.pegawaiPemberhentians[i].id*=-1;
            },

            addPendidikan: function() {
                this.pegawai.pegawaiPendidikans.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removePendidikan: function(i, isNew) {
                if (this.pegawai.pegawaiPendidikans[i].id == null)
                    this.pegawai.pegawaiPendidikans.splice(i, 1);
                else
                    this.pegawai.pegawaiPendidikans[i].id*=-1;
            },

            addPenetapanAngkaKredit: function() {
                this.pegawai.pegawaiPenetapanAngkaKredits.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removePenetapanAngkaKredit: function(i, isNew) {
                if (this.pegawai.pegawaiPenetapanAngkaKredits[i].id == null)
                    this.pegawai.pegawaiPenetapanAngkaKredits.splice(i, 1);
                else
                    this.pegawai.pegawaiPenetapanAngkaKredits[i].id*=-1;
            },

            addPenghargaan: function() {
                this.pegawai.pegawaiPenghargaans.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removePenghargaan: function(i, isNew) {
                if (this.pegawai.pegawaiPenghargaans[i].id == null)
                    this.pegawai.pegawaiPenghargaans.splice(i, 1);
                else
                    this.pegawai.pegawaiPenghargaans[i].id*=-1;
            },

            addSaudaraKandung: function() {
                this.pegawai.pegawaiSaudaraKandungs.push({
                    id: null,
                    id_pegawai: null,
                });
            },
            removeSaudaraKandung: function(i, isNew) {
                if (this.pegawai.pegawaiSaudaraKandungs[i].id == null)
                    this.pegawai.pegawaiSaudaraKandungs.splice(i, 1);
                else
                    this.pegawai.pegawaiSaudaraKandungs[i].id*=-1;
            },
        },
    });
}