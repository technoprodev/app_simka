<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_mutasi".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $instansi
 * @property string $unit_kerja
 * @property integer $jenjang_jabatan
 * @property string $nama_jabatan
 * @property string $terhitung_mulai_tanggal
 * @property string $terhitung_sampai_tanggal
 * @property string $terhitung_sampai_sekarang
 * @property string $catatan
 * @property integer $jenis_sk
 * @property string $nomor_sk
 * @property string $tanggal_sk
 * @property string $pejabat_penetap_sk
 * @property string $arsip_sk
 *
 * @property JenjangJabatan $jenjangJabatan
 * @property Pegawai $pegawai0
 * @property JenjangJabatan $jenjangJabatan0
 * @property JenisSk $jenisSk
 */
class PegawaiMutasi extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_arsip_sk_upload;
    public $virtual_arsip_sk_download;

    public static function tableName()
    {
        return 'pegawai_mutasi';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //instansi
            [['instansi'], 'required'],
            [['instansi'], 'string', 'max' => 256],

            //unit_kerja
            [['unit_kerja'], 'required'],
            [['unit_kerja'], 'string', 'max' => 256],

            //jenjang_jabatan
            [['jenjang_jabatan'], 'required'],
            [['jenjang_jabatan'], 'integer'],
            [['jenjang_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangJabatan::className(), 'targetAttribute' => ['jenjang_jabatan' => 'id']],
            [['jenjang_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangJabatan::className(), 'targetAttribute' => ['jenjang_jabatan' => 'id']],

            //nama_jabatan
            [['nama_jabatan'], 'required'],
            [['nama_jabatan'], 'string', 'max' => 256],

            //terhitung_mulai_tanggal
            [['terhitung_mulai_tanggal'], 'required'],
            [['terhitung_mulai_tanggal'], 'safe'],

            //terhitung_sampai_tanggal
            [['terhitung_sampai_tanggal'], 'safe'],

            //terhitung_sampai_sekarang
            [['terhitung_sampai_sekarang'], 'string'],

            //catatan
            [['catatan'], 'string', 'max' => 256],

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'integer'],
            [['jenis_sk'], 'exist', 'skipOnError' => true, 'targetClass' => JenisSk::className(), 'targetAttribute' => ['jenis_sk' => 'id']],

            //nomor_sk
            [['nomor_sk'], 'required'],
            [['nomor_sk'], 'string', 'max' => 256],

            //tanggal_sk
            [['tanggal_sk'], 'required'],
            [['tanggal_sk'], 'safe'],

            //pejabat_penetap_sk
            [['pejabat_penetap_sk'], 'required'],
            [['pejabat_penetap_sk'], 'string', 'max' => 256],

            //arsip_sk
            // [['arsip_sk'], 'required'],
            [['arsip_sk'], 'string', 'max' => 256],
            
            //virtual_arsip_sk_download
            [['virtual_arsip_sk_download'], 'safe'],
            
            //virtual_arsip_sk_upload
            [['virtual_arsip_sk_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_arsip_sk_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_sk_upload");
        if ($this->virtual_arsip_sk_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_mutasi-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->arsip_sk = $this->virtual_arsip_sk_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_arsip_sk_upload) {
            $path = Yii::getAlias('@upload-pegawai_mutasi-arsip_sk') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_arsip_sk_upload->saveAs($path . '/' . $this->arsip_sk);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_mutasi-arsip_sk') . '/' . $this->id . '/' . $this->arsip_sk;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->arsip_sk) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_mutasi-arsip_sk');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_arsip_sk_download = $path . '/' . $this->arsip_sk;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'instansi' => 'Instansi',
            'unit_kerja' => 'Unit Kerja',
            'jenjang_jabatan' => 'Jenjang Jabatan',
            'nama_jabatan' => 'Nama Jabatan',
            'terhitung_mulai_tanggal' => 'Terhitung Mulai Tanggal',
            'terhitung_sampai_tanggal' => 'Terhitung Sampai Tanggal',
            'terhitung_sampai_sekarang' => 'Terhitung Sampai Sekarang',
            'catatan' => 'Catatan',
            'jenis_sk' => 'Jenis Sk',
            'nomor_sk' => 'Nomor Sk',
            'tanggal_sk' => 'Tanggal Sk',
            'pejabat_penetap_sk' => 'Pejabat Penetap Sk',
            'arsip_sk' => 'Arsip Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatan()
    {
        return $this->hasOne(JenjangJabatan::className(), ['id' => 'jenjang_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatan0()
    {
        return $this->hasOne(JenjangJabatan::className(), ['id' => 'jenjang_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisSk()
    {
        return $this->hasOne(JenisSk::className(), ['id' => 'jenis_sk']);
    }
}
