<?php
namespace app_simka\models;

use Yii;
use app_simka\models\User;
use yii\base\InvalidArgumentException;

class ResetPassword extends \yii\base\Model
{
    public $password;

    private $_user;

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['password'], 'required'],
            [['password'], 'string', 'min' => 6],
            [['password'], 'match', 'pattern' => '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/i', 'message' => 'Password setidaknya memiliki 1 huruf besar, 1 huruf kecil, 1 angka, 1 simbol dan minimal terdiri dari 6 karakter.'],
        ];
    }

    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        return $user->save(false);
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Password Baru',
        ];
    }
}