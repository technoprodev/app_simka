<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/list-pegawai-kepangkatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-kenaikan-gaji-berkala.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-jabatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-pelaksana.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-penetapan-angka-kredit.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-orang-tua.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-saudara-kandung.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-pasangan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-anak.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-pendidikan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-penghargaan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-diklat.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-kursus-pelatihan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-kunjungan-luar-negeri.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-organisasi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-hukuman-disiplin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-pemberhentian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-pegawai-masa-persiapan-pensiun.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$this->registerJs(
    'var idPegawai = ' . json_encode($model['pegawai']->id) . ';' .
    '',
    3
);

$bulans = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', ];
$tahuns = ['2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024', ];
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-2 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="text-center">
            <?php if ($model['pegawai']->poto) : ?>
                <img src="<?= $model['pegawai']->virtual_poto_download ?>" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Laki-laki') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/male.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Perempuan') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/female.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php else : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/avatar.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php endif; ?>
        </div>
        <div class="margin-top-5"></div>
        <div class="text-center">
            <span class="text-azure fs-19 fw-bold"><?= $model['pegawai']->nama ? $model['pegawai']->nama : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">NIP</span><br>
            <span class="text-dark fs-14"><?= $model['pegawai']->nip ? $model['pegawai']->nip : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Telpon</span><br>
            <span class="text-dark"><?= $model['pegawai']->telpon ? $model['pegawai']->telpon : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Kantor</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_kantor ? $model['pegawai']->email_kantor : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Pribadi</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_pribadi ? $model['pegawai']->email_pribadi : '(belum diisi)' ?></span>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Personal
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama ? $model['pegawai']->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Panggilan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_panggilan ? $model['pegawai']->nama_panggilan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenis Kelamin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenis_kelamin ? $model['pegawai']->jenis_kelamin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">TTL</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->tempat_lahir ? $model['pegawai']->tempat_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>, <?= $model['pegawai']->tanggal_lahir ? $model['pegawai']->tanggal_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Agama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->agama ? $model['pegawai']->agama0->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Status Pernikahan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->status_marital ? $model['pegawai']->statusMarital->status_marital : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Golongan Darah</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->golongan_darah ? $model['pegawai']->golongan_darah : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Pegawai
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NIP</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nip ? $model['pegawai']->nip : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Pin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->pin ? $model['pegawai']->pin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kepangkatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenjang Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php
                $bottomToTop = [];
                $uk = $model['pegawai']->unitKerja;
                $i = 0;
                while ($uk && $i <= 10) {
                    $bottomToTop[] = [
                        'jenisUnitKerja' => $uk->jenis_unit_kerja ? $uk->jenisUnitKerja->jenis_unit_kerja : '',
                        'unitKerja' => $uk->unit_kerja,
                    ];
                    $uk = $uk->parent0;
                    $i++;
                }

                $topToBottom = [];
                for ($i = count($bottomToTop)-1; $i >= 0; $i--) { 
                    $topToBottom[] = $bottomToTop[$i];
                }
            ?>
            <?php foreach ($topToBottom as $key => $value) : ?>
            <?php if ($value['jenisUnitKerja'] != 'Staf') :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer"><?= $value['jenisUnitKerja'] ?></div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $value['unitKerja'] ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php if (!$topToBottom) :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Satuan Organisasi</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <span class="text-grayer">(belum diisi)</span>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Eselon</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->eselon ? $model['pegawai']->eselon0->eselon : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Grade</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->grade ? $model['pegawai']->grade0->grade : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kedudukan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kedudukan_pegawai ? $model['pegawai']->kedudukanPegawai->kedudukan_pegawai : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Mendapat Tunkin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->mendapat_tunkin ? $model['pegawai']->mendapat_tunkin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
                    
        <div class="margin-top-30"></div>

        <div>
            <hr class="margin-y-10 border-top border-light-orange">
            <?= Html::a('Print DRH', ['drh-print'], ['class' => 'button button-block button-md rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Rekapitulasi Kehadiran Bulan <?= $bulans[$model['filter_kehadiran']->bulan] ?>
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="box box-break-sm box-gutter">
                <div class="box-6">
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Hari Kerja : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['hari_kerja'] ?> Hari
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Hadir : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['hadir'] ?> Hari
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <?php if (false) : ?>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Tidak Hadir : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['tidak_hadir'] ?> Hari
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Belum Absen : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['belum_absen'] ?> Hari
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Hari Libur : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['libur'] ?> Hari
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                </div>
                <div class="box-6">
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Grade <?= $model['pegawai']->grade ? $model['pegawai']->grade0->grade : '' ?> : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?php $nominalTunkin = $model['pegawai']->grade && $model['pegawai']->status_kepegawaian ? $model['pegawai']->grade0->nominal_tunkin * $model['pegawai']->statusKepegawaian->pajak_tunkin / 100 : 0 ?>
                                <?= $model['pegawai']->grade ? 'Rp ' . number_format($nominalTunkin, 2) : '<span class="text-grayer">(belum diisi)</span>'; ?>
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Total Potongan Tunkin : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= $model['rekapitulasi_kehadiran_bulanan']['potongan_tunkin'] ?> % (Rp <?= number_format($model['pegawai']->grade ? $nominalTunkin * $model['rekapitulasi_kehadiran_bulanan']['potongan_tunkin'] / 100 : '0', 2) . ')'; ?>
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Total Perolehan Tunkin : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= 'Rp ' . number_format($model['pegawai']->grade ? $nominalTunkin * ((100-$model['rekapitulasi_kehadiran_bulanan']['potongan_tunkin']) / 100) : 0, 2); ?>
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Total Uang Makan : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?= 'Rp ' . number_format($model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->nominal_uang_makan * $model['rekapitulasi_kehadiran_bulanan']['uang_makan'] : 0, 2); ?>
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Kekurangan Jam : </div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?php
                                    $hours = floor($model['rekapitulasi_kehadiran_bulanan']['disiplin'] / 3600);
                                    $mins = floor($model['rekapitulasi_kehadiran_bulanan']['disiplin'] / 60 % 60);
                                    $secs = floor($model['rekapitulasi_kehadiran_bulanan']['disiplin'] % 60);
                                ?>
                                <?= sprintf('%02d:%02d', $hours, $mins) ?> Jam
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                    <div class="box box-break-sm">
                        <div class="box-3 padding-x-0 padding-y-5">
                            <div class="text-grayer">Akumulasi Kekurangan Jam :</div>
                        </div>
                        <div class="box-9 m-padding-x-0">
                            <div class="padding-y-5 text-dark">
                                <?php
                                    $hours = floor($model['rekapitulasi_kehadiran_tahunan']['disiplin'] / 3600);
                                    $mins = floor($model['rekapitulasi_kehadiran_tahunan']['disiplin'] / 60 % 60);
                                    $secs = floor($model['rekapitulasi_kehadiran_tahunan']['disiplin'] % 60);
                                ?>
                                <?= sprintf('%02d:%02d', $hours, $mins) ?> Jam
                            </div>
                            <hr class="margin-y-5 border-top border-light-azure">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>