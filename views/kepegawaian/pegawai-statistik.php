<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\ChartAsset::register($this);

$border = ['#663333', '#664d33', '#666633', '#4d6633', '#336633', '#33664d', '#336666', '#334d66', '#333366', '#4d3366', '#663366', '#66334d'];
$background = ['#e6b8b8', '#e6cfb8', '#e6e6b8', '#cfe6b8', '#b8e6b8', '#b8e6cf', '#b8e6e6', '#b8cfe6', '#b8b8e6', '#cfb8e6', '#e6b8e6', '#e6b8cf'];

$pies['statusKepegawaian'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['sk.status_kepegawaian AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'status_kepegawaian sk', 'sk.id = p.status_kepegawaian')
        ->groupBy(['sk.status_kepegawaian'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['kepangkatan'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['CONCAT(k.pangkat, " (", k.golongan, " / ", k.ruang, ")") AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan')
        ->groupBy(['k.pangkat', 'k.golongan', 'k.ruang'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['jenjangJabatan'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['jj.jenjang_jabatan AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
        ->groupBy(['jj.jenjang_jabatan'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['eselon'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['e.eselon AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'eselon e', 'e.id = p.eselon')
        ->groupBy(['e.eselon'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['grade'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['g.grade AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'grade g', 'g.id = p.grade')
        ->groupBy(['g.grade'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['masaKerja'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select([
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 1 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > -1 then p.id end) as tahun1',
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 3 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > 1 then p.id end) as tahun3',
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 5 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > 3 then p.id end) as tahun5',
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 10 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > 5 then p.id end) as tahun10',
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 20 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > 10 then p.id end) as tahun20',
            'count(case when p.nip IS NOT NULL AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) <= 30 AND (YEAR(CURDATE())) - (SUBSTR(p.nip, 9, 4)) > 20 then p.id end) as tahun30',
        ])
        ->from('pegawai p')
    ;
    $raw = $query->one();
    $return = [];
    $i = 0;
    $return['label'] = ['1 Tahun', '3 Tahun', '5 Tahun', '10 Tahun', '20 Tahun'];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value;
        $return['background'][] = $background[$i % 12];
        $return['border'][] = $border[$i % 12];
        $i++;
    }

    return $return;
})($border, $background);

$pies['umur'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select([
            'count(case when p.tanggal_lahir IS NOT NULL AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) > 19 AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) <= 30 then p.id end) as tahun20_30',
            'count(case when p.tanggal_lahir IS NOT NULL AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) > 30 AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) <= 40 then p.id end) as tahun30_40',
            'count(case when p.tanggal_lahir IS NOT NULL AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) > 40 AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) <= 50 then p.id end) as tahun40_50',
            'count(case when p.tanggal_lahir IS NOT NULL AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) > 50 AND YEAR(CURDATE()) - YEAR(p.tanggal_lahir) <= 60 then p.id end) as tahun50_60',
        ])
        ->from('pegawai p')
    ;
    $raw = $query->one();
    $return = [];
    $i = 0;
    $return['label'] = ['20 - 30 Tahun', '30 - 40 Tahun', '40 - 50 Tahun', '50 - 60 Tahun'];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value;
        $return['background'][] = $background[$i % 12];
        $return['border'][] = $border[$i % 12];
        $i++;
    }

    return $return;
})($border, $background);

$pies['jenjangPendidikanTerakhir'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['jp.jenjang_pendidikan AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'jenjang_pendidikan jp', 'jp.id = p.jenjang_pendidikan_terakhir')
        ->groupBy(['jp.jenjang_pendidikan'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['kedudukanPegawai'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['kp.kedudukan_pegawai AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'kedudukan_pegawai kp', 'kp.id = p.kedudukan_pegawai')
        ->groupBy(['kp.kedudukan_pegawai'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['agama'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['a.agama AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'agama a', 'a.id = p.agama')
        ->groupBy(['a.agama'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$pies['statusPernikahan'] = (function($border, $background){
    $query = new \yii\db\Query();
    $query
        ->select(['jj.status_marital AS label', 'count(*) AS data'])
        ->from('pegawai p')
        ->join('LEFT JOIN', 'status_marital jj', 'jj.id = p.status_marital')
        ->groupBy(['jj.status_marital'])
    ;
    $raw = $query->all();
    $return = [];
    foreach ($raw as $key => $value) {
        $return['data'][] = $value['data'];
        $return['label'][] = $value['label'];
        $return['background'][] = $background[$key % 12];
        $return['border'][] = $border[$key % 12];
    }

    return $return;
})($border, $background);

$this->registerJsFile('@web/app/kepegawaian/list-statistik.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <?php foreach ($pies as $key => $pie) : ?>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic"><?= ucwords(implode(' ', preg_split('/(?=[A-Z])/', $key))); ?></span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div>
            <div class="box box-break-sm">
                <div class="box-4">
                    <div class="scroll-x"><div style="width: 100%;"><canvas id="<?= $key ?>" style="height: 300px;"></canvas></div></div>
                    <script>
                        window.addEventListener('load', function() {
                            window.<?= $key ?> = new Chart(document.getElementById('<?= $key ?>').getContext('2d'), {
                                type: 'pie',
                                data: {
                                  labels: <?= json_encode($pie['label']) ?>,
                                  datasets: [{
                                    data: <?= json_encode($pie['data']) ?>,
                                    backgroundColor: <?= json_encode($pie['background']) ?>,
                                    borderColor: <?= json_encode($pie['border']) ?>,
                                    borderWidth: 1,
                                    label: 'asdf',
                                  }]
                                },
                                options: {
                                    maintainAspectRatio: false,
                                    elements: {
                                        line: {
                                            tension: 0, // disables bezier curves
                                        }
                                    },
                                    animation: {
                                        duration: 0, // general animation time
                                    },
                                    hover: {
                                        animationDuration: 0, // duration of animations when hovering an item
                                    },
                                    responsiveAnimationDuration: 0, // animation duration after a resize
                                    responsive: true,
                                    legend: {
                                        display: true,
                                        position: 'bottom',
                                    },
                                    plugins: {
                                        labels: {
                                            // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                                            render: function (args) {
                                                return args.label + ' ' + args.percentage + '%';
                                            },
                                            fontSize: 12,
                                            fontColor: <?= json_encode($pie['border']) ?>,
                                        },
                                    },
                                }
                            });
                        });
                    </script>
                </div>
                <div class="box-8">
                    <div class="scroll-x">
                        <table class="datatables-statistik-<?= strtolower(implode('-', preg_split('/(?=[A-Z])/', $key))) ?> table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th></th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Kepangkatan</th>
                                    <?php if ($key != 'kepangkatan') : ?>
                                    <th><?= ucwords(implode(' ', preg_split('/(?=[A-Z])/', $key))) ?></th>
                                    <?php endif; ?>
                                </tr>
                                <tr class="dt-search">
                                    <th></th>
                                    <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                                    <th><input type="text" class="form-text border-none padding-0" placeholder="search nip..."/></th>
                                    <th><input type="text" class="form-text border-none padding-0" placeholder="search kepangkatan..."/></th>
                                    <?php if ($key != 'kepangkatan') : ?>
                                    <th><input type="text" class="form-text border-none padding-0" placeholder="search <?= strtolower(implode(' ', preg_split('/(?=[A-Z])/', $key))) ?>..."/></th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>