<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Detail Keterangan Kehadiran
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'kode', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->kode ? $model['keterangan_kehadiran']->kode : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'keterangan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->keterangan ? $model['keterangan_kehadiran']->keterangan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'dihitung_hadir', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->dihitung_hadir ? $model['keterangan_kehadiran']->dihitung_hadir : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'potong_tunkin', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->potong_tunkin ? $model['keterangan_kehadiran']->potong_tunkin : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'potongan_tunkin_persen', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->potongan_tunkin_persen ? $model['keterangan_kehadiran']->potongan_tunkin_persen : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'potong_uang_makan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->potong_uang_makan ? $model['keterangan_kehadiran']->potong_uang_makan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'potong_disiplin', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->potong_disiplin ? $model['keterangan_kehadiran']->potong_disiplin : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'batas_awal_unggah', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->batas_awal_unggah ? $model['keterangan_kehadiran']->batas_awal_unggah : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['keterangan_kehadiran'], 'batas_akhir_unggah', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['keterangan_kehadiran']->batas_akhir_unggah ? $model['keterangan_kehadiran']->batas_akhir_unggah : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
</div>
