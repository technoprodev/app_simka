$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-kehadiran-all');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-kehadiran-all', {tahun : tahun, bulan : bulan}),
                    type: 'POST'
                },
                dom: '<"clearfix margin-bottom-30"B><"clearfix margin-bottom-30 scroll-x"r<"#wrap"t>><"clearfix"ip>',
                lengthMenu: [[-1], ['Seluruh Hari']],
                columns: [
                    {
                        data: {
                            _: 'p.nama',
                            display: 'nama',
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.tanggal',
                            display: 'tanggal',
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.jadwal_masuk',
                            display: 'jadwal_masuk',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.jadwal_pulang',
                            display: 'jadwal_pulang',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.waktu_masuk',
                            display: 'waktu_masuk',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.waktu_pulang',
                            display: 'waktu_pulang',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'telat',
                            display: 'telat',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kecepetan',
                            display: 'kecepetan',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jumlah_jam',
                            display: 'jumlah_jam',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kurang_jam',
                            display: 'kurang_jam',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kh.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'potongan_tunkin',
                            display: 'potongan_tunkin',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kurang_jam',
                            display: 'kurang_jam',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (rowData.idKeterangan == '7') {
                                td.className = 'bg-arsir';
                            } else {
                                td.className = 'text-' + rowData.color;
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }

    document.getElementById("wrap").addEventListener("scroll", function(){
       var translate = "translate(0,"+this.scrollTop+"px)";
       this.querySelector("thead").style.transform = translate;
    });
});