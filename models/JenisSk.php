<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenis_sk".
 *
 * @property integer $id
 * @property string $jenis_sk
 *
 * @property PegawaiHukumanDisiplin[] $pegawaiHukumanDisiplins
 * @property PegawaiKenaikanGajiBerkala[] $pegawaiKenaikanGajiBerkalas
 * @property PegawaiKepangkatan[] $pegawaiKepangkatans
 * @property PegawaiMasaPersiapanPensiun[] $pegawaiMasaPersiapanPensiuns
 * @property PegawaiMutasi[] $pegawaiMutasis
 * @property PegawaiPelaksana[] $pegawaiPelaksanas
 * @property PegawaiPemberhentian[] $pegawaiPemberhentians
 */
class JenisSk extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_sk';
    }

    public function rules()
    {
        return [
            //id

            //jenis_sk
            [['jenis_sk'], 'required'],
            [['jenis_sk'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_sk' => 'Jenis Sk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiHukumanDisiplins()
    {
        return $this->hasMany(PegawaiHukumanDisiplin::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKenaikanGajiBerkalas()
    {
        return $this->hasMany(PegawaiKenaikanGajiBerkala::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKepangkatans()
    {
        return $this->hasMany(PegawaiKepangkatan::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMasaPersiapanPensiuns()
    {
        return $this->hasMany(PegawaiMasaPersiapanPensiun::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMutasis()
    {
        return $this->hasMany(PegawaiMutasi::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPelaksanas()
    {
        return $this->hasMany(PegawaiPelaksana::className(), ['jenis_sk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPemberhentians()
    {
        return $this->hasMany(PegawaiPemberhentian::className(), ['jenis_sk' => 'id']);
    }
}
