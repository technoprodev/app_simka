<?php

use app_simka\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();

if (Yii::$app->session->hasFlash('success'))
    $this->registerJs(
        'fn.alert("Success", "' . Yii::$app->session->getFlash('success') . '", "success");',
        3
    );
if (Yii::$app->session->hasFlash('info'))
    $this->registerJs(
        'fn.alert("Info", "' . Yii::$app->session->getFlash('info') . '", "info");',
        3
    );
if (Yii::$app->session->hasFlash('warning'))
    $this->registerJs(
        'fn.alert("Warning", "' . Yii::$app->session->getFlash('warning') . '", "warning");',
        3
    );
if (Yii::$app->session->hasFlash('error'))
    $this->registerJs(
        'fn.alert("Error", "' . Yii::$app->session->getFlash('error') . '", "danger");',
        3
    );
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="device-width, height=device-height, initial-scale=1, minimum-scale=1">
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
        <style type="text/css">
            .table {
                display: block;
                border-top: 0 solid #ededed;
                border-collapse: separate;
            }
            .table th,
            .table td {
                padding: 8px 15px;
            }
            thead tr th {
                border-radius: 13px;
            }
            thead tr:not(.dt-search) th {
                color: white;
                background-color: #3376b8;
            }
            thead tr.dt-search th {
                border: 1px #bbd8f5 solid;
                box-shadow: 0 0 10px #cce6ff;
            }
            tbody tr td {
                border: 1px #bbd8f5 solid;
                box-shadow: 0 0 10px #cce6ff;
                border-radius: 13px;
            }
            .bg-arsir {
                background-image: url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/arsir.png');
            }
            
            #wrap {
                overflow: auto;
                height: 500px;
            }
            @keyframes blink {
              50% {
                opacity: 0.0;
              }
            }
            @-webkit-keyframes blink {
              50% {
                opacity: 0.0;
              }
            }
            .blink {
              animation: blink 1s step-start 0s infinite;
              -webkit-animation: blink 1s step-start 0s infinite;
            }
        </style>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <div class="wrapper bg-light-azure text-grayest">
            <div class="header visible-sm-greater">
                <div class="header-left fs-18 text-center padding-y-15 bg-azure darker-10">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="text-lightest">
                        <span class="text-uppercase"><?= Yii::$app->params['app.name'] ?> - <?= Yii::$app->params['app.owner'] ?></span>
                    </a>
                </div>
                <div class="header-right padding-y-15 padding-x-30 clearfix bg-azure">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.png" width="20px;" class="margin-right-5 text-middle">
                    <span class="fs-18 text-middle">
                        <?= Yii::$app->params['app.description'] ?> - Badan Ekonomi Kreatif
                    </span>
                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <div class="pull-right">
                            <a data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>" class="button button-xs border-lightest text-lightest rounded-md">Logout</a>
                        </div>
                        <div class="pull-right margin-right-15 fs-16">
                            <i class="fa fa-user margin-right-5 text-light-azure"></i><?= Yii::$app->user->identity->pegawai ? Yii::$app->user->identity->pegawai->nama : '' ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="header visible-sm-less">
                <div class="fs-18 padding-y-15 padding-x-15 bg-azure darker-10">
                    <a class="text-lightest margin-right-5" technoart-toggle=".has-sidebar-left" technoart-toggleclasses="toggle-left">☰</a>
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="text-lightest">
                        <span class="text-uppercase"><?= Yii::$app->params['app.name'] ?> - <?= Yii::$app->params['app.owner'] ?></span>
                    </a>
                    <?php if (!Yii::$app->user->isGuest) : ?>
                        <div class="pull-right">
                            <a data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>" class="button button-xs border-lightest text-lightest rounded-md">Logout</a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="padding-y-15 padding-x-15 bg-azure">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.png" width="20px;" class="margin-right-5 text-middle">
                    <span class="fs-18 text-middle">
                        <?= Yii::$app->params['app.description'] ?> - Badan Ekonomi Kreatif
                    </span>
                </div>
            </div>

            <div class="body has-sidebar-left">
                <div class="overlay-left" technoart-toggle=".has-sidebar-left" technoart-toggleclasses="toggle-left"></div>

                <aside class="sidebar-left" style="background-image:url('<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/sidebar.png');background-position:cover">
                    <?php $listMenu['pegawai'] = [
                        [
                            'label' => 'Beranda',
                            'url' => ['pegawai/index'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'index'),
                        ],
                        [
                            'label' => 'Daftar Riwayat Hidup',
                            'url' => ['pegawai/drh'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'drh'),
                        ],
                        [
                            'label' => 'Ubah Daftar Riwayat Hidup',
                            'url' => ['pegawai/pegawai-update', 'tab' => 'personal'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'pegawai-update'),
                        ],
                        [
                            'label' => 'Kehadiran',
                            'url' => ['pegawai/kehadiran'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'kehadiran'),
                        ],
                        [
                            'label' => 'Pengajuan Sakit / Izin',
                            'url' => ['pegawai/bukti-kehadiran-create'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'bukti-kehadiran-create'),
                        ],
                        [
                            'label' => 'Status Pengajuan Sakit / Izin',
                            'url' => ['pegawai/bukti-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'bukti-kehadiran'),
                        ],
                        [
                            'label' => 'Pengajuan Cuti Online',
                            'url' => ['pegawai/cuti-create'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'cuti-create'),
                        ],
                        [
                            'label' => 'Status Cuti Online Saya',
                            'url' => ['pegawai/cuti'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'cuti'),
                        ],
                        [
                            'label' => 'Verifikasi Cuti Online',
                            'url' => ['pegawai/cuti-verifikasi'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'cuti-verifikasi'),
                        ],
                        [
                            'label' => 'Ganti Password',
                            'url' => ['pegawai/ganti-password'],
                            'visible' => Menu::menuVisible(true, 'pegawai', 'ganti-password'),
                        ],
                    ]; ?>
                    <?php
                        $menu['pegawai'] = MenuWidget::widget([
                            'items' => $listMenu['pegawai'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $listMenu['kepegawaian'] = [
                        [
                            'label' => 'Daftar Pegawai',
                            'url' => ['kepegawaian/pegawai'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'pegawai'),
                        ],
                        [
                            'label' => 'Statistik Pegawai',
                            'url' => ['kepegawaian/pegawai-statistik'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'pegawai-statistik'),
                        ],
                        [
                            'label' => 'Tambah Pegawai',
                            'url' => ['kepegawaian/pegawai-create'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'pegawai-create'),
                        ],
                        [
                            'label' => 'Daftar Kehadiran',
                            'url' => ['kepegawaian/kehadiran'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'kehadiran'),
                        ],
                        [
                            'label' => 'Daftar Kehadiran (All)',
                            'url' => ['kepegawaian/kehadiran-all'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'kehadiran-all'),
                        ],
                        [
                            'label' => 'Pengaturan Jadwal Kehadiran',
                            'url' => ['jadwal-kehadiran/index'],
                            'visible' => Menu::menuVisible(true, 'jadwal-kehadiran', 'index'),
                        ],
                        [
                            'label' => (function($result){
                                $count = '<span class="rounded-md padding-y-5 padding-x-10 bg-azure margin-left-5 fs-11">' . $result . '</span>';
                                return 'Verif Bukti Kehadiran' . $count;

                                $circle = '';
                                switch ($result) {
                                    case 1:
                                        $circle = '<i class="fa fa-circle blink text-red pull-right fs-14"></i>';
                                        break;
                                    default:
                                        break;
                                }
                                return 'Verif Bukti Kehadiran' . $circle;
                            })(Yii::$app->db->createCommand('SELECT count(*) FROM bukti_kehadiran WHERE status_pengajuan = "Diajukan"')->queryScalar()),
                            // })(Yii::$app->db->createCommand('SELECT EXISTS(SELECT 1 FROM bukti_kehadiran WHERE status_pengajuan = "Diajukan" LIMIT 1)')->queryScalar()),
                            'url' => ['kepegawaian/bukti-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'bukti-kehadiran'),
                        ],
                        [
                            'label' => 'Unggah Bukti Kehadiran',
                            'url' => ['kepegawaian/bukti-kehadiran-create'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'bukti-kehadiran-create'),
                        ],
                        [
                            'label' => (function($result){
                                $count = '<span class="rounded-md padding-y-5 padding-x-10 bg-azure margin-left-5 fs-11">' . $result . '</span>';
                                return 'Verif Bukti Cctv' . $count;
                            })(Yii::$app->db->createCommand('SELECT count(*) FROM bukti_cctv WHERE status_pengajuan = "Diajukan"')->queryScalar()),
                            'url' => ['kepegawaian/bukti-cctv'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'bukti-cctv'),
                        ],
                        [
                            'label' => 'Unggah Bukti Cctv',
                            'url' => ['kepegawaian/bukti-cctv-create'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'bukti-cctv-create'),
                        ],
                        [
                            'label' => 'Ubah Data Master',
                            'url' => ['kepegawaian/master'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'master'),
                        ],
                        [
                            'label' => 'Proses Jadwal Kehadiran',
                            'url' => ['kepegawaian/proses-jadwal-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'proses-jadwal-kehadiran'),
                        ],
                        [
                            'label' => 'Proses Manual Kehadiran',
                            'url' => ['kepegawaian/proses-manual-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'proses-manual-kehadiran'),
                        ],
                        /*[
                            'label' => 'Keterangan Kehadiran',
                            'url' => ['kepegawaian/keterangan-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'kepegawaian', 'keterangan-kehadiran'),
                        ],*/
                    ]; ?>
                    <?php
                        $menu['kepegawaian'] = MenuWidget::widget([
                            'items' => $listMenu['kepegawaian'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $listMenu['kepala-biro'] = [
                        [
                            'label' => (function($result){
                                $count = '<span class="rounded-md padding-y-5 padding-x-10 bg-azure margin-left-5 fs-11">' . $result . '</span>';
                                return 'Verif Cuti Online' . $count;
                            })(Yii::$app->db->createCommand('SELECT count(*) FROM cuti WHERE status_pengajuan = "Diterima Atasan 2"')->queryScalar()),
                            'url' => ['kepala-biro/cuti'],
                            'visible' => Menu::menuVisible(true, 'kepala-biro', 'cuti'),
                        ],
                    ]; ?>
                    <?php
                        $menu['kepala-biro'] = MenuWidget::widget([
                            'items' => $listMenu['kepala-biro'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $listMenu['pejabat'] = [
                        [
                            'label' => 'Daftar Pegawai',
                            'url' => ['pejabat/pegawai'],
                            'visible' => Menu::menuVisible(true, 'pejabat', 'pegawai'),
                        ],
                        [
                            'label' => 'Daftar Kehadiran',
                            'url' => ['pejabat/kehadiran'],
                            'visible' => Menu::menuVisible(true, 'pejabat', 'kehadiran'),
                        ],
                    ]; ?>
                    <?php
                        $menu['pejabat'] = MenuWidget::widget([
                            'items' => $listMenu['pejabat'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $listMenu['administrasi'] = [
                        [
                            'label' => 'Unggah Bukti Kehadiran',
                            'url' => ['administrasi/bukti-kehadiran-create'],
                            'visible' => Menu::menuVisible(true, 'administrasi', 'bukti-kehadiran-create'),
                        ],
                        [
                            'label' => (function($result){
                                $count = '<span class="rounded-md padding-y-5 padding-x-10 bg-azure margin-left-5 fs-11">' . $result . '</span>';
                                return 'Status Pengajuan Bukti Kehadiran' . $count;
                            })(Yii::$app->db->createCommand('SELECT count(*) FROM bukti_kehadiran bk join keterangan_kehadiran kk on kk.id = bk.keterangan_kehadiran WHERE kk.diajukan_administrasi = "Ya" and bk.status_pengajuan = "Direvisi" and bk.diajukan = ' . Yii::$app->user->identity->id)->queryScalar()),
                            'url' => ['administrasi/bukti-kehadiran'],
                            'visible' => Menu::menuVisible(true, 'administrasi', 'bukti-kehadiran'),
                        ],
                        [
                            'label' => 'Unggah Bukti Cctv',
                            'url' => ['administrasi/bukti-cctv-create'],
                            'visible' => Menu::menuVisible(true, 'administrasi', 'bukti-cctv-create'),
                        ],
                        [
                            'label' => (function($result){
                                $count = '<span class="rounded-md padding-y-5 padding-x-10 bg-azure margin-left-5 fs-11">' . $result . '</span>';
                                return 'Status Pengajuan Bukti Cctv' . $count;
                            })(Yii::$app->db->createCommand('SELECT count(*) FROM bukti_cctv WHERE status_pengajuan = "Direvisi" and diajukan = ' . Yii::$app->user->identity->id)->queryScalar()),
                            'url' => ['administrasi/bukti-cctv'],
                            'visible' => Menu::menuVisible(true, 'administrasi', 'bukti-cctv'),
                        ],
                        [
                            'label' => 'Daftar Kehadiran',
                            'url' => ['administrasi/kehadiran'],
                            'visible' => Menu::menuVisible(true, 'administrasi', 'kehadiran'),
                        ],
                    ]; ?>
                    <?php
                        $menu['administrasi'] = MenuWidget::widget([
                            'items' => $listMenu['administrasi'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php $listMenu['export'] = [
                        [
                            'label' => 'Tunjangan Kinerja',
                            'url' => ['export/tunkin'],
                            'visible' => Menu::menuVisible(true, 'export', 'tunkin'),
                        ],
                        [
                            'label' => 'Uang Makan',
                            'url' => ['export/uang-makan'],
                            'visible' => Menu::menuVisible(true, 'export', 'uang-makan'),
                        ],
                        [
                            'label' => 'Setting Export',
                            'url' => ['export/master'],
                            'visible' => Menu::menuVisible(true, 'export', 'master'),
                        ],
                    ]; ?>
                    <?php
                        $menu['export'] = MenuWidget::widget([
                            'items' => $listMenu['export'],
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-lighter menu-active-text-rose menu-hover-darker-10 lighter-50',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                    ?>

                    <?php
                        $isPegawai = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['pegawai'])) ? true : false;
                        $isKepegawaian = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['kepegawaian'])) ? true : false;
                        // dd(Yii::$app->createControllerByID('kepegawaian')->canAccess('pegawai'));
                        // dd(Yii::$app->createControllerByID('kepegawaian')->canAccess('pegawai-create'));
                        // dd(Yii::$app->createControllerByID('kepegawaian')->canAccess('kehadiran'));
                        // dd(Yii::$app->createControllerByID('jadwal-kehadiran')->canAccess('index'));
                        // dd(Yii::$app->createControllerByID('kepegawaian')->canAccess('master'));
                        // ddx($menu['kepegawaian']);
                        $isKepalaBiro = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['kepala-biro'])) ? true : false;
                        $isPejabat = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['pejabat'])) ? true : false;
                        $isAdministrasi = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['administrasi'])) ? true : false;
                        $isExport = (isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['export'])) ? true : false;
                    ?>

                    <div class="margin-top-15"></div>

                    <div class="padding-x-15">
                        <?php if ($isPegawai): ?>
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Pegawai
                            </div>
                            <?= $menu['pegawai'] ?>
                        <?php endif; ?>
                        <?php if ($isKepegawaian): ?>
                            <hr class="border-top border-azure">
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Kepegawaian
                            </div>
                            <?= $menu['kepegawaian'] ?>
                        <?php endif; ?>
                        <?php if ($isKepalaBiro): ?>
                            <hr class="border-top border-azure">
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Kepala Biro
                            </div>
                            <?= $menu['kepala-biro'] ?>
                        <?php endif; ?>
                        <?php if ($isPejabat): ?>
                            <hr class="border-top border-azure">
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Pejabat
                            </div>
                            <?= $menu['pejabat'] ?>
                        <?php endif; ?>
                        <?php if ($isAdministrasi): ?>
                            <hr class="border-top border-azure">
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Administrasi
                            </div>
                            <?= $menu['administrasi'] ?>
                        <?php endif; ?>
                        <?php if ($isExport): ?>
                            <hr class="border-top border-azure">
                            <div class="padding-x-20 padding-y-10 fs-16 dark-10 bg-azure text-lightest text-center">
                                Menu Export
                            </div>
                            <?= $menu['export'] ?>
                        <?php endif; ?>
                    </div>

                    <div class="margin-top-100"></div>
                </aside>

                <div class="page-wrapper has-bg-img">
                    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg.png" class="bg-img">
                    <div class="bg-lightest fs-16 shadow-bottom-right clearfix border-bottom border-light-orange border-thick has-bg-img">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/bg-header.png" class="bg-img" style="height:auto;">
                        <div class="pull-left m-pull-none padding-x-30 m-padding-x-10 padding-y-15 text-lightest fw-bold">
                            <?= Html::decode($this->title) ?> <br>
                            <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/line-short.png" height="5px;" class="">
                        </div>
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/asn.png" height="75px;" class="pull-right visible-sm-greater padding-x-30 m-padding-x-10 padding-y-5">
                    </div>

                    <div class="padding-x-30 m-padding-x-10 padding-y-15">
                        <?= $content ?>
                    </div>

                    <div class="margin-top-60"></div>

                    <footer class="footer border-top padding-y-15 text-center">
                        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="40px;" class="margin-right-5 text-middle">
                        <span class="fs-18 text-middle">© <?= date('Y') ?></span>
                    </footer>
                </div>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>