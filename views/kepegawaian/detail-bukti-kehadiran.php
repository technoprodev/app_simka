<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Detail Bukti Kehadiran
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'keterangan_kehadiran', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->keterangan_kehadiran ? $model['bukti_kehadiran']->keteranganKehadiran->keterangan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'diajukan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->diajukan ? $model['bukti_kehadiran']->diajukan0->nama : '(kosong)'?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->catatan_pengajuan ? $model['bukti_kehadiran']->catatan_pengajuan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->dari_tanggal ? $model['bukti_kehadiran']->dari_tanggal : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->sampai_tanggal ? $model['bukti_kehadiran']->sampai_tanggal : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'nomor_surat', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->nomor_surat ? $model['bukti_kehadiran']->nomor_surat : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_utama ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_utama_download . '">' . $model['bukti_kehadiran']->bukti_utama . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_tambahan_1 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_1_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_1 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_tambahan_2 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_2_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_2 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_tambahan_3 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_3_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_3 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_tambahan_4 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_4_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_4 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->bukti_tambahan_5 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_5_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_5 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <?php foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai) : ?>
            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    Pegawai <?= ($key + 1) ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $buktiKehadiranPegawai->pegawai ? $buktiKehadiranPegawai->pegawai0->nama . ' - ' . $buktiKehadiranPegawai->pegawai0->nip : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endforeach; ?>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_kehadiran'], 'status_pengajuan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_kehadiran']->status_pengajuan ? $model['bukti_kehadiran']->status_pengajuan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
</div>
