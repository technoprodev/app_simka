<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/jadwal-kehadiran/list-jadwal-kehadiran1.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Kelola Rule Jadwal Kehadiran</span>
            <div class="pull-right">
                <?= Html::a('Tambah Jadwal Kehadiran', ['jadwal-kehadiran-create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-jadwal-kehadiran table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>berlaku dari</th>
                        <th>berlaku sampai</th>
                        <th>catatan</th>
                        <th>berlaku bagi</th>
                        <th>unit kerja</th>
                        <th>pegawai</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search berlaku dari..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search berlaku sampai..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search catatan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search berlaku bagi..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search unit kerja..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search pegawai..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>