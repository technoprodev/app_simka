DELIMITER $$

DROP FUNCTION IF EXISTS get_unit_kerja_rec;
CREATE FUNCTION get_unit_kerja_rec(root_id int) RETURNS varchar(255) READS SQL DATA
BEGIN
  return (
    select group_concat(id) from (
      select id from unit_kerja where id=root_id union all
      select id from (
        select * from unit_kerja order by parent, id
      ) unit_kerja_sorted, (select @pv := root_id) initialisation
      where find_in_set(parent, @pv) and length(@pv := concat(@pv, ',', id))
    ) t
  );
END$$

DROP FUNCTION IF EXISTS get_pegawai_rec;
CREATE FUNCTION get_pegawai_rec(unit_kerja_id int) RETURNS varchar(255) READS SQL DATA
BEGIN
  return (
    select group_concat(id) from pegawai
    where find_in_set(unit_kerja, get_unit_kerja_rec(unit_kerja_id))
  );
END$$

DROP FUNCTION IF EXISTS get_jadwal_masuk;
CREATE FUNCTION get_jadwal_masuk(
  tanggal date,
  minggu_masuk time, minggu_libur boolean,
  senin_masuk time, senin_libur boolean,
  selasa_masuk time, selasa_libur boolean,
  rabu_masuk time, rabu_libur boolean,
  kamis_masuk time, kamis_libur boolean,
  jumat_masuk time, jumat_libur boolean,
  sabtu_masuk time, sabtu_libur boolean
) RETURNS datetime
BEGIN
  return (
    case
      when DAYOFWEEK(tanggal)=1 then
        case when minggu_libur then null else timestamp(tanggal, minggu_masuk) end
      when DAYOFWEEK(tanggal)=2 then
        case when senin_libur then null else timestamp(tanggal, senin_masuk) end
      when DAYOFWEEK(tanggal)=3 then
        case when selasa_libur then null else timestamp(tanggal, selasa_masuk) end
      when DAYOFWEEK(tanggal)=4 then
        case when rabu_libur then null else timestamp(tanggal, rabu_masuk) end
      when DAYOFWEEK(tanggal)=5 then
        case when kamis_libur then null else timestamp(tanggal, kamis_masuk) end
      when DAYOFWEEK(tanggal)=6 then
        case when jumat_libur then null else timestamp(tanggal, jumat_masuk) end
      when DAYOFWEEK(tanggal)=7 then
        case when sabtu_libur then null else timestamp(tanggal, sabtu_masuk) end
    end
  );
END$$

DROP FUNCTION IF EXISTS get_jadwal_pulang;
CREATE FUNCTION get_jadwal_pulang(
  tanggal date,
  minggu_masuk time, minggu_pulang time, minggu_libur boolean,
  senin_masuk time, senin_pulang time, senin_libur boolean,
  selasa_masuk time, selasa_pulang time, selasa_libur boolean,
  rabu_masuk time, rabu_pulang time, rabu_libur boolean,
  kamis_masuk time, kamis_pulang time, kamis_libur boolean,
  jumat_masuk time, jumat_pulang time, jumat_libur boolean,
  sabtu_masuk time, sabtu_pulang time, sabtu_libur boolean
) RETURNS datetime
BEGIN
  return (
    case
      when DAYOFWEEK(tanggal)=1 then
        case
          when minggu_libur then null
          when minggu_pulang > minggu_masuk then timestamp(tanggal, minggu_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), minggu_pulang)
        end
      when DAYOFWEEK(tanggal)=2 then
        case
          when senin_libur then null
          when senin_pulang > senin_masuk then timestamp(tanggal, senin_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), senin_pulang)
        end
      when DAYOFWEEK(tanggal)=3 then
        case
          when selasa_libur then null
          when selasa_pulang > selasa_masuk then timestamp(tanggal, selasa_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), selasa_pulang)
        end
      when DAYOFWEEK(tanggal)=4 then
        case
          when rabu_libur then null
          when rabu_pulang > rabu_masuk then timestamp(tanggal, rabu_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), rabu_pulang)
        end
      when DAYOFWEEK(tanggal)=5 then
        case
          when kamis_libur then null
          when kamis_pulang > kamis_masuk then timestamp(tanggal, kamis_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), kamis_pulang)
        end
      when DAYOFWEEK(tanggal)=6 then
        case
          when jumat_libur then null
          when jumat_pulang > jumat_masuk then timestamp(tanggal, jumat_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), jumat_pulang)
        end
      when DAYOFWEEK(tanggal)=7 then
        case
          when sabtu_libur then null
          when sabtu_pulang > sabtu_masuk then timestamp(tanggal, sabtu_pulang)
          else timestamp(DATE_ADD(tanggal, INTERVAL 1 DAY), sabtu_pulang)
        end
    end
  );
END$$

DROP PROCEDURE IF EXISTS apply_jk_global;
CREATE PROCEDURE apply_jk_global()
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_masuk time;
  declare v_senin_batas_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_masuk time;
  declare v_selasa_batas_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_masuk time;
  declare v_rabu_batas_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_masuk time;
  declare v_kamis_batas_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_masuk time;
  declare v_jumat_batas_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_masuk time;
  declare v_sabtu_batas_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_masuk time;
  declare v_minggu_batas_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      p.id as pegawai, jk.berlaku_dari, jk.berlaku_sampai,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_masuk, jk.senin_batas_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_masuk, jk.selasa_batas_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_masuk, jk.rabu_batas_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_masuk, jk.kamis_batas_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_masuk, jk.jumat_batas_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_masuk, jk.sabtu_batas_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_masuk, jk.minggu_batas_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    left join pegawai p on true
    where jk.berlaku_bagi='Seluruh Unit Kerja';

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Seluruh Unit Kerja'
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
  set
    jadwal_masuk=get_jadwal_masuk(
      k.tanggal, minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_masuk=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_batas_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_pulang=get_jadwal_pulang(
      k.tanggal, minggu_pulang, minggu_batas_pulang, minggu_libur='Ya',
      senin_pulang, senin_batas_pulang, senin_libur='Ya',
      selasa_pulang, selasa_batas_pulang, selasa_libur='Ya',
      rabu_pulang, rabu_batas_pulang, rabu_libur='Ya',
      kamis_pulang, kamis_batas_pulang, kamis_libur='Ya',
      jumat_pulang, jumat_batas_pulang, jumat_libur='Ya',
      sabtu_pulang, sabtu_batas_pulang, sabtu_libur='Ya'
    );

  update kehadiran set keterangan='Hari Libur' where jadwal_masuk is null;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_senin_masuk, v_senin_pulang, v_senin_batas_masuk, v_senin_batas_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_masuk, v_selasa_batas_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_masuk, v_rabu_batas_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_masuk, v_kamis_batas_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_masuk, v_jumat_batas_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_masuk, v_sabtu_batas_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_masuk, v_minggu_batas_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, jadwal_masuk, jadwal_pulang, jadwal_batas_masuk, jadwal_batas_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          get_jadwal_masuk(
            v_tanggal, v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_batas_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_pulang, v_minggu_batas_pulang, v_minggu_libur='Ya',
            v_senin_pulang, v_senin_batas_pulang, v_senin_libur='Ya',
            v_selasa_pulang, v_selasa_batas_pulang, v_selasa_libur='Ya',
            v_rabu_pulang, v_rabu_batas_pulang, v_rabu_libur='Ya',
            v_kamis_pulang, v_kamis_batas_pulang, v_kamis_libur='Ya',
            v_jumat_pulang, v_jumat_batas_pulang, v_jumat_libur='Ya',
            v_sabtu_pulang, v_sabtu_batas_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 'Hari Libur'
              else 'Belum Absen'
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DROP PROCEDURE IF EXISTS apply_jk_pegawai;
CREATE PROCEDURE apply_jk_pegawai()
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_masuk time;
  declare v_senin_batas_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_masuk time;
  declare v_selasa_batas_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_masuk time;
  declare v_rabu_batas_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_masuk time;
  declare v_kamis_batas_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_masuk time;
  declare v_jumat_batas_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_masuk time;
  declare v_sabtu_batas_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_masuk time;
  declare v_minggu_batas_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      pegawai, jk.berlaku_dari, jk.berlaku_sampai,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_masuk, jk.senin_batas_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_masuk, jk.selasa_batas_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_masuk, jk.rabu_batas_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_masuk, jk.kamis_batas_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_masuk, jk.jumat_batas_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_masuk, jk.sabtu_batas_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_masuk, jk.minggu_batas_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    inner join pegawai p on p.id=jk.pegawai
    where jk.berlaku_bagi='Pegawai Tertentu'
      and jk.pegawai is not null;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Pegawai Tertentu'
    and jk.pegawai is not null
    and k.pegawai=jk.pegawai
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
  set
    jadwal_masuk=get_jadwal_masuk(
      k.tanggal, minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_masuk=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_batas_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_pulang=get_jadwal_pulang(
      k.tanggal, minggu_pulang, minggu_batas_pulang, minggu_libur='Ya',
      senin_pulang, senin_batas_pulang, senin_libur='Ya',
      selasa_pulang, selasa_batas_pulang, selasa_libur='Ya',
      rabu_pulang, rabu_batas_pulang, rabu_libur='Ya',
      kamis_pulang, kamis_batas_pulang, kamis_libur='Ya',
      jumat_pulang, jumat_batas_pulang, jumat_libur='Ya',
      sabtu_pulang, sabtu_batas_pulang, sabtu_libur='Ya'
    );

  update kehadiran set keterangan='Hari Libur' where jadwal_masuk is null;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_senin_masuk, v_senin_pulang, v_senin_batas_masuk, v_senin_batas_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_masuk, v_selasa_batas_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_masuk, v_rabu_batas_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_masuk, v_kamis_batas_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_masuk, v_jumat_batas_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_masuk, v_sabtu_batas_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_masuk, v_minggu_batas_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, jadwal_masuk, jadwal_pulang, jadwal_batas_masuk, jadwal_batas_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          get_jadwal_masuk(
            v_tanggal, v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_batas_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_pulang, v_minggu_batas_pulang, v_minggu_libur='Ya',
            v_senin_pulang, v_senin_batas_pulang, v_senin_libur='Ya',
            v_selasa_pulang, v_selasa_batas_pulang, v_selasa_libur='Ya',
            v_rabu_pulang, v_rabu_batas_pulang, v_rabu_libur='Ya',
            v_kamis_pulang, v_kamis_batas_pulang, v_kamis_libur='Ya',
            v_jumat_pulang, v_jumat_batas_pulang, v_jumat_libur='Ya',
            v_sabtu_pulang, v_sabtu_batas_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 'Hari Libur'
              else 'Belum Absen'
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DROP PROCEDURE IF EXISTS apply_jk_unit_kerja;
CREATE PROCEDURE apply_jk_unit_kerja()
BEGIN
  declare v_tanggal date;
  declare v_pegawai int;
  declare v_berlaku_dari date;
  declare v_berlaku_sampai date;
  declare v_senin_masuk time;
  declare v_senin_pulang time;
  declare v_senin_batas_masuk time;
  declare v_senin_batas_pulang time;
  declare v_senin_libur enum('Ya', 'Tidak');
  declare v_selasa_masuk time;
  declare v_selasa_pulang time;
  declare v_selasa_batas_masuk time;
  declare v_selasa_batas_pulang time;
  declare v_selasa_libur enum('Ya', 'Tidak');
  declare v_rabu_masuk time;
  declare v_rabu_pulang time;
  declare v_rabu_batas_masuk time;
  declare v_rabu_batas_pulang time;
  declare v_rabu_libur enum('Ya', 'Tidak');
  declare v_kamis_masuk time;
  declare v_kamis_pulang time;
  declare v_kamis_batas_masuk time;
  declare v_kamis_batas_pulang time;
  declare v_kamis_libur enum('Ya', 'Tidak');
  declare v_jumat_masuk time;
  declare v_jumat_pulang time;
  declare v_jumat_batas_masuk time;
  declare v_jumat_batas_pulang time;
  declare v_jumat_libur enum('Ya', 'Tidak');
  declare v_sabtu_masuk time;
  declare v_sabtu_pulang time;
  declare v_sabtu_batas_masuk time;
  declare v_sabtu_batas_pulang time;
  declare v_sabtu_libur enum('Ya', 'Tidak');
  declare v_minggu_masuk time;
  declare v_minggu_pulang time;
  declare v_minggu_batas_masuk time;
  declare v_minggu_batas_pulang time;
  declare v_minggu_libur enum('Ya', 'Tidak');

  declare jk_cursor_done int default 0;
  declare jk_cursor cursor for
    SELECT
      p.id as pegawai, jk.berlaku_dari, jk.berlaku_sampai,
      jk.senin_masuk, jk.senin_pulang, jk.senin_batas_masuk, jk.senin_batas_pulang, jk.senin_libur,
      jk.selasa_masuk, jk.selasa_pulang, jk.selasa_batas_masuk, jk.selasa_batas_pulang, jk.selasa_libur,
      jk.rabu_masuk, jk.rabu_pulang, jk.rabu_batas_masuk, jk.rabu_batas_pulang, jk.rabu_libur,
      jk.kamis_masuk, jk.kamis_pulang, jk.kamis_batas_masuk, jk.kamis_batas_pulang, jk.kamis_libur,
      jk.jumat_masuk, jk.jumat_pulang, jk.jumat_batas_masuk, jk.jumat_batas_pulang, jk.jumat_libur,
      jk.sabtu_masuk, jk.sabtu_pulang, jk.sabtu_batas_masuk, jk.sabtu_batas_pulang, jk.sabtu_libur,
      jk.minggu_masuk, jk.minggu_pulang, jk.minggu_batas_masuk, jk.minggu_batas_pulang, jk.minggu_libur
    from jadwal_kehadiran jk
    inner join unit_kerja uk on uk.id=jk.unit_kerja
    inner join pegawai p on find_in_set(p.unit_kerja, get_unit_kerja_rec(jk.unit_kerja))
    where jk.berlaku_bagi='Unit Kerja Tertentu'
      and jk.unit_kerja is not null;

  declare continue handler for not found set jk_cursor_done = 1;
  declare exit handler for sqlexception begin rollback; end;

  start transaction;

  -- update jadwal kehadiran
  update kehadiran k
  inner join jadwal_kehadiran as jk on jk.berlaku_bagi='Unit Kerja Tertentu'
    and jk.unit_kerja is not null
    and k.tanggal between jk.berlaku_dari and jk.berlaku_sampai
  inner join pegawai p on p.id=k.pegawai
    and find_in_set(p.unit_kerja, get_unit_kerja_rec(jk.unit_kerja))
  set
    jadwal_masuk=get_jadwal_masuk(
      k.tanggal, minggu_masuk, minggu_libur='Ya',
      senin_masuk, senin_libur='Ya',
      selasa_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_libur='Ya'
    ),
    jadwal_pulang=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_pulang, minggu_libur='Ya',
      senin_masuk, senin_pulang, senin_libur='Ya',
      selasa_masuk, selasa_pulang, selasa_libur='Ya',
      rabu_masuk, rabu_pulang, rabu_libur='Ya',
      kamis_masuk, kamis_pulang, kamis_libur='Ya',
      jumat_masuk, jumat_pulang, jumat_libur='Ya',
      sabtu_masuk, sabtu_pulang, sabtu_libur='Ya'
    ),
    jadwal_batas_masuk=get_jadwal_pulang(
      k.tanggal, minggu_masuk, minggu_batas_masuk, minggu_libur='Ya',
      senin_masuk, senin_batas_masuk, senin_libur='Ya',
      selasa_masuk, selasa_batas_masuk, selasa_libur='Ya',
      rabu_masuk, rabu_batas_masuk, rabu_libur='Ya',
      kamis_masuk, kamis_batas_masuk, kamis_libur='Ya',
      jumat_masuk, jumat_batas_masuk, jumat_libur='Ya',
      sabtu_masuk, sabtu_batas_masuk, sabtu_libur='Ya'
    ),
    jadwal_batas_pulang=get_jadwal_pulang(
      k.tanggal, minggu_pulang, minggu_batas_pulang, minggu_libur='Ya',
      senin_pulang, senin_batas_pulang, senin_libur='Ya',
      selasa_pulang, selasa_batas_pulang, selasa_libur='Ya',
      rabu_pulang, rabu_batas_pulang, rabu_libur='Ya',
      kamis_pulang, kamis_batas_pulang, kamis_libur='Ya',
      jumat_pulang, jumat_batas_pulang, jumat_libur='Ya',
      sabtu_pulang, sabtu_batas_pulang, sabtu_libur='Ya'
    );

  update kehadiran set keterangan='Hari Libur' where jadwal_masuk is null;

  -- insert jadwal kehadiran (kalau belum ada)
  open jk_cursor;
  repeat
    fetch jk_cursor into
      v_pegawai, v_berlaku_dari, v_berlaku_sampai,
      v_senin_masuk, v_senin_pulang, v_senin_batas_masuk, v_senin_batas_pulang, v_senin_libur,
      v_selasa_masuk, v_selasa_pulang, v_selasa_batas_masuk, v_selasa_batas_pulang, v_selasa_libur,
      v_rabu_masuk, v_rabu_pulang, v_rabu_batas_masuk, v_rabu_batas_pulang, v_rabu_libur,
      v_kamis_masuk, v_kamis_pulang, v_kamis_batas_masuk, v_kamis_batas_pulang, v_kamis_libur,
      v_jumat_masuk, v_jumat_pulang, v_jumat_batas_masuk, v_jumat_batas_pulang, v_jumat_libur,
      v_sabtu_masuk, v_sabtu_pulang, v_sabtu_batas_masuk, v_sabtu_batas_pulang, v_sabtu_libur,
      v_minggu_masuk, v_minggu_pulang, v_minggu_batas_masuk, v_minggu_batas_pulang, v_minggu_libur;

    if not jk_cursor_done then
      set v_tanggal = date(v_berlaku_dari);

      while v_tanggal <= v_berlaku_sampai do
        insert ignore into kehadiran (
          pegawai, tanggal, jadwal_masuk, jadwal_pulang, jadwal_batas_masuk, jadwal_batas_pulang, keterangan
        )
        values (
          v_pegawai, v_tanggal,
          get_jadwal_masuk(
            v_tanggal, v_minggu_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_pulang, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_pulang, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_pulang, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_pulang, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_pulang, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_pulang, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_pulang, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_masuk, v_minggu_batas_masuk, v_minggu_libur='Ya',
            v_senin_masuk, v_senin_batas_masuk, v_senin_libur='Ya',
            v_selasa_masuk, v_selasa_batas_masuk, v_selasa_libur='Ya',
            v_rabu_masuk, v_rabu_batas_masuk, v_rabu_libur='Ya',
            v_kamis_masuk, v_kamis_batas_masuk, v_kamis_libur='Ya',
            v_jumat_masuk, v_jumat_batas_masuk, v_jumat_libur='Ya',
            v_sabtu_masuk, v_sabtu_batas_masuk, v_sabtu_libur='Ya'
          ),
          get_jadwal_pulang(
            v_tanggal, v_minggu_pulang, v_minggu_batas_pulang, v_minggu_libur='Ya',
            v_senin_pulang, v_senin_batas_pulang, v_senin_libur='Ya',
            v_selasa_pulang, v_selasa_batas_pulang, v_selasa_libur='Ya',
            v_rabu_pulang, v_rabu_batas_pulang, v_rabu_libur='Ya',
            v_kamis_pulang, v_kamis_batas_pulang, v_kamis_libur='Ya',
            v_jumat_pulang, v_jumat_batas_pulang, v_jumat_libur='Ya',
            v_sabtu_pulang, v_sabtu_batas_pulang, v_sabtu_libur='Ya'
          ),
          (
            case
              when DAYOFWEEK(v_tanggal)=1 and v_minggu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=2 and v_senin_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=3 and v_selasa_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=4 and v_rabu_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=5 and v_kamis_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=6 and v_jumat_libur='Ya' then 'Hari Libur'
              when DAYOFWEEK(v_tanggal)=7 and v_sabtu_libur='Ya' then 'Hari Libur'
              else 'Belum Absen'
            end
          )
        );

        set v_tanggal = DATE_ADD(v_tanggal, INTERVAL 1 DAY);
      end while;
    end if;
  until jk_cursor_done end repeat;
  close jk_cursor;

  commit;
END$$

DROP PROCEDURE IF EXISTS apply_jk;
CREATE PROCEDURE apply_jk()
BEGIN
  call apply_jk_global();
  call apply_jk_unit_kerja();
  call apply_jk_pegawai();
END$$

DELIMITER ;