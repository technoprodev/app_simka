$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-cuti');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepala-biro/datatables-cuti'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('kepala-biro/cuti-update/' + data) + '" class=""><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Update"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kh.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.status_pengajuan',
                            display: 'status_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.dari_tanggal',
                            display: 'dari_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.sampai_tanggal',
                            display: 'sampai_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.waktu_pengajuan',
                            display: 'waktu_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p1.nama',
                            display: 'atasan_1',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p2.nama',
                            display: 'atasan_2',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                order: [[3, 'desc']],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});

$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-cuti-monitoring');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepala-biro/datatables-cuti-monitoring'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            var view = '';
                            view = '    <a href="' + fn.urlTo('kepala-biro/cuti/' + data) + '" class=""><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>';
                            return '' +
                                '<div class="fs-14 text-center">' +
                                view +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kh.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.status_pengajuan',
                            display: 'status_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.dari_tanggal',
                            display: 'dari_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.sampai_tanggal',
                            display: 'sampai_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.waktu_pengajuan',
                            display: 'waktu_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p1.nama',
                            display: 'atasan_1',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p2.nama',
                            display: 'atasan_2',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                order: [[3, 'desc']],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});

$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-cuti-riwayat');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepala-biro/datatables-cuti-riwayat'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            var view = '';
                            view += '    <a href="' + fn.urlTo('kepala-biro/cuti/' + data) + '" class=""><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>';
                            view += '    <a href="' + fn.urlTo('kepala-biro/cuti-print/' + data) + '" class=""><i style="width:20px;" class="fa fa-print cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Print"></i></a>';
                            return '' +
                                '<div class="fs-14 text-center">' +
                                view +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kh.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.status_pengajuan',
                            display: 'status_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.dari_tanggal',
                            display: 'dari_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.sampai_tanggal',
                            display: 'sampai_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'c.waktu_pengajuan',
                            display: 'waktu_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p1.nama',
                            display: 'atasan_1',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p2.nama',
                            display: 'atasan_2',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                order: [[3, 'desc']],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});