<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_penghargaan".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $jenis_penghargaan
 * @property string $nama_penghargaan
 * @property string $tahun_penghargaan
 * @property string $instansi_pemberi_penghargaan
 * @property string $sertifikat
 *
 * @property Pegawai $pegawai0
 */
class PegawaiPenghargaan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_sertifikat_upload;
    public $virtual_sertifikat_download;

    public static function tableName()
    {
        return 'pegawai_penghargaan';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //jenis_penghargaan
            [['jenis_penghargaan'], 'required'],
            [['jenis_penghargaan'], 'string', 'max' => 256],

            //nama_penghargaan
            [['nama_penghargaan'], 'required'],
            [['nama_penghargaan'], 'string', 'max' => 256],

            //tahun_penghargaan
            [['tahun_penghargaan'], 'required'],
            [['tahun_penghargaan'], 'safe'],

            //instansi_pemberi_penghargaan
            [['instansi_pemberi_penghargaan'], 'required'],
            [['instansi_pemberi_penghargaan'], 'string', 'max' => 256],

            //sertifikat
            [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 256],

            //sertifikat
            // [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 256],
            
            //virtual_sertifikat_download
            [['virtual_sertifikat_download'], 'safe'],
            
            //virtual_sertifikat_upload
            [['virtual_sertifikat_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_penghargaan-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_sertifikat_upload) {
            $path = Yii::getAlias('@upload-pegawai_penghargaan-sertifikat') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_sertifikat_upload->saveAs($path . '/' . $this->sertifikat);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_penghargaan-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->sertifikat) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_penghargaan-sertifikat');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_sertifikat_download = $path . '/' . $this->sertifikat;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'jenis_penghargaan' => 'Jenis Penghargaan',
            'nama_penghargaan' => 'Nama Penghargaan',
            'tahun_penghargaan' => 'Tahun Penghargaan',
            'instansi_pemberi_penghargaan' => 'Instansi Pemberi Penghargaan',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
