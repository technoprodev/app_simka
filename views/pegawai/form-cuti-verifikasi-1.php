<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FlatpickrAsset::register($this);

//
$errorMessage = '';
if ($model['cuti']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['cuti'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Verifikasi Cuti Online
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'keterangan_kehadiran', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'pegawai', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nama : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'alasan_cuti', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->alasan_cuti ? $model['cuti']->alasan_cuti : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'alamat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->alamat ? $model['cuti']->alamat : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'telp', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->telp ? $model['cuti']->telp : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->catatan_pengajuan ? $model['cuti']->catatan_pengajuan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'waktu_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->waktu_pengajuan ? $model['cuti']->waktu_pengajuan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'atasan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->atasan_1 ? $model['cuti']->atasan1->nama : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <?= $form->field($model['cuti'], 'catatan_atasan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'catatan_atasan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['cuti'], 'catatan_atasan_1', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['cuti'], 'catatan_atasan_1', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'catatan_atasan_1')->end(); ?>
                

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'atasan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->atasan_2 ? $model['cuti']->atasan2->nama : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->dari_tanggal ? $model['cuti']->dari_tanggal : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->sampai_tanggal ? $model['cuti']->sampai_tanggal : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_utama ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_utama_download . '">' . $model['cuti']->bukti_utama . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_tambahan_1 ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_tambahan_1_download . '">' . $model['cuti']->bukti_tambahan_1 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_tambahan_2 ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_tambahan_2_download . '">' . $model['cuti']->bukti_tambahan_2 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_tambahan_3 ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_tambahan_3_download . '">' . $model['cuti']->bukti_tambahan_3 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_tambahan_4 ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_tambahan_4_download . '">' . $model['cuti']->bukti_tambahan_4 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <div class="box box-break-sm">
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['cuti']->bukti_tambahan_5 ? '<a target="_blank" href="' . $model['cuti']->virtual_bukti_tambahan_5_download . '">' . $model['cuti']->bukti_tambahan_5 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                </div>

                <?= $form->field($model['cuti'], 'status_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['cuti'], 'status_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['cuti'], 'status_pengajuan', ['Diterima Atasan 1' => 'Diterima', 'Direvisi Atasan 1' => 'Direvisi', 'Ditolak Atasan 1' => 'Ditolak'], ['prompt' => '- pilih verifikasi -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['cuti'], 'status_pengajuan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['cuti'], 'status_pengajuan')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
