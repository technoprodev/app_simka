$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-bukti-kehadiran');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('administrasi/datatables-bukti-kehadiran'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            var edit = '';
                            var view = '';
                            var del = '';
                            if (row.status_pengajuan == 'Diajukan') {
                                edit = '    <a href="' + fn.urlTo('administrasi/bukti-kehadiran-update/' + data) + '" class=""><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Revisi"></i></a>';
                                del = '    <a href="' + fn.urlTo('administrasi/bukti-kehadiran-delete/' + data) + '" class="" data-confirm="Apakah Anda yakin menghapus bukti kehadiran ini ?" data-method="post"><i style="width:20px;" class="fa fa-trash-o cdexsw text-red xswzaq" data-toggle="tooltip" data-placement="top" title="Hapus Bukti Kehadiran"></i></a>';
                            } else if (row.status_pengajuan == 'Direvisi') {
                                edit = '    <a href="' + fn.urlTo('administrasi/bukti-kehadiran-update/' + data) + '" class=""><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Revisi"></i></a>';
                            } else {
                                view = '    <a href="' + fn.urlTo('administrasi/bukti-kehadiran/' + data) + '" class=""><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail"></i></a>';
                            }
                            return '' +
                                '<div class="fs-14 text-center">' +
                                edit +
                                view +
                                del +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bk.status_pengajuan',
                            display: 'status_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kh.keterangan',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bk.dari_tanggal',
                            display: 'dari_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bk.sampai_tanggal',
                            display: 'sampai_tanggal',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bk.nomor_surat',
                            display: 'nomor_surat',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bk.waktu_pengajuan',
                            display: 'waktu_pengajuan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
                order: [[5, 'desc']],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});