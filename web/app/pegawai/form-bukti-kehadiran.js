if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    }); 

    var vm = new Vue({
        el: '#app',
        data: {
            buktiKehadiran: {
                buktiKehadiranPegawais: [],
            },
        },
        methods: {
            addBuktiKehadiranPegawai: function() {
                this.buktiKehadiran.buktiKehadiranPegawais.push({
                    id: null,
                    bukti_kehadiran: null,
                    pegawai: '',
                });
            },
            removeBuktiKehadiranPegawai: function(i, isNew) {
                if (this.buktiKehadiran.buktiKehadiranPegawais[i].id == null)
                    this.buktiKehadiran.buktiKehadiranPegawais.splice(i, 1);
                else
                    this.buktiKehadiran.buktiKehadiranPegawais[i].id*=-1;
            },
        },
    });
}