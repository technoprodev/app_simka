$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-master-jenjang-jabatan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-master-jenjang-jabatan'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('kepegawaian/master-form/' + data, {tab: 'jenjang-jabatan'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Update"></i></a>' +
                                '    <a href="' + fn.urlTo('kepegawaian/master-delete/' + data, {tab: 'jenjang-jabatan'}) + '" class="inline-block" data-confirm="Apakah Anda yakin menghapus data ini ?" data-method="post"><i style="width:20px;" class="fa fa-trash-o cdexsw text-red xswzaq" data-toggle="tooltip" data-placement="bottom" title="Hapus"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.jenis_jabatan',
                            display: 'jenis_jabatan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.jenjang_jabatan',
                            display: 'jenjang_jabatan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.eselon_terendah',
                            display: 'eselon_terendah',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.eselon_tertinggi',
                            display: 'eselon_tertinggi',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.kepangkatan_terendah',
                            display: 'kepangkatan_terendah',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'm.kepangkatan_tertinggi',
                            display: 'kepangkatan_tertinggi',
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});