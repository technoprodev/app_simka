<?php
namespace app_simka\models;

use Yii;
use app_simka\models\User;

class RequestResetPassword extends \yii\base\Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'trim', 'when' => function($model) {
                return $model->email != NULL;
            }],
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'exist',
                'targetClass' => '\app_simka\models\User',
                'filter' => ['status' => '1'],
                'message' => 'User dengan email ini tidak ditemukan.'
            ],
        ];
    }

    public function sendEmail()
    {
        $user = User::find(['email' => $this->email, 'status' => 1])->one();
        if (!$user) return false;
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return \Yii::$app->mail->compose('email/request-reset-password', ['user' => $user])
            ->setFrom(['sadewa@bekraf.go.id' => 'Sadewa Bekraf (no-reply)'])
            ->setTo($this->email)
            ->setSubject('Password Reset for Sadewa Bekraf')
            ->send();
    }
}