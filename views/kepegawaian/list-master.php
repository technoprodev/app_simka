<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/kepegawaian/list-master-agama.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-bentuk-wajah.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-eselon.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-grade.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenis-hukuman-disiplin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenis-jabatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenis-pemberhentian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenis-sk.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenis-unit-kerja.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenjang-jabatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-jenjang-pendidikan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-kecamatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-kedudukan-pegawai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-kelurahan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-kepangkatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-kota-kabupaten.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-model-rambut.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-negara.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-pekerjaan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-provinsi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-status-kepegawaian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-status-marital.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-tingkat-hukuman-disiplin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-unit-kerja.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/kepegawaian/list-master-warna-kulit.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="tab-wrapper">
            <ul class="tab-menu tab-menu-selector">
                <li><a href="#agama" class="<?= $tab == 'agama' ? 'active' : '' ?>">Agama</a></li>
                <li><a href="#bentuk-wajah" class="<?= $tab == 'bentuk-wajah' ? 'active' : '' ?>">Bentuk Wajah</a></li>
                <li><a href="#eselon" class="<?= $tab == 'eselon' ? 'active' : '' ?>">Eselon</a></li>
                <li><a href="#grade" class="<?= $tab == 'grade' ? 'active' : '' ?>">Grade</a></li>
                <li><a href="#jenis-hukuman-disiplin" class="<?= $tab == 'jenis-hukuman-disiplin' ? 'active' : '' ?>">Jenis Hukuman Disiplin</a></li>
                <li><a href="#jenis-jabatan" class="<?= $tab == 'jenis-jabatan' ? 'active' : '' ?>">Jenis Jabatan</a></li>
                <li><a href="#jenis-pemberhentian" class="<?= $tab == 'jenis-pemberhentian' ? 'active' : '' ?>">Jenis Pemberhentian</a></li>
                <li><a href="#jenis-sk" class="<?= $tab == 'jenis-sk' ? 'active' : '' ?>">Jenis Sk</a></li>
                <li><a href="#jenis-unit-kerja" class="<?= $tab == 'jenis-unit-kerja' ? 'active' : '' ?>">Jenis Unit Kerja</a></li>
                <li><a href="#jenjang-jabatan" class="<?= $tab == 'jenjang-jabatan' ? 'active' : '' ?>">Jenjang Jabatan</a></li>
                <li><a href="#jenjang-pendidikan" class="<?= $tab == 'jenjang-pendidikan' ? 'active' : '' ?>">Jenjang Pendidikan</a></li>
                <li><a href="#kecamatan" class="<?= $tab == 'kecamatan' ? 'active' : '' ?>">Kecamatan</a></li>
                <li><a href="#kedudukan-pegawai" class="<?= $tab == 'kedudukan-pegawai' ? 'active' : '' ?>">Kedudukan Pegawai</a></li>
                <li><a href="#kelurahan" class="<?= $tab == 'kelurahan' ? 'active' : '' ?>">Kelurahan</a></li>
                <li><a href="#kepangkatan" class="<?= $tab == 'kepangkatan' ? 'active' : '' ?>">Kepangkatan</a></li>
                <li><a href="#kota-kabupaten" class="<?= $tab == 'kota-kabupaten' ? 'active' : '' ?>">Kota Kabupaten</a></li>
                <li><a href="#model-rambut" class="<?= $tab == 'model-rambut' ? 'active' : '' ?>">Model Rambut</a></li>
                <li><a href="#negara" class="<?= $tab == 'negara' ? 'active' : '' ?>">Negara</a></li>
                <li><a href="#pekerjaan" class="<?= $tab == 'pekerjaan' ? 'active' : '' ?>">Pekerjaan</a></li>
                <li><a href="#provinsi" class="<?= $tab == 'provinsi' ? 'active' : '' ?>">Provinsi</a></li>
                <li><a href="#status-kepegawaian" class="<?= $tab == 'status-kepegawaian' ? 'active' : '' ?>">Status Kepegawaian</a></li>
                <li><a href="#status-marital" class="<?= $tab == 'status-marital' ? 'active' : '' ?>">Status Pernikahan</a></li>
                <li><a href="#tingkat-hukuman-disiplin" class="<?= $tab == 'tingkat-hukuman-disiplin' ? 'active' : '' ?>">Tingkat Hukuman Disiplin</a></li>
                <li><a href="#unit-kerja" class="<?= $tab == 'unit-kerja' ? 'active' : '' ?>">Unit Kerja</a></li>
                <li><a href="#warna-kulit" class="<?= $tab == 'warna-kulit' ? 'active' : '' ?>">Warna Kulit</a></li>
            </ul>
            <div id="agama" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Agama</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'agama'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-agama table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Agama</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="bentuk-wajah" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Bentuk Wajah</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'bentuk-wajah'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-bentuk-wajah table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Bentuk Wajah</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="eselon" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Eselon</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'eselon'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-eselon table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Eselon</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="grade" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Grade</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'grade'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-grade table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Grade</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="jenis-hukuman-disiplin" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenis Hukuman Disiplin</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenis-hukuman-disiplin'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenis-hukuman-disiplin table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Hukuman Disiplin</th>
                                    <th>Tingkat Hukuman Disiplin</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="jenis-jabatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenis Jabatan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'd="jenis-jabatan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenis-jabatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Jabatan</th>
                                    <th>Jenjang Jabatan</th>
                                    <th>Eselon Terendah</th>
                                    <th>Eselon Tertinggi</th>
                                    <th>Kepangkatan Terendah</th>
                                    <th>Kepangkatan Tertinggi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> -->
            <div id="jenis-pemberhentian" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenis Pemberhentian</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenis-pemberhentian'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenis-pemberhentian table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Pemberhentian</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="jenis-sk" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenis Sk</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenis-sk'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenis-sk table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Sk</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="jenis-unit-kerja" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenis Unit Kerja</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenis-unit-kerja'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenis-unit-kerja table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Unit Kerja</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="jenjang-jabatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenjang Jabatan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenjang-jabatan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenjang-jabatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenis Jabatan</th>
                                    <th>Jenjang Jabatan</th>
                                    <th>Eselon Terendah</th>
                                    <th>Eselon Tertinggi</th>
                                    <th>Kepangkatan Terendah</th>
                                    <th>Kepangkatan Tertinggi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="jenjang-pendidikan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Jenjang Pendidikan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'jenjang-pendidikan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-jenjang-pendidikan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Jenjang Pendidikan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="kecamatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kecamatan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'd="kecamatan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kecamatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Kota / Kabupaten</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> -->
            <div id="kedudukan-pegawai" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kedudukan Pegawai</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'kedudukan-pegawai'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kedudukan-pegawai table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Kedudukan Pegawai</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="kelurahan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kelurahan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'd="kelurahan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kelurahan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Kecamatan</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> -->
            <div id="kepangkatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kepangkatan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kepangkatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Pangkat</th>
                                    <th>Golongan</th>
                                    <th>Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kota-kabupaten" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kota Kabupaten</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'kota-kabupaten'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kota-kabupaten table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>ID</th>
                                    <th>Kota / Kabupaten</th>
                                    <th>Provinsi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="model-rambut" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Model Rambut</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'model-rambut'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-model-rambut table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Model Rambut</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="negara" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Negara</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'negara'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-negara table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>ID</th>
                                    <th>Negara</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="pekerjaan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Pekerjaan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'pekerjaan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-pekerjaan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Pekerjaan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="provinsi" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Provinsi</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'provinsi'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-provinsi table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>ID</th>
                                    <th>Provinsi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="status-kepegawaian" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Status Kepegawaian</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'status-kepegawaian'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-status-kepegawaian table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Status Kepegawaian</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="status-marital" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Status Pernikahan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'status-marital'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-status-marital table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Status Pernikahan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="tingkat-hukuman-disiplin" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Tingkat Hukuman Disiplin</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'tingkat-hukuman-disiplin'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-tingkat-hukuman-disiplin table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Tingkat Hukuman Disiplin</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="unit-kerja" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Unit Kerja</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'unit-kerja'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-unit-kerja table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Unit Kerja</th>
                                    <th>Parent</th>
                                    <th>Jenis Unit Kerja</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="warna-kulit" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Warna Kulit</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'warna-kulit'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-warna-kulit table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Warna Kulit</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>