<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/administrasi/list-bukti-kehadiran3.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Daftar Pengajuan</span>
            <div class="pull-right">
                <?= Html::a('Unggah Bukti Kehadiran', ['bukti-kehadiran-create'], ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-bukti-kehadiran table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Status Pengajuan</th>
                        <th>Keterangan Kehadiran</th>
                        <th>Dari Tanggal</th>
                        <th>Sampai Tanggal</th>
                        <th>Nomor Surat</th>
                        <th>Waktu Pengajuan</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status pengajuan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan kehadiran..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search dari tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search sampai tanggal..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor surat..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search waktu pengajuan..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>