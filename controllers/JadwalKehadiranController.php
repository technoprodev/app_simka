<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\JadwalKehadiran;
use app_simka\models\Kehadiran;

class JadwalKehadiranController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kepegawaian']]
            ]),
        ];
    }

    public function actionDatatablesJadwalKehadiran()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'jk.id',
                'DATE_FORMAT(jk.berlaku_dari, "%d/%m/%Y") AS berlaku_dari',
                'DATE_FORMAT(jk.berlaku_sampai, "%d/%m/%Y") AS berlaku_sampai',
                'jk.catatan',
                'jk.berlaku_bagi',
                'jk.unit_kerja',
                'jk.pegawai',
            ])
            ->from('jadwal_kehadiran jk')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list-jadwal-kehadiran', [
                'title' => 'Rule Jadwal Kehadiran',
            ]);
        }

        if (($model['pegawai'] = JadwalKehadiran::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-jadwal-kehadiran', [
            'model' => $model,
            'title' => 'Detail Jadwal Kehadiran',
        ]);
    }

    public function actionJadwalKehadiranCreate()
    {
        $error = true;

        if (isset($id)) {
            if (($model['jadwal_kehadiran'] = JadwalKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['jadwal_kehadiran'] = new JadwalKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jadwal_kehadiran']->load($post);

            $transaction['jadwal_kehadiran'] = JadwalKehadiran::getDb()->beginTransaction();

            try {
                if (!$model['jadwal_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $result = \Yii::$app->db->createCommand("call apply_jk_global(NULL);")
                    ->execute();
                
                $error = false;

                $transaction['jadwal_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['jadwal_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['jadwal_kehadiran']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-jadwal-kehadiran', [
                'model' => $model,
                'title' => 'Tambah Rule Jadwal Kehadiran',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionJadwalKehadiranUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['jadwal_kehadiran'] = JadwalKehadiran::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['jadwal_kehadiran'] = new JadwalKehadiran();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['jadwal_kehadiran']->load($post);

            $transaction['jadwal_kehadiran'] = JadwalKehadiran::getDb()->beginTransaction();

            try {
                if (!$model['jadwal_kehadiran']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }

                $result = \Yii::$app->db->createCommand("call apply_jk_global(NULL);")
                    ->execute();
                
                $error = false;

                $transaction['jadwal_kehadiran']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['jadwal_kehadiran']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
            if ($model['jadwal_kehadiran']->isNewRecord) {
            }
        }

        if ($error)
            return $this->render('form-jadwal-kehadiran', [
                'model' => $model,
                'title' => 'Update Rule Jadwal Kehadiran',
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        if (($model['jadwal_kehadiran'] = JadwalKehadiran::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $model['jadwal_kehadiran']->delete();

        return $this->redirect(['index']);
    }
}
