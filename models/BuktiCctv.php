<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "bukti_cctv".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $status_pengajuan
 * @property integer $diajukan
 * @property string $waktu_pengajuan
 * @property string $catatan_pengajuan
 * @property integer $disetujui
 * @property string $waktu_disetujui
 * @property string $catatan_verifikasi
 * @property string $jenis
 * @property string $tanggal
 * @property string $waktu
 * @property string $bukti_utama
 * @property string $bukti_tambahan_1
 * @property string $bukti_tambahan_2
 * @property string $bukti_tambahan_3
 * @property string $bukti_tambahan_4
 * @property string $bukti_tambahan_5
 *
 * @property Pegawai $pegawai0
 * @property Pegawai $diajukan0
 * @property Pegawai $disetujui0
 */
class BuktiCctv extends \technosmart\yii\db\ActiveRecord
{
    public $virtual_bukti_utama_upload;
    public $virtual_bukti_utama_download;
    public $virtual_bukti_tambahan_1_upload;
    public $virtual_bukti_tambahan_1_download;
    public $virtual_bukti_tambahan_2_upload;
    public $virtual_bukti_tambahan_2_download;
    public $virtual_bukti_tambahan_3_upload;
    public $virtual_bukti_tambahan_3_download;
    public $virtual_bukti_tambahan_4_upload;
    public $virtual_bukti_tambahan_4_download;
    public $virtual_bukti_tambahan_5_upload;
    public $virtual_bukti_tambahan_5_download;

    public static function tableName()
    {
        return 'bukti_cctv';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //status_pengajuan
            [['status_pengajuan'], 'required'],
            [['status_pengajuan'], 'string'],

            //diajukan
            [['diajukan'], 'required'],
            [['diajukan'], 'integer'],
            [['diajukan'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['diajukan' => 'id']],

            //waktu_pengajuan
            [['waktu_pengajuan'], 'required'],
            [['waktu_pengajuan'], 'safe'],

            //catatan_pengajuan
            [['catatan_pengajuan'], 'string', 'max' => 256],

            //disetujui
            [['disetujui'], 'integer'],
            [['disetujui'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['disetujui' => 'id']],

            //waktu_disetujui
            [['waktu_disetujui'], 'safe'],

            //catatan_verifikasi
            [['catatan_verifikasi'], 'string', 'max' => 256],

            //jenis
            [['jenis'], 'required'],
            [['jenis'], 'string'],

            //tanggal
            [['tanggal'], 'required'],
            [['tanggal'], 'safe'],

            //waktu
            [['waktu'], 'required'],
            [['waktu'], 'safe'],

            //bukti_utama
            [['bukti_utama'], 'required'],
            [['bukti_utama'], 'string', 'max' => 256],

            //bukti_tambahan_1
            [['bukti_tambahan_1'], 'string', 'max' => 256],

            //bukti_tambahan_2
            [['bukti_tambahan_2'], 'string', 'max' => 256],

            //bukti_tambahan_3
            [['bukti_tambahan_3'], 'string', 'max' => 256],

            //bukti_tambahan_4
            [['bukti_tambahan_4'], 'string', 'max' => 256],

            //bukti_tambahan_5
            [['bukti_tambahan_5'], 'string', 'max' => 256],
            
            //virtual_bukti_utama_download
            [['virtual_bukti_utama_download'], 'safe'],
            
            //virtual_bukti_utama_upload
            [['virtual_bukti_utama_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_1_download
            [['virtual_bukti_tambahan_1_download'], 'safe'],
            
            //virtual_bukti_tambahan_1_upload
            [['virtual_bukti_tambahan_1_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_2_download
            [['virtual_bukti_tambahan_2_download'], 'safe'],
            
            //virtual_bukti_tambahan_2_upload
            [['virtual_bukti_tambahan_2_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_3_download
            [['virtual_bukti_tambahan_3_download'], 'safe'],
            
            //virtual_bukti_tambahan_3_upload
            [['virtual_bukti_tambahan_3_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_4_download
            [['virtual_bukti_tambahan_4_download'], 'safe'],
            
            //virtual_bukti_tambahan_4_upload
            [['virtual_bukti_tambahan_4_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
            
            //virtual_bukti_tambahan_5_download
            [['virtual_bukti_tambahan_5_download'], 'safe'],
            
            //virtual_bukti_tambahan_5_upload
            [['virtual_bukti_tambahan_5_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_bukti_utama_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_utama_upload');
        if ($this->virtual_bukti_utama_upload) {
            $this->bukti_utama = $this->virtual_bukti_utama_upload->name;
        }

        $this->virtual_bukti_tambahan_1_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_1_upload');
        if ($this->virtual_bukti_tambahan_1_upload) {
            $this->bukti_tambahan_1 = $this->virtual_bukti_tambahan_1_upload->name;
        }

        $this->virtual_bukti_tambahan_2_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_2_upload');
        if ($this->virtual_bukti_tambahan_2_upload) {
            $this->bukti_tambahan_2 = $this->virtual_bukti_tambahan_2_upload->name;
        }

        $this->virtual_bukti_tambahan_3_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_3_upload');
        if ($this->virtual_bukti_tambahan_3_upload) {
            $this->bukti_tambahan_3 = $this->virtual_bukti_tambahan_3_upload->name;
        }

        $this->virtual_bukti_tambahan_4_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_4_upload');
        if ($this->virtual_bukti_tambahan_4_upload) {
            $this->bukti_tambahan_4 = $this->virtual_bukti_tambahan_4_upload->name;
        }

        $this->virtual_bukti_tambahan_5_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_5_upload');
        if ($this->virtual_bukti_tambahan_5_upload) {
            $this->bukti_tambahan_5 = $this->virtual_bukti_tambahan_5_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_bukti_utama_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_utama_upload');
        if ($this->virtual_bukti_utama_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_utama') . '/' . $this->id . '/' . $this->bukti_utama;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_utama = $this->virtual_bukti_utama_upload->name;
        }

        $this->virtual_bukti_tambahan_1_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_1_upload');
        if ($this->virtual_bukti_tambahan_1_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_1') . '/' . $this->id . '/' . $this->bukti_tambahan_1;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_1 = $this->virtual_bukti_tambahan_1_upload->name;
        }

        $this->virtual_bukti_tambahan_2_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_2_upload');
        if ($this->virtual_bukti_tambahan_2_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_2') . '/' . $this->id . '/' . $this->bukti_tambahan_2;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_2 = $this->virtual_bukti_tambahan_2_upload->name;
        }

        $this->virtual_bukti_tambahan_3_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_3_upload');
        if ($this->virtual_bukti_tambahan_3_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_3') . '/' . $this->id . '/' . $this->bukti_tambahan_3;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_3 = $this->virtual_bukti_tambahan_3_upload->name;
        }

        $this->virtual_bukti_tambahan_4_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_4_upload');
        if ($this->virtual_bukti_tambahan_4_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_4') . '/' . $this->id . '/' . $this->bukti_tambahan_4;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_4 = $this->virtual_bukti_tambahan_4_upload->name;
        }

        $this->virtual_bukti_tambahan_5_upload = \yii\web\UploadedFile::getInstance($this, 'virtual_bukti_tambahan_5_upload');
        if ($this->virtual_bukti_tambahan_5_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_5') . '/' . $this->id . '/' . $this->bukti_tambahan_5;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->bukti_tambahan_5 = $this->virtual_bukti_tambahan_5_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_bukti_utama_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_utama') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_utama_upload->saveAs($path . '/' . $this->bukti_utama);
        }
        
        if ($this->virtual_bukti_tambahan_1_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_1') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_1_upload->saveAs($path . '/' . $this->bukti_tambahan_1);
        }
        
        if ($this->virtual_bukti_tambahan_2_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_2') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_2_upload->saveAs($path . '/' . $this->bukti_tambahan_2);
        }
        
        if ($this->virtual_bukti_tambahan_3_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_3') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_3_upload->saveAs($path . '/' . $this->bukti_tambahan_3);
        }
        
        if ($this->virtual_bukti_tambahan_4_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_4') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_4_upload->saveAs($path . '/' . $this->bukti_tambahan_4);
        }
        
        if ($this->virtual_bukti_tambahan_5_upload) {
            $path = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_5') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_bukti_tambahan_5_upload->saveAs($path . '/' . $this->bukti_tambahan_5);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_utama') . '/' . $this->id . '/' . $this->bukti_utama;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_1') . '/' . $this->id . '/' . $this->bukti_tambahan_1;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_2') . '/' . $this->id . '/' . $this->bukti_tambahan_2;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_3') . '/' . $this->id . '/' . $this->bukti_tambahan_3;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_4') . '/' . $this->id . '/' . $this->bukti_tambahan_4;
        if (is_file($filePath)) unlink($filePath);
        
        $filePath = Yii::getAlias('@upload-bukti_cctv-bukti_tambahan_5') . '/' . $this->id . '/' . $this->bukti_tambahan_5;
        if (is_file($filePath)) unlink($filePath);

        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->bukti_utama) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_utama');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_utama_download = $path . '/' . $this->bukti_utama;
        }
        
        if($this->bukti_tambahan_1) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_tambahan_1');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_1_download = $path . '/' . $this->bukti_tambahan_1;
        }
        
        if($this->bukti_tambahan_2) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_tambahan_2');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_2_download = $path . '/' . $this->bukti_tambahan_2;
        }
        
        if($this->bukti_tambahan_3) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_tambahan_3');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_3_download = $path . '/' . $this->bukti_tambahan_3;
        }
        
        if($this->bukti_tambahan_4) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_tambahan_4');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_4_download = $path . '/' . $this->bukti_tambahan_4;
        }
        
        if($this->bukti_tambahan_5) {
            $downloadBaseUrl = Yii::getAlias('@download-bukti_cctv-bukti_tambahan_5');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_bukti_tambahan_5_download = $path . '/' . $this->bukti_tambahan_5;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'status_pengajuan' => 'Status Pengajuan',
            'diajukan' => 'Diajukan',
            'waktu_pengajuan' => 'Waktu Pengajuan',
            'catatan_pengajuan' => 'Catatan Pengajuan',
            'disetujui' => 'Disetujui',
            'waktu_disetujui' => 'Waktu Disetujui',
            'catatan_verifikasi' => 'Catatan Verifikasi',
            'jenis' => 'Jenis',
            'tanggal' => 'Tanggal',
            'waktu' => 'Waktu',
            'bukti_utama' => 'Bukti Utama',
            'bukti_tambahan_1' => 'Bukti Tambahan 1',
            'bukti_tambahan_2' => 'Bukti Tambahan 2',
            'bukti_tambahan_3' => 'Bukti Tambahan 3',
            'bukti_tambahan_4' => 'Bukti Tambahan 4',
            'bukti_tambahan_5' => 'Bukti Tambahan 5',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiajukan0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'diajukan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisetujui0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'disetujui']);
    }
}
