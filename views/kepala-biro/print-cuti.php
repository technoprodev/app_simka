<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$generator = (new Picqer\Barcode\BarcodeGeneratorHTML());
?>

<style type="text/css">
    .table { empty-cells: show; }
    .table th,
    .table td {
        padding: 1px 5px;
    }
    .table tr:not(:first-child) {
        border: 0;
    }
</style>
<div>
    <h2 class="text-center">
        KEMENTERIAN PARIWISATA DAN EKONOMI KREATIF/<br>
        BADAN PARIWISATA DAN EKONOMI KREATIF
    </h2>

    <div class="text-center">Jalan Medan Merdeka Barat Nomor 17, Jakarta 10110</div>
    <div class="text-center">Telepon (021) 3838120, 3838167; Faksimile : (021) 3848245, 3840210;</div>

    <hr>

    <div class="fw-bold underline text-center">SURAT IZIN <?= strtoupper($model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : 'Cuti') ?></div>
    <div class="text-center">Nomor:<?= $model['cuti']->nomor_surat ?></div>

    <div class="margin-top-15"></div>
    <p class="margin-0">1. Diberikan <?= $model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : 'Cuti' ?> <?= substr($model['cuti']->sampai_tanggal, 6) ?> kepada:</p>
    <div class="margin-left-15">
        <table class="margin-0">
            <tbody>
                <tr>
                    <td class="padding-0">Nama</td>
                    <td class="padding-0">: <?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nama : '(kosong)' ?></td>
                </tr>
                <tr>
                    <td class="padding-0">NIP</td>
                    <td class="padding-0">: <?= $model['cuti']->pegawai && $model['cuti']->pegawai0->nip ? $model['cuti']->pegawai0->nip : '(kosong)' ?></td>
                </tr>
                <tr>
                    <td class="padding-0">Pangkat/Gol</td>
                    <td class="padding-0">: <?= $model['cuti']->pegawai && $model['cuti']->pegawai0->kepangkatan ? $model['cuti']->pegawai0->kepangkatan0->pangkat : '(kosong)' ?></td>
                </tr>
                <tr>
                    <td class="padding-0">Jabatan</td>
                    <td class="padding-0">: <?= $model['cuti']->pegawai && $model['cuti']->pegawai0->nama_jabatan ? $model['cuti']->pegawai0->nama_jabatan : '(kosong)' ?></td>
                </tr>
                <tr>
                    <td class="padding-0">Satuan Organisasi</td>
                    <td class="padding-0">: <?= $model['cuti']->pegawai && $model['cuti']->pegawai0->unit_kerja ? $model['cuti']->pegawai0->unitKerja->unit_kerja : '(kosong)' ?></td>
                </tr>
            </tbody>
        </table>
        <p>
            Selama <?= $model['cuti']->dari_tanggal && $model['cuti']->sampai_tanggal ? ((strtotime(str_replace('/', '-', $model['cuti']->sampai_tanggal)) - strtotime(str_replace('/', '-', $model['cuti']->dari_tanggal))) / 60 / 60 /24 + 1) . '' : '.......' ?> hari kerja, terhitung mulai tanggal <?= $model['cuti']->dari_tanggal ? $model['cuti']->dari_tanggal : '.......' ?> sampai dengan <?= $model['cuti']->sampai_tanggal ? $model['cuti']->sampai_tanggal : '.......' ?>, dengan ketentuan sebagai berikut:
            <br>
            a. Sebelum menjalankan <?= $model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : 'Cuti' ?> wajib menyerahkan pekerjaannya kepada atasan langsung;
            <br>
            b. Setelah selesai menjalankan <?= $model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : 'Cuti' ?>, wajib melaporkan diri kepada atasan langsungnya dan bekerja kembali sebagaimana mestinya.
        </p>
    </div>
    <p>2. Demikian surat izin <?= $model['cuti']->keterangan_kehadiran ? $model['cuti']->keteranganKehadiran->keterangan : 'Cuti' ?> ini dibuat untuk dapat digunakan sebagaimana semestinya.</p>

    <div class="margin-top-80"></div>
    <div class="margin-left-300">
        <div class="margin-left-100">
            <p>
                Jakarta, <?= date('d/m/Y', strtotime(str_replace('/', '-', $model['cuti']->waktu_disetujui))) ?><br>
                Kepala Biro Umum dan Kepegawaian,
            </p>
            <div class="">
                <?= $generator->getBarcode('7225', $generator::TYPE_CODE_128) ?>
            </div>
            <div class="margin-top-15"></div>
            <p>
                Dr. Anggara Hayun Anujuprana, ST., MT. <br>
                NIP 197704052003121003
            </p>
        </div>
    </div>
</div>
<div class="margin-top-300"></div>
<div>
    <div class="margin-left-250">
        <div class="margin-left-100">
            <div>Jakarta, <?= date('d/m/Y', strtotime(str_replace('/', '-', $model['cuti']->waktu_disetujui))) ?></div>
            <div>Kepada</div>
            <div>Yth. Sekretaris Utama dan Kepala Biro Umum Kepegawaian</div>
            <div>Bekraf</div>
        </div>
    </div>
    <div class="margin-top-20"></div>

    <div class="fs-16 fw-bold text-center">
        FORMULIR PERMINTAAN DAN PEMBERIAN CUTI
    </div>
    <div class="margin-y-10"></div>

    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">I. DATA PEGAWAI</td>
            </tr>
            <tr>
                <td style="width:120px;">Nama</td>
                <td class="fw-bold"><?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nama : '(kosong)' ?></td>
                <td style="width:120px;">NIP</td>
                <td class="fw-bold"><?= $model['cuti']->pegawai && $model['cuti']->pegawai0->nip ? $model['cuti']->pegawai0->nip : '(kosong)' ?></td>
            </tr>
            <tr>
                <td style="width:120px;">Jabatan</td>
                <td class="fw-bold"><?= $model['cuti']->pegawai && $model['cuti']->pegawai0->nama_jabatan ? $model['cuti']->pegawai0->nama_jabatan : '(kosong)' ?></td>
                <td style="width:120px;">Masa Kerja</td>
                <td class="fw-bold"><?= $model['cuti']->pegawai && $model['cuti']->pegawai0->nip ?
                    (function($nip){
                        $month1 = substr($nip, 12, 2);
                        $year1 = substr($nip, 8, 4);

                        $month2 = date('m');
                        $year2 = date('Y');

                        return ($year2 - $year1) . ' tahun, ' . ($month2 - $month1) . ' bulan';
                    })($model['cuti']->pegawai0->nip)
                    : '(kosong)' ?>
                </td>
            </tr>
            <tr>
                <td style="width:120px;">Unit Kerja</td>
                <td colspan="3" class="fw-bold"><?= $model['cuti']->pegawai && $model['cuti']->pegawai0->unit_kerja ? $model['cuti']->pegawai0->unitKerja->unit_kerja : '(kosong)' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">II. JENIS CUTI YANG DIAMBIL</td>
            </tr>
            <tr>
                <td colspan="1" class="fw-bold">1. Cuti Tahunan</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti Tahunan' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
                <td colspan="1" class="fw-bold">2. Cuti Sakit</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti Sakit' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
            </tr>
            <tr>
                <td colspan="1" class="fw-bold">3. Cuti Besar</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti Besar' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
                <td colspan="1" class="fw-bold">4. Cuti Melahirkan</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti Melahirkan' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
            </tr>
            <tr>
                <td colspan="1" class="fw-bold">5. Cuti Karena Alasan Penting</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti Karena Alasan Penting' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
                <td colspan="1" class="fw-bold">6. Cuti di Luar Tanggungan Negara</td>
                <td colspan="1" class="fw-bold"><?= $model['cuti']->keterangan_kehadiran && $model['cuti']->keteranganKehadiran->jenis == 'Cuti di Luar Tanggungan Negara' ?  ('<img src="' . $this->render('img-checklist') . '">') : '' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">III. ALASAN CUTI</td>
            </tr>
            <tr>
                <td colspan="4" class="fw-bold"><?= $model['cuti']->alasan_cuti ? $model['cuti']->alasan_cuti : '(kosong)' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">IV. LAMANYA CUTI</td>
            </tr>
            <tr>
                <td style="width:80px;">Selama</td>
                <td class="fw-bold"><?= $model['cuti']->dari_tanggal && $model['cuti']->sampai_tanggal ? ((strtotime(str_replace('/', '-', $model['cuti']->sampai_tanggal)) - strtotime(str_replace('/', '-', $model['cuti']->dari_tanggal))) / 60 / 60 /24 + 1) . ' hari' : '(kosong)' ?></td>
                <td style="width:90px;">Mulai Tanggal</td>
                <td class="fw-bold"><?= $model['cuti']->dari_tanggal ? $model['cuti']->dari_tanggal : '(kosong)' ?></td>
                <td style="width:70px;">S/D</td>
                <td class="fw-bold"><?= $model['cuti']->sampai_tanggal ? $model['cuti']->sampai_tanggal : '(kosong)' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="3" class="fs-14 fw-bold text-left">V. CATATAN CUTI</td>
            </tr>
            <tr>
                <td colspan="3" class="fw-bold">CUTI TAHUNAN</td>
            </tr>
            <tr>
                <td style="width:120px;">Tahun</td>
                <td style="width:120px;">Sisa</td>
                <td>Keterangan</td>
            </tr>
            <tr>
                <td style="width:120px;">N</td>
                <td style="width:120px;" class="fw-bold"><?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->cuti_N : '(kosong)' ?></td>
                <td rowspan="3">
                    <?= $model['cuti']->catatan_pengajuan ?>
                </td>
            </tr>
            <tr>
                <td style="width:120px;">N-1</td>
                <td style="width:120px;" class="fw-bold"><?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->cuti_N_1 : '(kosong)' ?></td>
            </tr>
            <tr>
                <td style="width:120px;">N-2</td>
                <td style="width:120px;" class="fw-bold"><?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->cuti_N_2 : '(kosong)' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="3" class="fs-14 fw-bold text-left">VI. ALAMAT SELAMA MENJALANKAN CUTI</td>
            </tr>
            <tr>
                <td colspan="2" class="fw-bold"><?= $model['cuti']->alamat ? $model['cuti']->alamat : '(kosong)' ?></td>
                <td rowspan="2" style="width:240px;" class="text-center">
                    <div>Hormat Saya</div>
                    <div class="margin-left-60 margin-y-15">
                        <div class="">
                            <?= $model['cuti']->pegawai && $model['cuti']->pegawai0->pin ? $generator->getBarcode($model['cuti']->pegawai0->pin, $generator::TYPE_CODE_128) : false ?>
                            
                        </div>
                    </div>
                    <div class="fw-bold">(<?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nama : 'kosong' ?>)</div>
                    <div class="fw-bold">NIP <?= $model['cuti']->pegawai ? $model['cuti']->pegawai0->nip : '(kosong)' ?></div>
                </td>
            </tr>
            <tr>
                <td style="width:120px;">Telp</td>
                <td class="fw-bold"><?= $model['cuti']->telp ? $model['cuti']->telp : '(kosong)' ?></td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">VII. PERTIMBANGAN ATASAN LANGSUNG</td>
            </tr>
            <tr class="text-center">
                <td>DISETUJUI</td>
                <td>PERUBAHAN</td>
                <td>DITANGGUHKAN</td>
                <td>TIDAK DISETUJUI</td>
            </tr>
            <tr class="text-center">
                <td><img src="<?= $this->render('img-checklist'); ?>"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">
                    <?= $model['cuti']->catatan_atasan_1 ?>
                </td>
                <td class="text-center" style="width:240px;">
                    <div>Atasan yang Menyetujui</div>
                    <div class="margin-left-60 margin-y-15">
                        <div class="">
                            <?= $model['cuti']->atasan_1 && $model['cuti']->atasan1->pin ? $generator->getBarcode($model['cuti']->atasan1->pin, $generator::TYPE_CODE_128) : false ?>
                            
                        </div>
                    </div>
                    <div class="fw-bold"><?= $model['cuti']->atasan_1 ? $model['cuti']->atasan1->nama : '(kosong)' ?></div>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">VIII. KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI</td>
            </tr>
            <tr class="text-center">
                <td>DISETUJUI</td>
                <td>PERUBAHAN</td>
                <td>DITANGGUHKAN</td>
                <td>TIDAK DISETUJUI</td>
            </tr>
            <tr class="text-center">
                <td><img src="<?= $this->render('img-checklist'); ?>"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">
                    <?= $model['cuti']->catatan_atasan_2 ?>
                </td>
                <td class="text-center" style="width:240px;">
                    <div>Pejabat yang Berwenang Memberi Cuti</div>
                    <div class="margin-left-60 margin-y-15">
                        <div class="margin-left-20">
                            <?= $model['cuti']->atasan_1 && $model['cuti']->atasan2->pin ? $generator->getBarcode($model['cuti']->atasan2->pin, $generator::TYPE_CODE_128) : false ?>
                            
                        </div>
                    </div>
                    <div class="fw-bold"><?= $model['cuti']->atasan_2 ? $model['cuti']->atasan2->nama : '(kosong)' ?></div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<?php if (false) : ?>
<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script type="text/javascript">
    JsBarcode('#pin1', <?= $model['cuti']->atasan_1 && $model['cuti']->atasan1->pin ? $model['cuti']->atasan1->pin : '' ?>, {height: '90px', displayValue: false});
    JsBarcode('#pin2', <?= $model['cuti']->atasan_2 && $model['cuti']->atasan2->pin ? $model['cuti']->atasan2->pin : '' ?>, {height: '90px', displayValue: false});
</script>
<?php endif; ?>
