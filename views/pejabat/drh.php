<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/pejabat/list-pegawai-jabatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-kepangkatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-kenaikan-gaji-berkala.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-pelaksana.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-penetapan-angka-kredit.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-orang-tua.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-saudara-kandung.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-pasangan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-anak.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-pendidikan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-penghargaan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-diklat.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-kursus-pelatihan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-kunjungan-luar-negeri.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-organisasi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-hukuman-disiplin.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-pemberhentian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/pejabat/list-pegawai-masa-persiapan-pensiun.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$this->registerJs(
    'var idPegawai = ' . json_encode($model['pegawai']->id) . ';' .
    '',
    3
);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-2 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="text-center">
            <?php if ($model['pegawai']->poto) : ?>
                <img src="<?= $model['pegawai']->virtual_poto_download ?>" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Laki-laki') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/male.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php elseif ($model['pegawai']->jenis_kelamin == 'Perempuan') : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/female.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php else : ?>
                <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/avatar.png" style="width:100%; max-width:130px;" class="border-lighter padding-5 rounded-xs">
            <?php endif; ?>
        </div>
        <div class="margin-top-5"></div>
        <div class="text-center">
            <span class="text-azure fs-19 fw-bold"><?= $model['pegawai']->nama ? $model['pegawai']->nama : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">NIP</span><br>
            <span class="text-dark fs-14"><?= $model['pegawai']->nip ? $model['pegawai']->nip : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Telpon</span><br>
            <span class="text-dark"><?= $model['pegawai']->telpon ? $model['pegawai']->telpon : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Kantor</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_kantor ? $model['pegawai']->email_kantor : '(belum diisi)' ?></span>
        </div>
        <hr class="margin-y-5 border-top border-light-orange">
        <div class="text-center">
            <span class="text-gray fs-12">Email Pribadi</span><br>
            <span class="text-dark"><?= $model['pegawai']->email_pribadi ? $model['pegawai']->email_pribadi : '(belum diisi)' ?></span>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Personal
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama ? $model['pegawai']->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Panggilan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_panggilan ? $model['pegawai']->nama_panggilan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenis Kelamin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenis_kelamin ? $model['pegawai']->jenis_kelamin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">TTL</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->tempat_lahir ? $model['pegawai']->tempat_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>, <?= $model['pegawai']->tanggal_lahir ? $model['pegawai']->tanggal_lahir : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Agama</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->agama ? $model['pegawai']->agama0->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Status Pernikahan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->status_marital ? $model['pegawai']->statusMarital->status_marital : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Golongan Darah</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->golongan_darah ? $model['pegawai']->golongan_darah : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <!--  -->
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Pendidikan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenjang_pendidikan_terakhir ? $model['pegawai']->jenjangPendidikanTerakhir->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Gelar</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= !$model['pegawai']->gelar_depan && !$model['pegawai']->gelar_belakang ? '-' : '' ?>
                        <?= $model['pegawai']->gelar_depan ? $model['pegawai']->gelar_depan : '' ?>
                        <?= $model['pegawai']->gelar_depan && $model['pegawai']->gelar_belakang ? ' & ' : '' ?>
                        <?= $model['pegawai']->gelar_belakang ? $model['pegawai']->gelar_belakang : '' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Tinggi / Berat</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->tinggi_badan_cm ? $model['pegawai']->tinggi_badan_cm . ' cm' : '<span class="text-grayer">(belum diisi)</span>' ?> / <?= $model['pegawai']->berat_badan_kg ? $model['pegawai']->berat_badan_kg . ' kg' : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Model Rambut</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->model_rambut ? $model['pegawai']->modelRambut->model_rambut : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Bentuk Wajah</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->bentuk_wajah ? $model['pegawai']->bentukWajah->bentuk_wajah : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Warna Kulit</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->warna_kulit ? $model['pegawai']->warnaKulit->warna_kulit : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Hobi</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->hobi ? $model['pegawai']->hobi : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kemampuan Berbahasa</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kemampuan_berbahasa ? $model['pegawai']->kemampuan_berbahasa : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-5 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Data Pegawai
            </div>
            <hr class="margin-y-10 border-top border-light-orange">
            
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NIP</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nip ? $model['pegawai']->nip : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Pin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->pin ? $model['pegawai']->pin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kepangkatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Jenjang Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Nama Jabatan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php
                $bottomToTop = [];
                $uk = $model['pegawai']->unitKerja;
                $i = 0;
                while ($uk && $i <= 10) {
                    $bottomToTop[] = [
                        'jenisUnitKerja' => $uk->jenis_unit_kerja ? $uk->jenisUnitKerja->jenis_unit_kerja : '',
                        'unitKerja' => $uk->unit_kerja,
                    ];
                    $uk = $uk->parent0;
                    $i++;
                }

                $topToBottom = [];
                for ($i = count($bottomToTop)-1; $i >= 0; $i--) { 
                    $topToBottom[] = $bottomToTop[$i];
                }
            ?>
            <?php foreach ($topToBottom as $key => $value) : ?>
            <?php if ($value['jenisUnitKerja'] != 'Staf') :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer"><?= $value['jenisUnitKerja'] ?></div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $value['unitKerja'] ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php if (!$topToBottom) :?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Satuan Organisasi</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <span class="text-grayer">(belum diisi)</span>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <?php endif; ?>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Eselon</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->eselon ? $model['pegawai']->eselon0->eselon : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Grade</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->grade ? $model['pegawai']->grade0->grade : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Kedudukan</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->kedudukan_pegawai ? $model['pegawai']->kedudukanPegawai->kedudukan_pegawai : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Mendapat Tunkin</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->mendapat_tunkin ? $model['pegawai']->mendapat_tunkin : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <!--  -->
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NIK</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->nik ? $model['pegawai']->nik : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">NPWP</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->npwp ? $model['pegawai']->npwp : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Karpeg</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->karpeg ? $model['pegawai']->karpeg : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Karis / Karsu</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->karis_atau_karsu ? $model['pegawai']->karis_atau_karsu : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
            <div class="box box-break-sm">
                <div class="box-3 padding-x-0 text-right m-text-left padding-y-5">
                    <div class="text-grayer">Taspen</div>
                </div>
                <div class="box-9 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['pegawai']->taspen ? $model['pegawai']->taspen : '<span class="text-grayer">(belum diisi)</span>' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="tab-wrapper">
            <ul class="tab-menu tab-menu-selector">
                <li><a href="#jabatan" class="<?= $tab == 'jabatan' ? 'active' : '' ?>">Jabatan</a></li>
                <li><a href="#kepangkatan" class="<?= $tab == 'kepangkatan' ? 'active' : '' ?>">Kepangkatan</a></li>
                <li><a href="#kenaikan-gaji-berkala" class="<?= $tab == 'kenaikan-gaji-berkala' ? 'active' : '' ?>">Kenaikan Gaji Berkala</a></li>
                <!-- <li><a href="#pelaksana" class="<?= $tab == 'pelaksana' ? 'active' : '' ?>">Pelaksana</a></li> -->
                <li><a href="#penetapan-angka-kredit" class="<?= $tab == 'penetapan-angka-kredit' ? 'active' : '' ?>">Penetapan Angka Kredit</a></li>
                <li><a href="#orang-tua" class="<?= $tab == 'orang-tua' ? 'active' : '' ?>">Orang Tua</a></li>
                <li><a href="#saudara-kandung" class="<?= $tab == 'saudara-kandung' ? 'active' : '' ?>">Saudara Kandung</a></li>
                <li><a href="#pasangan" class="<?= $tab == 'pasangan' ? 'active' : '' ?>">Suami / Istri</a></li>
                <li><a href="#anak" class="<?= $tab == 'anak' ? 'active' : '' ?>">Anak</a></li>
                <li><a href="#pendidikan" class="<?= $tab == 'pendidikan' ? 'active' : '' ?>">Pendidikan</a></li>
                <li><a href="#penghargaan" class="<?= $tab == 'penghargaan' ? 'active' : '' ?>">Penghargaan</a></li>
                <li><a href="#diklat" class="<?= $tab == 'diklat' ? 'active' : '' ?>">Diklat</a></li>
                <li><a href="#kursus-pelatihan" class="<?= $tab == 'kursus-pelatihan' ? 'active' : '' ?>">Kursus Pelatihan</a></li>
                <!-- <li><a href="#kunjungan-luar-negeri" class="<?= $tab == 'kunjungan-luar-negeri' ? 'active' : '' ?>">Kunjungan Luar Negeri</a></li> -->
                <li><a href="#organisasi" class="<?= $tab == 'organisasi' ? 'active' : '' ?>">Organisasi</a></li>
                <li><a href="#hukuman-disiplin" class="<?= $tab == 'hukuman-disiplin' ? 'active' : '' ?>">Hukuman Disiplin</a></li>
                <li><a href="#pemberhentian" class="<?= $tab == 'pemberhentian' ? 'active' : '' ?>">Pemberhentian</a></li>
                <li><a href="#masa-persiapan-pensiun" class="<?= $tab == 'masa-persiapan-pensiun' ? 'active' : '' ?>">Masa Persiapan Pensiun</a></li>
            </ul>
            <div id="jabatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Jabatan Terakhir
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Jenjang Jabatan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenisJabatan->jenis_jabatan . ' - ' . $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Nama Jabatan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Unit Kerja</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->unit_kerja ? $model['pegawai']->unitKerja->unit_kerja : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                    </div>
                    
                    <div class="margin-top-30"></div>
                    
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Jabatan
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-jabatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Instansi</th>
                                    <th>Unit Kerja</th>
                                    <th>Jenjang Jabatan</th>
                                    <th>Nama Jabatan</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Terhitung Sampai Tanggal</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kepangkatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Kepangkatan Terakhir
                        </div>

                        <hr class="margin-y-10 border-top border-light-orange">

                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Pangkat</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Golongan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->golongan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Ruang</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->ruang : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                    </div>

                    <div class="margin-top-30"></div>

                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Kepangkatan
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-kepangkatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Pangkat</th>
                                    <th>Golongan</th>
                                    <th>Ruang</th>
                                    <th>Status Kepegawaian</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Terhitung Sampai Tanggal</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kenaikan-gaji-berkala" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Kenaikan Gaji Berkala
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-kenaikan-gaji-berkala table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Gapok Lama</th>
                                    <th>Gapok Baru</th>
                                    <th>Kepangkatan Saat Kenaikan</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Terhitung Sampai Tanggal</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="pelaksana" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Pelaksana
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-jabatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Jenis Pelaksana</th>
                                    <th>Instansi</th>
                                    <th>Unit Kerja</th>
                                    <th>Jenjang Jabatan</th>
                                    <th>Nama Jabatan</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Terhitung Sampai Tanggal</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> -->
            <div id="penetapan-angka-kredit" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Penetapan Angka Kredit
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-penetapan-angka-kredit table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Periode Penilaian Awal</th>
                                    <th>Periode Penilaian Akhir</th>
                                    <th>Unsur Utama</th>
                                    <th>Unsur Penunjang</th>
                                    <th>Total Angkat Kredit</th>
                                    <th>Arsip PAK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="orang-tua" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Data Ayah
                        </div>

                        <hr class="margin-y-10 border-top border-light-orange">

                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Nama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->nama_ayah ? $model['pegawai']->nama_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tempat Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tempat_lahir_ayah ? $model['pegawai']->tempat_lahir_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tanggal Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tanggal_lahir_ayah ? $model['pegawai']->tanggal_lahir_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Agama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->agama_ayah ? $model['pegawai']->agamaAyah->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Jenjang Pendidikan Terakhir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_ayah ? $model['pegawai']->jenjangPendidikanTerakhirAyah->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Pekerjaan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->pekerjaan_ayah ? $model['pegawai']->pekerjaan_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Status Hidup</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->status_hidup_ayah ? $model['pegawai']->status_hidup_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Telpon</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->telpon_ayah ? $model['pegawai']->telpon_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Alamat</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->alamat_ayah ? $model['pegawai']->alamat_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kelurahan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahanAyah->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kecamatan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahanAyah->kecamatan->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kota / Kabupaten</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahanAyah->kecamatan->kotaKabupaten->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Provinsi</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahanAyah->kecamatan->kotaKabupaten->provinsi->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kode Pos</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kode_pos_ayah ? $model['pegawai']->kode_pos_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                    </div>
                    
                    <div class="margin-top-30"></div>
                    
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Data Ibu
                        </div>

                        <hr class="margin-y-10 border-top border-light-orange">

                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Nama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->nama_ibu ? $model['pegawai']->nama_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tempat Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tempat_lahir_ibu ? $model['pegawai']->tempat_lahir_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tanggal Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tanggal_lahir_ibu ? $model['pegawai']->tanggal_lahir_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Agama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->agama_ibu ? $model['pegawai']->agamaIbu->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Jenjang Pendidikan Terakhir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_ibu ? $model['pegawai']->jenjangPendidikanTerakhirIbu->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Pekerjaan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->pekerjaan_ibu ? $model['pegawai']->pekerjaan_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Status Hidup</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->status_hidup_ibu ? $model['pegawai']->status_hidup_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Telpon</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->telpon_ibu ? $model['pegawai']->telpon_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Alamat</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->alamat_ibu ? $model['pegawai']->alamat_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kelurahan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahanIbu->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kecamatan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahanIbu->kecamatan->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kota / Kabupaten</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahanIbu->kecamatan->kotaKabupaten->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Provinsi</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahanIbu->kecamatan->kotaKabupaten->provinsi->nama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Kode Pos</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->kode_pos_ibu ? $model['pegawai']->kode_pos_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="saudara-kandung" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Daftar Saudara Kandung
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-saudara-kandung table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Pekerjaan</th>
                                    <th>Anak Ke</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="pasangan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Data <?= $model['pegawai']->pasangan; ?>
                        </div>

                        <hr class="margin-y-10 border-top border-light-orange">

                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Nama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->nama_pasangan ? $model['pegawai']->nama_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tempat Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tempat_lahir_pasangan ? $model['pegawai']->tempat_lahir_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Tanggal Lahir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->tanggal_lahir_pasangan ? $model['pegawai']->tanggal_lahir_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Agama</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->agama_pasangan ? $model['pegawai']->agamaPasangan->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Jenjang Pendidikan Terakhir</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_pasangan ? $model['pegawai']->jenjangPendidikanTerakhirPasangan->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Pekerjaan</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->pekerjaan_pasangan ? $model['pegawai']->pekerjaan_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Status Hidup</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->status_hidup_pasangan ? $model['pegawai']->status_hidup_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                        <div class="box box-break-sm">
                            <div class="box-1 padding-x-0 padding-y-5">
                                <div class="text-grayer">Telpon</div>
                            </div>
                            <div class="box-11 m-padding-x-0">
                                <div class="padding-y-5 text-dark">
                                    <?= $model['pegawai']->telpon_pasangan ? $model['pegawai']->telpon_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                                </div>
                                <hr class="margin-y-5 border-top border-light-azure">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="anak" class="tab-content">
                <div
                 class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Data Anak
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-anak table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Nama Anak</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jenjang Pendidikan Terakhir</th>
                                    <th>Pekerjaan</th>
                                    <th>Status Anak</th>
                                    <th>Status Hidup</th>
                                    <th>Status Tunjangan</th>
                                    <th>Akta Lahir</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="margin-top-30"></div>
                    <?= Html::a('Ubah Anak', ['pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'anak'], ['class' => 'button button-block button-md rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
            <div id="pendidikan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Pendidikan
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-pendidikan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Nama Sekolah</th>
                                    <th>Jenjang Pendidikan</th>
                                    <th>Jurusan</th>
                                    <th>Nilai</th>
                                    <th>Skala Nilai</th>
                                    <th>Alamat Sekolah</th>
                                    <th>Tahun Masuk</th>
                                    <th>Tahun Lulus</th>
                                    <th>Ijazah</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="penghargaan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Daftar Penghargaan
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-penghargaan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Jenis Penghargaan</th>
                                    <th>Nama Penghargaan</th>
                                    <th>Tahun Penghargaan</th>
                                    <th>Instansi Pemberi Penghargaan</th>
                                    <th>Sertifikat</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="diklat" class="tab-content">
                <div
                 class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Diklat
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-diklat table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Jenis Diklat</th>
                                    <th>Nama Diklat</th>
                                    <th>Penyelenggara</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Tempat</th>
                                    <th>Keterangan</th>
                                    <th>Sertifikat</th>
                                    <th>Nomor Sttp</th>
                                    <th>Tanggal Sttp</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="margin-top-30"></div>
                    <?= Html::a('Ubah Diklat', ['pegawai-update', 'id' => $model['pegawai']->id, 'tab' => 'diklat'], ['class' => 'button button-block button-md rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
            <div id="kursus-pelatihan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Kursus Pelatihan
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-kursus-pelatihan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Nama Kursus Pelatihan</th>
                                    <th>Penyelenggara</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Lokasi</th>
                                    <th>Sertifikat</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="kunjungan-luar-negeri" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Kunjungan Luar Negeri
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-kunjungan-luar-negeri table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Negara Kunjungan</th>
                                    <th>Tujuan Kunjungan</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Sumber Biaya</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div> -->
            <div id="organisasi" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Organisasi
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-organisasi table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Organisasi</th>
                                    <th>Jabatan</th>
                                    <th>Tahun Mulai</th>
                                    <th>Tahun Selesai</th>
                                    <th>Lokasi</th>
                                    <th>Periode Organisasi</th>
                                    <th>Sertifikat</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="hukuman-disiplin" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Hukuman Disiplin
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-hukuman-disiplin table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Tingkat Hukuman Disiplin</th>
                                    <th>Penjelasan</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Terhitung Sampai Tanggal</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="pemberhentian" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Pemberhentian
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-pemberhentian table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Jenis Pemberhentian</th>
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Kepangkatan Saat Pemberhentian</th>
                                    <th>Instansi Saat Pemberhentian</th>
                                    <th>Unit Kerja Saat Pemberhentian</th>
                                    <th>Jenjang Jabatan Saat Diberhentikan</th>
                                    <th>Nama Jabatan Saat Diberhentikan</th>
                                    <th>Catatan</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="masa-persiapan-pensiun" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="fs-16 text-azure fs-italic">
                            Riwayat Masa Persiapan Pensiun
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-pegawai-masa-persiapan-pensiun table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th>Terhitung Mulai Tanggal</th>
                                    <th>Catatan</th>
                                    <th>Jenis SK</th>
                                    <th>Nomor SK</th>
                                    <th>Tanggal SK</th>
                                    <th>Pejabat Penetap SK</th>
                                    <th>Arsip SK</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>