<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "provinsi".
 *
 * @property string $id
 * @property string $nama
 *
 * @property KotaKabupaten[] $kotaKabupatens
 */
class Provinsi extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'provinsi';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 2],
            [['id'], 'unique'],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKotaKabupatens()
    {
        return $this->hasMany(KotaKabupaten::className(), ['id_provinsi' => 'id']);
    }
}
