<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenis_hukuman_disiplin".
 *
 * @property integer $id
 * @property string $jenis_hukuman_disiplin
 * @property integer $tingkat_hukuman_disiplin
 *
 * @property TingkatHukumanDisiplin $tingkatHukumanDisiplin
 * @property PegawaiHukumanDisiplin[] $pegawaiHukumanDisiplins
 */
class JenisHukumanDisiplin extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_hukuman_disiplin';
    }

    public function rules()
    {
        return [
            //id

            //jenis_hukuman_disiplin
            [['jenis_hukuman_disiplin'], 'required'],
            [['jenis_hukuman_disiplin'], 'string', 'max' => 256],

            //tingkat_hukuman_disiplin
            [['tingkat_hukuman_disiplin'], 'required'],
            [['tingkat_hukuman_disiplin'], 'integer'],
            [['tingkat_hukuman_disiplin'], 'exist', 'skipOnError' => true, 'targetClass' => TingkatHukumanDisiplin::className(), 'targetAttribute' => ['tingkat_hukuman_disiplin' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_hukuman_disiplin' => 'Jenis Hukuman Disiplin',
            'tingkat_hukuman_disiplin' => 'Tingkat Hukuman Disiplin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTingkatHukumanDisiplin()
    {
        return $this->hasOne(TingkatHukumanDisiplin::className(), ['id' => 'tingkat_hukuman_disiplin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiHukumanDisiplins()
    {
        return $this->hasMany(PegawaiHukumanDisiplin::className(), ['jenis_hukuman_disiplin' => 'id']);
    }
}
