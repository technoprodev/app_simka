<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_kursus_pelatihan".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $nama_kursus_pelatihan
 * @property string $penyelenggara
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $lokasi
 * @property string $keterangan
 * @property string $sertifikat
 *
 * @property Pegawai $pegawai0
 */
class PegawaiKursusPelatihan extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_sertifikat_upload;
    public $virtual_sertifikat_download;

    public static function tableName()
    {
        return 'pegawai_kursus_pelatihan';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //nama_kursus_pelatihan
            [['nama_kursus_pelatihan'], 'required'],
            [['nama_kursus_pelatihan'], 'string', 'max' => 256],

            //penyelenggara
            [['penyelenggara'], 'required'],
            [['penyelenggara'], 'string', 'max' => 256],

            //tanggal_mulai
            [['tanggal_mulai'], 'required'],
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'required'],
            [['tanggal_selesai'], 'safe'],

            //lokasi
            [['lokasi'], 'required'],
            [['lokasi'], 'string', 'max' => 256],

            //keterangan
            [['keterangan'], 'string', 'max' => 256],

            //sertifikat
            [['sertifikat'], 'string', 'max' => 256],
            
            //virtual_sertifikat_download
            [['virtual_sertifikat_download'], 'safe'],
            
            //virtual_sertifikat_upload
            [['virtual_sertifikat_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_kursus_pelatihan-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_sertifikat_upload) {
            $path = Yii::getAlias('@upload-pegawai_kursus_pelatihan-sertifikat') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_sertifikat_upload->saveAs($path . '/' . $this->sertifikat);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_kursus_pelatihan-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->sertifikat) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_kursus_pelatihan-sertifikat');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_sertifikat_download = $path . '/' . $this->sertifikat;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'nama_kursus_pelatihan' => 'Nama Kursus Pelatihan',
            'penyelenggara' => 'Penyelenggara',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'lokasi' => 'Lokasi',
            'keterangan' => 'Keterangan',
            'sertifikat' => 'Sertifikat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
