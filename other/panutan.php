            <!-- <?= Html::activeDropDownList($model['agama'], 'agama', $pilihanKepangkatan, ['prompt' => '- pilih agama -', 'class' => 'form-dropdown']); ?> -->


                            <?= $form->field($model['pegawai'], 'nama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <div class="box-1 padding-x-0 padding-y-5">
                                    <?= Html::activeLabel($model['pegawai'], 'nama', ['class' => 'form-label text-grayer']); ?>
                                </div>
                                <div class="box-11 m-padding-x-0">
                                    <?= Html::activeTextInput($model['pegawai'], 'nama', ['class' => 'form-text', 'maxlength' => true]); ?>
                                    <?= Html::error($model['pegawai'], 'nama', ['class' => 'form-info']); ?>
                                </div>
                            <?= $form->field($model['pegawai'], 'nama')->end(); ?>

                            <?php if(false) : ?>

                            <?= $form->field($model['pegawai'], 'link', ['options' => ['class' => 'form-wrapper radio-elegant']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'link', ['class' => 'form-label']); ?>
                                <?= Html::activeRadioList($model['pegawai'], 'link', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-radio box box-break-sm box-gutter', 'unselect' => null,
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                                        return "<label class='box-3'><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                                    }]); ?>
                                <?= Html::error($model['pegawai'], 'link', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'link')->end(); ?>

                            <?= $form->field($model['pegawai'], 'enum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'enum', ['class' => 'form-label']); ?>
                                <?= Html::activeRadioList($model['pegawai'], 'enum', $model['pegawai']->getEnum('enum'), ['class' => 'form-radio', 'unselect' => null,
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                                        return "<label><input type='radio' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                                    }]); ?>
                                <?= Html::error($model['pegawai'], 'enum', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'enum')->end(); ?>

                            <?= $form->field($model['pegawai'], 'virtual_category', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'virtual_category', ['class' => 'form-label']); ?>
                                <?= Html::activeCheckboxList($model['pegawai'], 'virtual_category', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-checkbox box box-break-sm box-gutter', 'unselect' => null,
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                                        return "<label class='col-xs-3'><input type='checkbox' name='$name' value='$value' $checked $disabled v-model='pegawai.virtual_category'><i></i>$label</label>";
                                    },
                                ]); ?>
                                <?= Html::error($model['pegawai'], 'virtual_category', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'virtual_category')->end(); ?>

                            <?= $form->field($model['pegawai'], 'set', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'set', ['class' => 'form-label']); ?>
                                <?= Html::activeCheckboxList($model['pegawai'], 'set', $model['pegawai']->getSet('set'), ['class' => 'form-checkbox', 'unselect' => null,
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $checked = $checked ? 'checked' : '';
                                        $disabled = in_array($value, ['val1', 'val2', 'technology']) ? 'disabled' : '';
                                        return "<label><input type='checkbox' name='$name' value='$value' $checked $disabled><i></i>$label</label>";
                                    }
                                ]); ?>
                                <?= Html::error($model['pegawai'], 'set', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'set')->end(); ?>
                            
                            <?= $form->field($model['pegawai'], 'checklist', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'checklist', ['class' => 'form-label']); ?>
                                <div class="form-checkbox">
                                    <?= Html::activeCheckbox($model['pegawai'], 'checklist', ['uncheck' => 0]); ?>
                                </div>
                                <?= Html::error($model['pegawai'], 'checklist', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'checklist')->end(); ?>

                            <hr>
                            
                            <?= $form->field($model['pegawai'], 'link', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'link', ['class' => 'form-label']); ?>
                                <?= Html::activeDropDownList($model['pegawai'], 'link', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name', 'parent'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
                                <?= Html::error($model['pegawai'], 'link', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'link')->end(); ?>

                            <?= $form->field($model['pegawai'], 'enum', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'enum', ['class' => 'form-label']); ?>
                                <?= Html::activeDropDownList($model['pegawai'], 'enum', $model['pegawai']->getEnum('enum'), ['prompt' => 'Choose one please', 'class' => 'form-dropdown']); ?>
                                <?= Html::error($model['pegawai'], 'enum', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'enum')->end(); ?>
                            
                            <?= $form->field($model['pegawai'], 'virtual_category', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'virtual_category', ['class' => 'form-label']); ?>
                                <?= Html::activeListBox($model['pegawai'], 'virtual_category', ArrayHelper::map(\app_starter\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-dropdown', 'multiple' => true, 'unselect' => null, 'v-model' => 'pegawai.virtual_category']); ?>
                                <?= Html::error($model['pegawai'], 'virtual_category', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'virtual_category')->end(); ?>
                            
                            <?= $form->field($model['pegawai'], 'set', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'set', ['class' => 'form-label']); ?>
                                <?= Html::activeListBox($model['pegawai'], 'set', $model['pegawai']->getSet('set'), ['class' => 'form-dropdown', 'multiple' => true, 'unselect' => null]); ?>
                                <?= Html::error($model['pegawai'], 'set', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'set')->end(); ?>

                            <hr>

                            <?= $form->field($model['pegawai'], 'virtual_file_upload', ['options' => ['class' => 'form-wrapper'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                                <?= Html::activeLabel($model['pegawai'], 'virtual_file_upload', ['class' => 'form-label']); ?>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    <div class="form-text">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"><a href="<?= $model['pegawai']->virtual_file_download ?>"><?= $model['pegawai']->file ?></a></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <?= Html::activeFileInput($model['pegawai'], 'virtual_file_upload'); ?>
                                    </span>
                                </div>
                                <?= Html::error($model['pegawai'], 'virtual_file_upload', ['class' => 'form-info']); ?>
                            <?= $form->field($model['pegawai'], 'virtual_file_upload')->end(); ?>

                            <hr>

                            <?php endif; ?>