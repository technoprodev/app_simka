<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

define("DOMPDF_ENABLE_REMOTE", false);
// ddx($_SERVER["DOCUMENT_ROOT"]);
// dd($model['pegawai']->virtual_poto_download);
// dd(Yii::getAlias('@yii'));
// dd(Yii::getAlias('@app'));
// dd(Yii::getAlias('@runtime'));
// dd(Yii::getAlias('@webroot'));
// dd(Yii::getAlias('@web'));
// dd(Yii::getAlias('@vendor'));
// dd(Yii::getAlias('@bower'));
// dd(Yii::getAlias('@npm'));
// dd(Yii::getAlias('@technosmart'));
// dd(Yii::getAlias('@app_simka'));
// dd(Yii::getAlias('@console'));
$poto = $model['pegawai']->virtual_poto_download ? (dirname(dirname(dirname(dirname(__DIR__)))) . $model['pegawai']->virtual_poto_download) : '';
// ddx();
?>

<style type="text/css">
    .table th,
    .table td {
        padding: 3px 10px;
    }
    .table tr:not(:first-child) {
        border: 0;
    }
</style>
<div>
    <div class="fs-18 text-azure fs-italic text-center">
        Daftar Riwayat Hidup
    </div>
    <div class="margin-y-10"></div>

    <?php if ($poto) : ?>
    <div class="text-center margin-bottom-min-15 padding-top-30">
        <img src="<?= $poto ?>" width="120px;" height="auto"/>
    </div>
    <?php endif; ?>

    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Data Personal</td>
            </tr>
            <tr>
                <td style="width:180px;">Nama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama ? $model['pegawai']->nama : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Nama Panggilan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_panggilan ? $model['pegawai']->nama_panggilan : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Jenis Kelamin</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenis_kelamin ? $model['pegawai']->jenis_kelamin : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">TTL</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tempat_lahir ? $model['pegawai']->tempat_lahir : '<span class="text-grayer">-</span>' ?>, <?= $model['pegawai']->tanggal_lahir ? $model['pegawai']->tanggal_lahir : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Agama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->agama ? $model['pegawai']->agama0->agama : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Status Pernikahan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->status_marital ? $model['pegawai']->statusMarital->status_marital : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Golongan Darah</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->golongan_darah ? $model['pegawai']->golongan_darah : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Pendidikan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_pendidikan_terakhir ? $model['pegawai']->jenjangPendidikanTerakhir->jenjang_pendidikan : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Gelar</td>
                <td class="fw-bold">
                    <?= !$model['pegawai']->gelar_depan && !$model['pegawai']->gelar_belakang ? '-' : '' ?>
                    <?= $model['pegawai']->gelar_depan ? $model['pegawai']->gelar_depan : '' ?>
                    <?= $model['pegawai']->gelar_depan && $model['pegawai']->gelar_belakang ? ' & ' : '' ?>
                    <?= $model['pegawai']->gelar_belakang ? $model['pegawai']->gelar_belakang : '' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tinggi / Berat</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tinggi_badan_cm ? $model['pegawai']->tinggi_badan_cm . ' cm' : '<span class="text-grayer">-</span>' ?> / <?= $model['pegawai']->berat_badan_kg ? $model['pegawai']->berat_badan_kg . ' kg' : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Model Rambut</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->model_rambut ? $model['pegawai']->modelRambut->model_rambut : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Bentuk Wajah</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->bentuk_wajah ? $model['pegawai']->bentukWajah->bentuk_wajah : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Warna Kulit</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->warna_kulit ? $model['pegawai']->warnaKulit->warna_kulit : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Hobi</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->hobi ? $model['pegawai']->hobi : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kemampuan Berbahasa</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kemampuan_berbahasa ? $model['pegawai']->kemampuan_berbahasa : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Data Pegawai</td>
            </tr>
            <tr>
                <td style="width:180px;">NIP</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nip ? $model['pegawai']->nip : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Pin</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->pin ? $model['pegawai']->pin : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kepangkatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Jenjang Jabatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Nama Jabatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <?php
                $bottomToTop = [];
                $uk = $model['pegawai']->unitKerja;
                $i = 0;
                while ($uk && $i <= 10) {
                    $bottomToTop[] = [
                        'jenisUnitKerja' => $uk->jenis_unit_kerja ? $uk->jenisUnitKerja->jenis_unit_kerja : '',
                        'unitKerja' => $uk->unit_kerja,
                    ];
                    $uk = $uk->parent0;
                    $i++;
                }

                $topToBottom = [];
                for ($i = count($bottomToTop)-1; $i >= 0; $i--) { 
                    $topToBottom[] = $bottomToTop[$i];
                }
            ?>
            <?php foreach ($topToBottom as $key => $value) : ?>
            <?php if ($value['jenisUnitKerja'] != 'Staf') :?>
            <tr>
                <td style="width:180px;"><?= $value['jenisUnitKerja'] ?></td>
                <td class="fw-bold">
                    <?= $value['unitKerja'] ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php if (!$topToBottom) :?>
            <tr>
                <td style="width:180px;">Satuan Organisasi</td>
                <td class="fw-bold">
                    <span class="text-grayer">-</span>
                </td>
            </tr>
            <?php endif; ?>
            <tr>
                <td style="width:180px;">Eselon</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->eselon ? $model['pegawai']->eselon0->eselon : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Grade</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->grade ? $model['pegawai']->grade0->grade : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kedudukan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kedudukan_pegawai ? $model['pegawai']->kedudukanPegawai->kedudukan_pegawai : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <!-- <tr>
                <td style="width:180px;">Mendapat Tunkin</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->mendapat_tunkin ? $model['pegawai']->mendapat_tunkin : '<span class="text-grayer">-</span>' ?>
                    </td>
            </tr> -->
            <tr>
                <td style="width:180px;">NIK</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nik ? $model['pegawai']->nik : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">NPWP</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->npwp ? $model['pegawai']->npwp : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Karpeg</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->karpeg ? $model['pegawai']->karpeg : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Karis / Karsu</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->karis_atau_karsu ? $model['pegawai']->karis_atau_karsu : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Taspen</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->taspen ? $model['pegawai']->taspen : '<span class="text-grayer">-</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Jabatan Terakhir</td>
            </tr>
            <tr>
                <td style="width:180px;">Jenjang Jabatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_jabatan ? $model['pegawai']->jenjangJabatan->jenisJabatan->jenis_jabatan . ' - ' . $model['pegawai']->jenjangJabatan->jenjang_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Nama Jabatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_jabatan ? $model['pegawai']->nama_jabatan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Unit Kerja</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->unit_kerja ? $model['pegawai']->unitKerja->unit_kerja : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">Riwayat Jabatan</td>
            </tr>
            <tr>
                <td>Instansi</td>
                <td>Unit Kerja</td>
                <td>Jenjang Jabatan</td>
                <td>Nama Jabatan</td>
                <td>Terhitung Mulai Tanggal</td>
                <td>Terhitung Sampai Tanggal</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiMutasis as $key => $pegawaiMutasi) : ?>
            <tr>
                <td><?= $pegawaiMutasi->instansi ?></td>
                <td><?= $pegawaiMutasi->unit_kerja ?></td>
                <td><?= $pegawaiMutasi->jenjang_jabatan ? $pegawaiMutasi->jenjangJabatan->jenjang_jabatan : '-' ?></td>
                <td><?= $pegawaiMutasi->nama_jabatan ?></td>
                <td><?= $pegawaiMutasi->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiMutasi->terhitung_sampai_tanggal ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Kepangkatan Terakhir</td>
            </tr>
            <tr>
                <td style="width:180px;">Pangkat</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->pangkat : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Golongan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->golongan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Ruang</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kepangkatan ? $model['pegawai']->kepangkatan0->ruang : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">Riwayat Kepangkatan</td>
            </tr>
            <tr>
                <td>Pangkat</td>
                <td>Golongan</td>
                <td>Ruang</td>
                <td>Status Kepegawaian</td>
                <td>Terhitung Mulai Tanggal</td>
                <td>Terhitung Sampai Tanggal</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiKepangkatans as $key => $pegawaiKepangkatan) : ?>
            <tr>
                <td><?= $pegawaiKepangkatan->kepangkatan0->pangkat ?></td>
                <td><?= $pegawaiKepangkatan->kepangkatan0->golongan ?></td>
                <td><?= $pegawaiKepangkatan->kepangkatan0->ruang ?></td>
                <td><?= $pegawaiKepangkatan->statusKepegawaian ? $pegawaiKepangkatan->statusKepegawaian->status_kepegawaian : '-' ?></td>
                <td><?= $pegawaiKepangkatan->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiKepangkatan->terhitung_sampai_tanggal ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="5" class="fs-14 fw-bold text-left">Kenaikan Gaji Berkala</td>
            </tr>
            <tr>
                <td>Gapok Lama</td>
                <td>Gapok Baru</td>
                <td>Kepangkatan Saat Kenaikan</td>
                <td>Terhitung Mulai Tanggal</td>
                <td>Terhitung Sampai Tanggal</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiKenaikanGajiBerkalas as $key => $pegawaiKenaikanGajiBerkala) : ?>
            <tr>
                <td><?= $pegawaiKenaikanGajiBerkala->gaji_pokok_lama ?></td>
                <td><?= $pegawaiKenaikanGajiBerkala->gaji_pokok_baru ?></td>
                <td><?= $pegawaiKenaikanGajiBerkala->kepangkatan_saat_kenaikan ?></td>
                <td><?= $pegawaiKenaikanGajiBerkala->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiKenaikanGajiBerkala->terhitung_sampai_tanggal ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="9" class="fs-14 fw-bold text-left">Riwayat Pelaksana</td>
            </tr>
        </tbody>
    </table> -->
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="5" class="fs-14 fw-bold text-left">Riwayat Penetapan Angka Kredit</td>
            </tr>
            <tr>
                <td>Periode Penilaian Awal</td>
                <td>Periode Penilaian Akhir</td>
                <td>Unsur Utama</td>
                <td>Unsur Penunjang</td>
                <td>Total Angka Kredit</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiPenetapanAngkaKredits as $key => $pegawaiPenetapanAngkaKredit) : ?>
            <tr>
                <td><?= $pegawaiPenetapanAngkaKredit->periode_penilaian_awal ?></td>
                <td><?= $pegawaiPenetapanAngkaKredit->periode_penilaian_akhir ?></td>
                <td><?= $pegawaiPenetapanAngkaKredit->unsur_utama ?></td>
                <td><?= $pegawaiPenetapanAngkaKredit->unsur_penunjang ?></td>
                <td><?= $pegawaiPenetapanAngkaKredit->total_angkat_kredit ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Data Ayah</td>
            </tr>
            <tr>
                <td style="width:180px;">Nama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_ayah ? $model['pegawai']->nama_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tempat Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tempat_lahir_ayah ? $model['pegawai']->tempat_lahir_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tanggal Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tanggal_lahir_ayah ? $model['pegawai']->tanggal_lahir_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Agama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->agama_ayah ? $model['pegawai']->agamaAyah->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Jenjang Pendidikan Terakhir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_ayah ? $model['pegawai']->jenjangPendidikanTerakhirAyah->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Pekerjaan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->pekerjaan_ayah ? $model['pegawai']->pekerjaanAyah->pekerjaan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Status Hidup</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->status_hidup_ayah ? $model['pegawai']->status_hidup_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Telpon</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->telpon_ayah ? $model['pegawai']->telpon_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Alamat</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->alamat_ayah ? $model['pegawai']->alamat_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kelurahan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahan_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kecamatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahan_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kota / Kabupaten</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahan_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Provinsi</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ayah ? $model['pegawai']->kelurahan_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kode Pos</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kode_pos_ayah ? $model['pegawai']->kode_pos_ayah : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Data Ibu</td>
            </tr>
            <tr>
                <td style="width:180px;">Nama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_ibu ? $model['pegawai']->nama_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tempat Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tempat_lahir_ibu ? $model['pegawai']->tempat_lahir_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tanggal Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tanggal_lahir_ibu ? $model['pegawai']->tanggal_lahir_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Agama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->agama_ibu ? $model['pegawai']->agamaIbu->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Jenjang Pendidikan Terakhir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_ibu ? $model['pegawai']->jenjangPendidikanTerakhirIbu->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Pekerjaan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->pekerjaan_ibu ? $model['pegawai']->pekerjaanIbu->pekerjaan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Status Hidup</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->status_hidup_ibu ? $model['pegawai']->status_hidup_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Telpon</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->telpon_ibu ? $model['pegawai']->telpon_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Alamat</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->alamat_ibu ? $model['pegawai']->alamat_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kelurahan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahan_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kecamatan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahan_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kota / Kabupaten</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahan_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Provinsi</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kelurahan_ibu ? $model['pegawai']->kelurahan_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Kode Pos</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->kode_pos_ibu ? $model['pegawai']->kode_pos_ibu : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">Daftar Saudara Kandung</td>
            </tr>
            <tr>
                <td>Nama</td>
                <td>Jenis Kelamin</td>
                <td>Tempat Lahir</td>
                <td>Tanggal Lahir</td>
                <td>Pekerjaan</td>
                <td>Anak Ke</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiSaudaraKandungs as $key => $pegawaiSaudaraKandung) : ?>
            <tr>
                <td><?= $pegawaiSaudaraKandung->nama ?></td>
                <td><?= $pegawaiSaudaraKandung->jenis_kelamin ?></td>
                <td><?= $pegawaiSaudaraKandung->tempat_lahir ?></td>
                <td><?= $pegawaiSaudaraKandung->tanggal_lahir ?></td>
                <td><?= $pegawaiSaudaraKandung->pekerjaan ?></td>
                <td><?= $pegawaiSaudaraKandung->anak_ke ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Data <?= $model['pegawai']->pasangan; ?></td>
            </tr>
            <tr>
                <td style="width:180px;">Nama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->nama_pasangan ? $model['pegawai']->nama_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tempat Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tempat_lahir_pasangan ? $model['pegawai']->tempat_lahir_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Tanggal Lahir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->tanggal_lahir_pasangan ? $model['pegawai']->tanggal_lahir_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Agama</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->agama_pasangan ? $model['pegawai']->agamaPasangan->agama : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Jenjang Pendidikan Terakhir</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->jenjang_pendidikan_terakhir_pasangan ? $model['pegawai']->jenjangPendidikanTerakhirPasangan->jenjang_pendidikan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Pekerjaan</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->pekerjaan_pasangan ? $model['pegawai']->pekerjaanPasangan->pekerjaan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Status Hidup</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->status_hidup_pasangan ? $model['pegawai']->status_hidup_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
            <tr>
                <td style="width:180px;">Telpon</td>
                <td class="fw-bold">
                    <?= $model['pegawai']->telpon_pasangan ? $model['pegawai']->telpon_pasangan : '<span class="text-grayer">(belum diisi)</span>' ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="9" class="fs-14 fw-bold text-left">Data Anak</td>
            </tr>
            <tr>
                <td>Nama Anak</td>
                <td>Jenis Kelamin</td>
                <td>Tempat Lahir</td>
                <td>Tanggal Lahir</td>
                <td>Jenjang Pendidikan Terakhir</td>
                <td>Pekerjaan</td>
                <td>Status Anak</td>
                <td>Status Hidup</td>
                <td>Status Tunjangan</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiAnaks as $key => $pegawaiAnak) : ?>
            <tr>
                <td><?= $pegawaiAnak->nama_anak ?></td>
                <td><?= $pegawaiAnak->jenis_kelamin ?></td>
                <td><?= $pegawaiAnak->tempat_lahir ?></td>
                <td><?= $pegawaiAnak->tanggal_lahir ?></td>
                <td><?= $pegawaiAnak->jenjang_pendidikan_terakhir ?></td>
                <td><?= $pegawaiAnak->pekerjaan ?></td>
                <td><?= $pegawaiAnak->status_anak ?></td>
                <td><?= $pegawaiAnak->status_hidup ?></td>
                <td><?= $pegawaiAnak->status_tunjangan ?></td>
            </tr>
            <?php endforeach; ?>
            </tr>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="8" class="fs-14 fw-bold text-left">Riwayat Pendidikan</td>
            </tr>
            <tr>
                <td>Nama Sekolah</td>
                <td>Jenjang Pendidikan</td>
                <td>Jurusan</td>
                <td>Nilai</td>
                <td>Skala Nilai</td>
                <td>Alamat Sekolah</td>
                <td>Tahun Masuk</td>
                <td>Tahun Lulus</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiPendidikans as $key => $pegawaiPendidikan) : ?>
            <tr>
                <td><?= $pegawaiPendidikan->nama_sekolah ?></td>
                <td><?= $pegawaiPendidikan->jenjang_pendidikan ?></td>
                <td><?= $pegawaiPendidikan->jurusan ?></td>
                <td><?= $pegawaiPendidikan->nilai ?></td>
                <td><?= $pegawaiPendidikan->skala_nilai ?></td>
                <td><?= $pegawaiPendidikan->alamat_sekolah ?></td>
                <td><?= $pegawaiPendidikan->tahun_masuk ?></td>
                <td><?= $pegawaiPendidikan->tahun_lulus ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">Daftar Penghargaan</td>
            </tr>
            <tr>
                <td>Jenis Penghargaan</td>
                <td>Nama Penghargaan</td>
                <td>Tahun Penghargaan</td>
                <td>Instansi Pemberi Penghargaan</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiPenghargaans as $key => $pegawaiPenghargaan) : ?>
            <tr>
                <td><?= $pegawaiPenghargaan->jenis_penghargaan ?></td>
                <td><?= $pegawaiPenghargaan->nama_penghargaan ?></td>
                <td><?= $pegawaiPenghargaan->tahun_penghargaan ?></td>
                <td><?= $pegawaiPenghargaan->instansi_pemberi_penghargaan ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">Riwayat Diklat</td>
            </tr>
            <tr>
                <td>Jenis Diklat</td>
                <td>Nama Diklat</td>
                <td>Penyelenggara</td>
                <td>Tanggal Mulai</td>
                <td>Tanggal Selesai</td>
                <td>Tempat</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiDiklats as $key => $pegawaiDiklat) : ?>
            <tr>
                <td><?= $pegawaiDiklat->jenis_diklat ?></td>
                <td><?= $pegawaiDiklat->nama_diklat ?></td>
                <td><?= $pegawaiDiklat->penyelenggara ?></td>
                <td><?= $pegawaiDiklat->tanggal_mulai ?></td>
                <td><?= $pegawaiDiklat->tanggal_selesai ?></td>
                <td><?= $pegawaiDiklat->tempat ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="5" class="fs-14 fw-bold text-left">Riwayat Kursus Pelatihan</td>
            </tr>
            <tr>
                <td>Nama Kursus Pelatihan</td>
                <td>Penyelenggara</td>
                <td>Tanggal Mulai</td>
                <td>Tanggal Selesai</td>
                <td>Lokasi</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiKursusPelatihans as $key => $pegawaiKursusPelatihan) : ?>
            <tr>
                <td><?= $pegawaiKursusPelatihan->nama_kursus_pelatihan ?></td>
                <td><?= $pegawaiKursusPelatihan->penyelenggara ?></td>
                <td><?= $pegawaiKursusPelatihan->tanggal_mulai ?></td>
                <td><?= $pegawaiKursusPelatihan->tanggal_selesai ?></td>
                <td><?= $pegawaiKursusPelatihan->lokasi ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="9" class="fs-14 fw-bold text-left">Riwayat Kunjungan Luar Negeri</td>
            </tr>
            <tr>
                
            </tr>
        </tbody>
    </table> -->
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="6" class="fs-14 fw-bold text-left">Riwayat Organisasi</td>
            </tr>
            <tr>
                <td>Organisasi</td>
                <td>Jabatan</td>
                <td>Tahun Mulai</td>
                <td>Tahun Selesai</td>
                <td>Lokasi</td>
                <td>Periode Organisasi</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiOrganisasis as $key => $pegawaiOrganisasi) : ?>
            <tr>
                <td><?= $pegawaiOrganisasi->organisasi ?></td>
                <td><?= $pegawaiOrganisasi->jabatan ?></td>
                <td><?= $pegawaiOrganisasi->tahun_mulai ?></td>
                <td><?= $pegawaiOrganisasi->tahun_selesai ?></td>
                <td><?= $pegawaiOrganisasi->lokasi ?></td>
                <td><?= $pegawaiOrganisasi->periode_organisasi ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="4" class="fs-14 fw-bold text-left">Riwayat Hukuman Disiplin</td>
            </tr>
            <tr>
                <td>Tingkat Hukuman Disiplin</td>
                <td>Penjelasan</td>
                <td>Terhitung Mulai Tanggal</td>
                <td>Terhitung Sampai Tanggal</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiHukumanDisiplins as $key => $pegawaiHukumanDisiplin) : ?>
            <tr>
                <td><?= $pegawaiHukumanDisiplin->jenisHukumanDisiplin->tingkat_hukuman_disiplin ?></td>
                <td><?= $pegawaiHukumanDisiplin->penjelasan ?></td>
                <td><?= $pegawaiHukumanDisiplin->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiHukumanDisiplin->terhitung_sampai_tanggal ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="7" class="fs-14 fw-bold text-left">Riwayat Pemberhentian</td>
            </tr>
            <tr>
                <td>Jenis Pemberhentian</td>
                <td>Terhitung Mulai Tanggal</td>
                <td>Kepangkatan Saat Pemberhentian</td>
                <td>Instansi Saat Pemberhentian</td>
                <td>Unit Kerja Saat Pemberhentian</td>
                <td>Jenjang Jabatan Saat Diberhentikan</td>
                <td>Nama Jabatan Saat Diberhentikan</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiPemberhentians as $key => $pegawaiPemberhentian) : ?>
            <tr>
                <td><?= $pegawaiPemberhentian->jenis_pemberhentian ?></td>
                <td><?= $pegawaiPemberhentian->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiPemberhentian->kepangkatan_saat_pemberhentian ?></td>
                <td><?= $pegawaiPemberhentian->instansi_saat_pemberhentian ?></td>
                <td><?= $pegawaiPemberhentian->unit_kerja_saat_pemberhentian ?></td>
                <td><?= $pegawaiPemberhentian->jenjang_jabatan_saat_diberhentikan ?></td>
                <td><?= $pegawaiPemberhentian->nama_jabatan_saat_diberhentikan ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="table" border="1">
        <tbody>
            <tr>
                <td colspan="2" class="fs-14 fw-bold text-left">Riwayat Masa Persiapan Pensiun</td>
            </tr>
            <tr>
                <td>Terhitung Mulai Tanggal</td>
                <td>Catatan</td>
            </tr>
            <?php foreach ($model['pegawai']->pegawaiMasaPersiapanPensiuns as $key => $pegawaiMasaPersiapanPensiun) : ?>
            <tr>
                <td><?= $pegawaiMasaPersiapanPensiun->terhitung_mulai_tanggal ?></td>
                <td><?= $pegawaiMasaPersiapanPensiun->catatan ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>