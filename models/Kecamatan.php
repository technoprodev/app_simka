<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kecamatan".
 *
 * @property string $id
 * @property string $id_kota_kabupaten
 * @property string $nama
 *
 * @property KotaKabupaten $kotaKabupaten
 * @property Kelurahan[] $kelurahans
 */
class Kecamatan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kecamatan';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 7],
            [['id'], 'unique'],

            //id_kota_kabupaten
            [['id_kota_kabupaten'], 'required'],
            [['id_kota_kabupaten'], 'string', 'max' => 4],
            [['id_kota_kabupaten'], 'exist', 'skipOnError' => true, 'targetClass' => KotaKabupaten::className(), 'targetAttribute' => ['id_kota_kabupaten' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kota_kabupaten' => 'Id Kota Kabupaten',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKotaKabupaten()
    {
        return $this->hasOne(KotaKabupaten::className(), ['id' => 'id_kota_kabupaten']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelurahans()
    {
        return $this->hasMany(Kelurahan::className(), ['id_kecamatan' => 'id']);
    }
}
