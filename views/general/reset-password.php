<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model, ['class' => '']);
}
?>

<div class="padding-top-60 m-padding-top-30">
    <div class="text-center">
        <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo.png" width="140px;" class="">
    </div>
    <div class="margin-top-15"></div>
    <div class="border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
        <div class="clearfix padding-15 border-bottom bg-azure text-center fs-22">
            <span class="text-uppercase">Request Reset Password <?= Yii::$app->params['app.name'] ?></span>
        </div>
        <div class="padding-20 bg-lightest">
            <?php $form = ActiveForm::begin(['id' => 'app']); ?>

            <?= $form->field($model, 'password', ['options' => ['class' => 'form-wrapper']])->begin(); ?>
                <?= Html::activeLabel($model, 'password', ['class' => 'form-label']); ?>
                <?= Html::activePasswordInput($model, 'password', ['class' => 'form-text', 'maxlength' => true]); ?>
                <?= Html::error($model, 'password', ['class' => 'help-block help-block-error']); ?>
            <?= $form->field($model, 'password')->end(); ?>

            <?php if ($error) : ?>
                <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <?= Html::submitButton('Reset Password', ['class' => 'button border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="bg-lighter padding-y-10">
            <div class="text-center text-azure fs-18"><?= Yii::$app->params['app.description'] ?></div>
            <div class="text-center fs-12 margin-top-2"><?= Yii::$app->params['app.owner'] ?> © <?= date('Y') ?></div>
        </div>
    </div>
</div>