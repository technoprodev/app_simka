$(document).ready(function() {
    if (true) {
        window.datatablesDefaultOptions = {
            autoWidth: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            dom: '<"clearfix margin-bottom-30"lBf><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
            lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
            orderCellsTop: true,
            // stateSave: true,
            // ordering: false,
            // order: [[1, 'asc'], [2, 'asc']],
            order: [],
            buttons: {
                dom: {
                    container: {
                        className: 'dt-buttons pull-left hidden-sm-less'
                    },
                    button: {
                        className: 'margin-left-5 margin-bottom-5 button',
                        active: 'bg-azure border-azure'
                    }
                },
                buttons: [
                    {
                        extend: 'colvis',
                        title: 'Data show/hide',
                        text: 'Show/hide <i class="fa fa-angle-down"></i>'
                    },
                    {
                        extend: 'copy',
                        title: 'Data export',
                        text: 'Copy'
                    },
                    {
                        extend: 'csv',
                        title: 'Data export',
                        text: 'Csv'
                    },
                    {
                        extend: 'excel',
                        title: 'Data export',
                        text: 'Excel',
                        customize: function (xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];

                            var table = $('row c', sheet);
                            table.attr('s', '25');

                            var colsB = $('row c[r^="B"]', sheet);
                            // console.log($(sheet).find('colsB')[0].childNodes.length) // lenght columns
                            // console.log($(sheet).find('sheetData')[0].childNodes.length) // lenght rows
                            colsB.each(function(i, item) {
                                var $item = $(item);
                                // $item.attr('s', '57');
                                $item.attr('t', 'inlineStr');
                                $item.html("<is><t>"+ $item.text() +"</t></is>");
                            });

                            var colsO = $('row c[r^="O"]', sheet);
                            colsO.each(function(i, item) {
                                var $item = $(item);
                                $item.attr('t', 'inlineStr');
                                $item.html("<is><t>"+ $item.text() +"</t></is>");
                            });
                        },
                        exportOptions: {
                            columns: ':not(:first-child)',
                        }
                    },
                    {
                        extend: 'pdf',
                        title: 'Data export',
                        text: 'Pdf'
                    },
                    {
                        extend: 'print',
                        title: 'Data export',
                        text: 'Print'
                    }
                ]
            },
            language: {
                lengthMenu : '_MENU_',
                search: '',
                searchPlaceholder: 'search all here...',
                buttons: {
                    copyTitle: 'Title',
                    copyKeys: 'copy keys',
                    copySuccess: {
                        _: '%d rows copied',
                        1: '1 row copied'
                    }
                }
            },
            colReorder: true
        }
    }

    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-tunkin');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('export/datatables-tunkin', {tahun : tahun, bulan : bulan}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('export/tunkin-update/' + data, {tab: 'tunkin'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-pencil cdexsw text-cyan xswzaq" data-toggle="tooltip" data-placement="top" title="Update Pegawai"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'status_kepegawaian',
                            display: 'status_kepegawaian',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'nip',
                            display: 'nip',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-orange">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'tunkin_nama',
                            display: 'tunkin_nama',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '        <span class="fs-14 text-dark">' + data + '</span>' +
                                
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'golongan',
                            display: 'kepangkatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'id',
                            display: 'keterangan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'grade',
                            display: 'grade',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'nominal_tunkin',
                            display: 'nominal_tunkin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'potongan_tunkin',
                            display: 'potongan_tunkin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'nilai_pemotongan',
                            display: 'nilai_pemotongan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jumlah_pemotongan',
                            display: 'jumlah_pemotongan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pajak_pegawai',
                            display: 'pajak_pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'bruto',
                            display: 'bruto',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'potongan_pajak',
                            display: 'potongan_pajak',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'netto',
                            display: 'netto',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'tunkin_nomor_rekening',
                            display: 'tunkin_nomor_rekening',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});