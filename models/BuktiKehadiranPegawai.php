<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "bukti_kehadiran_pegawai".
 *
 * @property integer $id
 * @property integer $bukti_kehadiran
 * @property integer $pegawai
 *
 * @property BuktiKehadiran $buktiKehadiran
 * @property Pegawai $pegawai0
 */
class BuktiKehadiranPegawai extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    
    public static function tableName()
    {
        return 'bukti_kehadiran_pegawai';
    }

    public function rules()
    {
        return [
            //id

            //bukti_kehadiran
            [['bukti_kehadiran'], 'required'],
            [['bukti_kehadiran'], 'integer'],
            [['bukti_kehadiran'], 'exist', 'skipOnError' => true, 'targetClass' => BuktiKehadiran::className(), 'targetAttribute' => ['bukti_kehadiran' => 'id']],

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bukti_kehadiran' => 'Bukti Kehadiran',
            'pegawai' => 'Pegawai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuktiKehadiran()
    {
        return $this->hasOne(BuktiKehadiran::className(), ['id' => 'bukti_kehadiran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
