<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_anak".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $nama_anak
 * @property string $jenis_kelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property integer $jenjang_pendidikan_terakhir
 * @property integer $pekerjaan
 * @property string $status_anak
 * @property string $status_hidup
 * @property string $status_tunjangan
 * @property string $akta_lahir
 *
 * @property Pegawai $pegawai0
 * @property JenjangPendidikan $jenjangPendidikanTerakhir
 * @property Pekerjaan $pekerjaan0
 */
class PegawaiAnak extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_akta_lahir_upload;
    public $virtual_akta_lahir_download;

    public static function tableName()
    {
        return 'pegawai_anak';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //nama_anak
            [['nama_anak'], 'required'],
            [['nama_anak'], 'string', 'max' => 256],

            //jenis_kelamin
            [['jenis_kelamin'], 'required'],
            [['jenis_kelamin'], 'string'],

            //tempat_lahir
            [['tempat_lahir'], 'required'],
            [['tempat_lahir'], 'string', 'max' => 256],

            //tanggal_lahir
            [['tanggal_lahir'], 'required'],
            [['tanggal_lahir'], 'safe'],

            //jenjang_pendidikan_terakhir
            [['jenjang_pendidikan_terakhir'], 'required'],
            [['jenjang_pendidikan_terakhir'], 'integer'],
            [['jenjang_pendidikan_terakhir'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['jenjang_pendidikan_terakhir' => 'id']],

            //pekerjaan
            [['pekerjaan'], 'required'],
            [['pekerjaan'], 'integer'],
            [['pekerjaan'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan' => 'id']],

            //status_anak
            [['status_anak'], 'required'],
            [['status_anak'], 'string'],

            //status_hidup
            [['status_hidup'], 'required'],
            [['status_hidup'], 'string'],

            //status_tunjangan
            [['status_tunjangan'], 'required'],
            [['status_tunjangan'], 'string'],

            //akta_lahir
            [['akta_lahir'], 'string', 'max' => 256],
            
            //virtual_akta_lahir_download
            [['virtual_akta_lahir_download'], 'safe'],
            
            //virtual_akta_lahir_upload
            [['virtual_akta_lahir_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_akta_lahir_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_akta_lahir_upload");
        if ($this->virtual_akta_lahir_upload) {
            $this->akta_lahir = $this->virtual_akta_lahir_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_akta_lahir_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_akta_lahir_upload");
        if ($this->virtual_akta_lahir_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_anak-akta_lahir') . '/' . $this->id . '/' . $this->akta_lahir;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->akta_lahir = $this->virtual_akta_lahir_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_akta_lahir_upload) {
            $path = Yii::getAlias('@upload-pegawai_anak-akta_lahir') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_akta_lahir_upload->saveAs($path . '/' . $this->akta_lahir);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_anak-akta_lahir') . '/' . $this->id . '/' . $this->akta_lahir;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->akta_lahir) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_anak-akta_lahir');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_akta_lahir_download = $path . '/' . $this->akta_lahir;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'nama_anak' => 'Nama Anak',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenjang_pendidikan_terakhir' => 'Jenjang Pendidikan Terakhir',
            'pekerjaan' => 'Pekerjaan',
            'status_anak' => 'Status Anak',
            'status_hidup' => 'Status Hidup',
            'status_tunjangan' => 'Status Tunjangan',
            'akta_lahir' => 'Akta Lahir',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangPendidikanTerakhir()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id' => 'jenjang_pendidikan_terakhir']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaan0()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan']);
    }
}
