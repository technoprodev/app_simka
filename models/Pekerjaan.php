<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property integer $id
 * @property string $pekerjaan
 *
 * @property Pegawai[] $pegawais
 * @property Pegawai[] $pegawais0
 * @property Pegawai[] $pegawais1
 * @property PegawaiAnak[] $pegawaiAnaks
 * @property PegawaiSaudaraKandung[] $pegawaiSaudaraKandungs
 */
class Pekerjaan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'pekerjaan';
    }

    public function rules()
    {
        return [
            //id

            //pekerjaan
            [['pekerjaan'], 'required'],
            [['pekerjaan'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pekerjaan' => 'Pekerjaan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['pekerjaan_ayah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais0()
    {
        return $this->hasMany(Pegawai::className(), ['pekerjaan_ibu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais1()
    {
        return $this->hasMany(Pegawai::className(), ['pekerjaan_pasangan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiAnaks()
    {
        return $this->hasMany(PegawaiAnak::className(), ['pekerjaan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiSaudaraKandungs()
    {
        return $this->hasMany(PegawaiSaudaraKandung::className(), ['pekerjaan' => 'id']);
    }
}
