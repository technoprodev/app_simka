<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kehadiran".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $tanggal
 * @property integer $fleksi_time
 * @property string $waktu_mulai_istirahat
 * @property string $waktu_selesai_istirahat
 * @property string $jadwal_masuk
 * @property string $jadwal_batas_awal_masuk
 * @property string $jadwal_batas_akhir_masuk
 * @property string $jadwal_pulang
 * @property string $jadwal_batas_awal_pulang
 * @property string $jadwal_batas_akhir_pulang
 * @property string $waktu_masuk
 * @property string $waktu_pulang
 * @property string $waktu_terakhir_diupdate
 * @property integer $keterangan
 * @property string $keterangan_diupdate
 *
 * @property Pegawai $pegawai0
 * @property KeteranganKehadiran $keterangan0
 */
class Kehadiran extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kehadiran';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'unique', 'targetAttribute' => ['pegawai', 'tanggal']],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //tanggal
            [['tanggal'], 'required'],
            [['tanggal'], 'safe'],
            [['tanggal'], 'unique', 'targetAttribute' => ['pegawai', 'tanggal']],

            //fleksi_time
            [['fleksi_time'], 'integer'],

            //waktu_mulai_istirahat
            [['waktu_mulai_istirahat'], 'safe'],

            //waktu_selesai_istirahat
            [['waktu_selesai_istirahat'], 'safe'],

            //jadwal_masuk
            [['jadwal_masuk'], 'safe'],

            //jadwal_batas_awal_masuk
            [['jadwal_batas_awal_masuk'], 'safe'],

            //jadwal_batas_akhir_masuk
            [['jadwal_batas_akhir_masuk'], 'safe'],

            //jadwal_pulang
            [['jadwal_pulang'], 'safe'],

            //jadwal_batas_awal_pulang
            [['jadwal_batas_awal_pulang'], 'safe'],

            //jadwal_batas_akhir_pulang
            [['jadwal_batas_akhir_pulang'], 'safe'],

            //waktu_masuk
            [['waktu_masuk'], 'safe'],

            //waktu_pulang
            [['waktu_pulang'], 'safe'],

            //waktu_terakhir_diupdate
            [['waktu_terakhir_diupdate'], 'safe'],

            //keterangan
            [['keterangan'], 'integer'],
            [['keterangan'], 'exist', 'skipOnError' => true, 'targetClass' => KeteranganKehadiran::className(), 'targetAttribute' => ['keterangan' => 'id']],

            //keterangan_diupdate
            [['keterangan_diupdate'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'tanggal' => 'Tanggal',
            'fleksi_time' => 'Fleksi Time',
            'waktu_mulai_istirahat' => 'Waktu Mulai Istirahat',
            'waktu_selesai_istirahat' => 'Waktu Selesai Istirahat',
            'jadwal_masuk' => 'Jadwal Masuk',
            'jadwal_batas_awal_masuk' => 'Jadwal Batas Awal Masuk',
            'jadwal_batas_akhir_masuk' => 'Jadwal Batas Akhir Masuk',
            'jadwal_pulang' => 'Jadwal Pulang',
            'jadwal_batas_awal_pulang' => 'Jadwal Batas Awal Pulang',
            'jadwal_batas_akhir_pulang' => 'Jadwal Batas Akhir Pulang',
            'waktu_masuk' => 'Waktu Masuk',
            'waktu_pulang' => 'Waktu Pulang',
            'waktu_terakhir_diupdate' => 'Waktu Terakhir Diupdate',
            'keterangan' => 'Keterangan',
            'keterangan_diupdate' => 'Keterangan Diupdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeterangan0()
    {
        return $this->hasOne(KeteranganKehadiran::className(), ['id' => 'keterangan']);
    }
}
