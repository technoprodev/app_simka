<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\Cuti;
use app_simka\models\Kehadiran;

class KepalaBiroController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['kepala-biro']]
            ]),
        ];
    }

    ///

    public function actionDatatablesCuti()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'c.status_pengajuan' => 'Diterima Atasan 2',
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionDatatablesCutiMonitoring()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'c.status_pengajuan' => ['Diajukan', 'Diterima Atasan 1', 'Direvisi Atasan 1', 'Direvisi Atasan 2', 'Direvisi'],
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionDatatablesCutiRiwayat()
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'c.id',
                'kh.keterangan',
                'c.status_pengajuan',
                'DATE_FORMAT(c.dari_tanggal, "%d/%m/%Y") AS dari_tanggal',
                'DATE_FORMAT(c.sampai_tanggal, "%d/%m/%Y") AS sampai_tanggal',
                'DATE_FORMAT(c.waktu_pengajuan, "%d/%m/%Y %H:%i") AS waktu_pengajuan',
                'p.nama AS pegawai',
                'p1.nama AS atasan_1',
                'p2.nama AS atasan_2',
            ])
            ->from('cuti c')
            ->join('JOIN', 'keterangan_kehadiran kh', 'kh.id = c.keterangan_kehadiran')
            ->join('LEFT JOIN', 'pegawai p', 'p.id = c.pegawai')
            ->join('LEFT JOIN', 'pegawai p1', 'p1.id = c.atasan_1')
            ->join('LEFT JOIN', 'pegawai p2', 'p2.id = c.atasan_2')
            ->where([
                'c.status_pengajuan' => ['Ditolak Atasan 1', 'Ditolak Atasan 2', 'Diterima', 'Ditolak'],
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Cuti::getDb());
    }

    public function actionCuti($id = null)
    {
        if (!$id) {
            return $this->render('list-cuti', [
                'title' => 'Pengajuan Cuti Online',
            ]);
        }

        if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('detail-cuti', [
            'model' => $model,
            'title' => 'Detail Cuti',
        ]);
    }

    public function actionCutiUpdate($id)
    {
        $error = true;

        if (isset($id)) {
            if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
                throw new \yii\web\HttpException(404, 'The requested page does not exist.');
            }
        } else {
            $model['cuti'] = new Cuti();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['cuti']->load($post);

            $transaction['cuti'] = Cuti::getDb()->beginTransaction();

            try {
                if (!$model['cuti']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                }
                
                $error = false;

                if ($model['cuti']->status_pengajuan == 'Diterima') {
                    $begin = \DateTime::createFromFormat('d/m/Y', $model['cuti']->dari_tanggal);
                    $end = \DateTime::createFromFormat('d/m/Y', $model['cuti']->sampai_tanggal);
                    $range = \DateInterval::createFromDateString('1 day');
                    $allInterval = new \DatePeriod($begin, $range, $end->modify('+1 day'));

                    $count = 0;
                    foreach ($allInterval as $date) {
                        $d = $date->format('Y-m-d');
                        if ($kehadiran = Kehadiran::find()->where(['tanggal' => $d, 'pegawai' => $model['cuti']->pegawai])->one()) {
                            if ($kehadiran->keterangan == 7) continue;
                            if ($model['cuti']->keterangan_kehadiran == 1111) {
                                $kehadiran->keterangan_diupdate = 'Tidak';
                            } else {
                                $kehadiran->keterangan = $model['cuti']->keterangan_kehadiran;
                                $kehadiran->keterangan_diupdate = 'Ya';
                                $count++;
                            }
                            if (!$kehadiran->save()) {
                                $error = true;

                                throw new \yii\web\HttpException(400, 'Data cannot be saved because there is an error in form validation. Please fix the error before submitting the form.');
                            }
                        }
                    }
                    if ($model['cuti']->keteranganKehadiran->mengurangi_sisa_cuti == 'Cuti Tahunan') {
                        if ($count && $count <= $model['cuti']->pegawai0->cuti_N_2) {
                            $model['cuti']->pegawai0->cuti_N_2 = $model['cuti']->pegawai0->cuti_N_2 - $count;
                            $count = 0;
                        } else if ($count && $count > $model['cuti']->pegawai0->cuti_N_2) {
                            $count = $count - $model['cuti']->pegawai0->cuti_N_2;
                            $model['cuti']->pegawai0->cuti_N_2 = 0;
                        }
                        if ($count && $count <= $model['cuti']->pegawai0->cuti_N_1) {
                            $model['cuti']->pegawai0->cuti_N_1 = $model['cuti']->pegawai0->cuti_N_1 - $count;
                            $count = 0;
                        } else if ($count && $count > $model['cuti']->pegawai0->cuti_N_1) {
                            $count = $count - $model['cuti']->pegawai0->cuti_N_1;
                            $model['cuti']->pegawai0->cuti_N_1 = 0;
                        }
                        if ($count && $count <= $model['cuti']->pegawai0->cuti_N) {
                            $model['cuti']->pegawai0->cuti_N = $model['cuti']->pegawai0->cuti_N - $count;
                            $count = 0;
                        } else if ($count && $count > $model['cuti']->pegawai0->cuti_N) {
                            $count = $count - $model['cuti']->pegawai0->cuti_N;
                            $model['cuti']->pegawai0->cuti_N = 0;
                        }
                    }
                    if ($model['cuti']->keterangan_kehadiran == 1111) {
                        $result = \Yii::$app->db->createCommand("call assign_kehadiran(:dari_tanggal, :sampai_tanggal);")
                            ->bindValue(':dari_tanggal', date('Y-m-d', strtotime(str_replace('/', '-', $model['cuti']->dari_tanggal))))
                            ->bindValue(':sampai_tanggal', date('Y-m-d', strtotime(str_replace('/', '-', $model['cuti']->sampai_tanggal))))
                            ->execute();
                    }
                }


                $transaction['cuti']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Throwable $e) {
                $error = true;
                $transaction['cuti']->rollBack();
                if (get_class($e) == 'yii\web\HttpException') Yii::$app->session->setFlash('error', $e->getMessage()); else throw $e;
            }
        } else {
        }

        if ($error)
            return $this->render('form-cuti-update', [
                'model' => $model,
                'title' => 'Verifikasi Cuti Online',
            ]);
        else
            return $this->redirect(['cuti']);
    }

    public function actionCutiPrint($id)
    {
        if (($model['cuti'] = Cuti::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $title = 'Bukti Cuti Sadewa ' . $model['cuti']->pegawai0->nama;
        $this->layout = 'download';
        // return $this->render('print-cuti', ['model' => $model, 'title' => $title]);

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($this->render('print-cuti', ['model' => $model, 'title' => $title]));
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream($title . '.pdf');
        exit;

    }
}
