<?php
namespace app_simka\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_simka\models\User;
use app_simka\models\Pegawai;
use app_simka\models\PegawaiAnak;
use app_simka\models\PegawaiDiklat;
use app_simka\models\PegawaiHukumanDisiplin;
use app_simka\models\PegawaiKenaikanGajiBerkala;
use app_simka\models\PegawaiKepangkatan;
use app_simka\models\PegawaiKunjunganLuarNegeri;
use app_simka\models\PegawaiKursusPelatihan;
use app_simka\models\PegawaiMasaPersiapanPensiun;
use app_simka\models\PegawaiMutasi;
use app_simka\models\PegawaiOrganisasi;
use app_simka\models\PegawaiPelaksana;
use app_simka\models\PegawaiPemberhentian;
use app_simka\models\PegawaiPendidikan;
use app_simka\models\PegawaiPenetapanAngkaKredit;
use app_simka\models\PegawaiPenghargaan;
use app_simka\models\PegawaiSaudaraKandung;
use app_simka\models\FilterKehadiran;

class PejabatController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => $this->access([
                ['*', true, ['pejabat']]
            ]),
        ];
    }

    public function actionDatatablesPegawai()
    {
        $result = (new \yii\db\Query())->select('get_pegawai_rec(' . Yii::$app->user->identity->pegawai->unit_kerja . ')')->scalar();
        $pegawais = $result ? explode(',', $result) : null;
           
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama',
                'p.nip',
                'p.pin',
                'CONCAT(k.pangkat, " (", k.golongan, " / ", k.ruang, ")") AS kepangkatan',
                'CONCAT(jjj.jenis_jabatan, " - ", jj.jenjang_jabatan) AS jenjang_jabatan',
                'p.nama_jabatan',
                'uk.unit_kerja',
                'e.eselon',
                'g.grade',
                'kp.kedudukan_pegawai',
                'p.mendapat_tunkin',
                'p.nama_panggilan',
                'p.jenis_kelamin',
                'CONCAT(p.tempat_lahir, ", ", DATE_FORMAT(p.tanggal_lahir, "%d/%m/%Y")) AS ttl',
                'a.agama',
                'sm.status_marital',
                'p.golongan_darah',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'user u', 'u.id = p.id')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
            ->join('LEFT JOIN', 'jenis_jabatan jjj', 'jjj.id = jj.jenis_jabatan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->join('LEFT JOIN', 'eselon e', 'e.id = p.eselon')
            ->join('LEFT JOIN', 'grade g', 'g.id = p.grade')
            ->join('LEFT JOIN', 'kedudukan_pegawai kp', 'kp.id = p.kedudukan_pegawai')
            ->join('LEFT JOIN', 'agama a', 'a.id = p.agama')
            ->join('LEFT JOIN', 'status_marital sm', 'sm.id = p.status_marital')
            ->where([
                'p.id' => $pegawais,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKepangkatan($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'k.pangkat',
                'k.golongan',
                'k.ruang',
                's.status_kepegawaian',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_kepangkatan-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_kepangkatan p')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan')
            ->join('LEFT JOIN', 'status_kepegawaian s', 's.id = p.status_kepegawaian')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKenaikanGajiBerkala($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.gaji_pokok_lama',
                'p.gaji_pokok_baru',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'CONCAT(k.pangkat, " (", k.golongan, "/", k.golongan, ")") AS kepangkatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_kenaikan_gaji_berkala-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_kenaikan_gaji_berkala p')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan_saat_kenaikan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiJabatan($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.instansi',
                'p.unit_kerja',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_mutasi-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_mutasi p')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPelaksana($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_pelaksana',
                'p.instansi',
                'p.unit_kerja',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.terhitung_sampai_sekarang',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
            ])
            ->from('pegawai_pelaksana p')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPenetapanAngkaKredit($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.periode_penilaian_awal',
                'p.periode_penilaian_akhir',
                'p.unsur_utama',
                'p.unsur_penunjang',
                'p.total_angkat_kredit',
                'CONCAT("upload/pegawai_penetapan_angka_kredit-arsip_pak/", p.id, "/", p.arsip_pak) AS arsip_pak',
            ])
            ->from('pegawai_penetapan_angka_kredit p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiSaudaraKandung($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama',
                'p.jenis_kelamin',
                'p.tempat_lahir',
                'p.tanggal_lahir',
                'pk.pekerjaan',
                'p.anak_ke',
            ])
            ->from('pegawai_saudara_kandung p')
            ->join('LEFT JOIN', 'pekerjaan pk', 'pk.id = p.pekerjaan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiAnak($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_anak',
                'p.jenis_kelamin',
                'p.tempat_lahir',
                'p.tanggal_lahir',
                'jp.jenjang_pendidikan',
                'pk.pekerjaan',
                'p.status_anak',
                'p.status_hidup',
                'p.status_tunjangan',
                'CONCAT("upload/pegawai_anak-akta_lahir/", p.id, "/", p.akta_lahir) AS akta_lahir',
            ])
            ->from('pegawai_anak p')
            ->join('LEFT JOIN', 'jenjang_pendidikan jp', 'jp.id = p.jenjang_pendidikan_terakhir')
            ->join('LEFT JOIN', 'pekerjaan pk', 'pk.id = p.pekerjaan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPendidikan($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_sekolah',
                'jp.jenjang_pendidikan',
                'p.jurusan',
                'p.nilai',
                'p.skala_nilai',
                'p.alamat_sekolah',
                'p.tahun_masuk',
                'p.tahun_lulus',
                'CONCAT("upload/pegawai_pendidikan-ijazah/", p.id, "/", p.ijazah) AS ijazah',
            ])
            ->from('pegawai_pendidikan p')
            ->join('LEFT JOIN', 'jenjang_pendidikan jp', 'jp.id = p.jenjang_pendidikan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPenghargaan($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_penghargaan',
                'p.nama_penghargaan',
                'p.tahun_penghargaan',
                'p.instansi_pemberi_penghargaan',
                'CONCAT("upload/pegawai_penghargaan-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_penghargaan p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiDiklat($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.jenis_diklat',
                'p.nama_diklat',
                'p.penyelenggara',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.tempat',
                'p.keterangan',
                'CONCAT("upload/pegawai_diklat-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
                'p.nomor_sttp',
                'p.tanggal_sttp',
            ])
            ->from('pegawai_diklat p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKursusPelatihan($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.nama_kursus_pelatihan',
                'p.penyelenggara',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.lokasi',
                'CONCAT("upload/pegawai_kursus_pelatihan-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_kursus_pelatihan p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiKunjunganLuarNegeri($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'n.nama AS negara_kunjungan',
                'p.tujuan_kunjungan',
                'p.tanggal_mulai',
                'p.tanggal_selesai',
                'p.sumber_biaya',
            ])
            ->from('pegawai_kunjungan_luar_negeri p')
            ->join('LEFT JOIN', 'negara n', 'n.id = p.negara_kunjungan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiOrganisasi($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.organisasi',
                'p.jabatan',
                'p.tahun_mulai',
                'p.tahun_selesai',
                'p.lokasi',
                'p.periode_organisasi',
                'CONCAT("upload/pegawai_organisasi-sertifikat/", p.id, "/", p.sertifikat) AS sertifikat',
            ])
            ->from('pegawai_organisasi p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiHukumanDisiplin($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'jhd.jenis_hukuman_disiplin',
                'p.penjelasan',
                'p.terhitung_mulai_tanggal',
                'p.terhitung_sampai_tanggal',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_hukuman_disiplin-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_hukuman_disiplin p')
            ->join('LEFT JOIN', 'jenis_hukuman_disiplin jhd', 'jhd.id = p.jenis_hukuman_disiplin')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiPemberhentian($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'jp.jenis_pemberhentian',
                'p.terhitung_mulai_tanggal',
                'k.pangkat',
                'k.golongan',
                'k.ruang',
                'p.instansi_saat_pemberhentian',
                'p.unit_kerja_saat_pemberhentian',
                'jj.jenjang_jabatan',
                'jej.jenis_jabatan',
                'p.nama_jabatan_saat_diberhentikan',
                'p.catatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_pemberhentian-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_pemberhentian p')
            ->join('LEFT JOIN', 'jenis_pemberhentian jp', 'jp.id = p.jenis_pemberhentian')
            ->join('LEFT JOIN', 'kepangkatan k', 'k.id = p.kepangkatan_saat_pemberhentian')
            ->join('LEFT JOIN', 'jenjang_jabatan jj', 'jj.id = p.jenjang_jabatan_saat_diberhentikan')
            ->join('LEFT JOIN', 'jenis_jabatan jej', 'jej.id = jj.jenis_jabatan')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionDatatablesPegawaiMasaPersiapanPensiun($idPegawai)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'p.id',
                'p.terhitung_mulai_tanggal',
                'p.catatan',
                'p.jenis_sk',
                'p.nomor_sk',
                'p.tanggal_sk',
                'p.pejabat_penetap_sk',
                'CONCAT("upload/pegawai_masa_persiapan_pensiun-arsip_sk/", p.id, "/", p.arsip_sk) AS arsip_sk',
            ])
            ->from('pegawai_masa_persiapan_pensiun p')
            ->where([
                'p.pegawai' => $idPegawai,
            ])
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionPegawai($id = null, $tab = 'jabatan')
    {
        if (!$id) {
            return $this->render('list-pegawai', [
                'title' => 'Daftar Pegawai',
            ]);
        }

        if (($model['pegawai'] = Pegawai::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        return $this->render('drh', [
            'model' => $model,
            'tab' => $tab,
            'title' => 'Daftar Riwayat Hidup Pegawai : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
        ]);
    }

    public function datatablesKehadiran($idPegawai, $bulan, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'k.id',
                'DATE_FORMAT(k.tanggal, "%d %b") AS tanggal',
                'DATE_FORMAT(k.jadwal_masuk, "%H:%i:%s") AS jadwal_masuk',
                'DATE_FORMAT(k.jadwal_pulang, "%H:%i:%s") AS jadwal_pulang',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_masuk, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_masuk',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(k.waktu_pulang, "%H:%i:%s")
                    ELSE NULL
                END AS waktu_pulang',
                'DATE_FORMAT(k.waktu_terakhir_diupdate, "%H:%i:%s") AS waktu_terakhir_diupdate',
                '@telat_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS telat_detik',
                '@kecepetan_detik := CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kecepetan_detik',
                'TIME_FORMAT(SEC_TO_TIME(@telat_detik), "%H:%i:%s") AS telat',
                'CASE WHEN @kecepetan_detik > 0 THEN TIME_FORMAT(SEC_TO_TIME(@kecepetan_detik), "%H:%i:%s") ELSE NULL END AS kecepetan',
                'LEAST(
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END +
                    CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END
                , 100) AS potongan_tunkin',
                'CASE
                    WHEN kh.tampilkan_waktu_absen = "Ya" THEN DATE_FORMAT(TIMEDIFF(TIMEDIFF(k.waktu_pulang, k.waktu_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)), "%H:%i:%s")
                    ELSE NULL
                END AS jumlah_jam',
                '@kurang_detik := CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'k.keterangan as idKeterangan',
                'kh.color as color',
                'kh.keterangan as keterangan',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
        ;

        return $query;
    }

    public function datatablesKehadiranTahunan($idPegawai, $tahun)
    {
        $query = new \yii\db\Query();
        $query
            ->select([
                'SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN 0
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE 0
                END) AS kurang_detik',
            ])
            ->from('kehadiran k')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->where([
                'k.pegawai' => $idPegawai,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->andWhere([
                'between', 'MONTH(k.tanggal)', '01', date('m'),
            ])
            ->groupBy(['k.pegawai'])
        ;

        return $query;
    }

    public function actionDatatablesKehadiran($idPegawai, $bulan, $tahun)
    {
        return $this->datatables($this->datatablesKehadiran($idPegawai, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function datatablesKehadiranBulanan($idPegawai = null, $bulan, $tahun)
    {
        $subQuery = new \yii\db\Query();
        $subQuery
            ->select([
                'p.id',
                'p.nama',
                'p.nip',
                'p.pin',
                'uk.unit_kerja',
                'LEAST(
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN potongan_tunkin_persen
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL AND DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_masuk, SEC_TO_TIME(fleksi_time*60)) THEN 2
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        GREATEST(
                                            TIMEDIFF(
                                                IFNULL(DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"), k.jadwal_pulang),
                                                k.jadwal_masuk
                                            ),
                                            0
                                        ),
                                        IFNULL(
                                            GREATEST(
                                                TIMEDIFF(
                                                    CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                    k.jadwal_pulang
                                                ),
                                                0
                                            ),
                                            0
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END) +
                    SUM(CASE
                        WHEN kh.potongan_tunkin_persen IS NOT NULL THEN 0
                        WHEN kh.potong_tunkin = "Tidak" THEN 0
                        WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                            LEAST((TIME_TO_SEC(
                                GREATEST(
                                    TIMEDIFF(
                                        k.jadwal_pulang,
                                        IFNULL(
                                            DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i"),
                                            k.jadwal_masuk
                                        )
                                    ),
                                    "00:00:00"
                                )
                            ) - 1 + 30 * 60) DIV (30 * 60) * 0.5, 2)
                        ELSE 0
                    END)
                , 100) AS potongan_tunkin',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_masuk IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    DATE_FORMAT(k.waktu_masuk, "%Y-%m-%d %H:%i"),
                                    k.jadwal_masuk
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS telat',
                'TIME_FORMAT(SEC_TO_TIME(SUM(CASE
                    WHEN kh.potong_tunkin = "Tidak" OR k.waktu_pulang IS NULL THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    k.jadwal_pulang,
                                    IFNULL(
                                        waktu_pulang,
                                        jadwal_masuk
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END)), "%H:%i:%s") AS kecepetan',
                'TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(k.waktu_pulang, k.waktu_masuk)))), "%H:%i:%s") AS jumlah_jam',
                '@kurang_detik := SUM(CASE
                    WHEN kh.potong_disiplin = "Tidak" THEN NULL
                    WHEN k.jadwal_masuk IS NOT NULL AND k.jadwal_pulang IS NOT NULL THEN
                        TIME_TO_SEC(
                            GREATEST(
                                TIMEDIFF(
                                    TIMEDIFF(TIMEDIFF(k.jadwal_pulang, k.jadwal_masuk), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)),
                                    IFNULL(
                                        TIMEDIFF(
                                            TIMEDIFF(
                                                CASE WHEN DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") > ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) THEN ADDTIME(k.jadwal_pulang, SEC_TO_TIME(fleksi_time*60)) ELSE DATE_FORMAT(k.waktu_pulang, "%Y-%m-%d %H:%i") END,
                                                DATE_FORMAT(GREATEST(k.waktu_masuk, k.jadwal_masuk), "%Y-%m-%d %H:%i")
                                            ), TIMEDIFF(k.waktu_selesai_istirahat, k.waktu_mulai_istirahat)
                                        ),
                                        0
                                    )
                                ),
                                "00:00:00"
                            )
                        )
                    ELSE NULL
                END) AS kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(@kurang_detik), "%H:%i:%s") AS kurang_jam',
                'count(case when kh.dihitung_hadir = "Hadir" then k.id end) AS hadir',
                'count(case when kh.dihitung_hadir = "Tidak Hadir" then k.id end) AS tidak_hadir',
                'count(case when kh.id = 1 then k.id end) AS belum_absen',
                'count(case when kh.id = 7 then k.id end) AS libur',
            ])
            ->from('pegawai p')
            ->join('LEFT JOIN', 'kehadiran k', 'k.pegawai = p.id')
            ->join('LEFT JOIN', 'keterangan_kehadiran kh', 'kh.id = k.keterangan')
            ->join('LEFT JOIN', 'unit_kerja uk', 'uk.id = p.unit_kerja')
            ->where([
                // 'p.id' => $idPegawai,
                'MONTH(k.tanggal)' => $bulan,
                'YEAR(k.tanggal)' => $tahun,
            ])
            ->orderBy(['uk.level' => SORT_ASC])
            ->groupBy(['p.id', 'p.nama', 'p.nip', 'p.pin'])
        ;
        $uk = Yii::$app->request->isPost ? Yii::$app->request->post()['columns'][4]['search']['value'] : '';
        if ($uk) {
            $ukIdMentah = (new \yii\db\Query())->select('id')->from('unit_kerja uk')->where(['like', 'unit_kerja', $uk])->orderBy(['level' => SORT_ASC])->scalar();
            $result = (new \yii\db\Query())->select('get_pegawai_rec(' . $ukIdMentah . ')')->scalar();
            $pegawais = $result ? explode(',', $result) : [];
            $subQuery->andWhere(['p.id' => $pegawais]);
        }
        if ($idPegawai) {
            $subQuery->andWhere(['p.id' => $idPegawai]);
        }

        $query = new \yii\db\Query();
        $query
            ->select([
                'id',
                'nama',
                'nip',
                'pin',
                'unit_kerja',
                'potongan_tunkin',
                'telat',
                'kecepetan',
                'jumlah_jam',
                'kurang_detik',
                'TIME_FORMAT(SEC_TO_TIME(kurang_detik), "%H:%i:%s") AS kurang_jam',
                '(hadir + tidak_hadir + belum_absen) AS hari_kerja',
                'hadir',
                'tidak_hadir',
                'belum_absen',
                'libur',
            ])
            ->from(['sq' => $subQuery])
        ;

        return $query;
        // return $this->json($query->all());
    }

    public function actionDatatablesKehadiranBulanan(/*$idPegawai, */$bulan, $tahun)
    {
        return $this->datatables($this->datatablesKehadiranBulanan(null, $bulan, $tahun), Yii::$app->request->post(), Pegawai::getDb());
    }

    public function actionKehadiran($id = null, $bulan = null, $tahun = null)
    {
        $model['filter_kehadiran'] = new FilterKehadiran();
        if ($tahun) {
            $model['filter_kehadiran']->tahun = $tahun;
        }
        
        if (!$id) {
            return $this->render('list-kehadiran-bulanan', [
                'model' => $model,
                'title' => 'Daftar Kehadiran Pegawai',
            ]);
        }

        if (($model['pegawai'] = Pegawai::find()->where(['id' => $id])->one()) == null) {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }

        $model['kehadiran_bulanan'] = $this->datatablesKehadiranBulanan($model['pegawai']->id, $model['filter_kehadiran']->bulan, $model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_bulanan'] = [
            'hari_kerja' => $model['kehadiran_bulanan']['hari_kerja'],
            'hadir' => $model['kehadiran_bulanan']['hadir'],
            'tidak_hadir' => $model['kehadiran_bulanan']['tidak_hadir'],
            'belum_absen' => $model['kehadiran_bulanan']['belum_absen'],
            'libur' => $model['kehadiran_bulanan']['libur'],
            'potongan_tunkin' => $model['kehadiran_bulanan']['potongan_tunkin'],
            'uang_makan' => $model['kehadiran_bulanan']['hadir'],
            'disiplin' => $model['kehadiran_bulanan']['kurang_detik'],
        ];

        $model['kehadiran_tahunan'] = $this->datatablesKehadiranTahunan($model['pegawai']->id,$model['filter_kehadiran']->tahun)->one();
        $model['rekapitulasi_kehadiran_tahunan'] = [
            'disiplin' => $model['kehadiran_tahunan']['kurang_detik'],
        ];
        
        return $this->render('list-kehadiran', [
            'model' => $model,
            'title' => 'Daftar Kehadiran : <span class="text-lightest">' . $model['pegawai']->nama . '</span>',
        ]);
    }
}