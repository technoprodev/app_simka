<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/pejabat/list-pegawai.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Daftar Pegawai</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-pegawai table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pin</th>
                        <th>Kepangkatan</th>
                        <th>Jenjang Jabatan</th>
                        <th>Nama Jabatan</th>
                        <th>Unit Kerja</th>
                        <th>Eselon</th>
                        <th>Grade</th>
                        <th>Kedudukan</th>
                        <th>Mendapat Tunkin</th>
                        <th>Nama Panggilan</th>
                        <th>Jenis Kelamin</th>
                        <th>TTL</th>
                        <th>Agama</th>
                        <th>Status Pernikahan</th>
                        <th>Golongan Darah</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nip..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search pin..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kepangkatan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jenjang jabatan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama jabatan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search unit kerja..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search eselon..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search grade..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search kedudukan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search mendapat tunkin..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama panggilan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jenis kelamin..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search ttl..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search agama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status marital..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search golongan darah..."/></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>