<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/export/list-tunkin1.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$this->registerJs(
    'var tahun = ' . json_encode($model['filter_kehadiran']->tahun) . ';' .
    'var bulan = ' . json_encode($model['filter_kehadiran']->bulan) . ';' .
    '',
    3
);

$bulans = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', ];
$tahuns = ['2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024', ];
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Kelola Data Pegawai</span>
            <?php if ($this->context->can('create')): ?>
        <?php endif; ?>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app'], 'method' => 'get', 'action' => Url::to(['export/tunkin'])]); ?>
            <div class="box box-break-md box-gutter">
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'bulan', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'bulan', $bulans, ['class' => 'form-dropdown', 'name' => 'bulan']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'bulan')->end(); ?>
                </div>
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'tahun', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'tahun', $tahuns, ['class' => 'form-dropdown', 'name' => 'tahun']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'tahun')->end(); ?>
                </div>
                <div class="box-2">
                    <div class="form-wrapper margin-0">
                        <button class="button button-block border-azure bg-azure m-margin-top-5" type="submit">Tampilkan</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-tunkin table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th></th>
                        <th>Status</th>
                        <th>Nip</th>
                        <th>Nama</th>
                        <th>Golongan</th>
                        <th>Keterangan</th>
                        <th>Grade</th>
                        <th>Tunjangan</th>
                        <th>Potongan</th>
                        <th>Nilai Pemotongan</th>
                        <th>Jumlah Pemotongan</th>
                        <th>Pajak Pegawai</th>
                        <th>Bruto</th>
                        <th>Potongan Pajak</th>
                        <th>Netto</th>
                        <th>Nomor Rekening</th>
                    </tr>
                    <tr class="dt-search">
                        <th></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search status..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nip..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search golongan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search keterangan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search grade..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search tunjangan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potongan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nilai pemotongan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search jumlah pemotongan..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search pajak pegawai..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search bruto..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search potongan pajak..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search netto..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nomor rekening..."/></th>
                </thead>
            </table>
        </div>
    </div>
</div>