<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jadwal_kehadiran".
 *
 * @property integer $id
 * @property string $berlaku_bagi
 * @property integer $unit_kerja
 * @property integer $pegawai
 * @property integer $prioritas
 * @property string $status
 * @property string $berlaku_dari
 * @property string $berlaku_sampai
 * @property string $catatan
 * @property integer $fleksi_time
 * @property string $senin_waktu_mulai_istirahat
 * @property string $selasa_waktu_mulai_istirahat
 * @property string $rabu_waktu_mulai_istirahat
 * @property string $kamis_waktu_mulai_istirahat
 * @property string $jumat_waktu_mulai_istirahat
 * @property string $sabtu_waktu_mulai_istirahat
 * @property string $minggu_waktu_mulai_istirahat
 * @property string $senin_waktu_selesai_istirahat
 * @property string $selasa_waktu_selesai_istirahat
 * @property string $rabu_waktu_selesai_istirahat
 * @property string $kamis_waktu_selesai_istirahat
 * @property string $jumat_waktu_selesai_istirahat
 * @property string $sabtu_waktu_selesai_istirahat
 * @property string $minggu_waktu_selesai_istirahat
 * @property string $senin_masuk
 * @property string $senin_pulang
 * @property string $senin_batas_awal_masuk
 * @property string $senin_batas_akhir_masuk
 * @property string $senin_batas_awal_pulang
 * @property string $senin_batas_akhir_pulang
 * @property string $senin_libur
 * @property string $selasa_masuk
 * @property string $selasa_pulang
 * @property string $selasa_batas_awal_masuk
 * @property string $selasa_batas_akhir_masuk
 * @property string $selasa_batas_awal_pulang
 * @property string $selasa_batas_akhir_pulang
 * @property string $selasa_libur
 * @property string $rabu_masuk
 * @property string $rabu_pulang
 * @property string $rabu_batas_awal_masuk
 * @property string $rabu_batas_akhir_masuk
 * @property string $rabu_batas_awal_pulang
 * @property string $rabu_batas_akhir_pulang
 * @property string $rabu_libur
 * @property string $kamis_masuk
 * @property string $kamis_pulang
 * @property string $kamis_batas_awal_masuk
 * @property string $kamis_batas_akhir_masuk
 * @property string $kamis_batas_awal_pulang
 * @property string $kamis_batas_akhir_pulang
 * @property string $kamis_libur
 * @property string $jumat_masuk
 * @property string $jumat_pulang
 * @property string $jumat_batas_awal_masuk
 * @property string $jumat_batas_akhir_masuk
 * @property string $jumat_batas_awal_pulang
 * @property string $jumat_batas_akhir_pulang
 * @property string $jumat_libur
 * @property string $sabtu_masuk
 * @property string $sabtu_pulang
 * @property string $sabtu_batas_awal_masuk
 * @property string $sabtu_batas_akhir_masuk
 * @property string $sabtu_batas_awal_pulang
 * @property string $sabtu_batas_akhir_pulang
 * @property string $sabtu_libur
 * @property string $minggu_masuk
 * @property string $minggu_pulang
 * @property string $minggu_batas_awal_masuk
 * @property string $minggu_batas_akhir_masuk
 * @property string $minggu_batas_awal_pulang
 * @property string $minggu_batas_akhir_pulang
 * @property string $minggu_libur
 *
 * @property UnitKerja $unitKerja
 * @property Pegawai $pegawai0
 */
class JadwalKehadiran extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jadwal_kehadiran';
    }

    public function rules()
    {
        return [
            //id

            //berlaku_bagi
            [['berlaku_bagi'], 'required'],
            [['berlaku_bagi'], 'string'],

            //unit_kerja
            [['unit_kerja'], 'integer'],
            [['unit_kerja'], 'exist', 'skipOnError' => true, 'targetClass' => UnitKerja::className(), 'targetAttribute' => ['unit_kerja' => 'id']],

            //pegawai
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //prioritas
            [['prioritas'], 'integer'],

            //status
            [['status'], 'string'],

            //berlaku_dari
            [['berlaku_dari'], 'required'],
            [['berlaku_dari'], 'safe'],

            //berlaku_sampai
            [['berlaku_sampai'], 'required'],
            [['berlaku_sampai'], 'safe'],

            //catatan
            [['catatan'], 'required'],
            [['catatan'], 'string', 'max' => 256],

            //fleksi_time
            [['fleksi_time'], 'required'],
            [['fleksi_time'], 'integer'],

            //senin_waktu_mulai_istirahat
            [['senin_waktu_mulai_istirahat'], 'safe'],

            //selasa_waktu_mulai_istirahat
            [['selasa_waktu_mulai_istirahat'], 'safe'],

            //rabu_waktu_mulai_istirahat
            [['rabu_waktu_mulai_istirahat'], 'safe'],

            //kamis_waktu_mulai_istirahat
            [['kamis_waktu_mulai_istirahat'], 'safe'],

            //jumat_waktu_mulai_istirahat
            [['jumat_waktu_mulai_istirahat'], 'safe'],

            //sabtu_waktu_mulai_istirahat
            [['sabtu_waktu_mulai_istirahat'], 'safe'],

            //minggu_waktu_mulai_istirahat
            [['minggu_waktu_mulai_istirahat'], 'safe'],

            //senin_waktu_selesai_istirahat
            [['senin_waktu_selesai_istirahat'], 'safe'],

            //selasa_waktu_selesai_istirahat
            [['selasa_waktu_selesai_istirahat'], 'safe'],

            //rabu_waktu_selesai_istirahat
            [['rabu_waktu_selesai_istirahat'], 'safe'],

            //kamis_waktu_selesai_istirahat
            [['kamis_waktu_selesai_istirahat'], 'safe'],

            //jumat_waktu_selesai_istirahat
            [['jumat_waktu_selesai_istirahat'], 'safe'],

            //sabtu_waktu_selesai_istirahat
            [['sabtu_waktu_selesai_istirahat'], 'safe'],

            //minggu_waktu_selesai_istirahat
            [['minggu_waktu_selesai_istirahat'], 'safe'],

            //senin_masuk
            [['senin_masuk'], 'safe'],

            //senin_pulang
            [['senin_pulang'], 'safe'],

            //senin_batas_awal_masuk
            [['senin_batas_awal_masuk'], 'safe'],

            //senin_batas_akhir_masuk
            [['senin_batas_akhir_masuk'], 'safe'],

            //senin_batas_awal_pulang
            [['senin_batas_awal_pulang'], 'safe'],

            //senin_batas_akhir_pulang
            [['senin_batas_akhir_pulang'], 'safe'],

            //senin_libur
            [['senin_libur'], 'string'],

            //selasa_masuk
            [['selasa_masuk'], 'safe'],

            //selasa_pulang
            [['selasa_pulang'], 'safe'],

            //selasa_batas_awal_masuk
            [['selasa_batas_awal_masuk'], 'safe'],

            //selasa_batas_akhir_masuk
            [['selasa_batas_akhir_masuk'], 'safe'],

            //selasa_batas_awal_pulang
            [['selasa_batas_awal_pulang'], 'safe'],

            //selasa_batas_akhir_pulang
            [['selasa_batas_akhir_pulang'], 'safe'],

            //selasa_libur
            [['selasa_libur'], 'string'],

            //rabu_masuk
            [['rabu_masuk'], 'safe'],

            //rabu_pulang
            [['rabu_pulang'], 'safe'],

            //rabu_batas_awal_masuk
            [['rabu_batas_awal_masuk'], 'safe'],

            //rabu_batas_akhir_masuk
            [['rabu_batas_akhir_masuk'], 'safe'],

            //rabu_batas_awal_pulang
            [['rabu_batas_awal_pulang'], 'safe'],

            //rabu_batas_akhir_pulang
            [['rabu_batas_akhir_pulang'], 'safe'],

            //rabu_libur
            [['rabu_libur'], 'string'],

            //kamis_masuk
            [['kamis_masuk'], 'safe'],

            //kamis_pulang
            [['kamis_pulang'], 'safe'],

            //kamis_batas_awal_masuk
            [['kamis_batas_awal_masuk'], 'safe'],

            //kamis_batas_akhir_masuk
            [['kamis_batas_akhir_masuk'], 'safe'],

            //kamis_batas_awal_pulang
            [['kamis_batas_awal_pulang'], 'safe'],

            //kamis_batas_akhir_pulang
            [['kamis_batas_akhir_pulang'], 'safe'],

            //kamis_libur
            [['kamis_libur'], 'string'],

            //jumat_masuk
            [['jumat_masuk'], 'safe'],

            //jumat_pulang
            [['jumat_pulang'], 'safe'],

            //jumat_batas_awal_masuk
            [['jumat_batas_awal_masuk'], 'safe'],

            //jumat_batas_akhir_masuk
            [['jumat_batas_akhir_masuk'], 'safe'],

            //jumat_batas_awal_pulang
            [['jumat_batas_awal_pulang'], 'safe'],

            //jumat_batas_akhir_pulang
            [['jumat_batas_akhir_pulang'], 'safe'],

            //jumat_libur
            [['jumat_libur'], 'string'],

            //sabtu_masuk
            [['sabtu_masuk'], 'safe'],

            //sabtu_pulang
            [['sabtu_pulang'], 'safe'],

            //sabtu_batas_awal_masuk
            [['sabtu_batas_awal_masuk'], 'safe'],

            //sabtu_batas_akhir_masuk
            [['sabtu_batas_akhir_masuk'], 'safe'],

            //sabtu_batas_awal_pulang
            [['sabtu_batas_awal_pulang'], 'safe'],

            //sabtu_batas_akhir_pulang
            [['sabtu_batas_akhir_pulang'], 'safe'],

            //sabtu_libur
            [['sabtu_libur'], 'string'],

            //minggu_masuk
            [['minggu_masuk'], 'safe'],

            //minggu_pulang
            [['minggu_pulang'], 'safe'],

            //minggu_batas_awal_masuk
            [['minggu_batas_awal_masuk'], 'safe'],

            //minggu_batas_akhir_masuk
            [['minggu_batas_akhir_masuk'], 'safe'],

            //minggu_batas_awal_pulang
            [['minggu_batas_awal_pulang'], 'safe'],

            //minggu_batas_akhir_pulang
            [['minggu_batas_akhir_pulang'], 'safe'],

            //minggu_libur
            [['minggu_libur'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'berlaku_bagi' => 'Berlaku Bagi',
            'unit_kerja' => 'Unit Kerja',
            'pegawai' => 'Pegawai',
            'prioritas' => 'Prioritas',
            'status' => 'Status',
            'berlaku_dari' => 'Berlaku Dari',
            'berlaku_sampai' => 'Berlaku Sampai',
            'catatan' => 'Catatan',
            'fleksi_time' => 'Fleksi Time',
            'senin_waktu_mulai_istirahat' => 'Senin Waktu Mulai Istirahat',
            'selasa_waktu_mulai_istirahat' => 'Selasa Waktu Mulai Istirahat',
            'rabu_waktu_mulai_istirahat' => 'Rabu Waktu Mulai Istirahat',
            'kamis_waktu_mulai_istirahat' => 'Kamis Waktu Mulai Istirahat',
            'jumat_waktu_mulai_istirahat' => 'Jumat Waktu Mulai Istirahat',
            'sabtu_waktu_mulai_istirahat' => 'Sabtu Waktu Mulai Istirahat',
            'minggu_waktu_mulai_istirahat' => 'Minggu Waktu Mulai Istirahat',
            'senin_waktu_selesai_istirahat' => 'Senin Waktu Selesai Istirahat',
            'selasa_waktu_selesai_istirahat' => 'Selasa Waktu Selesai Istirahat',
            'rabu_waktu_selesai_istirahat' => 'Rabu Waktu Selesai Istirahat',
            'kamis_waktu_selesai_istirahat' => 'Kamis Waktu Selesai Istirahat',
            'jumat_waktu_selesai_istirahat' => 'Jumat Waktu Selesai Istirahat',
            'sabtu_waktu_selesai_istirahat' => 'Sabtu Waktu Selesai Istirahat',
            'minggu_waktu_selesai_istirahat' => 'Minggu Waktu Selesai Istirahat',
            'senin_masuk' => 'Senin Masuk',
            'senin_pulang' => 'Senin Pulang',
            'senin_batas_awal_masuk' => 'Senin Batas Awal Masuk',
            'senin_batas_akhir_masuk' => 'Senin Batas Akhir Masuk',
            'senin_batas_awal_pulang' => 'Senin Batas Awal Pulang',
            'senin_batas_akhir_pulang' => 'Senin Batas Akhir Pulang',
            'senin_libur' => 'Senin Libur',
            'selasa_masuk' => 'Selasa Masuk',
            'selasa_pulang' => 'Selasa Pulang',
            'selasa_batas_awal_masuk' => 'Selasa Batas Awal Masuk',
            'selasa_batas_akhir_masuk' => 'Selasa Batas Akhir Masuk',
            'selasa_batas_awal_pulang' => 'Selasa Batas Awal Pulang',
            'selasa_batas_akhir_pulang' => 'Selasa Batas Akhir Pulang',
            'selasa_libur' => 'Selasa Libur',
            'rabu_masuk' => 'Rabu Masuk',
            'rabu_pulang' => 'Rabu Pulang',
            'rabu_batas_awal_masuk' => 'Rabu Batas Awal Masuk',
            'rabu_batas_akhir_masuk' => 'Rabu Batas Akhir Masuk',
            'rabu_batas_awal_pulang' => 'Rabu Batas Awal Pulang',
            'rabu_batas_akhir_pulang' => 'Rabu Batas Akhir Pulang',
            'rabu_libur' => 'Rabu Libur',
            'kamis_masuk' => 'Kamis Masuk',
            'kamis_pulang' => 'Kamis Pulang',
            'kamis_batas_awal_masuk' => 'Kamis Batas Awal Masuk',
            'kamis_batas_akhir_masuk' => 'Kamis Batas Akhir Masuk',
            'kamis_batas_awal_pulang' => 'Kamis Batas Awal Pulang',
            'kamis_batas_akhir_pulang' => 'Kamis Batas Akhir Pulang',
            'kamis_libur' => 'Kamis Libur',
            'jumat_masuk' => 'Jumat Masuk',
            'jumat_pulang' => 'Jumat Pulang',
            'jumat_batas_awal_masuk' => 'Jumat Batas Awal Masuk',
            'jumat_batas_akhir_masuk' => 'Jumat Batas Akhir Masuk',
            'jumat_batas_awal_pulang' => 'Jumat Batas Awal Pulang',
            'jumat_batas_akhir_pulang' => 'Jumat Batas Akhir Pulang',
            'jumat_libur' => 'Jumat Libur',
            'sabtu_masuk' => 'Sabtu Masuk',
            'sabtu_pulang' => 'Sabtu Pulang',
            'sabtu_batas_awal_masuk' => 'Sabtu Batas Awal Masuk',
            'sabtu_batas_akhir_masuk' => 'Sabtu Batas Akhir Masuk',
            'sabtu_batas_awal_pulang' => 'Sabtu Batas Awal Pulang',
            'sabtu_batas_akhir_pulang' => 'Sabtu Batas Akhir Pulang',
            'sabtu_libur' => 'Sabtu Libur',
            'minggu_masuk' => 'Minggu Masuk',
            'minggu_pulang' => 'Minggu Pulang',
            'minggu_batas_awal_masuk' => 'Minggu Batas Awal Masuk',
            'minggu_batas_akhir_masuk' => 'Minggu Batas Akhir Masuk',
            'minggu_batas_awal_pulang' => 'Minggu Batas Awal Pulang',
            'minggu_batas_akhir_pulang' => 'Minggu Batas Akhir Pulang',
            'minggu_libur' => 'Minggu Libur',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerja()
    {
        return $this->hasOne(UnitKerja::className(), ['id' => 'unit_kerja']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
