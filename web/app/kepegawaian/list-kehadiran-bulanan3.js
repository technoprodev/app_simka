$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-kehadiran-bulanan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('kepegawaian/datatables-kehadiran-bulanan', {tahun : tahun, bulan : bulan}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('kepegawaian/kehadiran/' + data) + '" class="inline-block"><i style="width:20px;" class="fa fa-pencil cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Ubah Kehadiran"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'nama',
                            display: 'nama',
                        },
                        render: function ( data, type, row ) {
                            firstLetter = row.nama.match(/\b(\w)/g)[0].toUpperCase();
                            color = '';
                            if (firstLetter >= 'A' && firstLetter <= 'C') {
                                color = 'azure';
                            } else if (firstLetter >= 'D' && firstLetter <= 'F') {
                                color = 'spring';
                            } else if (firstLetter >= 'G' && firstLetter <= 'H') {
                                color = 'red';
                            } else if (firstLetter >= 'I' && firstLetter <= 'J') {
                                color = 'yellow';
                            } else if (firstLetter >= 'K' && firstLetter <= 'L') {
                                color = 'orange';
                            } else if (firstLetter >= 'M' && firstLetter <= 'N') {
                                color = 'chartreuse';
                            } else if (firstLetter >= 'O' && firstLetter <= 'P') {
                                color = 'green';
                            } else if (firstLetter >= 'Q' && firstLetter <= 'R') {
                                color = 'cyan';
                            } else if (firstLetter >= 'S' && firstLetter <= 'T') {
                                color = 'blue';
                            } else if (firstLetter >= 'U' && firstLetter <= 'V') {
                                color = 'violet';
                            } else if (firstLetter >= 'W' && firstLetter <= 'X') {
                                color = 'magenta';
                            } else if (firstLetter >= 'Y' && firstLetter <= 'Z') {
                                color = 'rose';
                            }
                            return '' +
                                '        <span class="circle-icon margin-right-5 fs-11 bg-' + color + ' border-transparent" style="width: 30px; height: 30px; line-height: 28px;">' + row.nama.match(/\b(\w)/g).slice(0, 2).join('').toUpperCase() + '</span>' +
                                '        <span class="fs-14 text-dark">' + row.nama + '</span>' +
                                
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'nip',
                            display: 'nip',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'pin',
                            display: 'pin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'unit_kerja',
                            display: 'unit_kerja',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'potongan_tunkin',
                            display: 'potongan_tunkin',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'telat',
                            display: 'telat',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kecepetan',
                            display: 'kecepetan',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jumlah_jam',
                            display: 'jumlah_jam',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kurang_jam',
                            display: 'kurang_jam',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + '</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'hadir',
                            display: 'hadir',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + ' hari</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'tidak_hadir',
                            display: 'tidak_hadir',
                        },
                        render: function ( data, type, row ) {
                            if (!data) {
                                return '-';
                            } else {
                                return '' +
                                    '<div>' +
                                    '    <span class="">' + data + ' hari</span>' +
                                    '</div>' +
                                    '';
                            }
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                if (index == 4) {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                } else {
                    var that = this;
                    
                    var timer = null;
                    dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                        var val = {
                            that : that,
                            this : this,
                        };

                        clearTimeout(timer); 
                           timer = setTimeout(function () {
                            if(val.that.search() !== val.this.value){
                                val.that.search( val.this.value ).draw();
                            }
                        }, 500);
                    });
                }
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});