<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/pegawai/form-bukti-kehadiran.js', ['depends' => [
    'technosmart\assets_manager\VueAsset',
    'technosmart\assets_manager\VueResourceAsset',
    'technosmart\assets_manager\RequiredAsset',
]]);

technosmart\assets_manager\FlatpickrAsset::register($this);

$pilihanKehadiran = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kh.id, CONCAT(kh.kode, ' - ', kh.keterangan) as keterangan FROM keterangan_kehadiran kh
            where kh.diajukan_pegawai = 'Ya' order by kh.kode
        ", []
    )->queryAll(), 'id', 'keterangan'
);

$pilihanPegawai = [];
if ($unitKerjaAdministrasi = Yii::$app->user->identity->pegawai->unit_kerja_administrasi) {
    $result = (new \yii\db\Query())->select('get_pegawai_rec(' . Yii::$app->user->identity->pegawai->unit_kerja_administrasi . ')')->scalar();
    $pegawais = $result ? explode(',', $result) : null;

    $query = new \yii\db\Query();
    $query
        ->select([
            'p.id',
            'CONCAT(p.nama, \' - \', IFNULL(p.nip, \'\')) AS nama',
        ])
        ->from('pegawai p')
        ->where([
            'p.id' => $pegawais,
        ])
    ;

    $pilihanPegawai = ArrayHelper::map($query->all(), 'id', 'nama');
}

//
$buktiKehadiranPegawais = [];
if (isset($model['bukti_kehadiran_pegawai']))
    foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai)
        $buktiKehadiranPegawais[] = $buktiKehadiranPegawai->attributes;

$this->registerJs(
    'vm.$data.buktiKehadiran.buktiKehadiranPegawais = vm.$data.buktiKehadiran.buktiKehadiranPegawais.concat(' . json_encode($buktiKehadiranPegawais) . ');' .
    '',
    3
);

//
$errorMessage = '';
$errorVue = false;
if ($model['bukti_kehadiran']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['bukti_kehadiran'], ['class' => '']);
}

if (isset($model['bukti_kehadiran_pegawai'])) foreach ($model['bukti_kehadiran_pegawai'] as $key => $buktiKehadiranPegawai) {
    if ($buktiKehadiranPegawai->hasErrors()) {
        $errorMessage .= Html::errorSummary($buktiKehadiranPegawai, ['class' => '']);
        $errorVue = true; 
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Formulir Unggah Bukti Kehadiran
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['bukti_kehadiran'], 'keterangan_kehadiran', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'keterangan_kehadiran', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['bukti_kehadiran'], 'keterangan_kehadiran', $pilihanKehadiran, ['prompt' => '- pilih jenis bukti -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'keterangan_kehadiran', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'keterangan_kehadiran')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'catatan_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['bukti_kehadiran'], 'catatan_pengajuan', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'catatan_pengajuan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'catatan_pengajuan')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'dari_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['bukti_kehadiran'], 'dari_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'dari_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'dari_tanggal')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'sampai_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextInput($model['bukti_kehadiran'], 'sampai_tanggal', ['class' => 'form-text input-flatpickr']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'sampai_tanggal', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'sampai_tanggal')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_utama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_utama_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_utama', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_utama')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_tambahan_1_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_tambahan_1', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_1')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_tambahan_2_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_tambahan_2', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_2')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_3', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_tambahan_3_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_tambahan_3', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_3')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_4', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_tambahan_4_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_tambahan_4', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_4')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_5', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeFileInput($model['bukti_kehadiran'], 'virtual_bukti_tambahan_5_upload'); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'bukti_tambahan_5', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_5')->end(); ?>

                <?php if (isset($model['bukti_kehadiran_pegawai'])) foreach ($model['bukti_kehadiran_pegawai'] as $key => $value): ?>
                    <?= $form->field($model['bukti_kehadiran_pegawai'][$key], "[$key]pegawai")->begin(); ?>
                    <?= $form->field($model['bukti_kehadiran_pegawai'][$key], "[$key]pegawai")->end(); ?>
                <?php endforeach; ?>

                <!-- <template v-if="typeof buktiKehadiran.buktiKehadiranPegawais == 'object'">
                    <template v-for="(value, key, index) in buktiKehadiran.buktiKehadiranPegawais">
                        <div v-show="!(value.id < 0)">
                            <div class="box box-break-sm">
                                <div class="box-11 padding-left-0">
                                    <input type="hidden" v-bind:id="'buktikehadiranpegawai-' + key + '-id'" v-bind:name="'BuktiKehadiranPegawai[' + key + '][id]'" class="form-text" type="text" v-model="buktiKehadiran.buktiKehadiranPegawais[key].id">
                                    <div v-bind:class="'form-wrapper field-buktikehadiranpegawai-' + key + '-pegawai'">
                                        <select v-bind:id="'buktikehadiranpegawai-' + key + '-pegawai'" v-bind:name="'BuktiKehadiranPegawai[' + key + '][pegawai]'" class="form-dropdown input-choices" v-model="buktiKehadiran.buktiKehadiranPegawais[key].pegawai">
                                            <option value="">Pilih Pegawai</option>
                                            <?php foreach ($pilihanPegawai as $key => $value) : ?>
                                                <option value="<?= $key ?>"><?= $value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="box-1 padding-0 text-right">
                                    <div v-on:click="removeBuktiKehadiranPegawai(key)" class="btn btn-default btn-sm margin-left-5 bg-light-red hover-pointer"><i class="fa fa-close"></i></div>
                                </div>
                            </div>
                        </div>
                    </template>
                </template> -->

                <!-- <div class="padding-y-5" >
                    <a v-on:click="addBuktiKehadiranPegawai" class="button button-sm text-azure border-azure hover-bg-light-azure">Pilih Pegawai</a>
                </div> -->
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
