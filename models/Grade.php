<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "grade".
 *
 * @property integer $id
 * @property integer $grade
 * @property integer $nominal_tunkin
 * @property integer $pajak_tunkin
 *
 * @property Pegawai[] $pegawais
 */
class Grade extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'grade';
    }

    public function rules()
    {
        return [
            //id

            //grade
            [['grade'], 'required'],
            [['grade'], 'integer'],

            //nominal_tunkin
            [['nominal_tunkin'], 'required'],
            [['nominal_tunkin'], 'integer'],

            //pajak_tunkin
            [['pajak_tunkin'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grade' => 'Grade',
            'nominal_tunkin' => 'Nominal Tunkin',
            'pajak_tunkin' => 'Pajak Tunkin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['grade' => 'id']);
    }
}
