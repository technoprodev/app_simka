<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_penetapan_angka_kredit".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $periode_penilaian_awal
 * @property string $periode_penilaian_akhir
 * @property string $unsur_utama
 * @property string $unsur_penunjang
 * @property string $total_angkat_kredit
 * @property string $arsip_pak
 *
 * @property Pegawai $pegawai0
 */
class PegawaiPenetapanAngkaKredit extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_arsip_pak_upload;
    public $virtual_arsip_pak_download;

    public static function tableName()
    {
        return 'pegawai_penetapan_angka_kredit';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //periode_penilaian_awal
            [['periode_penilaian_awal'], 'required'],
            [['periode_penilaian_awal'], 'safe'],

            //periode_penilaian_akhir
            [['periode_penilaian_akhir'], 'required'],
            [['periode_penilaian_akhir'], 'safe'],

            //unsur_utama
            [['unsur_utama'], 'required'],
            [['unsur_utama'], 'string', 'max' => 256],

            //unsur_penunjang
            [['unsur_penunjang'], 'string', 'max' => 256],

            //total_angkat_kredit
            [['total_angkat_kredit'], 'required'],
            [['total_angkat_kredit'], 'string', 'max' => 256],

            //arsip_pak
            // [['arsip_pak'], 'required'],
            [['arsip_pak'], 'string', 'max' => 256],
            
            //virtual_arsip_pak_download
            [['virtual_arsip_pak_download'], 'safe'],
            
            //virtual_arsip_pak_upload
            [['virtual_arsip_pak_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_arsip_pak_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_pak_upload");
        if ($this->virtual_arsip_pak_upload) {
            $this->arsip_pak = $this->virtual_arsip_pak_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_arsip_pak_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_arsip_pak_upload");
        if ($this->virtual_arsip_pak_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_penetapan_angka_kredit-arsip_pak') . '/' . $this->id . '/' . $this->arsip_pak;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->arsip_pak = $this->virtual_arsip_pak_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_arsip_pak_upload) {
            $path = Yii::getAlias('@upload-pegawai_penetapan_angka_kredit-arsip_pak') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_arsip_pak_upload->saveAs($path . '/' . $this->arsip_pak);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_penetapan_angka_kredit-arsip_pak') . '/' . $this->id . '/' . $this->arsip_pak;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->arsip_pak) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_penetapan_angka_kredit-arsip_pak');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_arsip_pak_download = $path . '/' . $this->arsip_pak;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'periode_penilaian_awal' => 'Periode Penilaian Awal',
            'periode_penilaian_akhir' => 'Periode Penilaian Akhir',
            'unsur_utama' => 'Unsur Utama',
            'unsur_penunjang' => 'Unsur Penunjang',
            'total_angkat_kredit' => 'Total Angkat Kredit',
            'arsip_pak' => 'Arsip Pak',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
