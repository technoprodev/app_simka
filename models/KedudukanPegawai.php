<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kedudukan_pegawai".
 *
 * @property integer $id
 * @property string $kedudukan_pegawai
 * @property string $bisa_login
 *
 * @property Pegawai[] $pegawais
 */
class KedudukanPegawai extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kedudukan_pegawai';
    }

    public function rules()
    {
        return [
            //id

            //kedudukan_pegawai
            [['kedudukan_pegawai'], 'required'],
            [['kedudukan_pegawai'], 'string', 'max' => 256],

            //bisa_login
            [['bisa_login'], 'required'],
            [['bisa_login'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kedudukan_pegawai' => 'Kedudukan Pegawai',
            'bisa_login' => 'Bisa Login',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['kedudukan_pegawai' => 'id']);
    }
}
