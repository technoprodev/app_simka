<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/export/list-master-grade.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/export/list-master-kepangkatan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/export/list-master-status-kepegawaian.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="tab-wrapper">
            <ul class="tab-menu tab-menu-selector">
                <li><a href="#grade" class="<?= $tab == 'grade' ? 'active' : '' ?>">Grade</a></li>
                <li><a href="#kepangkatan" class="<?= $tab == 'kepangkatan' ? 'active' : '' ?>">Kepangkatan</a></li>
                <li><a href="#status-kepegawaian" class="<?= $tab == 'status-kepegawaian' ? 'active' : '' ?>">Status Kepegawaian</a></li>
            </ul>
            <div id="grade" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Grade</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'grade'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-grade table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Grade</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="kepangkatan" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Kepangkatan</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'kepangkatan'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-kepangkatan table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Pangkat</th>
                                    <th>Golongan</th>
                                    <th>Ruang</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div id="status-kepegawaian" class="tab-content">
                <div class="padding-x-30 m-padding-x-15 padding-y-15">
                    <div>
                        <div class="clearfix">
                            <span class="fs-16 text-azure fs-italic">Daftar Status Kepegawaian</span>
                            <?= Html::a('Tambah Data', ['master-form', 'tab' => 'status-kepegawaian'], ['class' => 'button pull-right m-pull-none m-button-block bg-azure border-azure']) ?>
                        </div>
                        <hr class="margin-y-10 border-top border-light-orange">

                        <table class="datatables-master-status-kepegawaian table table-nowrap">
                            <thead>
                                <tr class="text-dark">
                                    <th style="width:50px;"></th>
                                    <th>Status Kepegawaian</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>