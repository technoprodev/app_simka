<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenjang_jabatan".
 *
 * @property integer $id
 * @property integer $jenis_jabatan
 * @property string $jenjang_jabatan
 * @property integer $eselon_terendah
 * @property integer $eselon_tertinggi
 * @property integer $kepangkatan_terendah
 * @property integer $kepangkatan_tertinggi
 *
 * @property JenisJabatan $jenisJabatan
 * @property Kepangkatan $kepangkatanTerendah
 * @property Kepangkatan $kepangkatanTertinggi
 * @property Eselon $eselonTertinggi
 * @property Eselon $eselonTerendah
 * @property Pegawai[] $pegawais
 * @property PegawaiMutasi[] $pegawaiMutasis
 * @property PegawaiMutasi[] $pegawaiMutasis0
 * @property PegawaiPelaksana[] $pegawaiPelaksanas
 * @property PegawaiPemberhentian[] $pegawaiPemberhentians
 */
class JenjangJabatan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenjang_jabatan';
    }

    public function rules()
    {
        return [
            //id

            //jenis_jabatan
            [['jenis_jabatan'], 'required'],
            [['jenis_jabatan'], 'integer'],
            [['jenis_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => JenisJabatan::className(), 'targetAttribute' => ['jenis_jabatan' => 'id']],

            //jenjang_jabatan
            [['jenjang_jabatan'], 'required'],
            [['jenjang_jabatan'], 'string', 'max' => 256],

            //eselon_terendah
            [['eselon_terendah'], 'integer'],
            [['eselon_terendah'], 'exist', 'skipOnError' => true, 'targetClass' => Eselon::className(), 'targetAttribute' => ['eselon_terendah' => 'id']],

            //eselon_tertinggi
            [['eselon_tertinggi'], 'integer'],
            [['eselon_tertinggi'], 'exist', 'skipOnError' => true, 'targetClass' => Eselon::className(), 'targetAttribute' => ['eselon_tertinggi' => 'id']],

            //kepangkatan_terendah
            [['kepangkatan_terendah'], 'integer'],
            [['kepangkatan_terendah'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan_terendah' => 'id']],

            //kepangkatan_tertinggi
            [['kepangkatan_tertinggi'], 'integer'],
            [['kepangkatan_tertinggi'], 'exist', 'skipOnError' => true, 'targetClass' => Kepangkatan::className(), 'targetAttribute' => ['kepangkatan_tertinggi' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_jabatan' => 'Jenis Jabatan',
            'jenjang_jabatan' => 'Jenjang Jabatan',
            'eselon_terendah' => 'Eselon Terendah',
            'eselon_tertinggi' => 'Eselon Tertinggi',
            'kepangkatan_terendah' => 'Kepangkatan Terendah',
            'kepangkatan_tertinggi' => 'Kepangkatan Tertinggi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisJabatan()
    {
        return $this->hasOne(JenisJabatan::className(), ['id' => 'jenis_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatanTerendah()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan_terendah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKepangkatanTertinggi()
    {
        return $this->hasOne(Kepangkatan::className(), ['id' => 'kepangkatan_tertinggi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEselonTertinggi()
    {
        return $this->hasOne(Eselon::className(), ['id' => 'eselon_tertinggi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEselonTerendah()
    {
        return $this->hasOne(Eselon::className(), ['id' => 'eselon_terendah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['jenjang_jabatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMutasis()
    {
        return $this->hasMany(PegawaiMutasi::className(), ['jenjang_jabatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiMutasis0()
    {
        return $this->hasMany(PegawaiMutasi::className(), ['jenjang_jabatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPelaksanas()
    {
        return $this->hasMany(PegawaiPelaksana::className(), ['jenjang_jabatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPemberhentians()
    {
        return $this->hasMany(PegawaiPemberhentian::className(), ['jenjang_jabatan_saat_diberhentikan' => 'id']);
    }
}
