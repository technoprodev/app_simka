<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "warna_kulit".
 *
 * @property integer $id
 * @property string $warna_kulit
 *
 * @property Pegawai[] $pegawais
 */
class WarnaKulit extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'warna_kulit';
    }

    public function rules()
    {
        return [
            //id

            //warna_kulit
            [['warna_kulit'], 'required'],
            [['warna_kulit'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'warna_kulit' => 'Warna Kulit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['warna_kulit' => 'id']);
    }
}
