<?php
namespace app_simka\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 */
class User extends \technosmart\models\User
{
    public function rules()
    {
        return [
            //id

            //username
            [['username'], 'trim', 'when' => function($model) {
                return $model->username != NULL;
            }],
            [['username'], 'unique', 'message' => 'This username has already been taken.'],
            [['username'], 'string', 'min' => 2, 'max' => 16],
            [['username'], 'required'],

            //name
            [['name'], 'trim', 'when' => function($model) {
                return $model->name != NULL;
            }],
            [['name'], 'string', 'max' => 64],

            //email
            [['email'], 'trim', 'when' => function($model) {
                return $model->email != NULL;
            }],
            [['email'], 'unique', 'message' => 'This email address has already been taken.'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],
            [['email'], 'safe'],

            //phone
            [['phone'], 'trim', 'when' => function($model) {
                return $model->phone != NULL;
            }],
            [['phone'], 'number'],
            [['phone'], 'string', 'min' => 6, 'max' => 32],

            //auth_key
            [['auth_key'], 'required'],
            [['auth_key'], 'string', 'max' => 32],

            //password_hash
            [['password_hash'], 'required'],
            [['password_hash'], 'string', 'max' => 255],

            //password_reset_token
            [['password_reset_token'], 'unique'],
            [['password_reset_token'], 'string', 'max' => 255],

            // status
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE, self::STATUS_DELETED]],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //password
            [['password'], 'required', 'on' => ['password', 'password-change']],
            [['password'], 'string', 'min' => 6],
            [['password'], 'match', 'pattern' => '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/i', 'message' => 'Password setidaknya memiliki 1 huruf besar, 1 huruf kecil, 1 angka, 1 simbol dan minimal terdiri dari 6 karakter.'],

            //password_repeat
            [['password_repeat'], 'required', 'on' => ['password', 'password-change']],
            [['password_repeat'], 'compare', 'compareAttribute'=> 'password', 'message'=> 'Passwords do not match'],

            //password_old
            [['password_old'], 'required', 'on' => ['password-change']],
            [['password_old'], function ($attribute, $params, $validator) {
                if (!$this->validatePassword($this->$attribute)) {
                    $this->addError($attribute, 'Incorrect old password.');
                }
            }, 'on' => ['password-change']],
        ];
    }

    public static function findByLogin($login)
    {
        return static::find()
            ->join('INNER JOIN', 'pegawai p', 'p.id = user.id')
            ->join('INNER JOIN', 'kedudukan_pegawai kp', 'kp.id = p.kedudukan_pegawai')
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->andWhere(['kp.bisa_login' => 'Ya'])
            ->one();
    }

    public function getPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'id']);
    }
}