<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kota_kabupaten".
 *
 * @property string $id
 * @property string $id_provinsi
 * @property string $nama
 *
 * @property Kecamatan[] $kecamatans
 * @property Provinsi $provinsi
 */
class KotaKabupaten extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kota_kabupaten';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 4],
            [['id'], 'unique'],

            //id_provinsi
            [['id_provinsi'], 'required'],
            [['id_provinsi'], 'string', 'max' => 2],
            [['id_provinsi'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi' => 'id']],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_provinsi' => 'Id Provinsi',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatans()
    {
        return $this->hasMany(Kecamatan::className(), ['id_kota_kabupaten' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::className(), ['id' => 'id_provinsi']);
    }
}
