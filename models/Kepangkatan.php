<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "kepangkatan".
 *
 * @property integer $id
 * @property string $pangkat
 * @property string $golongan
 * @property string $ruang
 * @property integer $nominal_uang_makan
 *
 * @property JenjangJabatan[] $jenjangJabatans
 * @property JenjangJabatan[] $jenjangJabatans0
 * @property Pegawai[] $pegawais
 * @property PegawaiKenaikanGajiBerkala[] $pegawaiKenaikanGajiBerkalas
 * @property PegawaiKepangkatan[] $pegawaiKepangkatans
 * @property PegawaiPemberhentian[] $pegawaiPemberhentians
 */
class Kepangkatan extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'kepangkatan';
    }

    public function rules()
    {
        return [
            //id

            //pangkat
            [['pangkat'], 'required'],
            [['pangkat'], 'string', 'max' => 256],

            //golongan
            [['golongan'], 'required'],
            [['golongan'], 'string', 'max' => 256],

            //ruang
            [['ruang'], 'required'],
            [['ruang'], 'string', 'max' => 256],

            //nominal_uang_makan
            [['nominal_uang_makan'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pangkat' => 'Pangkat',
            'golongan' => 'Golongan',
            'ruang' => 'Ruang',
            'nominal_uang_makan' => 'Nominal Uang Makan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatans()
    {
        return $this->hasMany(JenjangJabatan::className(), ['kepangkatan_terendah' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenjangJabatans0()
    {
        return $this->hasMany(JenjangJabatan::className(), ['kepangkatan_tertinggi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['kepangkatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKenaikanGajiBerkalas()
    {
        return $this->hasMany(PegawaiKenaikanGajiBerkala::className(), ['kepangkatan_saat_kenaikan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKepangkatans()
    {
        return $this->hasMany(PegawaiKepangkatan::className(), ['kepangkatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPemberhentians()
    {
        return $this->hasMany(PegawaiPemberhentian::className(), ['kepangkatan_saat_pemberhentian' => 'id']);
    }
}
