<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "jenis_unit_kerja".
 *
 * @property integer $id
 * @property string $jenis_unit_kerja
 *
 * @property UnitKerja[] $unitKerjas
 */
class JenisUnitKerja extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'jenis_unit_kerja';
    }

    public function rules()
    {
        return [
            //id

            //jenis_unit_kerja
            [['jenis_unit_kerja'], 'required'],
            [['jenis_unit_kerja'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_unit_kerja' => 'Jenis Unit Kerja',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitKerjas()
    {
        return $this->hasMany(UnitKerja::className(), ['jenis_unit_kerja' => 'id']);
    }
}
