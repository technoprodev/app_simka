<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div>
            <div class="fs-18 text-azure fs-italic">
                Detail Bukti Cctv
            </div>
            <hr class="margin-y-10 border-top border-light-orange">

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'pegawai', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->pegawai ? $model['bukti_cctv']->pegawai0->nama : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'jenis', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->jenis ? $model['bukti_cctv']->jenis : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'diajukan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->diajukan ? $model['bukti_cctv']->diajukan0->nama : '(kosong)'?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->catatan_pengajuan ? $model['bukti_cctv']->catatan_pengajuan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'tanggal', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->tanggal ? $model['bukti_cctv']->tanggal : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'waktu', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->waktu ? $model['bukti_cctv']->waktu : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_utama ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_utama_download . '">' . $model['bukti_cctv']->bukti_utama . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_tambahan_1 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_1_download . '">' . $model['bukti_cctv']->bukti_tambahan_1 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_tambahan_2 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_2_download . '">' . $model['bukti_cctv']->bukti_tambahan_2 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_tambahan_3 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_3_download . '">' . $model['bukti_cctv']->bukti_tambahan_3 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_tambahan_4 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_4_download . '">' . $model['bukti_cctv']->bukti_tambahan_4 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->bukti_tambahan_5 ? '<a target="_blank" href="' . $model['bukti_cctv']->virtual_bukti_tambahan_5_download . '">' . $model['bukti_cctv']->bukti_tambahan_5 . '</a>' : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>

            <div class="box box-break-sm">
                <div class="box-2 padding-x-0 padding-y-5">
                    <?= Html::activeLabel($model['bukti_cctv'], 'status_pengajuan', ['class' => 'form-label text-grayer']); ?>
                </div>
                <div class="box-10 m-padding-x-0">
                    <div class="padding-y-5 text-dark">
                        <?= $model['bukti_cctv']->status_pengajuan ? $model['bukti_cctv']->status_pengajuan : '(kosong)' ?>
                    </div>
                    <hr class="margin-y-5 border-top border-light-azure">
                </div>
            </div>
        </div>
    </div>
</div>
