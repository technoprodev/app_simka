<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\FlatpickrAsset::register($this);

$pilihanKehadiran = ArrayHelper::map(
    \Yii::$app->db->createCommand(
        "
            SELECT kh.id, CONCAT(kh.kode, ' - ', kh.keterangan) as keterangan FROM keterangan_kehadiran kh
            order by kh.kode
        ", []
    )->queryAll(), 'id', 'keterangan'
);

//
$errorMessage = '';
if ($model['bukti_kehadiran']->hasErrors()) {
    $errorMessage .= Html::errorSummary($model['bukti_kehadiran'], ['class' => '']);
}
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app', 'enctype'=>'multipart/form-data']]); ?>
            <div>
                <div class="fs-18 text-azure fs-italic">
                    Verifikasi Bukti Kehadiran
                </div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>

                <?= $form->field($model['bukti_kehadiran'], 'keterangan_kehadiran', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'keterangan_kehadiran', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->keterangan_kehadiran ? $model['bukti_kehadiran']->keteranganKehadiran->keterangan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'keterangan_kehadiran')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'diajukan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'diajukan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->diajukan ? $model['bukti_kehadiran']->diajukan0->nama : '(kosong)'?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'diajukan')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'catatan_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'catatan_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->catatan_pengajuan ? $model['bukti_kehadiran']->catatan_pengajuan : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'catatan_pengajuan')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'dari_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'dari_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->dari_tanggal ? $model['bukti_kehadiran']->dari_tanggal : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'dari_tanggal')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'sampai_tanggal', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'sampai_tanggal', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->sampai_tanggal ? $model['bukti_kehadiran']->sampai_tanggal : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'sampai_tanggal')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'nomor_surat', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'nomor_surat', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->nomor_surat ? $model['bukti_kehadiran']->nomor_surat : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'nomor_surat')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_utama', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_utama', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_utama ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_utama_download . '">' . $model['bukti_kehadiran']->bukti_utama . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_utama')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_1', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_1', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_tambahan_1 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_1_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_1 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_1')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_2', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_2', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_tambahan_2 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_2_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_2 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_2')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_3', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_3', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_tambahan_3 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_3_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_3 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_3')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_4', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_4', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_tambahan_4 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_4_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_4 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_4')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_5', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'bukti_tambahan_5', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $model['bukti_kehadiran']->bukti_tambahan_5 ? '<a target="_blank" href="' . $model['bukti_kehadiran']->virtual_bukti_tambahan_5_download . '">' . $model['bukti_kehadiran']->bukti_tambahan_5 . '</a>' : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'bukti_tambahan_5')->end(); ?>

                <?php foreach ($model['bukti_kehadiran']->buktiKehadiranPegawais as $key => $buktiKehadiranPegawai) : ?>
                <?= $form->field($model['bukti_kehadiran'], 'pegawai', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        Pegawai <?= ($key + 1) ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <div class="padding-y-5 text-dark">
                            <?= $buktiKehadiranPegawai->pegawai ? $buktiKehadiranPegawai->pegawai0->nama . ' - ' . $buktiKehadiranPegawai->pegawai0->nip : '(kosong)' ?>
                        </div>
                        <hr class="margin-y-5 border-top border-light-azure">
                    </div>
                <?= $form->field($buktiKehadiranPegawai, 'pegawai')->end(); ?>
                <?php endforeach; ?>

                <?= $form->field($model['bukti_kehadiran'], 'catatan_verifikasi', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'catatan_verifikasi', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeTextArea($model['bukti_kehadiran'], 'catatan_verifikasi', ['class' => 'form-textarea']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'catatan_verifikasi', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'catatan_verifikasi')->end(); ?>

                <?= $form->field($model['bukti_kehadiran'], 'status_pengajuan', ['options' => ['class' => 'form-wrapper box box-break-sm'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                    <div class="box-2 padding-x-0 padding-y-5">
                        <?= Html::activeLabel($model['bukti_kehadiran'], 'status_pengajuan', ['class' => 'form-label text-grayer']); ?>
                    </div>
                    <div class="box-10 m-padding-x-0">
                        <?= Html::activeDropDownList($model['bukti_kehadiran'], 'status_pengajuan', ['Diterima' => 'Diterima', 'Direvisi' => 'Direvisi', 'Ditolak' => 'Ditolak'], ['prompt' => '- pilih verifikasi -', 'class' => 'form-dropdown']); ?>
                        <?= Html::error($model['bukti_kehadiran'], 'status_pengajuan', ['class' => 'form-info']); ?>
                    </div>
                <?= $form->field($model['bukti_kehadiran'], 'status_pengajuan')->end(); ?>
            </div>

            <div class="margin-top-30"></div>

            <div>
                <hr class="margin-y-10 border-top border-light-orange">

                <?php if ($errorMessage) : ?>
                    <div class="padding-top-15 padding-x-15 margin-bottom-30 border-light-red bg-light-red">
                        <?= $errorMessage ?>
                    </div>
                <?php endif; ?>
                
                <div class="form-wrapper">
                    <?= Html::submitButton('Submit', ['class' => 'button button-block button-lg rounded-xs border-azure bg-azure hover-bg-lightest hover-text-azure']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
