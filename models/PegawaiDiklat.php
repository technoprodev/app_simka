<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "pegawai_diklat".
 *
 * @property integer $id
 * @property integer $pegawai
 * @property string $jenis_diklat
 * @property string $nama_diklat
 * @property string $penyelenggara
 * @property string $tanggal_mulai
 * @property string $tanggal_selesai
 * @property string $tempat
 * @property string $keterangan
 * @property string $sertifikat
 * @property string $nomor_sttp
 * @property string $tanggal_sttp
 *
 * @property Pegawai $pegawai0
 */
class PegawaiDiklat extends \technosmart\yii\db\ActiveRecord
{
    public $isDeleted;
    public $index;
    public $virtual_sertifikat_upload;
    public $virtual_sertifikat_download;

    public static function tableName()
    {
        return 'pegawai_diklat';
    }

    public function rules()
    {
        return [
            //id

            //pegawai
            [['pegawai'], 'required'],
            [['pegawai'], 'integer'],
            [['pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['pegawai' => 'id']],

            //jenis_diklat
            [['jenis_diklat'], 'required'],
            [['jenis_diklat'], 'string'],

            //nama_diklat
            [['nama_diklat'], 'required'],
            [['nama_diklat'], 'string', 'max' => 256],

            //penyelenggara
            [['penyelenggara'], 'required'],
            [['penyelenggara'], 'string', 'max' => 256],

            //tanggal_mulai
            [['tanggal_mulai'], 'required'],
            [['tanggal_mulai'], 'safe'],

            //tanggal_selesai
            [['tanggal_selesai'], 'required'],
            [['tanggal_selesai'], 'safe'],

            //tempat
            [['tempat'], 'required'],
            [['tempat'], 'string', 'max' => 256],

            //keterangan
            [['keterangan'], 'required'],
            [['keterangan'], 'string', 'max' => 256],

            //sertifikat
            // [['sertifikat'], 'required'],
            [['sertifikat'], 'string', 'max' => 256],

            //nomor_sttp
            [['nomor_sttp'], 'required'],
            [['nomor_sttp'], 'string', 'max' => 256],

            //tanggal_sttp
            [['tanggal_sttp'], 'required'],
            [['tanggal_sttp'], 'safe'],
            
            //virtual_sertifikat_download
            [['virtual_sertifikat_download'], 'safe'],
            
            //virtual_sertifikat_upload
            [['virtual_sertifikat_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, ppt, pptx, txt'],
        ];
    }

    public function beforeValidate()
    {
        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return parent::beforeValidate();
    }
    
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->virtual_sertifikat_upload = \yii\web\UploadedFile::getInstance($this, "[$this->index]virtual_sertifikat_upload");
        if ($this->virtual_sertifikat_upload) {
            if (!$insert) {
                $filePath = Yii::getAlias('@upload-pegawai_diklat-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
                if (is_file($filePath)) unlink($filePath);
            }
            $this->sertifikat = $this->virtual_sertifikat_upload->name;
        }

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->virtual_sertifikat_upload) {
            $path = Yii::getAlias('@upload-pegawai_diklat-sertifikat') . '/' . $this->id;
            if ( !is_dir($path) ) mkdir($path);
            $this->virtual_sertifikat_upload->saveAs($path . '/' . $this->sertifikat);
        }
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        
        $filePath = Yii::getAlias('@upload-pegawai_diklat-sertifikat') . '/' . $this->id . '/' . $this->sertifikat;
        if (is_file($filePath)) unlink($filePath);
        
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        
        if($this->sertifikat) {
            $downloadBaseUrl = Yii::getAlias('@download-pegawai_diklat-sertifikat');
            $path = Yii::getAlias($downloadBaseUrl) . '/' . $this->id;
            $this->virtual_sertifikat_download = $path . '/' . $this->sertifikat;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pegawai' => 'Pegawai',
            'jenis_diklat' => 'Jenis Diklat',
            'nama_diklat' => 'Nama Diklat',
            'penyelenggara' => 'Penyelenggara',
            'tanggal_mulai' => 'Tanggal Mulai',
            'tanggal_selesai' => 'Tanggal Selesai',
            'tempat' => 'Tempat',
            'keterangan' => 'Keterangan',
            'sertifikat' => 'Sertifikat',
            'nomor_sttp' => 'Nomor Sttp',
            'tanggal_sttp' => 'Tanggal Sttp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawai0()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'pegawai']);
    }
}
