<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "status_kepegawaian".
 *
 * @property integer $id
 * @property string $status_kepegawaian
 * @property integer $pajak_tunkin
 *
 * @property Pegawai[] $pegawais
 * @property PegawaiKepangkatan[] $pegawaiKepangkatans
 */
class StatusKepegawaian extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'status_kepegawaian';
    }

    public function rules()
    {
        return [
            //id

            //status_kepegawaian
            [['status_kepegawaian'], 'required'],
            [['status_kepegawaian'], 'string', 'max' => 256],

            //pajak_tunkin
            [['pajak_tunkin'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_kepegawaian' => 'Status Kepegawaian',
            'pajak_tunkin' => 'Pajak Tunkin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['status_kepegawaian' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKepangkatans()
    {
        return $this->hasMany(PegawaiKepangkatan::className(), ['status_kepegawaian' => 'id']);
    }
}
