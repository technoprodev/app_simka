$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('pejabat/datatables-pegawai'),
                    type: 'POST'
                },
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function ( data, type, row ) {
                            return '' +
                                '<div class="fs-14 text-center">' +
                                '    <a href="' + fn.urlTo('pejabat/pegawai/' + data, {tab: 'jabatan'}) + '" class="inline-block"><i style="width:20px;" class="fa fa-eye cdexsw text-azure xswzaq" data-toggle="tooltip" data-placement="top" title="Detail Pegawai"></i></a>' +
                                '</div>';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama',
                            display: 'nama',
                        },
                        render: function ( data, type, row ) {
                            firstLetter = row.nama.match(/\b(\w)/g)[0].toUpperCase();
                            color = '';
                            if (firstLetter >= 'A' && firstLetter <= 'C') {
                                color = 'azure';
                            } else if (firstLetter >= 'D' && firstLetter <= 'F') {
                                color = 'spring';
                            } else if (firstLetter >= 'G' && firstLetter <= 'H') {
                                color = 'red';
                            } else if (firstLetter >= 'I' && firstLetter <= 'J') {
                                color = 'yellow';
                            } else if (firstLetter >= 'K' && firstLetter <= 'L') {
                                color = 'orange';
                            } else if (firstLetter >= 'M' && firstLetter <= 'N') {
                                color = 'chartreuse';
                            } else if (firstLetter >= 'O' && firstLetter <= 'P') {
                                color = 'green';
                            } else if (firstLetter >= 'Q' && firstLetter <= 'R') {
                                color = 'cyan';
                            } else if (firstLetter >= 'S' && firstLetter <= 'T') {
                                color = 'blue';
                            } else if (firstLetter >= 'U' && firstLetter <= 'V') {
                                color = 'violet';
                            } else if (firstLetter >= 'W' && firstLetter <= 'X') {
                                color = 'magenta';
                            } else if (firstLetter >= 'Y' && firstLetter <= 'Z') {
                                color = 'rose';
                            }
                            return '' +
                                '        <span class="circle-icon margin-right-5 fs-11 bg-' + color + ' border-transparent" style="width: 30px; height: 30px; line-height: 28px;">' + row.nama.match(/\b(\w)/g).slice(0, 2).join('').toUpperCase() + '</span>' +
                                '        <span class="fs-14 text-dark">' + row.nama + '</span>' +
                                
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nip',
                            display: 'nip',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-orange">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.pin',
                            display: 'pin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="text-orange">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'k.pangkat',
                            display: 'kepangkatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jj.jenjang_jabatan',
                            display: 'jenjang_jabatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_jabatan',
                            display: 'nama_jabatan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'uk.unit_kerja',
                            display: 'unit_kerja',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'e.eselon',
                            display: 'eselon',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'g.grade',
                            display: 'grade',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'kp.kedudukan_pegawai',
                            display: 'kedudukan_pegawai',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.mendapat_tunkin',
                            display: 'mendapat_tunkin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nama_panggilan',
                            display: 'nama_panggilan',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jenis_kelamin',
                            display: 'jenis_kelamin',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.ttl',
                            display: 'ttl',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.agama',
                            display: 'agama',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.status_marital',
                            display: 'status_marital',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.golongan_darah',
                            display: 'golongan_darah',
                        },
                        render: function ( data, type, row ) {
                            return '' +
                                '<div>' +
                                '    <span class="">' + data + '</span>' +
                                '</div>' +
                                '';
                        },
                        createdCell: function (td, cellData, rowData, row, col) {
                            pluginInit(td);
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});