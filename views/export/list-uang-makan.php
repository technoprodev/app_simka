<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/export/list-uang-makan.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);

$this->registerJs(
    'var tahun = ' . json_encode($model['filter_kehadiran']->tahun) . ';' .
    'var bulan = ' . json_encode($model['filter_kehadiran']->bulan) . ';' .
    '',
    3
);

$bulans = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', ];
$tahuns = ['2019' => '2019', '2020' => '2020', '2021' => '2021', '2022' => '2022', '2023' => '2023', '2024' => '2024', ];

$query = new \yii\db\Query();
$query
    ->select([
        'uk.unit_kerja',
    ])
    ->from('unit_kerja uk')
;
$pilihanUnitKerja = $query->all();
?>

<div class="box box-break-sm box-space-md box-gutter box-equal">
    <div class="box-12 bg-lightest shadow-bottom-right rounded-xs padding-x-30 m-padding-x-15 padding-y-15 border-azure border-thin rounded-md">
        <div class="clearfix">
            <span class="fs-18 text-azure fs-italic">Daftar Uang Makan</span>
        </div>
        <hr class="margin-y-10 border-top border-light-orange">

        <?php $form = ActiveForm::begin([/*'enableClientValidation' => true, */'options' => ['id' => 'app'], 'method' => 'get', 'action' => Url::to(['export/uang-makan'])]); ?>
            <div class="box box-break-md box-gutter">
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'bulan', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'bulan', $bulans, ['class' => 'form-dropdown', 'name' => 'bulan']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'bulan')->end(); ?>
                </div>
                <div class="box-2">
                    <?= $form->field($model['filter_kehadiran'], 'tahun', ['options' => ['class' => 'form-wrapper margin-0'], 'selectors' => ['error' => '.form-info']])->begin(); ?>
                        <?= Html::activeDropDownList($model['filter_kehadiran'], 'tahun', $tahuns, ['class' => 'form-dropdown', 'name' => 'tahun']); ?>
                    <?= $form->field($model['filter_kehadiran'], 'tahun')->end(); ?>
                </div>
                <div class="box-2">
                    <div class="form-wrapper margin-0">
                        <button class="button button-block border-azure bg-azure m-margin-top-5" type="submit">Tampilkan</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>

        <hr class="margin-y-10 border-top border-light-orange">

        <div class="scroll-x">
            <table class="datatables-kehadiran-bulanan table table-nowrap">
                <thead>
                    <tr class="text-dark">
                        <th>Nama<br>Pegawai</th>
                        <th>NIP<br>Pegawai</th>
                        <th>Unit<br>Kerja</th>
                        <th>Total<br>Kehadiran</th>
                        <th>Total<br>Ketidakhadiran</th>
                    </tr>
                    <tr class="dt-search">
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nama..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search nip..."/></th>
                        <th><input type="text" class="form-text border-none padding-0" placeholder="search unit kerja..." list="unitKerja"/>
                            <datalist id="unitKerja">
                                <?php foreach ($pilihanUnitKerja as $key => $value) : ?>
                                    <option value="<?= $value['unit_kerja'] ?>">
                                <?php endforeach; ?>
                            </datalist>
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>