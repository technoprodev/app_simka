$(document).ready(function() {
    // datatabless
    if (typeof $.fn.dataTable === 'function') {
        datatablesCustomClass();
        var el = $('.datatables-pegawai-pendidikan');
        $.each(el, function() {
            var table = $(this).DataTable(Object.assign({}, datatablesDefaultOptions, {
                ajax: {
                    url: fn.urlTo('pegawai/datatables-pegawai-pendidikan', {idPegawai : idPegawai}),
                    type: 'POST'
                },
                columns: [
                    {
                        data: {
                            _: 'p.nama_sekolah',
                            display: 'nama_sekolah',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'jp.jenjang_pendidikan',
                            display: 'jenjang_pendidikan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.jurusan',
                            display: 'jurusan',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.nilai',
                            display: 'nilai',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.skala_nilai',
                            display: 'skala_nilai',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.alamat_sekolah',
                            display: 'alamat_sekolah',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tahun_masuk',
                            display: 'tahun_masuk',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.tahun_lulus',
                            display: 'tahun_lulus',
                        },
                        defaultContent: '&nbsp;',
                    },
                    {
                        data: {
                            _: 'p.ijazah',
                            display: 'ijazah',
                        },
                        render: function ( data, type, row ) {
                            if (data) {
                                return '' +
                                    '<a href="' +
                                    baseUrl +
                                    data +
                                    '">Download</a>' +
                                    '';
                            } else {
                                return 'tidak tersedia';
                            }
                        },
                        defaultContent: '&nbsp;',
                    },
                ],
            }));

            var dtSearch = $('.dt-search', $(this));
            table.columns().every(function(index, table, column){
                var that = this;
                
                var timer = null;
                dtSearch.find('th:nth-child(' + (index + 1) + ') input').on('keyup change', function(){
                    var val = {
                        that : that,
                        this : this,
                    };

                    clearTimeout(timer); 
                       timer = setTimeout(function () {
                        if(val.that.search() !== val.this.value){
                            val.that.search( val.this.value ).draw();
                        }
                    }, 500);
                });
            });
        });
        // $('th', el).unbind('click.DT');
        // $('th', el).remove();
    }
});