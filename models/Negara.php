<?php
namespace app_simka\models;

use Yii;

/**
 * This is the model class for table "negara".
 *
 * @property string $id
 * @property string $nama
 *
 * @property PegawaiKunjunganLuarNegeri[] $pegawaiKunjunganLuarNegeris
 */
class Negara extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'negara';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 2],
            [['id'], 'unique'],

            //nama
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 256],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiKunjunganLuarNegeris()
    {
        return $this->hasMany(PegawaiKunjunganLuarNegeri::className(), ['negara_kunjungan' => 'id']);
    }
}
